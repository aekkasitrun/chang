import Head from 'next/head'
import Image from 'next/image'
import { Inter } from '@next/font/google'
import styles from '../styles/Home.module.css'

import * as React from 'react';
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';

import Grid from '@mui/material/Grid';
import { height } from '@mui/system';

// import makeStyles from '@mui/styles/makeStyles';


export default function Home() {
    return (
        <div className={styles.main}>



            <Grid container spacing={{ xs: 2, md: 3 }} columns={{ xs: 4, sm: 8, md: 16 }} sx={{ p: 1 }}>
                <Grid item xs={12} sm={6} md={4}>
                    <Card sx={{ height: 150, p: 2 }} className='bg-[#00c5dc]' >
                        <CardContent>
                            <div className='flex justify-end'>
                                <Typography sx={{ fontSize: 20, p: 2, flexGrow: 1 }} color='primary.contrastText' gutterBottom>
                                    Connected
                                </Typography>
                                <Typography sx={{ fontSize: 20, p: 2, }} color='primary.contrastText' gutterBottom>
                                    0
                                </Typography>
                            </div>
                        </CardContent>
                    </Card>
                </Grid>

                <Grid item xs={12} sm={6} md={4}>
                    <Card sx={{ height: 150, p: 2 }} className='bg-[#5d78ff]'>
                        <CardContent>
                            <div className='flex justify-end'>
                                <Typography sx={{ fontSize: 20, p: 2, flexGrow: 1 }} color='primary.contrastText' gutterBottom>
                                    Online
                                </Typography>
                                <Typography sx={{ fontSize: 20, p: 2, }} color='primary.contrastText' gutterBottom>
                                    0
                                </Typography>
                            </div>
                        </CardContent>
                    </Card>
                </Grid>

                <Grid item xs={12} sm={6} md={4}>
                    <Card sx={{ height: 150, p: 2 }} className='bg-[#1dc9b7]'>
                        <CardContent>
                            <div className='flex justify-end'>
                                <Typography sx={{ fontSize: 20, p: 2, flexGrow: 1 }} color='primary.contrastText' gutterBottom>
                                    Charging
                                </Typography>
                                <Typography sx={{ fontSize: 20, p: 2, }} color='primary.contrastText' gutterBottom>
                                    0
                                </Typography>
                            </div>
                        </CardContent>
                    </Card>
                </Grid>

                <Grid item xs={12} sm={6} md={4}>
                    <Card sx={{ height: 150, p: 2 }} className='bg-[#9816f4]'>
                        <CardContent>
                            <div className='flex justify-end'>
                                <Typography sx={{ fontSize: 20, p: 2, flexGrow: 1 }} color='primary.contrastText' gutterBottom>
                                    Faulted
                                </Typography>
                                <Typography sx={{ fontSize: 20, p: 2, }} color='primary.contrastText' gutterBottom>
                                    0
                                </Typography>
                            </div>
                        </CardContent>
                    </Card>
                </Grid>
            </Grid>


        </div>
    )
}
