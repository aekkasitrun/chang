import { GetStaticProps } from 'next';
import styles from '../styles/Home.module.css'

export const getStaticProps: GetStaticProps = async () => {

    const res_user = await fetch('http://localhost:8080/v1/sample-api');
    const data_user = await res_user.json();

    const res_rate = await fetch('http://localhost:8080/v1/rate-api');
    const data_rate = await res_rate.json();

    return {
        props: {
            rates: data_rate,
            users: data_user
        }
    }
}

interface TransactionProps {
    rates: any
    users: any
}

const Transaction = ({ rates, users }: TransactionProps) => {

    console.log(rates)
    console.log(users)

    return (
        <div className={styles.main}>

            <h1>All Rates</h1>
            {rates.map((rate: any) => (
                <div key={rate.id}>
                    <a>
                        <h3>{rate.name}</h3>
                        <h3>{rate.description}</h3>
                    </a>
                </div>
            ))}

            <h1>All User</h1>
            {users.map((user: any) => (
                <div key={user.id}>
                    <a>
                        <h3>{user.userName}</h3>
                    </a>
                </div>
            ))}

        </div>

    );
}

export default Transaction;
