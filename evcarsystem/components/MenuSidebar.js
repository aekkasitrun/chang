import DashboardCustomizeRoundedIcon from '@mui/icons-material/DashboardCustomizeRounded';
import LocationOnRoundedIcon from '@mui/icons-material/LocationOnRounded';
import EvStationRoundedIcon from '@mui/icons-material/EvStationRounded';
import DescriptionIcon from '@mui/icons-material/Description';
import PersonIcon from '@mui/icons-material/Person';
import NearMeIcon from '@mui/icons-material/NearMe';
import RequestQuoteIcon from '@mui/icons-material/RequestQuote';
import DocumentScannerOutlinedIcon from '@mui/icons-material/DocumentScannerOutlined';
import PermPhoneMsgIcon from '@mui/icons-material/PermPhoneMsg';
import CloudUploadOutlinedIcon from '@mui/icons-material/CloudUploadOutlined';
import SmartphoneOutlinedIcon from '@mui/icons-material/SmartphoneOutlined';
import PeopleIcon from '@mui/icons-material/People';
import TerminalIcon from '@mui/icons-material/Terminal';
import AutoStoriesOutlinedIcon from '@mui/icons-material/AutoStoriesOutlined';
import PsychologyOutlinedIcon from '@mui/icons-material/PsychologyOutlined';
import PaymentsIcon from '@mui/icons-material/Payments';
import InsertChartIcon from '@mui/icons-material/InsertChart';

export const Menu1 = [
    {
        id: 1,
        label: 'Dashboard',
        path: '/dashboard',
        icon: DashboardCustomizeRoundedIcon
    },
    {
        id: 2,
        label: 'Location',
        path: '/location',
        icon: LocationOnRoundedIcon
    },
    {
        id: 3,
        label: 'Charge Stations',
        path: '/charge-stations',
        icon: EvStationRoundedIcon
    },
    {
        id: 4,
        label: 'Transaction',
        path: '/transaction',
        icon: DescriptionIcon
    },
    {
        id: 5,
        label: 'Users',
        path: '/users',
        icon: PeopleIcon
    },
    {
        id: 6,
        label: 'RFIDs',
        path: '/rfids',
        icon: NearMeIcon
    },
    {
        id: 7,
        label: 'Rates',
        path: '/rates',
        icon: RequestQuoteIcon
    },
]

export const Menu2 = [
    {
        id: 1,
        label: 'logs',
        path: '/logs',
        icon: DocumentScannerOutlinedIcon
    },
    {
        id: 2,
        label: 'Support',
        path: '/support',
        icon: PermPhoneMsgIcon
    },
]

export const MenuSetting = [
    {
        id: 1,
        label: 'API',
        path: '/setting-pages/apipages',
        icon: CloudUploadOutlinedIcon
    },
    {
        id: 2,
        label: 'Integrations',
        path: '/setting-pages/integrations',
        icon: PsychologyOutlinedIcon
    },
    {
        id: 3,
        label: 'Driver App',
        path: '/setting-pages/driverapp',
        icon: SmartphoneOutlinedIcon
    },
    {
        id: 4,
        label: 'Account',
        path: '/setting-pages/account',
        icon: PersonIcon
    },
    {
        id: 5,
        label: 'API Playground',
        path: '/setting-pages/api-playground',
        icon: TerminalIcon
    },
]

export const MenuBilling = [
    {
        id: 1,
        label: 'Plans',
        path: '/billing-pages/plans',
        icon: AutoStoriesOutlinedIcon
    },
    {
        id: 2,
        label: 'Payment',
        path: '/billing-pages/payment',
        icon: PaymentsIcon
    },
    {
        id: 3,
        label: 'Usage',
        path: '/billing-pages/usage',
        icon: InsertChartIcon
    },

]


