import Sidebar from './Sidebar'
import SidebarTest from './SidebarTest'

export default function Layout({ children }) {
  return (
    <>
      <SidebarTest />
      <main>{children}</main>
      {/* </SidebarTest> */}

    </>
  )
}