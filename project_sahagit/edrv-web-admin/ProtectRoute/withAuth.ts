import { GetServerSideProps } from "next";
import { getSession } from "next-auth/react";
//this code not use to adapt on any page
const getServerSideProps: GetServerSideProps = async ({ req }) => {
  const session = await getSession({ req });

  if (!session) {
    return {
      redirect: {
        destination: "/signin",
        permanent: false,
      },
    };
  }

  return {
    props: { session },
  };
};

export default getServerSideProps;
