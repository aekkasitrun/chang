export default function formatTime(rawTime: string) {
  const dateObj = new Date(rawTime);
  const hours = String(dateObj.getUTCHours()).padStart(2, "0");
  const minutes = String(dateObj.getUTCMinutes()).padStart(2, "0");
  const seconds = String(dateObj.getUTCSeconds()).padStart(2, "0");
  const finalTime = `${hours}:${minutes}:${seconds}`;
  return finalTime;
}
