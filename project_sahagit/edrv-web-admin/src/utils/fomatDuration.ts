export default function formatDuration(duration: number) {
  const finalDuration = new Date(duration * 1000)
    .toISOString()
    .substring(11, 19);
  return finalDuration;
}
