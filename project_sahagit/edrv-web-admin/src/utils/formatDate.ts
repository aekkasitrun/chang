export default function formatDate(rawDate: string) {
  const dateObj = new Date(rawDate);
  const day = String(dateObj.getUTCDate()).padStart(2, "0");
  const month = String(dateObj.getUTCMonth() + 1).padStart(2, "0");
  const year = String(dateObj.getUTCFullYear() + 543);
  const finalDate = `${day}/${month}/${year}`;
  return finalDate;
}
