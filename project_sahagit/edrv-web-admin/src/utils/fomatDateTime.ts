export default function formatDateAndTime(rawDateAndTime: string) {
  const dateObj = new Date(rawDateAndTime);
  const hours = String(dateObj.getUTCHours()).padStart(2, "0");
  const minutes = String(dateObj.getUTCMinutes()).padStart(2, "0");
  const seconds = String(dateObj.getUTCSeconds()).padStart(2, "0");
  const day = String(dateObj.getUTCDate()).padStart(2, "0");
  const month = String(dateObj.getUTCMonth() + 1).padStart(2, "0");
  const year = String(dateObj.getUTCFullYear() + 543);
  const finalDateTime = `${hours}:${minutes}:${seconds} ${day}/${month}/${year}`;
  return finalDateTime;
}
