export default function formatCost(cost: number) {
  const finalCost = (cost * 30).toFixed(2);
  return finalCost;
}
