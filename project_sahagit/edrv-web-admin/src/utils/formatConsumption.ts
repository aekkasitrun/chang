export default function formatConsumption(consumption: number) {
  const finalConsumption = consumption / 1000;
  return finalConsumption;
}
