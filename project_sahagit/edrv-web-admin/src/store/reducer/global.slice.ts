import { createSlice } from "@reduxjs/toolkit";
import { RootState } from "store/store";

type IInitialState = {
  modal: boolean;
  error: object;
};

const NAME = "global";

const initialState: IInitialState = {
  modal: false,
  error: {},
};

const globalSlice = createSlice({
  name: NAME,
  initialState,
  reducers: {
    setApiError: (state, action) => {
      state.error = action.payload;
    },

    resetApiError: (state) => {
      state.error = {};
    },
    setModalOpen: (state) => {
      state.modal = true;
    },
    resetModal: (state) => {
      state.modal = false;
    },
  },
});

export const { resetModal, setModalOpen, setApiError, resetApiError } =
  globalSlice.actions;

export const baseSelector = (state: RootState) => state.globalSlice;
export const modalSelector = (state: RootState) => state.globalSlice.modal;

export default globalSlice.reducer;
