// import { authApi } from 'services/auth.service';
import { createSlice } from "@reduxjs/toolkit";
import { RootState } from "@/store/store";

type User = any;
type Token = string;

type InitialState = {
  user: User;
  token: Token;
};
const initialState: InitialState = {
  user: "",
  token: "",
};

const name = "auth";

const slice = createSlice({
  name,
  initialState,
  reducers: {
    setCredentials: (state, { payload: { user, token } }) => {
      state.user = user;
      state.token = token;
    },
    setProfileImage: (state, action) => {
      state.user = { ...state.user, profileImageUrl: action.payload };
    },
  },
  // extraReducers: (builder) => {
  //   builder
  //     // .addMatcher(authApi.endpoints.login.matchPending, (state, action) => {
  //     //   console.log('pending', action);
  //     // })
  //     .addMatcher(
  //       authApi.endpoints.login.matchFulfilled,
  //       (state, { payload }) => {
  //         // console.log('fulfilled', payload);
  //         // state.user = payload.user;
  //         // state.token = payload.accessToken;
  //       }
  //     );
  // },
});

export const { setCredentials, setProfileImage } = slice.actions;

export default slice.reducer;

export const currentUser = (state: RootState) => state.auth.user;
