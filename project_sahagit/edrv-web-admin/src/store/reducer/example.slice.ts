import { createSlice } from "@reduxjs/toolkit";
import { RootState } from "store/store";

export interface Todo {
  id?: string;
  title: string;
  content: string;
  done: boolean;
}

type InitialState = {
  todoList: Todo[];
};

const NAME = "example";

const initialState: InitialState = {
  todoList: [],
};

const example = createSlice({
  name: NAME,
  initialState,
  reducers: {
    addTodo: (state, action: { type: string; payload: Todo }) => {
      state.todoList.push(action.payload);
    },
    editTodo: (state, action: { type: string; payload: Todo }) => {
      state.todoList.map((todo) => {
        if (todo.id === action.payload.id) {
          return action.payload;
        }
        return todo;
      });
    },
    deleteTodo: (state, action: { type: string; payload: string }) => {
      state.todoList = state.todoList.filter(
        (todo: Todo) => todo.id !== action.payload
      );
    },
  },
});

export const { addTodo, deleteTodo, editTodo } = example.actions;

export const todoSelector = (state: RootState) => state.exampleSlice.todoList;

export default example.reducer;
