import { combineReducers } from "redux";
import globalSlice from "./global.slice";
import auth from "./auth.slice";
import exampleSlice from "./example.slice";
import { pokemonApi } from "@/services/pokemon";
import { chargeStationApi } from "@/services/chargestation.service";
import { locationApi } from "@/services/location.service";
import { connectorApi } from "@/services/connector.service";
import { rateApi } from "@/services/rate.service";
import { transactionApi } from "@/services/transaction.service";
import { usageApi } from "@/services/usage.service";
import { paymentApi } from "@/services/payment.service";
import { signupApi } from "@/services/signup.service";
// import {} from "services";

export const rootReducer = combineReducers({
  globalSlice,
  auth,
  exampleSlice,
  [pokemonApi.reducerPath]: pokemonApi.reducer,
  [chargeStationApi.reducerPath]: chargeStationApi.reducer,
  [locationApi.reducerPath]: locationApi.reducer,
  [connectorApi.reducerPath]: connectorApi.reducer,
  [rateApi.reducerPath]: rateApi.reducer,
  [transactionApi.reducerPath]: transactionApi.reducer,
  [usageApi.reducerPath]: usageApi.reducer,
  [paymentApi.reducerPath]: paymentApi.reducer,
  [signupApi.reducerPath]: signupApi.reducer,
  // [locationDeleteApi.reducerPath]: locationDeleteApi.reducer,
  // [deviceApi.reducerPath]: deviceApi.reducer,
  // [patientApi.reducerPath]: patientApi.reducer,
  // [careerApi.reducerPath]: careerApi.reducer,
  // [historyTakingApi.reducerPath]: historyTakingApi.reducer,
  // [patientDiaryApi.reducerPath]: patientDiaryApi.reducer,
  // [bodyExaminationApi.reducerPath]: bodyExaminationApi.reducer,
});
