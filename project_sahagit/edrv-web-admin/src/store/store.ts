import { configureStore } from "@reduxjs/toolkit";
import { rootReducer } from "./reducer";
import { pokemonApi } from "@/services/pokemon";
import { chargeStationApi } from "@/services/chargestation.service";
import { locationApi } from "@/services/location.service";
import { connectorApi } from "@/services/connector.service";
import { rateApi } from "@/services/rate.service";
import { transactionApi } from "@/services/transaction.service";
import { usageApi } from "@/services/usage.service";
import { paymentApi } from "@/services/payment.service";
import { signupApi } from "@/services/signup.service";
// import {} from "services";

export const store = configureStore({
  reducer: rootReducer,
  devTools: process.env.NODE_ENV === "development",
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().concat(
      pokemonApi.middleware,
      chargeStationApi.middleware,
      locationApi.middleware,
      connectorApi.middleware,
      rateApi.middleware,
      transactionApi.middleware,
      usageApi.middleware,
      paymentApi.middleware,
      signupApi.middleware
      // locationDeleteApi.middleware
    ),
});

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>;
// Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}
export type AppDispatch = typeof store.dispatch;
