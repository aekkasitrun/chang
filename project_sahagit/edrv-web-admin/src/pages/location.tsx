import React from "react";
import dynamic from "next/dynamic";
import CenterScreen from "@/components/Layouts/CenterScreen";
import BouncingIcon from "@/components/Bouncing";
import { NextPageWithLayout } from "./page";
import { DefaultLayout } from "@/views/Layouts";

const LocationView = dynamic(() => import("@/views/Location"), {
    loading: () => (
        <CenterScreen>
            <BouncingIcon />
        </CenterScreen>
    ),
});

const LocationPage: NextPageWithLayout = () => {
    return <LocationView />;
};

LocationPage.getLayout = DefaultLayout;
// เขียนแบบนี้เพื่อให้เราสามารถเลือกได้ว่าจะใส่ layout หน้าไหนบ้าง
// ถ้าจะไม่เอา layout ก็เอาบรรทัดนี้ออก

export default LocationPage;

//Protected Route

import { GetServerSideProps } from 'next'
import { getSession } from "next-auth/react"


export const getServerSideProps: GetServerSideProps = async ({ req }) => {
    const session = await getSession({ req })

    if (!session) {
        return {
            redirect: {
                destination: '/signin',
                permanent: false
            }
        }
    }

    return {
        props: { session }
    }
}