import React from "react";
import dynamic from "next/dynamic";
import CenterScreen from "@/components/Layouts/CenterScreen";
import BouncingIcon from "@/components/Bouncing";
import { NextPageWithLayout } from "../page";
import { DefaultLayout } from "@/views/Layouts";

const PokemonView = dynamic(() => import("@/views/Pokemon"), {
  loading: () => (
    <CenterScreen>
      <BouncingIcon />
    </CenterScreen>
  ),
});

const PokemonPage: NextPageWithLayout = () => {
  return <PokemonView />;
};

PokemonPage.getLayout = DefaultLayout;
// เขียนแบบนี้เพื่อให้เราสามารถเลือกได้ว่าจะใส่ layout หน้าไหนบ้าง
// ถ้าจะไม่เอา layout ก็เอาบรรทัดนี้ออก

export default PokemonPage;
