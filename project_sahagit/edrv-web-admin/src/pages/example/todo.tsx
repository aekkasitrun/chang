import React from "react";
import dynamic from "next/dynamic";
import CenterScreen from "@/components/Layouts/CenterScreen";
import BouncingIcon from "@/components/Bouncing";
import { NextPageWithLayout } from "../page";
import { DefaultLayout } from "@/views/Layouts";

const TodoView = dynamic(() => import("@/views/Todo/Todo"), {
  // dynamic/loading คือ lazyloading version nextjs มีไว้เพื่อใช้กับการโหลดข้อมูลทีละส่วน 
  // อย่าเว็บการ์ตูนที่เวลาอ่านแล้วภาพมันโหลดทีละภาพอันนี้คือ lazyloading ทำเพื่อให้คนเน็ตกากสามารถอ่านเว็บการ์ตูนต่อได้
  // เพราะถ้าไม่ใส่ lazyloading ตัวภาพมันจะโหลดทีเดียวทุกภาพทำให้ตอนเราเปิดครั้งแรกถ้าเน็ตไม่ดีหน้าเว็บก็จะขาวยาวๆ
  loading: () => (
    <CenterScreen>
      <BouncingIcon />
    </CenterScreen>
  ),
});

const TodoPage: NextPageWithLayout = () => {
  return <TodoView />;
};

TodoPage.getLayout = DefaultLayout;

export default TodoPage;
