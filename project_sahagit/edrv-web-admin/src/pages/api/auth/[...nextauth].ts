// import NextAuth from "next-auth";
// import { Provider } from "next-auth/providers";
// import CredentialsProvider from "next-auth/providers/credentials";

// interface AuthOptions {
//   providers: Provider[];
// }

// const authOptions: AuthOptions = {
//   providers: [
//     CredentialsProvider({
//       name: "Credentials",
//       credentials: {
//         username: { label: "Username", type: "text" },
//         password: { label: "Password", type: "password" },
//       },
//       async authorize(credentials, req) {
//         const res = await fetch("https://www.melivecode.com/api/login", {
//           method: "POST",
//           body: JSON.stringify(credentials),
//           headers: { "Content-Type": "application/json" },
//         });
//         const data = await res.json();

//         if (data == "ok") {
//           return data.user;
//         }
//         return null;
//       },
//     }),
//   ],
//   // secret: "8PayqeOVnJwQdum3Xb0/d8V49+tOlnhOu/3EY50vVAc=",
//   // callbacks: {
//   //   async jwt({ token, user, account }) {
//   //     // Persist the OAuth access_token to the token right after signin
//   //     console.log(user);

//   //     if (account) {
//   //       token.accessToken = account.access_token;
//   //     }
//   //     return token;
//   //   },
//   //   async session({ session, token, user }) {
//   //     // Send properties to the client, like an access_token from a provider.
//   //     session.accessToken = token.accessToken;
//   //     return session;
//   //   },
//   // },
// };

// export default NextAuth(authOptions);

// import NextAuth from "next-auth";
// import GoogleProvider from "next-auth/providers/google";

// const nextAuthOptions = {
//   providers: [
//     // OAuth authentication providers...
//     GoogleProvider({
//       clientId: process.env.NEXT_PUBLIC_GOOGLE_ID!,
//       clientSecret: process.env.NEXT_PUBLIC_GOOGLE_SECRET!,
//     }),
//   ],
// };

// export default NextAuth(nextAuthOptions);

import NextAuth from "next-auth";
import GoogleProvider from "next-auth/providers/google";
import CredentialsProvider from "next-auth/providers/credentials";

export default NextAuth({
  providers: [
    // OAuth authentication providers...
    GoogleProvider({
      clientId: process.env.NEXT_PUBLIC_GOOGLE_ID!,
      clientSecret: process.env.NEXT_PUBLIC_GOOGLE_SECRET!,
    }), // <--- added comma here

    CredentialsProvider({
      name: "Credentials",
      credentials: {
        providerId: { label: "UserId", type: "text" },
        password: { label: "Password", type: "password" },
      },
      async authorize(credentials, req) {
        const res = await fetch("http://localhost:8080/v1/login/login", {
          method: "POST",
          body: JSON.stringify(credentials),
          headers: { "Content-Type": "application/json" },
        });
        const data = await res.json();

        if (!res.ok) {
          throw new Error(data.message);
        }
        // If no error and we have user data, return it
        if (res.ok && data) {
          return data;
        }

        // Return null if user data could not be retrieved
        return null;
      },
    }),
  ],
  // secret: "8PayqeOVnJwQdum3Xb0/d8V49+tOlnhOu/3EY50vVAc=",
  // pages: {
  //   signIn: "/signin",
  // },
  // callbacks: {
  //   async jwt({ token, user, account }) {
  //     if (account && user) {
  //       return {
  //         ...token,
  //         accessToken: user.token,
  //         refreshToken: user.refreshToken,
  //       };
  //     }

  //     return token;
  //   },

  //   async session({ session, token }) {
  //     session.user.accessToken = token.accessToken;
  //     session.user.refreshToken = token.refreshToken;
  //     session.user.accessTokenExpires = token.accessTokenExpires;

  //     return session;
  //   },

  // },
});

// export default NextAuth(nextAuthOptions);
