import React from "react";
import dynamic from "next/dynamic";
import CenterScreen from "@/components/Layouts/CenterScreen";
import BouncingIcon from "@/components/Bouncing";
import { NextPageWithLayout } from "./page";
import { DefaultLayout } from "@/views/Layouts";
import { GetStaticProps } from 'next';

export const getStaticProps: GetStaticProps = async () => {

    const res_user = await fetch('http://localhost:8080/v1/users/');
    const data_user = await res_user.json();



    return {
        props: {
            users: data_user.result
        }
    }
}

const UsersView = dynamic(() => import("@/views/Users"), {
    loading: () => (
        <CenterScreen>
            <BouncingIcon />
        </CenterScreen>
    ),
});

const UsersPage: NextPageWithLayout = ({ users }) => {
    return <UsersView users={users} />;
};

UsersPage.getLayout = DefaultLayout;
// เขียนแบบนี้เพื่อให้เราสามารถเลือกได้ว่าจะใส่ layout หน้าไหนบ้าง
// ถ้าจะไม่เอา layout ก็เอาบรรทัดนี้ออก

export default UsersPage;

//Protected Route

import { GetServerSideProps } from 'next'
import { getSession } from "next-auth/react"


export const getServerSideProps: GetServerSideProps = async ({ req }) => {
    const session = await getSession({ req })

    if (!session) {
        return {
            redirect: {
                destination: '/signin',
                permanent: false
            }
        }
    }

    return {
        props: { session }
    }
}