import React from "react";
import dynamic from "next/dynamic";
import CenterScreen from "@/components/Layouts/CenterScreen";
import BouncingIcon from "@/components/Bouncing";
import { NextPageWithLayout } from "./page";
import { DefaultLayout } from "@/views/Layouts";

const SignUpView = dynamic(() => import("@/views/SignUp"), {
    loading: () => (
        <CenterScreen>
            <BouncingIcon />
        </CenterScreen>
    ),
});

const SignUpPage: NextPageWithLayout = () => {
    return <SignUpView />;
};

// SignUpPage.getLayout = DefaultLayout;

export default SignUpPage;
