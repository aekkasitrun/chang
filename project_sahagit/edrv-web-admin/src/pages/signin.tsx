import React from "react";
import dynamic from "next/dynamic";
import CenterScreen from "@/components/Layouts/CenterScreen";
import BouncingIcon from "@/components/Bouncing";
import { NextPageWithLayout } from "./page";
import { DefaultLayout } from "@/views/Layouts";

const SignInView = dynamic(() => import("@/views/SignIn"), {
    loading: () => (
        <CenterScreen>
            <BouncingIcon />
        </CenterScreen>
    ),
});

const SignInPage: NextPageWithLayout = () => {
    return <SignInView />;
};

// SignInPage.getLayout = DefaultLayout;

export default SignInPage;
