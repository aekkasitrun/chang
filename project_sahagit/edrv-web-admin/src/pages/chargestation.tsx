import React from "react";
import dynamic from "next/dynamic";
import CenterScreen from "@/components/Layouts/CenterScreen";
import BouncingIcon from "@/components/Bouncing";
import { NextPageWithLayout } from "./page";
import { DefaultLayout } from "@/views/Layouts";

// export const getServerSideProps: GetServerSideProps = async () => {

//     const res_chargeStation = await fetch('http://localhost:8080/v1/charge-stations/');
//     const data_chargeStation = await res_chargeStation.json();

//     console.log(data_chargeStation.result);


//     return {
//         props: {
//             chargeStations: data_chargeStation.result,


//         }
//     }
// }

const ChargeStationView = dynamic(() => import("@/views/ChargeStation"), {
    loading: () => (
        <CenterScreen>
            <BouncingIcon />
        </CenterScreen>
    ),
});

const ChargeStationPage: NextPageWithLayout = ({ chargeStations }) => {
    return <ChargeStationView />;
};

ChargeStationPage.getLayout = DefaultLayout;

export default ChargeStationPage;

//Protected Route

import { GetServerSideProps } from 'next'
import { getSession } from "next-auth/react"


export const getServerSideProps: GetServerSideProps = async ({ req }) => {
    const session = await getSession({ req })

    if (!session) {
        return {
            redirect: {
                destination: '/signin',
                permanent: false
            }
        }
    }

    return {
        props: { session }
    }
}