import ErrorBoundary from "@/components/ErrorBoundary";
import type { AppProps } from "next/app";
import { Provider } from "react-redux";
import { store } from "@/store/store";
import { StyledEngineProvider } from "@mui/material";
import { CssBaseline } from "@mui/material";
import { NextPageWithLayout } from "./page";
import { Suspense } from "react";
import CenterScreen from "@/components/Layouts/CenterScreen";
import BouncingIcon from "@/components/Bouncing";
import "@/styles/globals.css";
import { SessionProvider } from "next-auth/react"

interface AppPropsWithLayout extends AppProps {
  Component: NextPageWithLayout;
}

export default function App({ Component,
  pageProps: { session, ...pageProps } }: AppPropsWithLayout) {
  const getLayout = Component.getLayout || ((page) => page);

  return (
    <SessionProvider session={session}>
      <ErrorBoundary>
        {/* ตัวจับ error */}
        <Provider store={store}>
          {/* เกี่ยวกับ Redux toolkit */}
          <Suspense
            // มีเพื่อเอาไว้กันปัญหาเว็บอืดถ้าเกิดเว็บเราใหญ่ในอนาคต
            fallback={
              <CenterScreen>
                <BouncingIcon />
              </CenterScreen>
            }
          >
            <StyledEngineProvider>
              <CssBaseline />

              {getLayout(<Component {...pageProps} />)}
            </StyledEngineProvider>
          </Suspense>
        </Provider>
      </ErrorBoundary>
    </SessionProvider>
  );
}
