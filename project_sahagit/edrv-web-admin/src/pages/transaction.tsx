import React from "react";
import dynamic from "next/dynamic";
import CenterScreen from "@/components/Layouts/CenterScreen";
import BouncingIcon from "@/components/Bouncing";
import { NextPageWithLayout } from "./page";
import { DefaultLayout } from "@/views/Layouts";

const TransactionView = dynamic(() => import("@/views/Transaction"), {
    loading: () => (
        <CenterScreen>
            <BouncingIcon />
        </CenterScreen>
    ),
});

const TransactionPage: NextPageWithLayout = () => {
    return <TransactionView />;
};

TransactionPage.getLayout = DefaultLayout;

export default TransactionPage;

//Protected Route
import { GetServerSideProps } from 'next'
import { getSession } from "next-auth/react"


export const getServerSideProps: GetServerSideProps = async ({ req }) => {
    const session = await getSession({ req })

    if (!session) {
        return {
            redirect: {
                destination: '/signin',
                permanent: false
            }
        }
    }

    return {
        props: { session }
    }
}