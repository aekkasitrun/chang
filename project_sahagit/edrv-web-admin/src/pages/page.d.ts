import { NextPage } from "next";
import React, { ComponentType, ReactElement, ReactNode } from "react";

export type NextAuthProps =
  | boolean
  | {
      role?: string;
      loading: React.ReactNode;
      unauthorized: string;
    };

export type NextPageWithLayout<P = Record<string, unknown>> = NextPage<P> & {
  getLayout?: (_page: ReactElement) => ReactNode;
  auth?: NextAuthProps;
  layout?: ComponentType;
};
