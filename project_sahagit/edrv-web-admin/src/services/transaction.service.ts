import axiosBaseQuery from "@/api/api";
import { createApi } from "@reduxjs/toolkit/query/react";
import { object } from "yup";

const NAME = "transactionApi";

export const transactionApi = createApi({
  reducerPath: NAME,
  baseQuery: axiosBaseQuery(),
  endpoints: (builder) => ({
    getAllTransactionService: builder.query<any, void>({
      query: () => {
        return {
          method: "GET",
          url: "transaction/",
        };
      },
    }),
  }),
});

export const { useGetAllTransactionServiceQuery } = transactionApi;
