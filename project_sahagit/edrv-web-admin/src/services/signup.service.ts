import axiosBaseQuery from "@/api/api";
import { createApi } from "@reduxjs/toolkit/query/react";

const NAME = "signupApi";

export const signupApi = createApi({
  reducerPath: NAME, //ชื่อของ reducer
  baseQuery: axiosBaseQuery(),
  endpoints: (builder) => ({
    addSignupService: builder.mutation<any, any>({
      query: (body) => {
        return {
          method: "POST",
          url: "login/register",
          data: {
            firstName: body.firstName,
            lastName: body.lastName,
            id: body.providerId,
            email: body.email,
            phoneNumber: body.phoneNumber,
            password: body.password,
          },
        };
      },
    }),
  }),
});

export const { useAddSignupServiceMutation } = signupApi;
