import axiosBaseQuery from "@/api/api";
import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

const NAME = "connectorApi";

export const connectorApi = createApi({
  reducerPath: NAME,
  baseQuery: axiosBaseQuery(),
  endpoints: (builder) => ({
    addConnectorService: builder.mutation({
      query: (body) => ({
        method: "POST",
        url: `connectors/${body.id}`,
        data: {
          name: body.name,
          description: body.description,
          active: body.active,
          rate: body.rate,
          format: body.format,
          type: body.type,
          power_type: body.power_type,
          power: parseFloat(body.power),
        },
        // console.log( id )
      }),
    }),

    updateConnectorSwitchService: builder.mutation({
      query: (body) => ({
        method: "PATCH",
        url: `connectors/${body.id}/active`,
        data: { active: body.active },
        // console.log( id )
      }),
    }),

    updateConnectorService: builder.mutation({
      query: (body) => ({
        method: "PATCH",
        url: `connectors/${body.id}`,
        data: {
          rate: body.rateId,
          format: body.format,
          type: body.type,
          power_type: body.power_type,
          power: body.power,
        },
        // console.log( id )
      }),
    }),

    deleteConnectorService: builder.mutation({
      query: (id) => ({
        method: "DELETE",
        url: `connectors/${id}`,
        // console.log( id )
      }),
    }),

    getAllConnectorGroupbyRateService: builder.query<any, void>({
      query: () => {
        return {
          method: "GET",
          url: "connectors/",
        };
      },
    }),

    getChargingStatusService: builder.query<any, void>({
      query: () => {
        return {
          method: "GET",
          url: "connectors/onlineCount",
        };
      },
    }),
  }),
});

export const {
  useAddConnectorServiceMutation,
  useUpdateConnectorSwitchServiceMutation,
  useDeleteConnectorServiceMutation,
  useUpdateConnectorServiceMutation,
  useGetAllConnectorGroupbyRateServiceQuery,
  useGetChargingStatusServiceQuery,
} = connectorApi;
