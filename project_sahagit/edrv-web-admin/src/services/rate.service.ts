import axiosBaseQuery from "@/api/api";
import { createApi } from "@reduxjs/toolkit/query/react";

const NAME = "rateApi";

export const rateApi = createApi({
  reducerPath: NAME, //ชื่อของ reducer
  baseQuery: axiosBaseQuery(),
  endpoints: (builder) => ({
    getAllRateService: builder.query<any, void>({
      // query: (id) => {
      query: () => {
        return {
          method: "GET",
          url: "rates/", //`charge-stations/${id}`
        };
      },
    }),

    addRateService: builder.mutation<any, any>({
      query: (body) => {
        return {
          method: "POST",
          url: "rates",
          data: body,
        };
      },
    }),

    updateRateService: builder.mutation({
      query: (body) => ({
        method: "PATCH",
        url: `rates/${body.id}`,
        data: {
          name: body.name,
          description: body.description,
        },
        // console.log( id )
      }),
    }),
  }),
});

export const {
  useGetAllRateServiceQuery,
  useAddRateServiceMutation,
  useUpdateRateServiceMutation,
} = rateApi;
