import axiosBaseQuery from "@/api/api";
import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { object } from "yup";

const NAME = "locationApi";
const DeleteLocation = "locationDeleteApi";

export const locationApi = createApi({
  reducerPath: NAME,
  baseQuery: axiosBaseQuery(),

  endpoints: (builder) => ({
    getAllLocationService: builder.query<any, void>({
      query: () => {
        return {
          method: "GET",
          url: "locations/",
        };
      },
    }),

    addLocationService: builder.mutation<any, any>({
      query: (body) => {
        return {
          method: "POST",
          url: "locations",
          data: body,
        };
      },
    }),

    deleteLocationService: builder.mutation({
      query: (id) => ({
        method: "DELETE",
        url: `locations/${id}`,
        // console.log( id )
      }),
    }),

    updateLocationService: builder.mutation({
      query: (body) => ({
        method: "PATCH",
        url: `locations/${body.id}`,
        data: body,
        // console.log( id )
      }),
    }),

    updateLocationSwitchService: builder.mutation({
      query: (body) => ({
        method: "PATCH",
        url: `locations/${body.id}/active`,
        data: { active: body.active },
        // console.log( id )
      }),
    }),

    getLocationByIdService: builder.query<any, string>({
      query: (locationId) => {
        return {
          method: "GET",
          url: `locations/${locationId}`,
        };
      },
    }),

    getAllLocationPrismaService: builder.query<any, void>({
      query: () => {
        return {
          method: "GET",
          url: "locations/prisma",
        };
      },
    }),
  }),
});

// Export hooks for usage in functional components, which are
// auto-generated based on the defined endpoints
export const {
  useAddLocationServiceMutation,
  useDeleteLocationServiceMutation,
  useUpdateLocationServiceMutation,
  useUpdateLocationSwitchServiceMutation,
  useGetAllLocationServiceQuery,
  useGetLocationByIdServiceQuery,
  useGetAllLocationPrismaServiceQuery,
} = locationApi;
// export const { useDeleteLocationServiceMutation } = locationDeleteApi;
