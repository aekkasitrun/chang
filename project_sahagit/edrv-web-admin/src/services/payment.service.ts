import axiosBaseQuery from "@/api/api";
import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

const NAME = "paymentApi";

export const paymentApi = createApi({
  reducerPath: NAME,
  baseQuery: axiosBaseQuery(),
  endpoints: (builder) => ({
    addPaymentService: builder.mutation({
      query: (body) => ({
        method: "POST",
        url: "payment",
        data: body,
        // console.log( id )
      }),
    }),
  }),
});

export const { useAddPaymentServiceMutation } = paymentApi;
