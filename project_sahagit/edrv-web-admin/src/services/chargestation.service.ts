import axiosBaseQuery from "@/api/api";
import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { object } from "yup";
// import type { Pokemon } from "./types";

const NAME = "chargeStationApi"; //re

// Define a service using a base URL and expected endpoints
//query ใช้แค่กับ get
//mutation ใช้กับที่เหลือ
export const chargeStationApi = createApi({
  reducerPath: NAME, //ชื่อของ reducer
  baseQuery: axiosBaseQuery(),
  endpoints: (builder) => ({
    //get แบบ Id
    // getAllChargeStation: builder.query<any, {id: string}>
    // getAllChargeStation: builder.query<any, string>
    getAllChargeStationService: builder.query<any, void>({
      // query: (id) => {
      query: () => {
        return {
          method: "GET",
          url: "charge-stations/", //`charge-stations/${id}`
        };
      },
    }),

    getCountChargeStationService: builder.query<any, void>({
      // query: (id) => {
      query: () => {
        return {
          method: "GET",
          url: "charge-stations/count", //`charge-stations/${id}`
        };
      },
    }),

    addChargeStationService: builder.mutation<any, any>({
      query: (body) => {
        return {
          method: "POST",
          url: "charge-stations",
          data: body,
        };
      },
    }),

    addAdvanceChargeStationService: builder.mutation<any, any>({
      query: (body) => {
        return {
          method: "POST",
          url: "charge-stations/all",
          data: body,
        };
      },
    }),

    updateChargeStationService: builder.mutation({
      query: (body) => ({
        method: "PATCH",
        url: `charge-stations/${body.id}`,
        data: {
          location: body.locationId,
          protocol: body.protocol,
        },
        // console.log( id )
      }),
    }),

    updateChargeStationSwitchService: builder.mutation({
      query: (body) => ({
        method: "PATCH",
        url: `charge-stations/${body.id}/public`,
        data: { public: body.public },
        // console.log( id )
      }),
    }),

    deleteChargeStationService: builder.mutation({
      query: (id) => ({
        method: "DELETE",
        url: `charge-stations/${id}`,
        // console.log( id )
      }),
    }),

    getByLocationIdChargeStationService: builder.query<any, string>({
      query: (locationId) => {
        return {
          method: "GET",
          url: `charge-stations/query/${locationId}`,
        };
      },
    }),
  }),
});

// Export hooks for usage in functional components, which are
// auto-generated based on the defined endpoints
export const {
  useAddChargeStationServiceMutation,
  useDeleteChargeStationServiceMutation,
  useGetAllChargeStationServiceQuery,
  useUpdateChargeStationServiceMutation,
  useUpdateChargeStationSwitchServiceMutation,
  useAddAdvanceChargeStationServiceMutation,
  useGetCountChargeStationServiceQuery,
  useGetByLocationIdChargeStationServiceQuery,
} = chargeStationApi;
// export const { useGetLocationQuery } = locationGetApi;
