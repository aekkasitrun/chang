import axiosBaseQuery from "@/api/api";
import { createApi } from "@reduxjs/toolkit/query/react";

const NAME = "usageApi";

export const usageApi = createApi({
  reducerPath: NAME,
  baseQuery: axiosBaseQuery(),
  endpoints: (builder) => ({
    getAllUsageService: builder.query<any, void>({
      query: () => {
        return {
          method: "GET",
          url: "transaction/summary",
        };
      },
    }),

    getByDateUsageService: builder.query<any, string>({
      query: (date) => {
        return {
          method: "GET",
          url: `transaction/usage/${date}`,
        };
      },
    }),

    getUsageSummaryDashboardService: builder.query<any, void>({
      query: () => {
        return {
          method: "GET",
          url: "transaction/summaryDashboard",
        };
      },
    }),
  }),
});

export const {
  useGetAllUsageServiceQuery,
  useGetByDateUsageServiceQuery,
  useGetUsageSummaryDashboardServiceQuery,
} = usageApi;
