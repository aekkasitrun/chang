import React from 'react';
import { Stack, StackProps } from '@mui/material';

const MuiStack: React.FC<StackProps> = ({ children, ...props }) => {
  return (
    <Stack gap={props?.gap || 2} {...props}>
      {children}
    </Stack>
  );
};

export default MuiStack;
