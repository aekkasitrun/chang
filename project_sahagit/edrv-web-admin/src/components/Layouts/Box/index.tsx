import { Box, BoxProps } from "@mui/material";
import React from "react";

interface MuiBoxProps extends BoxProps {
  children: React.ReactNode;
}

const MuiBox: React.FC<MuiBoxProps> = ({ children, ...props }) => {
  return <Box {...props}>{children}</Box>;
};

export default MuiBox;
