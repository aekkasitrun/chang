import React from 'react';

interface ICenterScreen {
  children: React.ReactNode;
}

const CenterScreen: React.FC<ICenterScreen> = ({ children }) => {
  return <div className="h-[calc(100vh-60px)] w-full grid place-content-center">{children}</div>;
};

export default CenterScreen;
