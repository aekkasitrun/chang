import React from 'react';
import {
  TextField,
  StandardTextFieldProps,
  OutlinedTextFieldProps,
  FilledTextFieldProps,
} from '@mui/material';

const MuiTextField: React.FC<
  StandardTextFieldProps | OutlinedTextFieldProps | FilledTextFieldProps
> = ({ variant, ...props }) => <TextField variant={variant} {...props} />;

export default MuiTextField;
