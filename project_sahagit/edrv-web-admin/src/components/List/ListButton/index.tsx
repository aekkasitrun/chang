import React from "react";
import { Menu } from "..";
import { ListItemButton, ListItemIcon, ListItemText } from "@mui/material";
import { ExpandMore } from "@mui/icons-material";

interface IMuiListItemButtonProps extends Menu {
  open: boolean;
  handleToggleSubMenu: (_key: string) => void;
  handleRouteToUrl: (_url: string) => void;
  subMenuList: string[];
  isSubMenu?: boolean;
}
const MuiListItemButton: React.FC<IMuiListItemButtonProps> = ({
  icon,
  titleIndex,
  title,
  url,
  children,
  open,
  handleRouteToUrl,
  handleToggleSubMenu,
  subMenuList,
  isSubMenu,
}) => {
  const hasChildren = subMenuList.includes(titleIndex);
  return (
    <ListItemButton
      sx={{
        minHeight: 48,
        justifyContent: open ? "initial" : "center",
        px: 2.5,
        pl: isSubMenu && open ? 4 : 2.5,
      }}
      onClick={() =>
        children?.length
          ? handleToggleSubMenu(titleIndex)
          : handleRouteToUrl(url)
      }
    >
      <ListItemIcon
        sx={{
          minWidth: 0,
          mr: open ? 3 : "auto",
          justifyContent: "center",
        }}
      >
        {icon}
      </ListItemIcon>
      <ListItemText primary={title} sx={{ opacity: open ? 1 : 0 }} />
      {open && children?.length ? (
        <>{<ExpandMore className={`${hasChildren ? "rotate-180" : ""}`} />}</>
      ) : (
        <></>
      )}
    </ListItemButton>
  );
};

export default MuiListItemButton;
