import { Collapse, List, ListItem, ListProps } from "@mui/material";
import { useRouter } from "next/router";
import React, { useState } from "react";
import MuiListItemButton from "./ListButton";

export interface Menu {
  titleIndex: string;
  title: string;
  url: string;
  icon: React.ReactNode;
  children?: Menu[];
}

interface IMuiListProps extends ListProps {
  open: boolean;
  menuList: Menu[];
}

const MuiList: React.FC<IMuiListProps> = ({ open, menuList }) => {
  const router = useRouter();
  const [subMenuList, setSubMenuList] = useState<string[]>([]);

  const handleToggleSubMenu = (key: string) => {
    if (subMenuList.includes(key))
      return setSubMenuList(subMenuList.filter((subMenu) => subMenu !== key));
    return setSubMenuList([...subMenuList, key]);
  };

  const handleRouteToUrl = (url: string) => {
    router.push(url);
  };

  return (
    <List >
      {menuList.map((menu: Menu) => (
        <ListItem key={menu.titleIndex} disablePadding className="block">
          <MuiListItemButton
            {...menu}
            handleToggleSubMenu={handleToggleSubMenu}
            handleRouteToUrl={handleRouteToUrl}
            open={open}
            subMenuList={subMenuList}
          />

          {menu?.children?.length ? (
            <Collapse in={subMenuList.includes(menu.titleIndex)}>
              {menu?.children?.map((subMenu: Menu) => {
                return (
                  <MuiListItemButton
                    {...subMenu}
                    key={subMenu.titleIndex}
                    handleToggleSubMenu={handleToggleSubMenu}
                    handleRouteToUrl={handleRouteToUrl}
                    open={open}
                    subMenuList={subMenuList}
                    isSubMenu
                  />
                );
              })}
            </Collapse>
          ) : (
            <></>
          )}
        </ListItem>
      ))}
    </List>
  );
};

export default MuiList;
