import React from 'react';
import { IconButton, IconButtonProps } from '@mui/material';

export interface MuiIconButtonProps extends IconButtonProps {
  color?: 'inherit' | 'primary' | 'secondary' | 'success' | 'error' | 'info' | 'warning' | undefined;
}

const MuiIconButton: React.FC<MuiIconButtonProps> = ({ sx, onClick, children, ...props }) => (
  <IconButton sx={sx} onClick={onClick} {...props}>
    {children}
  </IconButton>
);

MuiIconButton.defaultProps = {
  color: 'inherit',
};

export default MuiIconButton;
