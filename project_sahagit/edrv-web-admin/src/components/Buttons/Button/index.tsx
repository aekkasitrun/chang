import React from "react";
import { Button, ButtonProps } from "@mui/material";

interface IButtonProps extends ButtonProps {
  variant?: "contained" | "text" | "outlined";
  color?:
    | "inherit"
    | "primary"
    | "secondary"
    | "success"
    | "error"
    | "info"
    | "warning"
    | undefined;
  [props: string]: unknown;
}

const MuiButton: React.FC<IButtonProps> = ({
  variant = "contained",
  sx,
  color,
  className,
  children,
  ...props
}) => {
  return (
    <Button
      variant={variant}
      className={className}
      sx={{
        ...sx,
      }}
      color={color}
      {...props}
    >
      {children}
    </Button>
  );
};

export default MuiButton;
