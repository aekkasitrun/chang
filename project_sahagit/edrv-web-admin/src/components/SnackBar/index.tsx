import React from 'react';
import { Snackbar, SnackbarProps, Alert as MuiAlert, AlertProps } from '@mui/material';
import Slide, { SlideProps } from '@mui/material/Slide';
import { TransitionProps } from '@mui/material/transitions';


export type SnackBarType = 'error' | 'info' | 'success' | 'warning';
interface IMuiSnackbarProps extends SnackbarProps {
  message: string;
  open: boolean;
  setOpen: (_param?: any) => void;
  type: SnackBarType;
  duration?: number;
}

const Transition = React.forwardRef(function Transition(
  props: TransitionProps & {
    children: React.ReactElement<any, any>;
  },
  ref: React.Ref<unknown>,
) {
  return <Slide direction="down" ref={ref} {...props} />;
});

// eslint-disable-next-line react/display-name
const Alert = React.forwardRef<HTMLDivElement, AlertProps>((props, ref) => (
  // <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />
  <MuiAlert elevation={6} ref={ref} {...props} />
));

const MuiSnackbar: React.FC<IMuiSnackbarProps> = ({ type, duration = 3000, open, message, setOpen }) => {
  const handleClose = (event?: React.SyntheticEvent | Event, reason?: string) => {
    if (reason === 'clickaway') {
      return;
    }

    setOpen();
  };

  return (
    <Snackbar
      open={open}
      autoHideDuration={duration}
      onClose={handleClose}
      anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
      TransitionComponent={Transition}
    >
      <Alert onClose={handleClose} severity={type} sx={{ width: '100%' }} className="font-bold" >
        {message}
      </Alert>
    </Snackbar>
  );
};

export default MuiSnackbar;
