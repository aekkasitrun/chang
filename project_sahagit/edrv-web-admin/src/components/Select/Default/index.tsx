import React from 'react';
import {
    SelectProps
} from '@mui/material';
import Select from '@mui/material/Select';

const MuiSelect: React.FC<SelectProps> = ({ ...props }) => <Select {...props} />;

export default MuiSelect;
