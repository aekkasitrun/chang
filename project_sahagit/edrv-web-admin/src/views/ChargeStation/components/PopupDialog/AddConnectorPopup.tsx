import IconButton from '@mui/material/IconButton';
import React from "react";
import Dialog from '@mui/material/Dialog';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import ClearRoundedIcon from '@mui/icons-material/ClearRounded';
import GroupWorkOutlinedIcon from '@mui/icons-material/GroupWorkOutlined';
import { AddConnectorDTO } from '../DTO/AddConnectorDTO';
import useConnector from '../../hooks/connector';
import Tooltip from '@mui/material/Tooltip';

import * as Yup from "yup";
import { FormikHelpers, useFormik } from "formik";
import ConnectorInput from '../Input/ConnectorInput';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import CloseIcon from '@mui/icons-material/Close';
import AddIcon from '@mui/icons-material/Add';
import { SnackbarProvider, VariantType, useSnackbar } from 'notistack';
import Snackbar from '@mui/material/Snackbar';
import MuiSnackbar from '@/components/SnackBar';


interface AddConnectorProps {
    chargeStation: any;
}

export default function AddConnectorPopupDialog(props: AddConnectorProps) {

    const { chargeStation } = props;

    const [alert, setAlert] = React.useState(false);

    const handleResetAlert = () => {
        setAlert(false)
    }

    const initConnector: AddConnectorDTO = {
        id: chargeStation.chargeStationEdrvId,
        name: "",
        description: "",
        active: true,
        rate: "",
        format: "",
        type: "",
        power_type: "",
        power: 0,
    }

    const validateSchema = Yup.object().shape({
        id: Yup.string(),
        name: Yup.string().required("Content cannot be empty."),
        description: Yup.string().required("Content cannot be empty."),
        active: Yup.boolean(),
        rate: Yup.string().required("Content cannot be empty."),
        format: Yup.string().required("Content cannot be empty."),
        type: Yup.string().required("Content cannot be empty."),
        power_type: Yup.string().required("Content cannot be empty."),
        power: Yup.number().required("Content cannot be empty."),
    });
    // console.log(locationId)
    const { handleAddConnector, checkErrorAlert } = useConnector();

    const { values, handleChange, handleSubmit, errors } = useFormik({
        initialValues: initConnector,
        validationSchema: validateSchema,
        validateOnChange: true, // if true the form will validate the value every time the onChange is triggered
        onSubmit: handleSubmitLocation,

    });

    async function handleSubmitLocation(value: AddConnectorDTO, formikHelper: FormikHelpers<AddConnectorDTO>) {
        console.log(value);
        await handleAddConnector(value)
        formikHelper.resetForm({ values: initConnector });
        console.log("Chenfggggg");
        setAlert(true)
    }

    const [open, setOpen] = React.useState(false);

    const handleClickOpen = () => {
        setOpen(true);
    };
    const handleClose = () => {
        setOpen(false);
    };

    return (
        <div>
            <Toolbar className='bg-[#1890FF1A] rounded-t-lg'>
                <Typography sx={{ ml: 2, flex: 1 }} variant="h6" component="div" className='font-bold '>
                    ข้อมูลหัวชาร์จสถานี : {chargeStation.name}
                </Typography>
                <Tooltip title="เพิ่มหัวชาร์จ" placement="top">
                    <Button variant="contained" startIcon={<AddIcon />} onClick={handleClickOpen} style={{ width: '140px', height: '40px', backgroundColor: '#56C3CD' }}>
                        <Typography>เพิ่มหัวชาร์จ</Typography>
                    </Button>
                </Tooltip>
                {/* <AddConnectorPopupDialog chargeStationId={chargeStation._id} /> */}
            </Toolbar>

            <Dialog open={open} onClose={handleClose} fullWidth>
                <div className='flex justify-end gap-2'>
                    <Typography sx={{ flex: '1 1 100%' }}>
                        <DialogTitle>
                            Add Connector
                        </DialogTitle>
                    </Typography>
                    <Button variant="text" onClick={handleClose}>
                        <ClearRoundedIcon />
                    </Button>
                </div>
                <DialogContent>
                    <Box
                        sx={{
                            display: 'flex',
                            flexWrap: 'wrap',
                            flexDirection: 'column',
                            m: 'auto',
                            width: 'fit-content',
                            borderRadius: 1,
                        }}
                    >
                        <ConnectorInput
                            handleChange={handleChange}
                            handleSubmit={handleSubmit}
                            values={values}
                            errors={errors}
                        />
                    </Box>
                </DialogContent>
            </Dialog>
            <MuiSnackbar message={checkErrorAlert.message} type={checkErrorAlert.type} open={alert} setOpen={handleResetAlert} />
        </div>
    )
}