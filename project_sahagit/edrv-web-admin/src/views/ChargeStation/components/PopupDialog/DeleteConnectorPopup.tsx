import IconButton from '@mui/material/IconButton';
import DeleteIcon from '@mui/icons-material/Delete';
import React from "react";
import useConnector from '../../hooks/connector';
import Dialog from '@mui/material/Dialog';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import DialogContentText from '@mui/material/DialogContentText';
import DialogActions from '@mui/material/DialogActions';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import ClearRoundedIcon from '@mui/icons-material/ClearRounded';
import Tooltip from '@mui/material/Tooltip';
import MuiSnackbar from '@/components/SnackBar';
import ErrorIcon from '@mui/icons-material/Error';
import { grey } from '@mui/material/colors';

interface DeleteConnectorProps {
    connectorId: string;
    connectorName: string;
}

export default function ConnectorDeletePopupDialog(props: DeleteConnectorProps) {

    const { handleDeleteConnector, checkErrorAlert } = useConnector();

    const [alert, setAlert] = React.useState(false);

    const handleResetAlert = () => {
        setAlert(false)
    }

    const { connectorId, connectorName } = props;
    const [open, setOpen] = React.useState(false);
    const handleClickOpen = () => {
        setOpen(true);
    };
    const handleClose = () => {
        setOpen(false);
    };

    async function DeleteConnector(id: string) {
        // console.log("RR = " + id)
        await handleDeleteConnector(id)
        setAlert(true)
    }

    // console.log("location = " + locationId);
    return (
        <div>
            <Tooltip title="ลบ" placement="top">
                <IconButton onClick={handleClickOpen}>
                    <DeleteIcon className="text-red-500" />
                </IconButton>
            </Tooltip >
            <Dialog open={open} onClose={handleClose} fullWidth>
                <DialogContent>
                    <DialogContentText id="alert-dialog-description">
                        <div className='grid grid-cols-1 '>
                            <div className='flex justify-center '>
                                <ErrorIcon
                                    fontSize="large"
                                    sx={{ color: '#FF3333' }}
                                    style={{ width: '120px', height: '120px' }}
                                />
                            </div>
                            <div className='flex justify-center text-2xl font-bold text-black'>ยืนยันการลบ</div>
                            <div className='flex justify-center text-black text-xl Inter'> คุณกำลังลบหัวชาร์จ {connectorName} ต้องการดำเนินต่อหรือไม่ ?
                            </div>
                        </div>
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} variant="outlined" sx={{ color: grey[500] }} className='border-[#868889]'>
                        <div className='text-[#868889]'>ยกเลิก</div>
                    </Button>
                    <Button onClick={() => DeleteConnector(connectorId)} autoFocus variant="contained" className='bg-[#FF3333]'>
                        ลบ
                    </Button>
                </DialogActions>
            </Dialog>
            <MuiSnackbar message={checkErrorAlert.message} type={checkErrorAlert.type} open={alert} setOpen={handleResetAlert} />
        </div>
    )
}
