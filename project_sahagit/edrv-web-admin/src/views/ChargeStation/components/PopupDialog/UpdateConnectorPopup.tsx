import IconButton from '@mui/material/IconButton';
import React from "react";

import Dialog from '@mui/material/Dialog';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';

import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import ClearRoundedIcon from '@mui/icons-material/ClearRounded';

import * as Yup from "yup";
import { FormikHelpers, useFormik } from "formik";
import { UpdateConnectorDTO } from '../DTO/UpdateConnectorDTO';
import ConnectorInputUpdate from '../Input/ConnectorInputUpdate';
import EditIcon from '@mui/icons-material/Edit';
import useConnector from '../../hooks/connector';
import MuiSnackbar from '@/components/SnackBar';

interface UpdateConnectorProps {
    connectorId: string;
    rateIdProps: string;
    formatProps: string;
    typeProps: string;
    powerTypeProps: string;
    powerProps: number;
    nameConnector: string;
}

const validateSchema = Yup.object().shape({
    connectorId: Yup.string(),
    rateIdProps: Yup.string(),
    formatProps: Yup.string(),
    typeProps: Yup.string(),
    powerTypeProps: Yup.string(),
    powerProps: Yup.number(),
});

export default function UpdateConnectorPopupDialog(props: UpdateConnectorProps) {

    const { connectorId, rateIdProps, formatProps, typeProps, powerTypeProps, powerProps, nameConnector } = props;
    // console.log("OperatorName = " + operatorName);

    const [alert, setAlert] = React.useState(false);

    const handleResetAlert = () => {
        setAlert(false)
    }

    const initConnector: UpdateConnectorDTO = {
        id: connectorId,
        rateId: rateIdProps,
        format: formatProps,
        type: typeProps,
        power_type: powerTypeProps,
        power: powerProps,
    }
    // console.log(chargeStationId)
    const { handleUpdateConnector, checkErrorAlert } = useConnector();

    const { values, handleChange, handleSubmit } = useFormik({
        initialValues: initConnector,
        validationSchema: validateSchema,
        validateOnChange: false, // if true the form will validate the value every time the onChange is triggered
        onSubmit: handleSubmitLocation,

    });
    async function handleSubmitLocation(value: UpdateConnectorDTO, formikHelper: FormikHelpers<UpdateConnectorDTO>) {
        console.log(value);
        await handleUpdateConnector(value)
        formikHelper.resetForm({ values: initConnector });
        setAlert(true)
    }

    const [open, setOpen] = React.useState(false);
    const handleClickOpen = () => {
        setOpen(true);
    };
    const handleClose = () => {
        setOpen(false);
    };

    return (
        <div>

            <IconButton aria-label="edit" onClick={handleClickOpen}>
                <EditIcon />
            </IconButton>

            <Dialog open={open} onClose={handleClose} fullWidth>
                <div className='flex justify-end gap-2'>
                    <Typography sx={{ flex: '1 1 100%' }}>
                        <DialogTitle>
                            Edit Connector : {nameConnector}
                        </DialogTitle>
                    </Typography>
                    <Button variant="text" onClick={handleClose}>
                        <ClearRoundedIcon />
                    </Button>
                </div>
                <DialogContent>
                    <Box
                        sx={{
                            display: 'flex',
                            flexWrap: 'wrap',
                            flexDirection: 'column',
                            m: 'auto',
                            width: 'fit-content',
                            borderRadius: 1,
                        }}
                    >
                        <ConnectorInputUpdate
                            handleChange={handleChange}
                            handleSubmit={handleSubmit}
                            values={values}
                        // operatorName={operatorName}
                        />
                    </Box>
                </DialogContent>
            </Dialog>
            <MuiSnackbar message={checkErrorAlert.message} type={checkErrorAlert.type} open={alert} setOpen={handleResetAlert} />
        </div>
    )
}