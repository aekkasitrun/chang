import IconButton from '@mui/material/IconButton';
import React from "react";
import { UpdateChargeStationDTO } from '../DTO/UpdateChargeStationDTO';
import useChargeStation from '../../hooks/chargestation';

import Dialog from '@mui/material/Dialog';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';

import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import ClearRoundedIcon from '@mui/icons-material/ClearRounded';
import ChargeStationInputUpdate from '../Input/ChargeStationInputUpdate';

import * as Yup from "yup";
import { FormikHelpers, useFormik } from "formik";
import Tooltip from '@mui/material/Tooltip';
import EvStationIcon from '@mui/icons-material/EvStation';
import MuiSnackbar from '@/components/SnackBar';
interface UpdateChargeStationProps {
    chargeStationId: string;
    locationId: string;
    changeStationName: string;
    protocolProps: string;
}

const validateSchema = Yup.object().shape({
    chargeStationId: Yup.string(),
    locationId: Yup.string(),
    protocolProps: Yup.string(),
});

export default function UpdateChargeStationPopupDialog(props: UpdateChargeStationProps) {

    const { chargeStationId, locationId, changeStationName, protocolProps } = props;
    // console.log("OperatorName = " + operatorName);

    const [alert, setAlert] = React.useState(false);

    const handleResetAlert = () => {
        setAlert(false)
    }

    const initLocation: UpdateChargeStationDTO = {
        id: chargeStationId,
        locationId: locationId,
        protocol: protocolProps,
    }
    // console.log(chargeStationId)
    const { handleUpdateChargeStation, checkErrorAlert } = useChargeStation();

    const { values, handleChange, handleSubmit } = useFormik({
        initialValues: initLocation,
        validationSchema: validateSchema,
        validateOnChange: false, // if true the form will validate the value every time the onChange is triggered
        onSubmit: handleSubmitLocation,

    });
    async function handleSubmitLocation(value: UpdateChargeStationDTO, formikHelper: FormikHelpers<UpdateChargeStationDTO>) {
        console.log(value);
        await handleUpdateChargeStation(value)
        formikHelper.resetForm({ values: initLocation });
        setAlert(true)
    }

    const [open, setOpen] = React.useState(false);
    const handleClickOpen = () => {
        setOpen(true);
    };
    const handleClose = () => {
        setOpen(false);
    };

    return (
        <div>
            <Tooltip title="สถานีชาร์จ" placement="top">
                <IconButton aria-label="edit" onClick={handleClickOpen}>
                    <EvStationIcon />
                </IconButton>
            </Tooltip>
            <Dialog open={open} onClose={handleClose} fullWidth>
                <div className='flex justify-end gap-2'>
                    <Typography sx={{ flex: '1 1 100%' }}>
                        <DialogTitle>
                            Edit Charge Station : {changeStationName}
                        </DialogTitle>
                    </Typography>
                    <Button variant="text" onClick={handleClose}>
                        <ClearRoundedIcon />
                    </Button>
                </div>
                <DialogContent>
                    <Box
                        sx={{
                            display: 'flex',
                            flexWrap: 'wrap',
                            flexDirection: 'column',
                            m: 'auto',
                            width: 'fit-content',
                            borderRadius: 1,
                        }}
                    >
                        <ChargeStationInputUpdate
                            handleChange={handleChange}
                            handleSubmit={handleSubmit}
                            values={values}
                        // operatorName={operatorName}
                        />
                    </Box>
                </DialogContent>
            </Dialog>
            <MuiSnackbar message={checkErrorAlert.message} type={checkErrorAlert.type} open={alert} setOpen={handleResetAlert} />
        </div>
    )
}