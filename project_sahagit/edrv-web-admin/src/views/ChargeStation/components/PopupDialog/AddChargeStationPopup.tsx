import * as React from 'react';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Tooltip from '@mui/material/Tooltip';
import Button from '@mui/material/Button';
import AddIcon from '@mui/icons-material/Add';

import Dialog from '@mui/material/Dialog';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';

import * as Yup from "yup";
import { FormikHelpers, useFormik } from "formik";
import ChargeStationInput from '../Input/ChargeStationInput';
import { AddChargeStationDTO } from '../DTO/AddChargeStationDTO';
import useChargeStation from '../../hooks/chargestation';
import ClearRoundedIcon from '@mui/icons-material/ClearRounded';
import MuiSnackbar from '@/components/SnackBar';


interface ChargeStationPopupDialog {
    numSelected: number;
}


const initChargeStation: AddChargeStationDTO = {
    name: "",
    description: "",
    location: "",
    protocol: "",
    public: true,
    // coordinates: [0, 0],
    is_simulator: true,
};

const validateSchema = Yup.object().shape({
    name: Yup.string().required("name cannot be empty."),
    description: Yup.string().required("description cannot be empty."),
    location: Yup.string().required("location cannot be empty."),
    protocol: Yup.string().required("protocol cannot be empty."),
    public: Yup.bool(),
    // coordinates: Yup.string().required("coordinates cannot be empty."),
    is_simulator: Yup.bool(),

});

const ChargeStationPopupDialog: React.FC<ChargeStationPopupDialog> = ({ numSelected }) => {
    // const { numSelected } = props;

    const [alert, setAlert] = React.useState(false);

    const handleResetAlert = () => {
        setAlert(false)
    }

    const { handleAddChargeStation, checkErrorAlert } = useChargeStation();

    const { values, handleChange, handleSubmit, errors } = useFormik({
        initialValues: initChargeStation,
        validationSchema: validateSchema,
        validateOnChange: true, // if true the form will validate the value every time the onChange is triggered
        onSubmit: handleSubmitChargeStation,

    });
    async function handleSubmitChargeStation(value: AddChargeStationDTO, formikHelper: FormikHelpers<AddChargeStationDTO>) {
        console.log(value);
        await handleAddChargeStation(value)
        formikHelper.resetForm({ values: initChargeStation });
        setAlert(true)
    }

    const [open, setOpen] = React.useState(false);

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };
    return (
        <div>
            <Toolbar
            // sx={{
            //     pl: { sm: 2 },
            //     pr: { xs: 1, sm: 1 },
            //     // ...(numSelected > 0 && {
            //     //     bgcolor: (theme) =>
            //     //         alpha(theme.palette.primary.main, theme.palette.action.activatedOpacity),
            //     // }),
            // }}
            >
                {/* {numSelected > 0 ? (
                    <Typography
                        sx={{ flex: '1 1 100%' }}
                        color="inherit"
                        variant="subtitle1"
                        component="div"
                    >
                        {numSelected} selected
                    </Typography>
                ) : ( */}
                <Typography
                    sx={{ flex: '1 1 100%' }}
                    variant="h6"
                    className='text-2xl font-bold'
                >
                    สถานีชาร์จ
                </Typography>

                {/* )} */}
                {/* {numSelected > 0 ? (
                    <Tooltip title="Delete">
                        <IconButton>
                            <DeleteIcon />
                        </IconButton>
                    </Tooltip>
                ) : ( */}
                <Tooltip title="เพิ่มสถานีชาร์จ" placement="top">
                    {/* <IconButton>
                        <FilterListIcon />
                    </IconButton> */}
                    <Button variant="contained" startIcon={<AddIcon />} onClick={handleClickOpen} style={{ width: '170px', height: '40px' }}>
                        <Typography>เพิ่มสถานีชาร์จ</Typography>
                    </Button>
                </Tooltip>
                {/* )} */}
            </Toolbar>

            <Dialog
                open={open}
                onClose={handleClose}
                fullWidth={true}
                maxWidth='sm'
            >
                <div className='flex justify-end gap-2'>
                    <Typography sx={{ flex: '1 1 100%' }}>
                        <DialogTitle>
                            เพิ่มสถานีชาร์จ
                        </DialogTitle>
                    </Typography>
                    <Button variant="text" onClick={handleClose}>
                        <ClearRoundedIcon />
                    </Button>
                </div>
                <DialogContent>
                    <Box
                        sx={{
                            display: 'flex',
                            flexDirection: 'column',
                            m: 'auto',
                            width: 'fit-content',
                        }}
                    >
                        <ChargeStationInput
                            handleChange={handleChange}
                            handleSubmit={handleSubmit}
                            values={values}
                            errors={errors}
                        />
                    </Box>
                </DialogContent>
            </Dialog>
            <MuiSnackbar message={checkErrorAlert.message} type={checkErrorAlert.type} open={alert} setOpen={handleResetAlert} />
        </div >
    )
}

export default ChargeStationPopupDialog;