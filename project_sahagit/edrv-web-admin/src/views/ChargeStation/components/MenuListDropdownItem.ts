export const protocolChargeStation = [
  {
    name: "ocpp1.6",
    protocol: "ocpp1.6",
  },
  {
    name: "ocpp2.0.1",
    protocol: "ocpp2.0.1",
  },
];

export const formatConnector = [
  {
    name: "Cable",
    format: "Cable",
  },
  {
    name: "Socket",
    format: "Socket",
  },
];

export const typeConnector = [
  {
    name: "Type 2",
    type: "Type 2",
  },

  {
    name: "J1772",
    type: "J1772",
  },

  {
    name: "CCS1",
    type: "CCS1",
  },

  {
    name: "CCS2",
    type: "CCS2",
  },

  {
    name: "CHAdeMO",
    type: "CHAdeMO",
  },

  {
    name: "IEC 60309",
    type: "IEC 60309",
  },
];

export const powerTypeConnector = [
  {
    name: "AC Single Phase",
    power_type: "AC Single Phase",
  },
  {
    name: "AC Three Phase",
    power_type: "AC Three Phase",
  },
  {
    name: "DC",
    power_type: "DC",
  },
];
