import * as React from 'react';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Tooltip from '@mui/material/Tooltip';
import Button from '@mui/material/Button';
import AddIcon from '@mui/icons-material/Add';

import Dialog from '@mui/material/Dialog';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';

import ClearRoundedIcon from '@mui/icons-material/ClearRounded';
import { useGetAllLocationServiceQuery } from "@/services/location.service";
import TableHead from '@mui/material/TableHead';
import AppBar from '@mui/material/AppBar';
import { TableCell, TableRow } from '@mui/material';
import Skeleton from '@mui/material/Skeleton';

interface ChargeStationOperatorListProps {
    chargeStationLocationId: string;
}

export default function GetChargeStationOperatorName(props: ChargeStationOperatorListProps) {
    const { chargeStationLocationId } = props;
    const locationQuery = useGetAllLocationServiceQuery()
    if (locationQuery.isLoading) return <TableCell align="center" sx={{ borderRight: 1 }}>
        <Skeleton variant="text" sx={{ fontSize: '1rem' }} />
    </TableCell>
    if (locationQuery.isError) return <TableCell align="center" sx={{ borderRight: 1 }}>
        Error...
    </TableCell>
    return (

        <TableCell align="center" sx={{ borderRight: 1 }}>
            {/* <Typography
           sx={{ flex: '1 1 100%' }}
             variant="h6"
         > */}

            {locationQuery?.data?.result.map((location: any) => (

                <div key={location._id}>
                    {chargeStationLocationId == location._id ? (
                        <div >
                            {location.operatorName}
                        </div>
                    ) : (<div></div>)}
                </div>
            ))}

            {/* </Typography> */}
        </TableCell>

    )
}