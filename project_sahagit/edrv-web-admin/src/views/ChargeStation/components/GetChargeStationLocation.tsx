import * as React from 'react';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import { Table, TableBody, TableCell, TableRow } from '@mui/material';


import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@mui/icons-material/KeyboardArrowUp';
import IconButton from '@mui/material/IconButton';
import Collapse from '@mui/material/Collapse';
import ChargeStationUpdateSwitch from './Switch/updateChargeStationSwitch';
import ConnectorList from './Switch/ConnectorList';
import ConnectorFullList from './List/ConnectorFullList';
import UpdateChargeStationPopupDialog from './PopupDialog/UpdateChargeStationPopup';
import ChargeStationDeletePopupDialog from './PopupDialog/DeletePopup';
import EvStationIcon from '@mui/icons-material/EvStation';
import LocationOnIcon from '@mui/icons-material/LocationOn';

interface ChargeStationLocationListProps {
    chargeStationProps: any;
}

export default function GetChargeStationLocation(props: ChargeStationLocationListProps) {
    const { chargeStationProps } = props;
    // const [open, setOpen] = React.useState(false);

    return (
        <React.Fragment>
            <TableRow>
                <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6} className='bg-[#D9D9D940]'>
                    <Box sx={{ margin: 1 }} >

                        <Typography variant="h6" gutterBottom component="div" className='flex'>
                            <LocationOnIcon
                                fontSize="large"
                                sx={{ color: '#0DA9B8' }}
                                style={{ width: '27.5px', height: '30px' }}

                            />
                            {'  ' + chargeStationProps.operatorName + ' '}
                            {chargeStationProps.locationAddresses[0].streetAndNumber + ' '}
                            {chargeStationProps.locationAddresses[0].city + ' '}
                            {chargeStationProps.locationAddresses[0].state + ' '}
                            {chargeStationProps.locationAddresses[0].country + ' '}
                            {chargeStationProps.locationAddresses[0].postalCode}
                        </Typography>
                    </Box>
                </TableCell>
            </TableRow>


            {chargeStationProps.ChargeStation.map((chargeStation: any, index: number) => {
                return (
                    <TableRow key={index}>
                        <TableCell align="center"  >{chargeStation.name}</TableCell>
                        <TableCell align="center" >{chargeStation.description}</TableCell>
                        <TableCell align="center" >{chargeStation.protocol}</TableCell>
                        <TableCell align="center" >
                            <ChargeStationUpdateSwitch chargeStationId={chargeStation.chargeStationEdrvId} chargeStationPublic={chargeStation.public} />
                        </TableCell>
                        <TableCell >
                            <ConnectorList chargeStationConnector={chargeStation} />
                        </TableCell>
                        <TableCell align="center" >
                            <div className='flex justify-center'>
                                <ConnectorFullList chargeStation={chargeStation} />
                                <UpdateChargeStationPopupDialog
                                    chargeStationId={chargeStation.chargeStationEdrvId}
                                    locationId={chargeStation.location}
                                    changeStationName={chargeStation?.name}
                                    protocolProps={chargeStation?.protocol}
                                />
                                <ChargeStationDeletePopupDialog chargeStationId={chargeStation.chargeStationEdrvId} chargeStationName={chargeStation?.name} />
                            </div>
                        </TableCell>
                    </TableRow>
                )
            })}
        </React.Fragment>

    )
}