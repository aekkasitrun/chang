import React from "react";
import GroupWorkOutlinedIcon from '@mui/icons-material/GroupWorkOutlined';
import { UpdateConnectorSwitchDTO } from "../DTO/UpdateConnectorDTO";
import useConnector from "../../hooks/connector";
import Checkbox from '@mui/material/Checkbox';
import Tooltip from '@mui/material/Tooltip';
import MuiSnackbar from "@/components/SnackBar";

interface ConnectorListProps {
    chargeStationConnector: any;
}

interface UpdateConnectorSwitchProps {
    connectorId: string;
    active: boolean;
    name?: string;
}

interface Alertprops {
    alertProps: boolean;
}

let temp: boolean

function ConnectorUpdateSwitch(props: UpdateConnectorSwitchProps) {

    const { connectorId, active, name } = props;

    const [checked, setChecked] = React.useState(active);
    const { handleUpdateConnectorSwitch, checkErrorAlert } = useConnector();

    const [open, setOpen] = React.useState(false);
    const handleResetAlert = () => {
        setOpen(false)
    }

    const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setChecked(event.target.checked);
        console.log("ConnectorDara = " + JSON.stringify(initConnector));
        // handleUpdateConnectorSwitch(initConnector);
        updateSwitch()
    };

    async function updateSwitch() {
        await handleUpdateConnectorSwitch(initConnector);
        // console.log("Chenfggggg");
        setOpen(true)
    }

    const initConnector: UpdateConnectorSwitchDTO = {
        id: connectorId,
        active: !checked
    }

    return (
        <div>
            <Tooltip title={name} placement="top">
                {active == true ? (
                    <Checkbox
                        checked={checked}
                        onChange={handleChange}
                        inputProps={{ 'aria-label': 'controlled' }}
                        icon={<GroupWorkOutlinedIcon className="text-red-500" />}
                        checkedIcon={<GroupWorkOutlinedIcon className="text-green-500" />}
                    />
                ) : (
                    <Checkbox
                        checked={checked}
                        onChange={handleChange}
                        inputProps={{ 'aria-label': 'controlled' }}
                        icon={<GroupWorkOutlinedIcon className="text-red-500" />}
                        checkedIcon={<GroupWorkOutlinedIcon className="text-green-500" />}
                    />
                )}
            </Tooltip>
            <MuiSnackbar message={checkErrorAlert.message} type={checkErrorAlert.type} open={open} setOpen={handleResetAlert} />

            {/* <div><AlertPopup openProps={open} /></div> */}
        </div>

        // <Tooltip title={name} placement="top">
        //     {active == true ? (
        //         <FormGroup>
        //             <FormControlLabel
        //                 control={<Checkbox
        //                     checked={checked}
        //                     onChange={handleChange}
        //                     inputProps={{ 'aria-label': 'controlled' }}
        //                     icon={<GroupWorkOutlinedIcon className="text-red-500" />}
        //                     checkedIcon={<GroupWorkOutlinedIcon className="text-green-500" />}

        //                 />}
        //                 label={name}
        //                 labelPlacement="bottom"
        //             />

        //         </FormGroup>
        //     ) : (
        //         <FormGroup>
        //             <FormControlLabel
        //                 control={<Checkbox
        //                     checked={checked}
        //                     onChange={handleChange}
        //                     inputProps={{ 'aria-label': 'controlled' }}
        //                     icon={<GroupWorkOutlinedIcon className="text-red-500" />}
        //                     checkedIcon={<GroupWorkOutlinedIcon className="text-green-500" />}

        //                 />}
        //                 label={name}
        //                 labelPlacement="bottom"
        //             />

        //         </FormGroup>
        //     )}
        // </Tooltip>

    )
}

export default function ConnectorList(props: ConnectorListProps) {
    const { chargeStationConnector } = props;

    return (

        <div className='flex'>
            {chargeStationConnector?.Connector.map((connector: any, index: number) => (
                <div key={index} >
                    {connector?.connectorEdrvId != null ? (
                        <div >
                            <Tooltip title={connector?.name}>
                                <ConnectorUpdateSwitch connectorId={connector?.connectorEdrvId} active={connector?.active} name={connector?.name} />
                            </Tooltip>
                        </div>
                    ) : (
                        <div></div>
                    )}
                </div>
            ))}
        </div>

    )
}