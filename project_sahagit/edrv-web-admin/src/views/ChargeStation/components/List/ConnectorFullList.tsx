import IconButton from '@mui/material/IconButton';
import React from "react";
import Dialog from '@mui/material/Dialog';
import Typography from '@mui/material/Typography';
import Tooltip from '@mui/material/Tooltip';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import GroupWorkOutlinedIcon from '@mui/icons-material/GroupWorkOutlined';
import { TransitionProps } from '@mui/material/transitions';
import Slide from '@mui/material/Slide';
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import CloseIcon from '@mui/icons-material/Close';
import ConnectorDeletePopupDialog from '../PopupDialog/DeleteConnectorPopup';
import Box from '@mui/material/Box';
import AddConnectorPopupDialog from '../PopupDialog/AddConnectorPopup';
import UpdateConnectorPopupDialog from '../PopupDialog/UpdateConnectorPopup';
import { useState } from 'react';
import TablePagination, { TablePaginationProps } from '@mui/material/TablePagination';

interface ConnectorListProps {
    chargeStation: any;
}

const Transition = React.forwardRef(function Transition(
    props: TransitionProps & {
        children: React.ReactElement;
    },
    ref: React.Ref<unknown>,
) {
    return <Slide direction="up" ref={ref} {...props} />;
});

export default function ConnectorFullList(props: ConnectorListProps) {
    const { chargeStation } = props;

    const rowsPerPageOptions = [5, 10, 25];
    const [rowsPerPage, setRowsPerPage] = useState(rowsPerPageOptions[0]);

    const [page, setPage] = useState(0);

    const handleChangePage = (_: React.MouseEvent<HTMLButtonElement> | null, newPage: number) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement>) => {
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(0);
    };

    const [open, setOpen] = React.useState(false);
    const handleClickOpen = () => {
        setOpen(true);
    };
    const handleClose = () => {
        setOpen(false);
    };
    // console.log(JSON.stringify(chargeStation));

    return (
        <div>
            <Tooltip title="หัวชาร์จ" placement="top">
                <IconButton aria-label="edit" onClick={handleClickOpen}>
                    <GroupWorkOutlinedIcon />
                </IconButton>
            </Tooltip>

            <Dialog
                fullScreen
                open={open}
                onClose={handleClose}
                TransitionComponent={Transition}
            >
                <AppBar sx={{ position: 'relative' }} className='bg-[#56C3CD]'>
                    <Toolbar>

                        <Typography sx={{ ml: 2, flex: 1 }} variant="h6" component="div">
                            Connector Info
                        </Typography>
                        <IconButton
                            edge="start"
                            color="inherit"
                            onClick={handleClose}
                            aria-label="close"
                        >
                            <CloseIcon />
                        </IconButton>
                    </Toolbar>
                    {/* <AddConnectorPopupDialog chargeStationId={chargeStation._id} /> */}
                </AppBar>
                {/* {(JSON.stringify(chargeStation))} */}

                <Box sx={{ width: '100%', p: 10 }}>
                    <Paper sx={{ width: '100%', mb: 2 }} >
                        <AddConnectorPopupDialog chargeStation={chargeStation} />
                        <TableContainer >
                            <Table sx={{ minWidth: 650 }} size="small" aria-label="a dense table">
                                <TableHead className='bg-[#D9D9D940]'>
                                    <TableRow >
                                        <TableCell align="center" className='text-sm font-semibold  text-black'>ชื่อหัวชาร์จ</TableCell>
                                        <TableCell align="center" className='text-sm font-semibold  text-black'>คำอธิบาย</TableCell>
                                        <TableCell align="center" className='text-sm font-semibold  text-black'>Format</TableCell>
                                        <TableCell align="center" className='text-sm font-semibold  text-black'>Type</TableCell>
                                        <TableCell align="center" className='text-sm font-semibold  text-black'>Power Type</TableCell>
                                        <TableCell align="center" className='text-sm font-semibold  text-black'>Power</TableCell>
                                        <TableCell align="center" className='text-sm font-semibold  text-black'>Rate</TableCell>
                                        <TableCell align="center" className='text-sm font-semibold  text-black'>แก้ไข</TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {(rowsPerPage > 0
                                        ? chargeStation?.Connector.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                                        : chargeStation?.Connector
                                    ).map((connector: any, index: number) => {
                                        return (
                                            <TableRow key={index}>
                                                <TableCell align="center"  >{connector.name}</TableCell>
                                                <TableCell align="center"  >{connector.description}</TableCell>
                                                <TableCell align="center"  >{connector.format}</TableCell>
                                                <TableCell align="center"  >{connector.type}</TableCell>
                                                <TableCell align="center"  >{connector.power_type}</TableCell>
                                                <TableCell align="center"  >{connector.power}</TableCell>
                                                <TableCell >
                                                    {connector?.Rate?.priceComponent?.map((priceComponents: any, index: number) => (
                                                        <div key={index} className='grid grid-cols-1 gap-1 justify-start border border-gray-300 rounded'>
                                                            <div className='flex'>
                                                                <div className='grow '>Type : {priceComponents?.type}</div>
                                                                <div className='text-[#56C3CD] font-bold pr-1'>{connector?.Rate?.currency}</div>
                                                            </div>
                                                            <div>Price : {priceComponents?.price}</div>
                                                            <div>Tax % : {priceComponents?.tax}</div>
                                                            <div>Grace Period : {priceComponents?.grace_period}</div>
                                                        </div>
                                                    ))}
                                                </TableCell>
                                                <TableCell align="center" >
                                                    <div className='flex justify-center'>
                                                        <UpdateConnectorPopupDialog
                                                            connectorId={connector.connectorEdrvId}
                                                            formatProps={connector.format}
                                                            typeProps={connector.type}
                                                            powerTypeProps={connector.power_type}
                                                            powerProps={connector.power}
                                                            rateIdProps={connector?.Rate?.rateEdrvId}
                                                            nameConnector={connector?.name}
                                                        />
                                                        <ConnectorDeletePopupDialog connectorId={connector.connectorEdrvId} connectorName={connector?.name} />
                                                    </div>
                                                </TableCell>
                                            </TableRow>
                                        )
                                    })}
                                </TableBody>
                            </Table>
                            <TablePagination
                                rowsPerPageOptions={rowsPerPageOptions}
                                component="div"
                                count={chargeStation?.Connector.length || 0}
                                rowsPerPage={rowsPerPage}
                                page={page}
                                onPageChange={handleChangePage}
                                onRowsPerPageChange={handleChangeRowsPerPage}
                            />
                        </TableContainer>
                    </Paper>
                </Box>
            </Dialog>
        </div>
    )
}

