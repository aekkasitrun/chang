import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import React from "react";
import { useGetAllChargeStationServiceQuery } from '@/services/chargestation.service';
import ChargeStationDeletePopupDialog from '../PopupDialog/DeletePopup';
import Box from '@mui/material/Box';
import LinearProgress from '@mui/material/LinearProgress';
import UpdateChargeStationPopupDialog from '../PopupDialog/UpdateChargeStationPopup';


import Alert from '@mui/material/Alert';
import AlertTitle from '@mui/material/AlertTitle';
import Stack from '@mui/material/Stack';
import ConnectorList from '../Switch/ConnectorList';
import ChargeStationUpdateSwitch from '../Switch/updateChargeStationSwitch';
import ConnectorFullList from './ConnectorFullList';
import GroupWorkOutlinedIcon from '@mui/icons-material/GroupWorkOutlined';
import GetChargeStationLocation from '../GetChargeStationLocation';
import AlertPopup from '../Alert/SwitchConnectorAlert';
import TablePagination, { TablePaginationProps } from '@mui/material/TablePagination';
import { useState } from 'react';
import GetChargeStationOperatorName from '../GetChargeStationOperatorName';
import { useGetAllLocationPrismaServiceQuery } from '@/services/location.service';

import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';

const ChargeStationList = () => {

    // const chargeStationQuery = useGetAllChargeStationServiceQuery()
    const chargeStationQuery = useGetAllLocationPrismaServiceQuery()


    const rowsPerPageOptions = [5, 10, 25];
    const [rowsPerPage, setRowsPerPage] = useState(rowsPerPageOptions[0]);

    const [page, setPage] = useState(0);

    const handleChangePage = (_: React.MouseEvent<HTMLButtonElement> | null, newPage: number) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement>) => {
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(0);
    };

    if (chargeStationQuery.isLoading) return <div>
        <Box sx={{ width: '100%' }}>
            <LinearProgress />
        </Box>
    </div>
    if (chargeStationQuery.isError) return <div>
        <Stack sx={{ width: '100%' }} spacing={2}>
            <Alert severity="error">
                <AlertTitle>Error</AlertTitle>
                Data Fetching failed — <strong>check it out!</strong>
            </Alert>
        </Stack>
    </div>
    // console.log(JSON.stringify(chargeStationQuery));

    return (

        <TableContainer component={Paper}>
            {/* <div>
                {JSON.stringify(chargeStationQuery)}
            </div> */}

            <Toolbar className='bg-[#56C3CD] rounded-t-lg '>
                <Typography
                    sx={{ flex: '1 1 100%' }}
                    variant="h6"
                    className='text-2xl font-bold text-white'
                >
                    สถานีชาร์จ
                </Typography>
            </Toolbar>

            <Table sx={{ minWidth: 650 }} size="small" aria-label="a dense table">
                <TableHead className='bg-[#1890FF1A]'>
                    <TableRow>
                        {/* <TableCell align="center">ชื่อสถานีชาร์จ</TableCell> */}
                        {/* <TableCell align="center">ที่อยู่</TableCell> */}
                        <TableCell align="center" className='text-base font-semibold text-black'>ตู้ชาร์จ</TableCell>
                        <TableCell align="center" className='text-base font-semibold text-black'>คำอธิบาย</TableCell>
                        <TableCell align="center" className='text-base font-semibold text-black'>โปรโตคอล</TableCell>
                        {/* <TableCell align="center">พิกัด(ละติจูด/ลองติจูด)</TableCell> */}
                        <TableCell align="center" className='text-base font-semibold text-black'>สถานะ เปิด/ปิด</TableCell>
                        <TableCell align="center" className='text-base font-semibold text-black'>
                            <div>
                                สถานะหัวชาร์จ
                                <div className='flex justify-center gap-1'>
                                    <div className='flex'><GroupWorkOutlinedIcon className="text-red-500" /> : OFF</div>
                                    <div className='flex'><GroupWorkOutlinedIcon className="text-green-500" /> : ON</div>
                                </div>
                            </div>
                        </TableCell>
                        <TableCell align="center" className='text-base font-semibold text-black'>แก้ไข</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {(rowsPerPage > 0
                        ? chargeStationQuery?.data.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                        : chargeStationQuery?.data
                    ).reverse().map((chargestation: any) => {
                        // console.log(chargestation.location);
                        return (

                            <GetChargeStationLocation key={chargestation.locationEdrvId} chargeStationProps={chargestation} />

                            // <TableRow key={chargestation._id}>
                            //     {/* <GetChargeStationOperatorName chargeStationLocationId={chargestation.location} /> */}
                            //     {/* <GetChargeStationLocation chargeStationProps={chargestation} /> */}
                            //     <TableCell align="center" sx={{ borderRight: 1 }} >none</TableCell>
                            //     <TableCell align="center" sx={{ borderRight: 1 }} >none</TableCell>
                            //     <TableCell align="center" sx={{ borderRight: 1 }} >{chargestation.name}</TableCell>
                            //     <TableCell align="center" sx={{ borderRight: 1 }}>{chargestation.description}</TableCell>
                            //     <TableCell align="center" sx={{ borderRight: 1 }}>{chargestation.protocol}</TableCell>


                            //     {/* <TableCell align="center" sx={{ borderRight: 1 }}>
                            //     <div className='grid gap-1 grid-cols-2'>
                            //         <p className='truncate ...'>{chargestation.coordinates[1]}</p>
                            //         <p className='truncate ...'>{chargestation.coordinates[0]}</p>
                            //     </div>
                            // </TableCell>  */}


                            //     <TableCell align="center" sx={{ borderRight: 1 }}>
                            //         <ChargeStationUpdateSwitch chargeStationId={chargestation._id} chargeStationPublic={chargestation.public} />
                            //     </TableCell>
                            //     <TableCell sx={{ borderRight: 1 }}>
                            //         <ConnectorList chargeStation={chargestation} />
                            //     </TableCell>
                            //     <TableCell align="center" sx={{ borderRight: 1 }}>
                            //         <div className='flex justify-center'>


                            //             {/* <AddConnectorPopupDialog chargeStationId={chargestation._id} />  */}


                            //             <ConnectorFullList chargeStation={chargestation} />
                            //             <UpdateChargeStationPopupDialog
                            //                 chargeStationId={chargestation._id}
                            //                 locationId={chargestation.location}
                            //                 changeStationName={chargestation?.name}
                            //                 protocolProps={chargestation?.protocol}
                            //             />
                            //             <ChargeStationDeletePopupDialog chargeStationId={chargestation._id} chargeStationName={chargestation?.name} />




                            //             {/* {JSON.stringify(chargestation.location)}  */}
                            //         </div>
                            //     </TableCell>
                            // </TableRow>
                        )
                    })}
                </TableBody>
            </Table>
            <TablePagination
                rowsPerPageOptions={rowsPerPageOptions}
                component="div"
                count={chargeStationQuery?.data.length || 0}
                rowsPerPage={rowsPerPage}
                page={page}
                onPageChange={handleChangePage}
                onRowsPerPageChange={handleChangeRowsPerPage}
            />
        </TableContainer>
    )

}

export default ChargeStationList;