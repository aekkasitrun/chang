export interface AddConnectorDTO {
  id: string;
  name: string;
  description: string;
  active: boolean;
  rate: string;
  format: string;
  type: string;
  power_type: string;
  power: number;
}
