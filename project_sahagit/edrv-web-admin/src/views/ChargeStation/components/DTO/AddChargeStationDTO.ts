export interface AddChargeStationDTO {
  name: string;
  description: string;
  location: string;
  protocol: string;
  public: boolean;
  // latitude: number;
  // longitude: number;
  // coordinates: [number, number];
  // coordinates: Coordinates;
  is_simulator: boolean;
}
