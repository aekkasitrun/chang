export interface UpdateChargeStationDTO {
  id: string;
  locationId: string;
  protocol: string;
}

export interface UpdateSwitchDTO {
  id: string;
  public: boolean;
}
