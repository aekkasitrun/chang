export interface UpdateConnectorSwitchDTO {
  id: string;
  active: boolean;
}

export interface UpdateConnectorDTO {
  id: string;
  rateId: string;
  format: string;
  type: string;
  power_type: string;
  power: number;
}
