import * as React from 'react';
import Button from "@/components/Buttons/Button";
import { FormikHandlers, FormikValues } from "formik";
// import React from "react";
import TextField from '@mui/material/TextField';
import { Input } from "@/components/Form";
import MenuItem from '@mui/material/MenuItem';
import Box from '@mui/material/Box';
import CircularProgress from '@mui/material/CircularProgress';
import { useGetAllRateServiceQuery } from '@/services/rate.service';
import { UpdateConnectorDTO } from '../DTO/UpdateConnectorDTO';
import { formatConnector, powerTypeConnector, typeConnector } from "../MenuListDropdownItem";
import Alert from '@mui/material/Alert';
import Stack from '@mui/material/Stack';

interface ChargeStationInputProps {
    handleSubmit: FormikHandlers["handleSubmit"];
    handleChange: FormikHandlers["handleChange"];
    values: UpdateConnectorDTO;
    // operatorName?: string;
}

const ConnectorInputUpdate: React.FC<ChargeStationInputProps> = ({
    handleChange,
    handleSubmit,
    values,
    // operatorName,
}) => {

    const rateQuery = useGetAllRateServiceQuery()

    if (rateQuery.isLoading) return (
        <div>
            <Box sx={{ display: 'flex' }}>
                <CircularProgress />
            </Box>
        </div>)
    if (rateQuery.isError) return <div>
        <Stack sx={{ width: '100%' }} spacing={2}>
            <Alert severity="error">
                Error Data Fetching failed — <strong>check it out!</strong>
            </Alert>
        </Stack>
    </div>

    return (
        <div>
            <form onSubmit={handleSubmit}>
                <div>
                    <Input
                        sx={{ m: 1, width: '60ch' }}
                        select
                        name="rateId"
                        // placeholder="Type your protocol here."
                        label="Rate"
                        onChange={handleChange}
                        value={values.rateId}
                    // defaultValue="operatorName"
                    >
                        {rateQuery?.data?.result.map((rate: any) => (
                            <MenuItem key={rate._id} value={rate._id}>
                                <div className='w-full'>
                                    {rate?.price_components.map((priceComponents: any, index: number) => (
                                        <div key={index} className='grid grid-cols-1 gap-1 justify-start border border-indigo-600'>
                                            <div className='flex'>
                                                <div className='grow '>Type : {priceComponents?.type}</div>
                                                <div >{rate?.currency}</div>
                                            </div>
                                            <div>Price : {priceComponents?.price}</div>
                                            <div>Tax % : {priceComponents?.tax}</div>
                                            <div>Grace Period : {priceComponents?.grace_period}</div>
                                        </div>
                                    ))}
                                </div>
                            </MenuItem>
                        ))}
                    </Input>

                    <Input
                        sx={{ m: 1, width: '25ch' }}
                        select
                        name="format"
                        label="Format"
                        onChange={handleChange}
                        value={values.format}
                    >
                        {formatConnector.map((formatMap) => (
                            <MenuItem key={formatMap.name} value={formatMap.format}>
                                {formatMap.name}
                            </MenuItem>
                        ))}
                    </Input>

                    <Input
                        sx={{ m: 1, width: '25ch' }}
                        select
                        name="type"
                        label="Type"
                        onChange={handleChange}
                        value={values.type}
                    >
                        {typeConnector.map((typeMap) => (
                            <MenuItem key={typeMap.name} value={typeMap.type}>
                                {typeMap.name}
                            </MenuItem>
                        ))}
                    </Input>

                    <Input
                        sx={{ m: 1, width: '25ch' }}
                        select
                        name="power_type"
                        label="Powertype"
                        onChange={handleChange}
                        value={values.power_type}
                    >
                        {powerTypeConnector.map((powerTypeMap) => (
                            <MenuItem key={powerTypeMap.name} value={powerTypeMap.power_type}>
                                {powerTypeMap.name}
                            </MenuItem>
                        ))}
                    </Input>

                    <Input
                        sx={{ m: 1, width: '25ch' }}
                        name="power"
                        placeholder="Type your name here."
                        label="power"
                        onChange={handleChange}
                        value={values.power}
                    />

                </div>
                <br />
                <div className="flex justify-center">
                    <Button
                        className="h-full"
                        size="small"
                        variant="contained"
                        color="primary"
                        type="submit"
                    >
                        Submit
                    </Button>
                </div>
            </form>
        </div>
    )
}

export default ConnectorInputUpdate;