import * as React from 'react';
import Button from "@/components/Buttons/Button";
import { FormikHandlers, FormikValues } from "formik";
// import React from "react";
import TextField from '@mui/material/TextField';
import { Input } from "@/components/Form";
import MenuItem from '@mui/material/MenuItem';
import { UpdateChargeStationDTO } from '../DTO/UpdateChargeStationDTO';
import Box from '@mui/material/Box';
import CircularProgress from '@mui/material/CircularProgress';
import { useGetAllLocationServiceQuery } from "@/services/location.service";
import { protocolChargeStation } from "../MenuListDropdownItem";


interface ChargeStationInputProps {
    handleSubmit: FormikHandlers["handleSubmit"];
    handleChange: FormikHandlers["handleChange"];
    values: UpdateChargeStationDTO;
    // operatorName?: string;
}

const ChargeStationInputUpdate: React.FC<ChargeStationInputProps> = ({
    handleChange,
    handleSubmit,
    values,
    // operatorName,
}) => {

    const locationQuery = useGetAllLocationServiceQuery()

    if (locationQuery.isLoading) return (
        <div>
            <Box sx={{ display: 'flex' }}>
                <CircularProgress />
            </Box>
        </div>)
    if (locationQuery.isError) return <div>Error...</div>

    return (
        <div>
            <form onSubmit={handleSubmit}>
                <div>
                    <Input
                        sx={{ m: 1, width: '60ch' }}
                        select
                        name="locationId"
                        placeholder="Type your protocol here."
                        label="Location"
                        onChange={handleChange}
                        value={values.locationId}
                    // defaultValue="operatorName"
                    >
                        {locationQuery?.data?.result.map((location: any) => (
                            <MenuItem key={location._id} value={location._id}>
                                {location.address.streetAndNumber + " " + location.address.city + " " + location.address.state + " " + location.address.country + " " + location.address.postalCode}
                            </MenuItem>
                        ))}
                    </Input>

                    <Input
                        sx={{ m: 1, width: '60ch' }}
                        select
                        name="protocol"
                        placeholder="Type your protocol here."
                        label="Protocol"
                        onChange={handleChange}
                        value={values.protocol}
                    >
                        {protocolChargeStation.map((protocolMap) => (
                            <MenuItem key={protocolMap.name} value={protocolMap.protocol}>
                                {protocolMap.name}
                            </MenuItem>
                        ))}
                    </Input>

                </div>
                <br />
                <div className="flex justify-center">
                    <Button
                        className="h-full"
                        size="small"
                        variant="contained"
                        color="primary"
                        type="submit"
                    >
                        Submit
                    </Button>
                </div>
            </form>
        </div>
    )
}

export default ChargeStationInputUpdate;