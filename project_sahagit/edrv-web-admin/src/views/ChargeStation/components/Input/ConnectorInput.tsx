import Button from "@/components/Buttons/Button";
import { FormikErrors, FormikHandlers, FormikValues } from "formik";
import React from "react";
import TextField from '@mui/material/TextField';
import MenuItem from '@mui/material/MenuItem';
import { AddConnectorDTO } from "../DTO/AddConnectorDTO";
import { Divider } from '@mui/material';
import { Input } from "@/components/Form";
import { formatConnector, powerTypeConnector, typeConnector } from "../MenuListDropdownItem";
import Box from '@mui/material/Box';
import CircularProgress from '@mui/material/CircularProgress';
import { useGetAllRateServiceQuery } from "@/services/rate.service";

import Alert from '@mui/material/Alert';
import AlertTitle from '@mui/material/AlertTitle';
import Stack from '@mui/material/Stack';

interface ConnectorInputProps {
    handleSubmit: FormikHandlers["handleSubmit"];
    handleChange: FormikHandlers["handleChange"];
    values: AddConnectorDTO;
    errors: FormikErrors<AddConnectorDTO>;
}

const ConnectorInput: React.FC<ConnectorInputProps> = ({
    handleChange,
    handleSubmit,
    values,
    errors,
}) => {

    const rateQuery = useGetAllRateServiceQuery()

    if (rateQuery.isLoading) return (
        <div>
            <Box sx={{ display: 'flex' }}>
                <CircularProgress />
            </Box>
        </div>)
    if (rateQuery.isError) return <div>
        <Stack sx={{ width: '100%' }} spacing={2}>
            <Alert severity="error">
                Error Data Fetching failed — <strong>check it out!</strong>
            </Alert>
        </Stack>
    </div>
    console.log('ErrorConnector: ' + JSON.stringify(errors));
    return (
        <div>
            <form onSubmit={handleSubmit}>
                <div className="grid grid-cols-2 gap-2 justify-center">
                    <Input
                        sx={{ m: 1, width: '25ch' }}
                        name="name"
                        placeholder="Type your name here."
                        label="name"
                        onChange={handleChange}
                        value={values.name}
                        error={!!errors.name}
                    // error={typeof errors.name !== undefined}
                    />
                    <Input
                        sx={{ m: 1, width: '25ch' }}
                        name="description"
                        placeholder="Type your name here."
                        label="description"
                        onChange={handleChange}
                        value={values.description}
                        error={!!errors.description}
                    />
                    {/* <Input
                        sx={{ m: 1, width: '25ch' }}
                        name="rate"
                        placeholder="Type your name here."
                        label="rateId"
                        onChange={handleChange}
                        value={values.rate}
                    /> */}

                    <Input
                        sx={{ m: 1, width: '25ch' }}
                        select
                        name="rate"
                        // placeholder="Type your protocol here."
                        label="Rate"
                        onChange={handleChange}
                        value={values.rate}
                        error={!!errors.rate}
                    >
                        {rateQuery?.data?.result.map((rate: any) => (
                            <MenuItem key={rate._id} value={rate._id}>
                                {rate?.description}
                            </MenuItem>
                        ))}
                    </Input>

                </div>
                <Divider variant="middle" sx={{ m: 3 }} />
                <div className="grid grid-cols-2 gap-2 justify-center">
                    <Input
                        sx={{ m: 1, width: '25ch' }}
                        select
                        name="format"
                        label="Format"
                        onChange={handleChange}
                        value={values.format}
                        error={!!errors.format}
                    >
                        {formatConnector.map((formatMap) => (
                            <MenuItem key={formatMap.name} value={formatMap.format}>
                                {formatMap.name}
                            </MenuItem>
                        ))}
                    </Input>

                    <Input
                        sx={{ m: 1, width: '25ch' }}
                        select
                        name="type"
                        label="Type"
                        onChange={handleChange}
                        value={values.type}
                        error={!!errors.type}
                    >
                        {typeConnector.map((typeMap) => (
                            <MenuItem key={typeMap.name} value={typeMap.type}>
                                {typeMap.name}
                            </MenuItem>
                        ))}
                    </Input>

                    <Input
                        sx={{ m: 1, width: '25ch' }}
                        select
                        name="power_type"
                        label="Powertype"
                        onChange={handleChange}
                        value={values.power_type}
                        error={!!errors.power_type}
                    >
                        {powerTypeConnector.map((powerTypeMap) => (
                            <MenuItem key={powerTypeMap.name} value={powerTypeMap.power_type}>
                                {powerTypeMap.name}
                            </MenuItem>
                        ))}
                    </Input>

                    <Input
                        sx={{ m: 1, width: '25ch' }}
                        name="power"
                        placeholder="Type your name here."
                        label="power"
                        onChange={handleChange}
                        value={values.power}
                        error={!!errors.power}
                    />
                </div>
                <br />
                <div className="flex justify-center">

                    <Button
                        variant="contained"
                        color="primary"
                        type="submit"
                    >
                        Add Connector
                    </Button>
                </div>


            </form>
        </div>
    )
}

export default ConnectorInput;