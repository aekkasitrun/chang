import Button from "@/components/Buttons/Button";
import { FormikHandlers, FormikValues } from "formik";
import React from "react";
import { AddChargeStationDTO } from "../DTO/AddChargeStationDTO";
import MenuItem from '@mui/material/MenuItem';
import { protocolChargeStation } from "../MenuListDropdownItem";
import { Input } from "@/components/Form";
import { useGetAllLocationServiceQuery } from "@/services/location.service";
import Box from '@mui/material/Box';
import CircularProgress from '@mui/material/CircularProgress';
import Alert from '@mui/material/Alert';
import Stack from '@mui/material/Stack';
import ConnectorAdvanceInput from "./ConnectorAdvanceInput";
import { Divider } from '@mui/material';


interface ChargeStationInputProps {
    handleSubmit: FormikHandlers["handleSubmit"];
    handleChange: FormikHandlers["handleChange"];
    values: AddChargeStationDTO;
}

const ChargeStationAdvanceInput: React.FC<ChargeStationInputProps> = ({
    handleChange,
    handleSubmit,
    values,
}) => {

    const locationQuery = useGetAllLocationServiceQuery()
    if (locationQuery.isLoading) return (
        <div>
            <Box sx={{ display: 'flex' }}>
                <CircularProgress />
            </Box>
        </div>)
    if (locationQuery.isError) return <div>
        <Stack sx={{ width: '100%' }} spacing={2}>
            <Alert severity="error">
                Error Data Fetching failed — <strong>check it out!</strong>
            </Alert>
        </Stack>
    </div>

    return (

        <div >
            <form onSubmit={handleSubmit}>
                <div className="grid grid-cols-2 gap-2 justify-center">
                    <Input
                        sx={{ m: 1, width: '25ch' }}
                        name="name"
                        placeholder="Type your name here."
                        label="ชื่อสถานีชาร์จ"
                        onChange={handleChange}
                        value={values.name}
                    />

                    <Input
                        sx={{ m: 1, width: '25ch' }}
                        name="description"
                        placeholder="Type your description here."
                        label="คำอธิบายเกี่ยวกับสถานีชาร์จ"
                        onChange={handleChange}
                        value={values.description}
                    />

                    <Input
                        sx={{ m: 1, width: '25ch' }}
                        select
                        name="location"
                        placeholder="Type your protocol here."
                        label="Location"
                        onChange={handleChange}
                        value={values.location}
                    >
                        {locationQuery?.data?.result.map((location: any) => (
                            <MenuItem key={location._id} value={location._id}>
                                {location.address.streetAndNumber + " " + location.address.city + " " + location.address.state + " " + location.address.country + " " + location.address.postalCode}
                            </MenuItem>
                        ))}
                    </Input>

                    <Input
                        sx={{ m: 1, width: '25ch' }}
                        select
                        name="protocol"
                        placeholder="Type your protocol here."
                        label="Protocol"
                        onChange={handleChange}
                        value={values.protocol}
                    >
                        {protocolChargeStation.map((protocolMap) => (
                            <MenuItem key={protocolMap.name} value={protocolMap.protocol}>
                                {protocolMap.name}
                            </MenuItem>
                        ))}
                    </Input>

                </div>
                <Divider variant="middle" sx={{ m: 3 }} />
                <ConnectorAdvanceInput
                    handleChange={handleChange}
                    values={values}
                    handleSubmit={handleSubmit}
                />

                <br />
                <div className="flex justify-center">
                    <Button
                        variant="contained"
                        color="primary"
                        type="submit"
                    >
                        เพิ่มสถานีชาร์จ
                    </Button>
                </div>

            </form >
        </div >
    )
}

export default ChargeStationAdvanceInput;
