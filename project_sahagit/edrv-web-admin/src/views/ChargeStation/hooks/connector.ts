import {
  useAddConnectorServiceMutation,
  useUpdateConnectorSwitchServiceMutation,
  useDeleteConnectorServiceMutation,
  useUpdateConnectorServiceMutation,
} from "@/services/connector.service";
import AlertPopup from "../components/Alert/SwitchConnectorAlert";
import { AddConnectorDTO } from "../components/DTO/AddConnectorDTO";
import { SnackBarType } from "@/components/SnackBar";
import React from "react";

interface CheckErrorAlert {
  message: string;
  type: SnackBarType;
}

const useConnector = () => {
  const [addConnectorService] = useAddConnectorServiceMutation();
  const [updateConnectorSwitchService] =
    useUpdateConnectorSwitchServiceMutation();
  const [deleteConnectorService] = useDeleteConnectorServiceMutation();
  const [updateConnectorService] = useUpdateConnectorServiceMutation();

  const [checkErrorAlert, setCheckErrorAlert] = React.useState<CheckErrorAlert>(
    {
      message: "",
      type: "success",
    }
  );

  async function handleAddConnector(data: AddConnectorDTO) {
    try {
      console.log("Connector = " + data);
      await addConnectorService(data).unwrap();
      setCheckErrorAlert({
        message: "Add Connector",
        type: "success",
      });
      console.log("Added connector");
    } catch (error) {
      console.log("Add Location fail");
      setCheckErrorAlert({
        message: "Add Connector fail",
        type: "error",
      });
    }
  }

  async function handleUpdateConnectorSwitch(data: any) {
    try {
      console.log("ConnectorId = " + data.id);
      console.log("ConnectorData = " + data.active);
      await updateConnectorSwitchService(data).unwrap();
      setCheckErrorAlert({
        message: "Connector Update Status",
        type: "success",
      });
      console.log("Updated Status");
    } catch (error) {
      console.log("Update Status fail");
      setCheckErrorAlert({
        message: "Update Status fail",
        type: "error",
      });
    }
  }

  async function handleDeleteConnector(id: string) {
    try {
      console.log("ConnectorId = " + id);
      await deleteConnectorService(id).unwrap();
      setCheckErrorAlert({
        message: "Connector Deleted",
        type: "success",
      });
      console.log("Deleted ");
    } catch (error) {
      console.log("Delete fail " + JSON.stringify(error));
      setCheckErrorAlert({
        message: "Delete fail",
        type: "error",
      });
    }
  }

  async function handleUpdateConnector(data: any) {
    try {
      console.log("ConnectorId = " + data.id);
      console.log("RateId = " + data.rateId);
      console.log("Format = " + data.format);
      console.log("Type = " + data.type);
      console.log("Power Type = " + data.power_type);
      console.log("Power = " + data.power);
      await updateConnectorService(data).unwrap();
      setCheckErrorAlert({
        message: "Update Connector",
        type: "success",
      });
      console.log("Updated ");
    } catch (error) {
      console.log("Update Location fail");
      setCheckErrorAlert({
        message: "Update Connector fail",
        type: "error",
      });
    }
  }

  return {
    handleAddConnector,
    handleUpdateConnectorSwitch,
    handleDeleteConnector,
    handleUpdateConnector,
    checkErrorAlert,
  };
};

export default useConnector;
