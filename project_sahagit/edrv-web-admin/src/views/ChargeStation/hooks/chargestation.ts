import {
  useDeleteChargeStationServiceMutation,
  useAddChargeStationServiceMutation,
  useUpdateChargeStationServiceMutation,
  useUpdateChargeStationSwitchServiceMutation,
  useAddAdvanceChargeStationServiceMutation,
} from "@/services/chargestation.service";
import { AddChargeStationDTO } from "../components/DTO/AddChargeStationDTO";
import { SnackBarType } from "@/components/SnackBar";
import React from "react";

interface CheckErrorAlert {
  message: string;
  type: SnackBarType;
}

const useChargeStation = () => {
  const [addChargeStationService] = useAddChargeStationServiceMutation();
  const [deleteChargeStationService] = useDeleteChargeStationServiceMutation();
  const [updateChargeStationService] = useUpdateChargeStationServiceMutation();
  const [updateChargeStationSwitchService] =
    useUpdateChargeStationSwitchServiceMutation();
  const [addAdvanceChargeStationService] =
    useAddAdvanceChargeStationServiceMutation();

  const [checkErrorAlert, setCheckErrorAlert] = React.useState<CheckErrorAlert>(
    {
      message: "",
      type: "success",
    }
  );

  async function handleDeleteChargeStation(id: string) {
    try {
      console.log("ChargeStationId = " + id);
      await deleteChargeStationService(id).unwrap();
      setCheckErrorAlert({
        message: "Charge Station Deleted",
        type: "success",
      });
      console.log("Deleted ");
    } catch (error) {
      console.log("Delete fail " + JSON.stringify(error));
      setCheckErrorAlert({
        message: "Delete fail",
        type: "error",
      });
    }
  }

  async function handleAddChargeStation(data: AddChargeStationDTO) {
    try {
      console.log("ChargeStationData = " + data);
      await addChargeStationService(data).unwrap();
      setCheckErrorAlert({
        message: "Add Charge Station",
        type: "success",
      });
      console.log("Added ");
    } catch (error) {
      console.log("Add Charge Station fail");
      setCheckErrorAlert({
        message: "Add Charge Station fail",
        type: "error",
      });
    }
  }

  async function handleAddAdvanceChargeStation(data: any) {
    console.log("ChargeStationAdvanceData = " + data);
    await addAdvanceChargeStationService(data);
    console.log("AddedAdvance ");
  }

  async function handleUpdateChargeStation(data: any) {
    try {
      console.log("ChargeStationId = " + data.id);
      console.log("LocationId = " + data.locationId);
      await updateChargeStationService(data).unwrap();
      setCheckErrorAlert({
        message: "Update Charge Station",
        type: "success",
      });
      console.log("Updated ");
    } catch (error) {
      console.log("Update Charge Station fail");
      setCheckErrorAlert({
        message: "Update Charge Station fail",
        type: "error",
      });
    }
  }

  async function handleUpdateChargeStationSwitch(data: any) {
    try {
      console.log("ChargeStationId = " + data.id);
      console.log("ChargeStationData = " + data.public);
      await updateChargeStationSwitchService(data).unwrap();
      setCheckErrorAlert({
        message: "Charge Station Update Status",
        type: "success",
      });
      console.log("Updated Status");
    } catch (error) {
      console.log("Update Status fail");
      setCheckErrorAlert({
        message: "Update Status fail",
        type: "error",
      });
    }
  }

  return {
    handleDeleteChargeStation,
    handleAddChargeStation,
    handleUpdateChargeStation,
    handleUpdateChargeStationSwitch,
    handleAddAdvanceChargeStation,
    checkErrorAlert,
  };
};

export default useChargeStation;
