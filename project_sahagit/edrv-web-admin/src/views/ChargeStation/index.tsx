import * as React from 'react';
import ChargeStationList from './components/List/ChargeStationList';
import ChargeStationPopupDialog from './components/PopupDialog/AddChargeStationPopup';
import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';
import ChargeStationAdvancePopupDialog from './components/PopupDialog/AdvanceAddChargeStation';
import AlertPopup from './components/Alert/SwitchConnectorAlert';



const ChargeStationView = () => {
    const [selected] = React.useState<readonly string[]>([]);

    return (
        <div >
            <Box >
                {/* <Paper sx={{ width: '100%', mb: 2 }}> */}
                <div className='pb-4'><ChargeStationPopupDialog numSelected={selected.length} /></div>
                {/* <ChargeStationAdvancePopupDialog numSelected={selected.length} /> */}
                <ChargeStationList />
                {/* </Paper> */}
            </Box>
        </div>

    )
}

export default ChargeStationView;
