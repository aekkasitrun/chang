import * as React from 'react';
import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';
import RateList from './components/RateList';
import RatePopupDialog from './components/PopupDialog/AddRatePopup';
import Divider from '@mui/material/Divider';

const Rates = () => {
    return (

        <div>
            <Box >
                {/* <Paper sx={{ width: '100%', mb: 2 }}> */}
                <RatePopupDialog />
                <div className='pb-8'><Divider /></div>
                <RateList />
                {/* </Paper> */}
            </Box>
        </div >
    )
}

export default Rates;
