import { useAddRateServiceMutation } from "@/services/rate.service";
import { SnackBarType } from "@/components/SnackBar";
import React from "react";

interface CheckErrorAlert {
  message: string;
  type: SnackBarType;
}

const useRate = () => {
  const [addRateService] = useAddRateServiceMutation();

  const [checkErrorAlert, setCheckErrorAlert] = React.useState<CheckErrorAlert>(
    {
      message: "",
      type: "success",
    }
  );

  async function handleAddRate(data: any) {
    try {
      console.log("RateData = " + data);
      await addRateService(data).unwrap();
      setCheckErrorAlert({
        message: "Add Rate",
        type: "success",
      });
      console.log("Added ");
    } catch (error) {
      console.log("Add Rate fail");
      setCheckErrorAlert({
        message: "Add Rate fail",
        type: "error",
      });
    }
  }

  return { handleAddRate, checkErrorAlert };
};

export default useRate;
