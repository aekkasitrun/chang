import Button from "@/components/Buttons/Button";
import { FormikErrors, FormikHandlers, FormikValues } from "formik";
import React from "react";
import { AddRateDTO } from "../DTO/AddRateDTO";
import MenuItem from '@mui/material/MenuItem';
import { Input } from "@/components/Form";
import { typePriceComponent } from "./MenuListDropdownItem";

interface RateInputProps {
    handleSubmit: FormikHandlers["handleSubmit"];
    handleChange: FormikHandlers["handleChange"];
    values: AddRateDTO;
    errors: FormikErrors<AddRateDTO>;
}

const RateInput: React.FC<RateInputProps> = ({
    handleChange,
    handleSubmit,
    values,
    errors,
}) => {

    // console.log('ErrorRate: ' + JSON.stringify(errors));

    return (
        <div>
            <form onSubmit={handleSubmit}>

                <div className="flex justify-center">
                    <Input
                        sx={{ m: 1, width: '25ch' }}
                        name="currency"
                        placeholder="Type your currency here."
                        label="Currency"
                        onChange={handleChange}
                        value={values.currency}
                        error={!!errors.currency}
                    />

                    <Input
                        sx={{ m: 1, width: '25ch' }}
                        name="description"
                        placeholder="Type your description here."
                        label="Description"
                        onChange={handleChange}
                        value={values.description}
                        error={!!errors.description}
                    />
                </div>
                <div className="flex justify-center">
                    <Input
                        sx={{ m: 1, width: '25ch' }}
                        select
                        name="price_components[0].type"
                        // placeholder="Type your protocol here."
                        label="Type"
                        onChange={handleChange}
                        value={values.price_components[0].type}
                        error={!!errors.price_components}
                    >
                        {typePriceComponent.map((typeMap) => (
                            <MenuItem key={typeMap.name} value={typeMap.price_components[0].type}>
                                {typeMap.name}
                            </MenuItem>
                        ))}
                    </Input>

                    <Input
                        sx={{ m: 1, width: '25ch' }}
                        name="price_components[0].price"
                        placeholder="Type your price here."
                        label="Price"
                        onChange={handleChange}
                        value={values.price_components[0].price}
                        error={!!errors.price_components}
                    />
                </div>
                <Input
                    sx={{ m: 1, width: '25ch' }}
                    name="price_components[0].tax"
                    placeholder="Type your tax here."
                    label="Tax %"
                    onChange={handleChange}
                    value={values.price_components[0].tax}
                    error={!!errors.price_components}
                />

                <Input
                    sx={{ m: 1, width: '25ch' }}
                    name="price_components[0].grace_period"
                    placeholder="Type your grace period here."
                    label="Grace Period"
                    onChange={handleChange}
                    value={values.price_components[0].grace_period}
                    error={!!errors.price_components}
                />
                <div>
                    <div className="flex justify-center">
                        <Button
                            className="h-full"
                            size="small"
                            variant="contained"
                            color="primary"
                            type="submit"
                        >
                            Submit
                        </Button>
                    </div>
                </div>

            </form>
        </div>
    )
}

export default RateInput;