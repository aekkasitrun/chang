export const typePriceComponent = [
  {
    name: "FLAT",
    price_components: [
      {
        type: "FLAT",
      },
    ],
  },
  {
    name: "ENERGY",
    price_components: [
      {
        type: "ENERGY",
      },
    ],
  },
  {
    name: "TIME",
    price_components: [
      {
        type: "TIME",
      },
    ],
  },
  {
    name: "PARKING_TIME",
    price_components: [
      {
        type: "PARKING_TIME",
      },
    ],
  },
];
