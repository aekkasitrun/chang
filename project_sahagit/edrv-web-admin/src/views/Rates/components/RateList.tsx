import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import React from "react";
import { useGetAllRateServiceQuery } from '@/services/rate.service';
import MenuItem from '@mui/material/MenuItem';
import Box from '@mui/material/Box';
import LinearProgress from '@mui/material/LinearProgress';
import Switch from '@mui/material/Switch';
import { Input } from '@/components/Form';
import { useState } from 'react';

import Alert from '@mui/material/Alert';
import AlertTitle from '@mui/material/AlertTitle';
import Stack from '@mui/material/Stack';
import CountConnector from './CountConnector';
import TablePagination, { TablePaginationProps } from '@mui/material/TablePagination';



const RateList = () => {

    const rateQuery = useGetAllRateServiceQuery()

    const rowsPerPageOptions = [5, 10, 25];
    const [rowsPerPage, setRowsPerPage] = useState(rowsPerPageOptions[0]);

    const [page, setPage] = useState(0);

    const handleChangePage = (_: React.MouseEvent<HTMLButtonElement> | null, newPage: number) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement>) => {
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(0);
    };

    if (rateQuery.isLoading) return <div>
        <Box sx={{ width: '100%' }}>
            <LinearProgress />
        </Box>
    </div>
    if (rateQuery.isError) return <div>
        <Stack sx={{ width: '100%' }} spacing={2}>
            <Alert severity="error">
                <AlertTitle>Error</AlertTitle>
                Data Fetching failed — <strong>check it out!</strong>
            </Alert>
        </Stack>
    </div>


    return (
        <TableContainer component={Paper}>
            {/* <div>
            {JSON.stringify(chargeStationQuery)}
        </div> */}
            <Table sx={{ minWidth: 650 }} size="small" aria-label="a dense table">
                <TableHead>
                    <TableRow className='bg-[#D9D9D940]'>
                        <TableCell align="center" className='text-base font-semibold text-black'>Currency</TableCell>
                        <TableCell align="center" className='text-base font-semibold text-black'>Description</TableCell>
                        <TableCell align="center" className='text-base font-semibold text-black'>Price Components	</TableCell>
                        {/* <TableCell align="center">Active</TableCell> */}
                        <TableCell align="center" className='text-base font-semibold text-black'>Connectors</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {(rowsPerPage > 0
                        ? rateQuery.data.result.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                        : rateQuery.data.result
                    ).map((rate: any) => {
                        // console.log(chargestation);

                        return (
                            <TableRow key={rate._id}>
                                <TableCell align="center" className='text-[#0DA9B8] font-extrabold '>{rate.currency}</TableCell>
                                <TableCell align="center" >{rate.description}</TableCell>
                                <TableCell >
                                    {rate?.price_components.map((priceComponents: any, index: number) => (
                                        <div key={index} className='grid grid-cols-1 gap-1 justify-start border border-gray-300 rounded'>
                                            <div>Type : {priceComponents?.type}</div>
                                            <div>Price : {priceComponents?.price}</div>
                                            <div>Tax % : {priceComponents?.tax}</div>
                                            <div>Grace Period : {priceComponents?.grace_period}</div>
                                        </div>
                                    ))}
                                </TableCell>
                                <TableCell align="center"  >
                                    <CountConnector rateId={rate._id} />
                                </TableCell>
                            </TableRow>
                        )
                    })}
                </TableBody>
            </Table>
            <TablePagination
                rowsPerPageOptions={rowsPerPageOptions}
                component="div"
                count={rateQuery?.data?.result.length || 0}
                rowsPerPage={rowsPerPage}
                page={page}
                onPageChange={handleChangePage}
                onRowsPerPageChange={handleChangeRowsPerPage}
            />
        </TableContainer>
    )
}

export default RateList;