import * as React from 'react';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Tooltip from '@mui/material/Tooltip';
import Button from '@mui/material/Button';
import AddIcon from '@mui/icons-material/Add';

import Dialog from '@mui/material/Dialog';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';

import * as Yup from "yup";
import { FormikHelpers, useFormik } from "formik";
import { AddRateDTO } from '../../DTO/AddRateDTO';
import ClearRoundedIcon from '@mui/icons-material/ClearRounded';
import useRate from '../../hooks/rate';
import RateInput from '../RateInput';
import MuiSnackbar from '@/components/SnackBar';

const initChargeStation: AddRateDTO = {
    currency: "",
    description: "",
    price_components: [{
        type: "",
        price: 0,
        tax: 0,
        grace_period: 0,
    }]
};

const validateSchema = Yup.object().shape({
    currency: Yup.string().required("Currency cannot be empty."),
    description: Yup.string().required("Description cannot be empty."),
    price_components: Yup.array().of(
        Yup.object().shape({
            type: Yup.string().required("Type cannot be empty."),
            price: Yup.number().required("Price cannot be empty."),
            tax: Yup.number().required("Tax cannot be empty."),
            grace_period: Yup.number().required("Grace period cannot be empty."),
        })
    ).required("Price components cannot be empty.")
    //ที่ add location กับ folder hooks ไม่ได้มีปัญหาตรง yup น่าจะต้อง check ข้อมูลตาม DTO ให้หมดซึ่งตอนนี้ยัง error อยู่

});

const RatePopupDialog = () => {
    const { handleAddRate, checkErrorAlert } = useRate();

    const [alert, setAlert] = React.useState(false);

    const handleResetAlert = () => {
        setAlert(false)
    }

    const { values, handleChange, handleSubmit, errors } = useFormik({
        initialValues: initChargeStation,
        validationSchema: validateSchema,
        validateOnChange: true, // if true the form will validate the value every time the onChange is triggered
        onSubmit: handleSubmitLocation,

    });
    async function handleSubmitLocation(value: AddRateDTO, formikHelper: FormikHelpers<AddRateDTO>) {
        console.log(value);
        await handleAddRate(value)
        formikHelper.resetForm({ values: initChargeStation });
        setAlert(true)
    }

    const [open, setOpen] = React.useState(false);

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    return (
        <div>
            <Toolbar sx={{ pl: { sm: 2 }, pr: { xs: 1, sm: 1 } }}>
                <Typography sx={{ flex: '1 1 100%' }} variant="h6" className='text-2xl font-bold text-black'>
                    Rates
                </Typography>
                <Tooltip title="Add Charge Station">
                    <Button variant="contained" startIcon={<AddIcon />} onClick={handleClickOpen} style={{ width: '150px', height: '40px' }}>
                        <Typography>เพิ่มเรตราคา</Typography>
                    </Button>
                </Tooltip>
            </Toolbar>

            <Dialog open={open} onClose={handleClose} fullWidth>
                <div className='flex justify-end gap-2'>
                    <Typography sx={{ flex: '1 1 100%' }}>
                        <DialogTitle>
                            Add Rate
                        </DialogTitle>
                    </Typography>
                    <Button variant="text" onClick={handleClose}>
                        <ClearRoundedIcon />
                    </Button>
                </div>
                <DialogContent>
                    <Box
                        sx={{
                            display: 'flex',
                            flexWrap: 'wrap',
                            flexDirection: 'column',
                            m: 'auto',
                            width: 'fit-content',
                            borderRadius: 1,
                        }}
                    >
                        <RateInput
                            handleChange={handleChange}
                            handleSubmit={handleSubmit}
                            values={values}
                            errors={errors}
                        />
                    </Box>
                </DialogContent>
            </Dialog>
            <MuiSnackbar message={checkErrorAlert.message} type={checkErrorAlert.type} open={alert} setOpen={handleResetAlert} />
        </div>
    )
}

export default RatePopupDialog;

