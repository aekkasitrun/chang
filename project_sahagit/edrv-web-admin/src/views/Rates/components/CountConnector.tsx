import React from "react";
import { useState } from "react";
import Box from '@mui/material/Box';
import { useGetAllConnectorGroupbyRateServiceQuery } from "@/services/connector.service";
import CircularProgress from '@mui/material/CircularProgress';
import handler from "@/pages/api/hello";

interface CountConnectorProps {
    rateId: string;
}

export default function CountConnector(props: CountConnectorProps) {
    const { rateId } = props;
    const connectorQuery = useGetAllConnectorGroupbyRateServiceQuery()
    if (connectorQuery.isLoading) return (
        <div>
            <Box sx={{ display: 'flex' }}>
                <CircularProgress />
            </Box>
        </div>)
    if (connectorQuery.isError) return <div>Error...</div>
    return (
        <div className="flex justify-center">

            {connectorQuery?.data?.map((connectorRate: any) => {
                return (
                    <div key={connectorRate.rateEdrvId}>

                        <div>
                            {connectorRate.rateEdrvId == rateId ? (
                                <div >
                                    {connectorRate._count}
                                </div>
                            ) : (
                                <div></div>
                            )}
                        </div>

                    </div>
                )
            })}



        </div>
    )

}