export interface UpdateRateDTO {
  id: string;
  name: string;
  description: string;
}
