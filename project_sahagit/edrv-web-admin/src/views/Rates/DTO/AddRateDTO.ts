export interface AddRateDTO {
  description: string;
  currency: string;
  price_components: [priceComponents];
}

interface priceComponents {
  type: string;
  price: number;
  tax: number;
  grace_period: number;
}
