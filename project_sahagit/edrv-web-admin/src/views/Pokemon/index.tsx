import { Stack } from "@/components/Layouts";
import { useGetPokemonByNameQuery } from "@/services/pokemon";
import React from "react";

const Pokemon = () => {
  const { data, isLoading, isError } = useGetPokemonByNameQuery("bulbasaur");

  if (isLoading) return <div>Loading...</div>;
  if (isError) return <div>Opps! there is an error occur!</div>;
  return (
    <div>
      <div className="text-lg font-bold">Abilities</div>
      <Stack>
        {data.abilities.map((ability: any, index: number) => {
          return (
            <>
              <div className="p-2 rounded-lg shadow-md flex gap-4">
                <div>Title {index + 1}</div>
                <div>: {ability.ability.name}</div>
              </div>
            </>
          );
        })}
      </Stack>
    </div>
  );
};

export default Pokemon;
