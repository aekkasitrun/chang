import * as React from 'react';
import Button from "@/components/Buttons/Button";
import { FormikErrors, FormikHandlers, FormikValues } from "formik";
import { Input } from "@/components/Form";
import { signIn } from "next-auth/react"
import GoogleIcon from '@mui/icons-material/Google';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import Link from '@mui/material/Link';
import { SignupDTO } from './DTO/SignupInputDTO';

interface SignupInputProps {
    handleSubmit: FormikHandlers["handleSubmit"];
    handleChange: FormikHandlers["handleChange"];
    values: SignupDTO;
    errors: FormikErrors<SignupDTO>
}

const SignupInput: React.FC<SignupInputProps> = ({
    handleChange,
    handleSubmit,
    values,
    errors,
}) => {

    // async function handleGoogleSignin() {
    //     signIn('google', { callbackUrl: 'http://localhost:3000' })
    // }

    // console.log('ErrorSignUp: ' + JSON.stringify(errors));

    return (
        // <div>
        <Box component="form" onSubmit={handleSubmit} sx={{ mt: 3 }} >
            <Grid container spacing={2}>
                <Grid item xs={12} sm={6}>
                    <Input
                        autoComplete="given-name"
                        name="firstName"
                        // required
                        fullWidth
                        id="firstName"
                        label="ชื่อ"
                        autoFocus
                        onChange={handleChange}
                        value={values.firstName}
                        error={!!errors.firstName}
                    />
                    {errors.firstName ?
                        <span className='text-rose-500'>{errors.firstName}</span>
                        :
                        <></>
                    }
                </Grid>
                <Grid item xs={12} sm={6}>
                    <Input
                        // required
                        fullWidth
                        id="lastName"
                        label="นามสกุล"
                        name="lastName"
                        autoComplete="family-name"
                        onChange={handleChange}
                        value={values.lastName}
                        error={!!errors.lastName}
                    />
                    {errors.lastName ?
                        <span className='text-rose-500'>{errors.lastName}</span>
                        :
                        <></>
                    }
                </Grid>
                <Grid item xs={12}>
                    <Input
                        // required
                        fullWidth
                        id="providerId"
                        label="ไอดีผู้ใช้"
                        name="providerId"
                        autoComplete="providerId"
                        onChange={handleChange}
                        value={values.providerId}
                        error={!!errors.providerId}
                    />
                    {errors.providerId ?
                        <span className='text-rose-500'>{errors.providerId}</span>
                        :
                        <></>
                    }
                </Grid>
                <Grid item xs={12}>
                    <Input
                        // required
                        fullWidth
                        id="email"
                        label="อีเมล"
                        name="email"
                        autoComplete="email"
                        onChange={handleChange}
                        value={values.email}
                        error={!!errors.email}
                    />
                    {errors.email ?
                        <span className='text-rose-500'>{errors.email}</span>
                        :
                        <></>
                    }
                </Grid>
                <Grid item xs={12}>
                    <Input
                        // required
                        fullWidth
                        id="phoneNumber"
                        label="หมายเลขโทรศัทพ์"
                        name="phoneNumber"
                        autoComplete="phoneNumber"
                        onChange={handleChange}
                        value={values.phoneNumber}
                        error={!!errors.phoneNumber}
                    />
                    {/* {errors.phoneNumber ?
                        <span className='text-rose-500'>{errors.phoneNumber}</span>
                        :
                        <></>
                    } */}
                </Grid>
                <Grid item xs={12}>
                    <Input
                        // required
                        fullWidth
                        name="password"
                        label="รหัสผ่าน"
                        type="password"
                        id="password"
                        autoComplete="new-password"
                        onChange={handleChange}
                        value={values.password}
                        error={!!errors.password}
                    />
                    {errors.password ?
                        <span className='text-rose-500'>{errors.password}</span>
                        :
                        <></>
                    }
                </Grid>
                <Grid item xs={12}>
                    <Input
                        // required
                        fullWidth
                        name="confirmPassword"
                        label="ยืนยันรหัสผ่าน"
                        type="password"
                        id="confirmPassword"
                        autoComplete="new-confirmPassword"
                        onChange={handleChange}
                        value={values.confirmPassword}
                        error={!!errors.confirmPassword}
                    />
                    {errors.confirmPassword ?
                        <span className='text-rose-500'>{errors.confirmPassword}</span>
                        :
                        <></>
                    }
                </Grid>
            </Grid>
            <Button
                type="submit"
                fullWidth
                variant="contained"
                sx={{ mt: 3, mb: 2 }}
            >
                ลงทะเบียน
            </Button>
            <Grid container justifyContent="flex-end">
                <Grid item>
                    <Link href="/signin" variant="body2" className='text-black'>
                        เข้าสู่ระบบ
                    </Link>
                </Grid>
            </Grid>

        </Box>
        // </div>
    )
}

export default SignupInput;