import * as React from 'react';
import Avatar from '@mui/material/Avatar';
import CssBaseline from '@mui/material/CssBaseline';
import Box from '@mui/material/Box';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import { FormikHelpers, useFormik } from "formik";
import * as Yup from "yup";
import SignupInput from './InputSignUp';
import { SignupDTO } from './DTO/SignupInputDTO';
import useSignUp from './hooks/signup';

const initSignup: SignupDTO = {
    firstName: '',
    lastName: '',
    providerId: '',
    email: '',
    password: '',
    confirmPassword: '',
    phoneNumber: '',
};

const validateSchema = Yup.object().shape({
    firstName: Yup.string().required('*จำเป็น'),
    lastName: Yup.string().required('*จำเป็น'),
    providerId: Yup.string().required('*จำเป็น'),
    phoneNumber: Yup.string().required('*จำเป็น'),
    email: Yup.string().email('อีเมลไม่ถูกต้อง').required('*จำเป็น'),
    password: Yup.string()
        .min(8, 'รหัสผ่านต้องมีความยาวมากกว่า 8 และน้อยกว่า 20 อักขระ')
        .max(20, 'รหัสผ่านต้องมีความยาวมากกว่า 8 และน้อยกว่า 20 อักขระ')
        .required('*จำเป็น'),

    confirmPassword: Yup.string()
        .oneOf([Yup.ref('password'), null], 'รหัสผ่านไม่ตรงกัน'),
});

const theme = createTheme();


const SignUp = () => {

    const { handleAddDataSignup } = useSignUp();
    const { values, handleChange, handleSubmit, errors } = useFormik({
        initialValues: initSignup,
        validationSchema: validateSchema,
        validateOnChange: true, // if true the form will validate the value every time the onChange is triggered
        onSubmit: handleSubmitSignin,
    });

    // console.log('Error: ' + JSON.stringify({ values, handleChange, setFieldValue, handleSubmit, errors }.errors.operatorName));

    async function handleSubmitSignin(value: SignupDTO, formikHelper: FormikHelpers<SignupDTO>) {
        console.log(value);
        await handleAddDataSignup(value)
        formikHelper.resetForm({ values: initSignup });
    }

    return (
        <ThemeProvider theme={theme}>
            <Container component="main" maxWidth="xs">
                <CssBaseline />
                <Box
                    sx={{
                        marginTop: 8,
                        display: 'flex',
                        flexDirection: 'column',
                        alignItems: 'center',
                    }}
                >
                    <Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
                        <LockOutlinedIcon />
                    </Avatar>
                    <Typography component="h1" variant="h5">
                        สมัครสมาชิก EDRV Web
                    </Typography>

                    <SignupInput
                        handleChange={handleChange}
                        handleSubmit={handleSubmit}
                        values={values}
                        errors={errors}
                    />
                </Box>
            </Container>
        </ThemeProvider>
    );
}

export default SignUp;
