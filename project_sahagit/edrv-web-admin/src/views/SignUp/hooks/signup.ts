import { useAddSignupServiceMutation } from "@/services/signup.service";
import { SignupDTO } from "../DTO/SignupInputDTO";

const useSignUp = () => {
  const [addSignupService] = useAddSignupServiceMutation();

  async function handleAddDataSignup(data: SignupDTO) {
    console.log("SignUpData = " + JSON.stringify(data));
    await addSignupService(data);
    console.log("Added ");
  }

  return { handleAddDataSignup };
};

export default useSignUp;
