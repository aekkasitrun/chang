export interface SignupDTO {
  firstName: string;
  lastName: string;
  email: string;
  providerId: string;
  phoneNumber: string;
  password: string;
  confirmPassword: string;
}
