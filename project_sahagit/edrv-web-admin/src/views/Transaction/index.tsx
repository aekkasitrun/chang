import * as React from 'react';
import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';
import TransactionList from './components/TransactionList';


const Transaction = () => {

    return (
        <div>
            <Box>
                {/* <Paper sx={{ width: '100%', mb: 2 }}> */}
                <TransactionList />
                {/* </Paper> */}
            </Box>
        </div>
    )
}

export default Transaction;
