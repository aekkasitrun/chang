import IconButton from '@mui/material/IconButton';
import React from "react";
import Dialog from '@mui/material/Dialog';
import Typography from '@mui/material/Typography';
import { TransitionProps } from '@mui/material/transitions';
import Slide from '@mui/material/Slide';
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import CloseIcon from '@mui/icons-material/Close';
import { Button } from '@mui/material';
import Box from '@mui/material/Box';
import EnergyInfo from './List/EnergyReportListPopup';
import TransactionPopupListInfo from './List/TransactionListPopup';
import Divider from '@mui/material/Divider';
import CardTransactionInfo from './List/TransactionCardPopup';

interface TransactionProps {
    transactionProps: any;
}

const Transition = React.forwardRef(function Transition(
    props: TransitionProps & {
        children: React.ReactElement;
    },
    ref: React.Ref<unknown>,
) {
    return <Slide direction="up" ref={ref} {...props} />;
});

export default function TransactionInfo(props: TransactionProps) {
    const { transactionProps } = props;

    const [open, setOpen] = React.useState(false);
    const handleClickOpen = () => {
        setOpen(true);
    };
    const handleClose = () => {
        setOpen(false);
    };
    // console.log(JSON.stringify(transaction));

    return (
        <div>
            <Button aria-label="edit" onClick={handleClickOpen} variant="outlined" style={{ width: '120px', height: '40px' }}>
                {/* <GroupWorkOutlinedIcon /> */}
                <Typography className='font-bold'>รายละเอียด</Typography>
            </Button>
            <Dialog
                fullScreen
                open={open}
                onClose={handleClose}
                TransitionComponent={Transition}
            >
                <AppBar sx={{ position: 'relative' }} className='bg-[#56C3CD]'>
                    <Toolbar>

                        <Typography sx={{ ml: 2, flex: 1 }} variant="h6" component="div">
                            Transaction Info
                        </Typography>
                        <IconButton
                            edge="start"
                            color="inherit"
                            onClick={handleClose}
                            aria-label="close"
                        >
                            <CloseIcon />
                        </IconButton>
                    </Toolbar>
                </AppBar>

                <div className='py-5 pl-8'>
                    <Typography sx={{ flex: '1 1 100%' }} variant="h6" className='text-2xl font-bold text-black'>
                        {transactionProps.transactionEdrvId} / {transactionProps?.Connector?.ChargeStation?.name}
                    </Typography>
                </div>

                <Divider />

                <Box sx={{ width: '100%', p: 5 }} >
                    <div className='grid grid-rows-3 grid-flow-col gap-4'>
                        <div className='row-span-1 col-span-2'><TransactionPopupListInfo transactionPopupProps={transactionProps} /></div>
                        <div className='col-span-2'><EnergyInfo energyProps={transactionProps} /></div>
                        <div className='row-span-3'><CardTransactionInfo transactionProps={transactionProps} /></div>
                    </div>
                </Box>

            </Dialog>
        </div>
    )
}

