import React from "react";
import Typography from '@mui/material/Typography';
import Paper from '@mui/material/Paper';
import Divider from '@mui/material/Divider';
import FormHelperText from '@mui/material/FormHelperText';
import FormControl from '@mui/material/FormControl';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import formatCost from "@/utils/formatCost";
import formatDuration from "@/utils/fomatDuration";
import formatConsumption from "@/utils/formatConsumption";

interface TransactionCardProps {
    transactionProps: any;
}

export default function CardTransactionInfo(props: TransactionCardProps) {
    const { transactionProps } = props;
    return (
        <Paper sx={{ width: '100%', height: 370, mb: 2 }} >
            <div className="grid grid-cols-2 gap-2 p-6">

                <Paper sx={{ width: '100%', height: 150, mb: 2 }} variant="outlined" className="border-[#0DA9B8] rounded-lg border-2">
                    <div className='py-3 flex justify-center font-bold text-black text-2xl'>
                        จำนวนระยะเวลา (ชม.)
                    </div>
                    <Divider variant="middle" />
                    <div className="flex justify-center p-8 font-extrabold text-black text-3xl">
                        {formatDuration(transactionProps.duration)}
                    </div>
                </Paper>

                <Paper sx={{ width: '100%', height: 150, mb: 2 }} variant="outlined" className="border-[#0DA9B8] rounded-lg border-2">
                    <div className='py-3 flex justify-center font-bold text-black text-2xl'>
                        จำนวนพลังงานที่ใช้ไป (kWh)
                    </div>
                    <Divider variant="middle" />
                    <div className="flex justify-center p-8 font-extrabold text-black text-3xl">
                        {formatConsumption(transactionProps.consumption)}
                    </div>
                </Paper>

                <Paper sx={{ width: '100%', height: 150, mb: 2 }} variant="outlined" className="border-[#0DA9B8] rounded-lg border-2">
                    <div className='py-3 flex justify-center font-bold text-black text-2xl'>
                        เปอร์เซ็นต์ที่เพิ่มขึ้น
                    </div>
                    <Divider variant="middle" />
                    <div className="flex justify-center p-8 font-extrabold text-black text-3xl">
                        {transactionProps.EnergyReport[0].stateOfChargeValue}
                    </div>
                </Paper>

                <Paper sx={{ width: '100%', height: 150, mb: 2 }} variant="outlined" className="border-[#0DA9B8] rounded-lg border-2">
                    <div className='py-3 flex justify-center font-bold text-black text-2xl'>
                        จำนวนราคา (บาท)
                    </div>
                    <Divider variant="middle" />
                    <div className="flex justify-center p-8 font-extrabold text-black text-3xl">
                        {formatCost(transactionProps.cost)}
                    </div>
                </Paper>

            </div>
        </Paper>
    )
}