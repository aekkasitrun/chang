import React from "react";
import Typography from '@mui/material/Typography';
import Paper from '@mui/material/Paper';
import Divider from '@mui/material/Divider';
import FormHelperText from '@mui/material/FormHelperText';
import FormControl from '@mui/material/FormControl';

import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import TablePagination from '@mui/material/TablePagination/TablePagination';

interface EnergyReportProps {
    energyProps: any;
}

export default function EnergyInfo(props: EnergyReportProps) {
    const { energyProps } = props;

    return (
        <Paper sx={{ width: '100%', mb: 2 }} >
            <div className='pt-5'>
                <Typography sx={{ ml: 2, flex: 1 }} variant="h6" component="div" className="font-bold text-black">
                    บันทึกค่าพลังงาน
                </Typography>
            </div>
            <div className="p-5">
                <Paper sx={{ width: '100%' }} variant='outlined'>
                    <TableContainer >
                        <Table sx={{ minWidth: 'auto' }} size="small" aria-label="a dense table">
                            <TableHead className='bg-[#D9D9D940]'>
                                <TableRow>
                                    <TableCell align="center" className='text-sm font-semibold  text-black'>ค่าที่วัดได้</TableCell>
                                    <TableCell align="center" className='text-sm font-semibold  text-black'>จำนวน</TableCell>
                                    <TableCell align="center" className='text-sm font-semibold  text-black'>หน่วย</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>

                                <TableRow>
                                    <TableCell align="center" >พลังงานที่ใช้ทั้งหมด</TableCell>
                                    <TableCell align="center" >{energyProps?.EnergyReport[0]?.energyMeterValue}</TableCell>
                                    <TableCell align="center" >Wh</TableCell>
                                </TableRow>

                                <TableRow>
                                    <TableCell align="center" >กระแสไฟฟ้า</TableCell>
                                    <TableCell align="center" >{energyProps?.EnergyReport[0]?.currentValue}</TableCell>
                                    <TableCell align="center" >A</TableCell>
                                </TableRow>

                                <TableRow>
                                    <TableCell align="center" >กำลังไฟฟ้า</TableCell>
                                    <TableCell align="center" >{energyProps?.EnergyReport[0]?.powerValue}</TableCell>
                                    <TableCell align="center" >kW</TableCell>
                                </TableRow>

                                <TableRow>
                                    <TableCell align="center" >เปอร์เซ็นต์การชาร์จแบตเตอรี่</TableCell>
                                    <TableCell align="center" >{energyProps?.EnergyReport[0]?.stateOfChargeValue}</TableCell>
                                    <TableCell align="center" >%</TableCell>
                                </TableRow>

                            </TableBody>
                        </Table>
                    </TableContainer>
                </Paper>
            </div>
        </Paper>
    )
}