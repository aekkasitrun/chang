import React from "react";
import Typography from '@mui/material/Typography';
import Paper from '@mui/material/Paper';
import Divider from '@mui/material/Divider';
import FormHelperText from '@mui/material/FormHelperText';
import FormControl from '@mui/material/FormControl';
import CalendarTodayIcon from '@mui/icons-material/CalendarToday';
import PersonIcon from '@mui/icons-material/Person';
import formatDate from "@/utils/formatDate";
import formatTime from "@/utils/formatTime";

interface TransactionPopupReportProps {
    transactionPopupProps: any;
}

export default function TransactionPopupListInfo(props: TransactionPopupReportProps) {
    const { transactionPopupProps } = props;


    return (
        <Paper sx={{ width: '100%', mb: 2 }} >
            <div className='flex flex-row gap-2 px-4' >

                <div className="basis-1/3 ">
                    <FormHelperText className="text-black font-extrabold">เลขที่</FormHelperText>
                    <Paper sx={{ width: '100%', mb: 2 }} variant="outlined" >
                        <div className='py-3 pl-2 '>
                            {transactionPopupProps.transactionEdrvId}
                        </div>
                    </Paper>
                </div>

                <div className="basis-2/3 ">
                    <FormHelperText className="text-black font-extrabold">สถานที่</FormHelperText>
                    <Paper sx={{ width: '100%', mb: 2 }} variant="outlined" >
                        <div className='py-3'>
                            <Typography sx={{ ml: 2, flex: 1 }} variant="inherit" component="div">
                                {transactionPopupProps.Connector?.ChargeStation?.Locations?.locationAddresses[0]?.streetAndNumber + ' '}
                                {transactionPopupProps.Connector?.ChargeStation?.Locations?.locationAddresses[0]?.city + ' '}
                                {transactionPopupProps.Connector?.ChargeStation?.Locations?.locationAddresses[0]?.state + ' '}
                                {transactionPopupProps.Connector?.ChargeStation?.Locations?.locationAddresses[0]?.country + ' '}
                                {transactionPopupProps.Connector?.ChargeStation?.Locations?.locationAddresses[0]?.postCode}
                            </Typography>
                        </div>
                    </Paper>
                </div>
            </div>

            <div className='flex flex-row gap-2 px-4' >

                <div className="basis-1/3 ">
                    <FormHelperText className="text-black font-extrabold">วันที่</FormHelperText>
                    <Paper sx={{ width: '100%', mb: 2 }} variant="outlined" >
                        <div className='py-3 pl-2 flex gap-2'>
                            <CalendarTodayIcon sx={{ color: '#B4B4B4' }} />
                            <div>{formatDate(transactionPopupProps.startTime)}</div>
                        </div>
                    </Paper>
                </div>

                <div className="basis-1/3 ">
                    <FormHelperText className="text-black font-extrabold">เวลาเริ่มชาร์จ</FormHelperText>
                    <Paper sx={{ width: '100%', mb: 2 }} variant="outlined" >
                        <div className='py-3 pl-2'>
                            <Typography sx={{ ml: 2, flex: 1 }} variant="inherit" component="div">
                                {formatTime(transactionPopupProps.startTime)}
                            </Typography>
                        </div>
                    </Paper>
                </div>

                <div className="basis-1/3 ">
                    <FormHelperText className="text-black font-extrabold">เวลาหยุดชาร์จ</FormHelperText>
                    <Paper sx={{ width: '100%', mb: 2 }} variant="outlined" >
                        <div className='py-3'>
                            <Typography sx={{ ml: 2, flex: 1 }} variant="inherit" component="div">
                                {formatTime(transactionPopupProps.endTime)}
                            </Typography>
                        </div>
                    </Paper>
                </div>
            </div>

            <div className='flex flex-row gap-2 px-4' >

                <div className="basis-1/2 ">
                    <FormHelperText className="text-black font-extrabold">ผู้ใช้บริการ</FormHelperText>
                    <Paper sx={{ width: '100%', mb: 2 }} variant="outlined" >
                        <div className='py-3 pl-2 flex gap-2'>
                            <PersonIcon sx={{ color: '#B4B4B4' }} />
                            <div>{transactionPopupProps?.UserEdrv?.username}</div>
                        </div>
                    </Paper>
                </div>

                <div className="basis-1/2 ">
                    <FormHelperText className="text-black font-extrabold">ช่องทางการชำระ</FormHelperText>
                    <Paper sx={{ width: '100%', mb: 2 }} variant="outlined" >
                        <div className='py-3 pl-2'>
                            บัตรเดบิต (mock data)
                        </div>
                    </Paper>
                </div>

            </div>
        </Paper>
    )
}