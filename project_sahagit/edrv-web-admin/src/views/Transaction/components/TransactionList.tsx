import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import React from "react";
import { useGetAllTransactionServiceQuery } from '@/services/transaction.service';
import Box from '@mui/material/Box';
import LinearProgress from '@mui/material/LinearProgress';
import Alert from '@mui/material/Alert';
import AlertTitle from '@mui/material/AlertTitle';
import Stack from '@mui/material/Stack';
import GroupWorkOutlinedIcon from '@mui/icons-material/GroupWorkOutlined';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import TransactionInfo from './Popup/TransactionMoreInfoPopup';
import TablePagination, { TablePaginationProps } from '@mui/material/TablePagination';
import { useState } from 'react';
import { TableSortLabel } from '@mui/material';
import Divider from '@mui/material/Divider';
import formatCost from '@/utils/formatCost';
import formatDateAndTime from "@/utils/fomatDateTime";
import formatDuration from '@/utils/fomatDuration';
import formatConsumption from '@/utils/formatConsumption';

const TransactionList = () => {
    const transactionQuery = useGetAllTransactionServiceQuery()

    const rowsPerPageOptions = [5, 10, 25];
    const [rowsPerPage, setRowsPerPage] = useState(rowsPerPageOptions[0]);

    const [page, setPage] = useState(0);

    const handleChangePage = (_: React.MouseEvent<HTMLButtonElement> | null, newPage: number) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement>) => {
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(0);
    };

    const [order, setOrder] = useState<"asc" | "desc">("asc");
    const [orderBy, setOrderBy] = useState("transactionEdrvId");

    const handleSort = (event: React.MouseEvent<HTMLSpanElement>) => {
        const field = event.currentTarget.getAttribute("data-field");
        const isAsc = orderBy === field && order === "asc";
        setOrder(isAsc ? "desc" : "asc");
        setOrderBy(field ?? "transactionEdrvId");
    };

    if (transactionQuery.isLoading) return <div>
        <Box sx={{ width: '100%' }}>
            <LinearProgress />
        </Box>
    </div>
    if (transactionQuery.isError) return <div>
        <Stack sx={{ width: '100%' }} spacing={2}>
            <Alert severity="error">
                <AlertTitle>Error</AlertTitle>
                Data Fetching failed — <strong>check it out!</strong>
            </Alert>
        </Stack>
    </div>
    return (
        <div>
            <Toolbar>
                <Typography sx={{ flex: '1 1 100%' }} variant="h6" className='text-2xl font-bold text-black'>
                    ประวัติการใช้งาน
                </Typography>
            </Toolbar>
            <div className='pb-8'><Divider /></div>
            <TableContainer component={Paper}>
                <Table sx={{ minWidth: 1000 }} size="small" aria-label="a dense table">
                    <TableHead>
                        <TableRow className='bg-[#D9D9D940]'>
                            {/* <TableCell align="center">Status</TableCell> */}
                            <TableCell align="center" className='text-base font-semibold text-black'>เลขที่</TableCell>
                            <TableCell align="center" className='text-base font-semibold text-black'>เวลา</TableCell>
                            <TableCell align="center" className='text-base font-semibold text-black'>ผู้ใช้บริการ</TableCell>
                            <TableCell align="center" className='text-base font-semibold text-black'>สถานที่</TableCell>
                            <TableCell align="center" className='text-base font-semibold text-black'>สถานีชาร์จ</TableCell>
                            <TableCell align="center" className='text-base font-semibold text-black'>ระยะเวลา(ชม.)</TableCell>
                            <TableCell align="center" className='text-base font-semibold text-black'>จำนวนพลังงานที่ใช้ไป(kWh)</TableCell>
                            <TableCell align="center" className='text-base font-semibold text-black'>
                                {/* <TableSortLabel
                                    active={orderBy === "cost"}
                                    direction={orderBy === "cost" ? order : "asc"}
                                    onClick={handleSort}
                                    data-field="cost"
                                > */}
                                จำนวนราคา
                                {/* </TableSortLabel> */}
                            </TableCell>
                            <TableCell align="center"></TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {(rowsPerPage > 0
                            ? transactionQuery?.data.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                            : transactionQuery?.data
                        ).map((transaction: any) => {
                            // console.log(JSON.stringify(transaction));
                            return (
                                <TableRow key={transaction.transactionEdrvId}>
                                    <TableCell align="center">{transaction.transactionEdrvId}</TableCell>
                                    <TableCell align="center">
                                        {formatDateAndTime(transaction.startTime)}
                                    </TableCell>
                                    <TableCell align="center">{transaction.UserEdrv.firstName} {transaction.UserEdrv.lastName}</TableCell>
                                    <TableCell align="center">{transaction?.Connector?.ChargeStation?.Locations?.locationAddresses[0]?.streetAndNumber}</TableCell>
                                    <TableCell align="center">{transaction?.Connector?.ChargeStation?.name}</TableCell>
                                    <TableCell align="center">
                                        {formatDuration(transaction.duration)}
                                    </TableCell>
                                    <TableCell align="center">{formatConsumption(transaction.consumption)}</TableCell>
                                    <TableCell align="center" className='text-[#4FC63B] font-bold'>{formatCost(transaction.cost)}</TableCell>
                                    <TableCell align="center">
                                        <TransactionInfo transactionProps={transaction} />
                                    </TableCell>
                                    {/* <TableCell align="center">{transaction.endTime}</TableCell> */}
                                </TableRow>
                            )
                        })}
                    </TableBody>
                </Table>
                <TablePagination
                    rowsPerPageOptions={rowsPerPageOptions}
                    component="div"
                    count={transactionQuery?.data.length || 0}
                    rowsPerPage={rowsPerPage}
                    page={page}
                    onPageChange={handleChangePage}
                    onRowsPerPageChange={handleChangeRowsPerPage}
                />
            </TableContainer>
        </div>
    )
}

export default TransactionList;