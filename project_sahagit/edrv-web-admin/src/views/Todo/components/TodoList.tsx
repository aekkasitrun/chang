import { Box, Stack } from "@/components/Layouts";
import { Todo } from "@/store/reducer/example.slice";
import { CheckBox } from "@mui/icons-material";
import { Delete } from "@mui/icons-material";
import IconButton from "@/components/IconButton";
import React from "react";

interface TodoListProps {
  todos: Todo[];
  handleDeleteTodo: (_id: string) => void;
}

const TodoList: React.FC<TodoListProps> = ({ todos, handleDeleteTodo }) => {
  return (
    <Stack>
      {todos.map(({ content, done, title, id }: Todo, index: number) => {
        return (
          <Box key={id} className="flex gap-4">
            <Box className="px-4 grow py-2 flex gap-4 border rounded-lg border-gray-300 ">
              <Box className="grow-0 pr-4 border-r border-gray-300">
                {index + 1}
              </Box>
              <Box className="px-4 grow border-r border-gray-300">{title}</Box>
              <Box className="grow">{content}</Box>
            </Box>
            <IconButton onClick={() => handleDeleteTodo(id as string)}>
              <Delete className="text-red-500" />
            </IconButton>
          </Box>
        );
      })}
    </Stack>
  );
};

export default TodoList;
