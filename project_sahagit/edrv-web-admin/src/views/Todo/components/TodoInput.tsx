import Input from "@/components/Form/Input";
import { Todo } from "@/store/reducer/example.slice";
import Button from "@/components/Buttons/Button";
import { FormikHandlers, FormikValues } from "formik";
import React from "react";

interface TodoInputProps {
  handleSubmit: FormikHandlers["handleSubmit"];
  handleChange: FormikHandlers["handleChange"];
  values: Todo;
}
const TodoInput: React.FC<TodoInputProps> = ({
  handleChange,
  handleSubmit,
  values,
}) => {
  return (
    <form onSubmit={handleSubmit}>
      <div className="flex gap-4 justify-center h-full  ">
        <div className="max-w-md">
          <Input
            name="title"
            className=""
            placeholder="Type your title here."
            label="Title"
            size="small"
            fullWidth
            onChange={handleChange}
            value={values.title}
          />
        </div>
        <div className="max-w-md">
          <Input
            name="content"
            className=""
            placeholder="Type your content here."
            label="Content"
            size="small"
            fullWidth
            onChange={handleChange}
            value={values.content}
          />
        </div>

        <div>
          <Button
            className="h-full"
            size="small"
            variant="contained"
            color="primary"
            type="submit"
          >
            Submit
          </Button>
        </div>
      </div>
    </form>
  );
};

export default TodoInput;
