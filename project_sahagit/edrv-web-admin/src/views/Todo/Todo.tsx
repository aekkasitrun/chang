import { Stack } from "@/components/Layouts";
import { Input } from "@/components/Form";
import React from "react";
import { FormikHelpers, useFormik } from "formik";
import { Todo } from "@/store/reducer/example.slice";
import useTodo from "./hooks/todo";
import * as Yup from "yup";
import TodoList from "./components/TodoList";
import TodoInput from "./components/TodoInput";

const initTodo: Todo = {
  content: "",
  done: false,
  title: "",
};

const validateSchema = Yup.object({
  content: Yup.string().required("Content cannot be empty."),
  done: Yup.bool(),
  title: Yup.string().required("Title cannot be empty."),
});

const TodoView = () => {
  const { todoList, handleAddTodo, handleDeleteTodo } = useTodo();
  const { values, handleChange, handleSubmit } = useFormik({
    initialValues: initTodo,
    validationSchema: validateSchema,
    validateOnChange: false, // if true the form will validate the value every time the onChange is triggered
    onSubmit: handleSubmitTodo,
  });

  function handleSubmitTodo(values: Todo, formikHelper: FormikHelpers<Todo>) {
    values.id = crypto.randomUUID();
    console.log(values);
    handleAddTodo(values);
    formikHelper.resetForm({ values: initTodo });
  }

  return (
    <Stack>
      <div className="text-lg font-bold"> TODO</div>
      <Stack>
        <TodoInput
          handleChange={handleChange}
          handleSubmit={handleSubmit}
          values={values}
        />
      </Stack>
      <TodoList todos={todoList} handleDeleteTodo={handleDeleteTodo} />
    </Stack>
  );
};

export default TodoView;
