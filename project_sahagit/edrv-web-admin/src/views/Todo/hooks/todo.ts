import { useAppDispatch, useAppSelector } from "@/Hooks/useRedux";
import {
  Todo,
  todoSelector,
  addTodo,
  deleteTodo,
  editTodo,
} from "@/store/reducer/example.slice";

const useTodo = () => {
  const dispatch = useAppDispatch();
  const todoList = useAppSelector(todoSelector);

  const handleAddTodo = (todo: Todo) => {
    return dispatch(addTodo(todo));
  };
  const handleEditTodo = (todo: Todo) => {
    return dispatch(editTodo(todo));
  };

  const handleDeleteTodo = (id: string) => {
    console.log(id);
    return dispatch(deleteTodo(id));
  };

  return {
    todoList,
    handleDeleteTodo,
    handleAddTodo,
    handleEditTodo,
  };
};

export default useTodo;
