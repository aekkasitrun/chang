import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import IconButton from '@mui/material/IconButton';
import DeleteIcon from '@mui/icons-material/Delete';
import EditIcon from '@mui/icons-material/Edit';
import Switch from '@mui/material/Switch';
import React from "react";
import { Button } from '@mui/material';
import { UserDTO } from './DTO/userDTO';

interface UserListProps {
    userList: UserDTO[];
}

const UserList: React.FC<UserListProps> = ({ userList }) => {
    return (
        <TableContainer component={Paper}>
            <Table sx={{ minWidth: 650 }} size="small" aria-label="a dense table">
                <TableHead>
                    <TableRow>
                        <TableCell align="center">Email</TableCell>
                        <TableCell align="center">First Name</TableCell>
                        <TableCell align="center">Last Name</TableCell>
                        <TableCell align="center">Mobile Number</TableCell>
                        <TableCell align="center">Enable</TableCell>
                        <TableCell align="center">Action</TableCell>
                        <TableCell align="center">Transactions</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {userList.map((user: any) => (
                        <TableRow key={user._id}>
                            <TableCell align="center" sx={{ borderRight: 1 }} >{user.email}</TableCell>
                            <TableCell align="center" sx={{ borderRight: 1 }}>{user.firstname}</TableCell>
                            <TableCell align="center" sx={{ borderRight: 1 }}>{user.lastname}</TableCell>
                            <TableCell align="center" sx={{ borderRight: 1 }}>{user.phone.mobile}</TableCell>
                            <TableCell align="center" sx={{ borderRight: 1 }}>
                                <Switch
                                    checked={user.active}
                                // onChange={() => setEnable(!loading)}
                                // name="loading"
                                // color="primary"
                                />
                            </TableCell>
                            <TableCell align="center" sx={{ borderRight: 1 }}>
                                <IconButton aria-label="delete">
                                    <EditIcon />
                                </IconButton>
                                <IconButton aria-label="delete">
                                    <DeleteIcon />
                                </IconButton>
                            </TableCell>
                            <TableCell align="center" sx={{ borderRight: 1 }}>
                                <Button variant="outlined">
                                    Show
                                </Button>
                            </TableCell>
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        </TableContainer>
    )
}

export default UserList;