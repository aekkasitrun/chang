export interface UserDTO {
  firstname: string;
  lastname: string;
  email: string;
  phone: userPhone;
}

interface userPhone {
  mobile: string;
}
