import * as React from 'react';
import UserList from './components/UserList';

interface UsersProps {
    users?: any
}

const Users = ({ users }: UsersProps) => {

    // console.log(users)
    // console.log(users)
    const [ennable, setEnable] = React.useState(true);
    function handleClick() {
        setEnable(true);
    }

    return (
        <div>
            <h1>Users</h1>
            <UserList userList={users} />
        </div>

    );
}

export default Users;
