import React from "react";

import {
  useDeleteLocationServiceMutation,
  useAddLocationServiceMutation,
  useUpdateLocationServiceMutation,
  useUpdateLocationSwitchServiceMutation,
} from "@/services/location.service";
import { LocationDTO } from "../components/DTO/LocationDTO";
import { SnackBarType } from "@/components/SnackBar";

interface CheckErrorAlert {
  message: string;
  type: SnackBarType;
}

const useLocation = () => {
  const [addLocationService] = useAddLocationServiceMutation();
  const [deleteLocationService] = useDeleteLocationServiceMutation();
  const [updateLocationService] = useUpdateLocationServiceMutation();
  const [updateLocationSwitchService] =
    useUpdateLocationSwitchServiceMutation();

  const [checkErrorAlert, setCheckErrorAlert] = React.useState<CheckErrorAlert>(
    {
      message: "",
      type: "success",
    }
  );

  async function handleDelete(id: string) {
    try {
      console.log("LocationId = " + id);
      await deleteLocationService(id).unwrap();
      setCheckErrorAlert({
        message: "Location Deleted",
        type: "success",
      });
      console.log("Deleted");
    } catch (error) {
      console.log("Delete fail " + JSON.stringify(error));
      setCheckErrorAlert({
        message: "Delete fail",
        type: "error",
      });
    }
  }

  async function handleAddLocation(data: any) {
    try {
      console.log("LocationData = " + data);
      await addLocationService(data).unwrap();
      setCheckErrorAlert({
        message: "Add Location",
        type: "success",
      });
      console.log("Added ");
    } catch (error) {
      console.log("Add Location fail");
      setCheckErrorAlert({
        message: "Add Location fail",
        type: "error",
      });
    }
  }

  async function handleUpdateLocation(data: any) {
    try {
      console.log("LocationData = " + data.id);
      console.log("LocationData = " + data.coordinates);
      await updateLocationService(data).unwrap();
      setCheckErrorAlert({
        message: "Update Location",
        type: "success",
      });
      console.log("Updated ");
    } catch (error) {
      console.log("Update Location fail");
      setCheckErrorAlert({
        message: "Update Location fail",
        type: "error",
      });
    }
  }

  async function handleUpdateLocationSwitch(data: any) {
    try {
      console.log("LocationData = " + data.id);
      console.log("LocationData = " + data.active);
      await updateLocationSwitchService(data).unwrap();
      setCheckErrorAlert({
        message: "Location Update Status",
        type: "success",
      });
      console.log("Updated Status");
    } catch (error) {
      console.log("Update Status fail");
      setCheckErrorAlert({
        message: "Update Status fail",
        type: "error",
      });
    }
  }

  return {
    handleDelete,
    handleAddLocation,
    handleUpdateLocation,
    handleUpdateLocationSwitch,
    checkErrorAlert,
  };
};

export default useLocation;
