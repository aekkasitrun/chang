import * as React from 'react';
import LocationList from './components/LocationList';
import LocationPopupDialog from './components/PopupDialog/AddLocationPopup';
import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';
import MapPage from './components/Map';
import Divider from '@mui/material/Divider';

const LocationView = () => {
    const [selected] = React.useState<readonly string[]>([]);

    return (
        <div >
            <Box sx={{ width: '100%' }}>
                {/* <Paper sx={{ width: '100%', mb: 2 }}> */}
                <LocationPopupDialog />
                <div className='pb-8'><Divider /></div>
                <LocationList />
                {/* </Paper> */}
            </Box>
        </div >
    );
}

export default LocationView;
