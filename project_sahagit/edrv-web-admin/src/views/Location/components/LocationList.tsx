import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import Switch from '@mui/material/Switch';
import React from "react";
import { LocationDTO } from './DTO/LocationDTO';
import { UpdateSwitchDTO } from './DTO/UpdateLocationDTO';
import useLocation from '../hooks/location';
import LocationUpdatePopupDialog from './PopupDialog/UpdateLocationPopup';
import LocationDeletePopupDialog from './PopupDialog/DeleteLocationPopup';
import LinearProgress from '@mui/material/LinearProgress';
import Box from '@mui/material/Box';
import { useGetAllLocationServiceQuery } from '@/services/location.service';

import Alert from '@mui/material/Alert';
import AlertTitle from '@mui/material/AlertTitle';
import Stack from '@mui/material/Stack';
import MuiSnackbar from '@/components/SnackBar';
import TablePagination, { TablePaginationProps } from '@mui/material/TablePagination';
import { useState } from 'react';
import { styled } from '@mui/material/styles';
import { SwitchProps } from '@mui/material/Switch';
import LocationUpdateSwitch from './Switch';

const LocationList = () => {

    const locationQuery = useGetAllLocationServiceQuery()

    const rowsPerPageOptions = [5, 10, 25];
    const [rowsPerPage, setRowsPerPage] = useState(rowsPerPageOptions[0]);

    const [page, setPage] = useState(0);

    const handleChangePage = (_: React.MouseEvent<HTMLButtonElement> | null, newPage: number) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement>) => {
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(0);
    };

    if (locationQuery.isLoading) return <div>
        <Box sx={{ width: '100%' }}>
            <LinearProgress />
        </Box>
    </div>
    if (locationQuery.isError) return <div>
        <Stack sx={{ width: '100%' }} spacing={2}>
            <Alert severity="error">
                <AlertTitle>Error</AlertTitle>
                Data Fetching failed — <strong>check it out!</strong>
            </Alert>
        </Stack>
    </div>

    return (

        <TableContainer component={Paper}>
            <Table sx={{ minWidth: 650 }} aria-label="simple table">
                <TableHead className='bg-[#D9D9D940]'>
                    <TableRow>
                        {/* <TableCell align="center">บ้านเลขที่</TableCell>
                        <TableCell align="center">ตำบล</TableCell>
                        <TableCell align="center">อำเภอ</TableCell>
                        <TableCell align="center">จังหวัด</TableCell>
                        <TableCell align="center">รหัสไปรษณีย์</TableCell> */}
                        <TableCell align="center" className='text-lg font-bold text-black'>ชื่อสถานที่</TableCell>
                        <TableCell align="center" className='text-lg font-bold text-black'>ที่อยู่</TableCell>
                        {/* <TableCell align="center">พิกัด(ละติจูด/ลองติจูด)</TableCell> */}
                        <TableCell align="center" className='text-lg font-bold text-black'>สถานะ เปิด/ปิด</TableCell>
                        <TableCell align="center" className='text-lg font-bold text-black'>แก้ไขข้อมูล</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {(rowsPerPage > 0
                        ? locationQuery?.data?.result.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                        : locationQuery?.data?.result
                    ).map((location: any) => (
                        <TableRow key={location._id}>
                            <TableCell align="center" >{location.operatorName}</TableCell>
                            <TableCell align="center"  >
                                {location.address.streetAndNumber + ' '}
                                {location.address.city + ' '}
                                {location.address.state + ' '}
                                {location.address.country + ' '}
                                {location.address.postalCode + ' '}
                            </TableCell>
                            {/* <TableCell align="center" sx={{ borderRight: 1 }}>{location.address.city}</TableCell>
                            <TableCell align="center" sx={{ borderRight: 1 }}>{location.address.state}</TableCell>
                            <TableCell align="center" sx={{ borderRight: 1 }}>{location.address.country}</TableCell>
                            <TableCell align="center" sx={{ borderRight: 1 }}>{location.address.postalCode}</TableCell> */}
                            {/* <TableCell align="center" sx={{ borderRight: 1 }}>
                                <div className='grid gap-1 grid-cols-2'>
                                    <p className='truncate ...'>{location.coordinates[1]}</p>
                                    <p className='truncate ...'>{location.coordinates[0]}</p>
                                </div>
                            </TableCell> */}
                            <TableCell align="center" >
                                <LocationUpdateSwitch locationId={location._id} locationActive={location.active} />
                            </TableCell>
                            <TableCell align="center" >
                                <div className='flex justify-center'>
                                    <LocationUpdatePopupDialog
                                        locationId={location._id}
                                        latitude={location.coordinates[1]}
                                        longitude={location.coordinates[0]}
                                        streetAndNumberProps={location.address.streetAndNumber}
                                        cityProps={location.address.city}
                                        stateProps={location.address.state}
                                        countryProps={location.address.country}
                                        postalCodeProps={location.address.postalCode}
                                        radiusProps={location?.meta_data?.radius}
                                        operatorProps={location.operatorName}
                                    />
                                    {/* {JSON.stringify(location.meta_data)} */}
                                    <LocationDeletePopupDialog
                                        locationId={location._id}
                                        streetAndNumberProps={location.address.streetAndNumber}
                                        cityProps={location.address.city}
                                        stateProps={location.address.state}
                                        countryProps={location.address.country}
                                        postalCodeProps={location.address.postalCode}
                                    />
                                </div>
                            </TableCell>
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
            <TablePagination
                rowsPerPageOptions={rowsPerPageOptions}
                component="div"
                count={locationQuery?.data?.result.length || 0}
                rowsPerPage={rowsPerPage}
                page={page}
                onPageChange={handleChangePage}
                onRowsPerPageChange={handleChangeRowsPerPage}
            />
        </TableContainer>
    )
}

export default LocationList;