import * as React from 'react';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Tooltip from '@mui/material/Tooltip';
import Button from '@mui/material/Button';
import AddIcon from '@mui/icons-material/Add';

import Dialog from '@mui/material/Dialog';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';

import * as Yup from "yup";
import { FormikHelpers, useFormik } from "formik";
import ClearRoundedIcon from '@mui/icons-material/ClearRounded';
import { LocationDTO } from '../DTO/LocationDTO';
import useLocation from '../../hooks/location';
import LocationInput from '../LocationInput';
import MuiSnackbar from '@/components/SnackBar';
import MapPage from '../Map';
import Divider from '@mui/material/Divider';

// interface LocationPopupDialog {
//     numSelected: number;
// }

// interface MapPageProps {
//     onLatLngChange: (lat: number, lng: number) => void;
//   }

// let latitude = 5.55
// let longitude = 4.44


const initLocation: LocationDTO = {
    active: true,
    address: {
        streetAndNumber: "",
        city: "",
        country: "",
        postalCode: "",
        state: "",
    },
    coordinates: [0, 0],
    meta_data: {
        radius: 0.00
    },
    operatorName: "",
    timezone: "",
};

const validateSchema = Yup.object().shape({
    active: Yup.bool(),
    address: Yup.object().shape({
        streetAndNumber: Yup.string().required("streetAndNumber cannot be empty."),
        city: Yup.string().required("city cannot be empty."),
        country: Yup.string().required("country cannot be empty."),
        postalCode: Yup.string().required("postalCode cannot be empty."),
        state: Yup.string().required("state cannot be empty."),
    }),

    // coordinates: Yup.array().required("coordinates cannot be empty."),
    coordinates: Yup.array().of(Yup.number().required()).min(2, 'coordinates should have at least 2 elements.'),
    meta_data: Yup.object().shape({
        radius: Yup.number().required("radius cannot be empty."),
    }),
    operatorName: Yup.string().required("operatorName cannot be empty."),
    timezone: Yup.string().required("timezone cannot be empty."),

    //ที่ add location กับ folder hooks ไม่ได้มีปัญหาตรง yup น่าจะต้อง check ข้อมูลตาม DTO ให้หมดซึ่งตอนนี้ยัง error อยู่

});

const LocationPopupDialog = () => {
    const { handleAddLocation, checkErrorAlert } = useLocation();

    const [alert, setAlert] = React.useState(false);

    const handleResetAlert = () => {
        setAlert(false)
    }



    const { values, handleChange, setFieldValue, handleSubmit, errors } = useFormik({
        initialValues: initLocation,
        validationSchema: validateSchema,
        validateOnChange: true, // if true the form will validate the value every time the onChange is triggered
        onSubmit: handleSubmitLocation,
    });

    // console.log('Error: ' + JSON.stringify({ values, handleChange, setFieldValue, handleSubmit, errors }.errors.operatorName));

    async function handleSubmitLocation(value: LocationDTO, formikHelper: FormikHelpers<LocationDTO>) {
        console.log(JSON.stringify(value));
        await handleAddLocation(value)
        formikHelper.resetForm({ values: initLocation });
        setAlert(true)
    }

    const [open, setOpen] = React.useState(false);

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    return (
        <div>
            <Toolbar sx={{ pl: { sm: 2 }, pr: { xs: 1, sm: 1 } }}>
                <Typography sx={{ flex: '1 1 100%' }} variant="h6" className='text-2xl font-bold text-black'>
                    Locations
                </Typography>
                <Tooltip title="Add Location" placement="top">
                    <Button variant="contained" startIcon={<AddIcon />} onClick={handleClickOpen} style={{ width: '150px', height: '40px' }}>
                        <Typography>เพิ่มสถานที่</Typography>
                    </Button>
                </Tooltip>
            </Toolbar>

            <Dialog
                open={open}
                onClose={handleClose}
                fullWidth={true}
                maxWidth='md'
            >
                <div className='flex justify-end gap-2'>
                    <Typography sx={{ flex: '1 1 100%' }}>
                        <DialogTitle>
                            Add Location
                        </DialogTitle>
                    </Typography>
                    <Button variant="text" onClick={handleClose}>
                        <ClearRoundedIcon />
                    </Button>
                </div>
                <DialogContent>
                    <Box
                        sx={{
                            display: 'flex',
                            flexWrap: 'wrap',
                            flexDirection: 'column',
                            m: 'auto',
                            width: 'fit-content',
                            borderRadius: 1,
                        }}
                    >

                        <MapPage setFieldValues={setFieldValue} />

                        <LocationInput
                            handleChange={handleChange}
                            handleSubmit={handleSubmit}
                            values={values}
                            errors={errors}
                        />
                    </Box>
                </DialogContent>
            </Dialog>
            <MuiSnackbar message={checkErrorAlert.message} type={checkErrorAlert.type} open={alert} setOpen={handleResetAlert} />
        </div>
    )
}

export default LocationPopupDialog;