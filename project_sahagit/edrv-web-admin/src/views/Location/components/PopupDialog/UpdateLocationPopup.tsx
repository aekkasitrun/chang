import IconButton from '@mui/material/IconButton';
import EditIcon from '@mui/icons-material/Edit';
import React from "react";
import { UpdateLocationDTO } from '../DTO/UpdateLocationDTO';
import useLocation from '../../hooks/location';

import Dialog from '@mui/material/Dialog';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';

import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import ClearRoundedIcon from '@mui/icons-material/ClearRounded';
import LocationInputUpdate from '../LocationInputUpdate';

import * as Yup from "yup";
import { FormikHelpers, useFormik } from "formik";
import MuiSnackbar from '@/components/SnackBar';
import MapPage from '../Map';

interface UpdateLocationProps {
    locationId: string;
    latitude: number;
    longitude: number;
    streetAndNumberProps: string;
    postalCodeProps: string;
    cityProps: string;
    stateProps: string;
    countryProps: string;
    radiusProps?: number;
    operatorProps: string;
}

const validateSchema = Yup.object().shape({
    // active: Yup.bool(),
    // address: {
    //     streetAndNumber: Yup.string().required("Content cannot be empty."),
    //     city: Yup.string().required("Content cannot be empty."),
    //     country: Yup.string().required("Content cannot be empty."),
    //     postalCode: Yup.string().required("Content cannot be empty."),
    //     state: Yup.string().required("Content cannot be empty."),
    // },
    // id: Yup.array().required("Content cannot be empty."),
    // id: Yup.string().required("Content cannot be empty."),
    // timezone: Yup.string().required("Content cannot be empty."),

    //ที่ add location กับ folder hooks ไม่ได้มีปัญหาตรง yup น่าจะต้อง check ข้อมูลตาม DTO ให้หมดซึ่งตอนนี้ยัง error อยู่

});

export default function LocationUpdatePopupDialog(props: UpdateLocationProps) {

    const { locationId, latitude, longitude, streetAndNumberProps, cityProps, stateProps, countryProps, postalCodeProps, radiusProps, operatorProps } = props;

    const [alert, setAlert] = React.useState(false);

    const handleResetAlert = () => {
        setAlert(false)
    }

    const initLocation: UpdateLocationDTO = {
        id: locationId,
        coordinates: [latitude, longitude],
        operatorName: operatorProps,
        address: {
            streetAndNumber: streetAndNumberProps,
            postalCode: postalCodeProps,
            city: cityProps,
            state: stateProps,
            country: countryProps,
        },
        meta_data: {
            radius: radiusProps,
        },

    }
    // console.log(locationId)
    const { handleUpdateLocation, checkErrorAlert } = useLocation();

    const { values, handleChange, handleSubmit, setFieldValue } = useFormik({
        initialValues: initLocation,
        validationSchema: validateSchema,
        validateOnChange: false, // if true the form will validate the value every time the onChange is triggered
        onSubmit: handleSubmitLocation,

    });
    async function handleSubmitLocation(value: UpdateLocationDTO, formikHelper: FormikHelpers<UpdateLocationDTO>) {
        console.log(value);
        await handleUpdateLocation(value)
        formikHelper.resetForm({ values: initLocation });
        setAlert(true)
    }

    const [open, setOpen] = React.useState(false);
    const handleClickOpen = () => {
        setOpen(true);
    };
    const handleClose = () => {
        setOpen(false);
    };

    return (
        <div>
            <IconButton aria-label="edit" onClick={handleClickOpen}>
                <EditIcon />
            </IconButton>
            <Dialog
                open={open}
                onClose={handleClose}
                fullWidth={true}
                maxWidth='md'>
                <div className='flex justify-end gap-2'>
                    <Typography sx={{ flex: '1 1 100%' }}>
                        <DialogTitle>
                            Edit Location : {
                                streetAndNumberProps + " " +
                                cityProps + " " +
                                stateProps + " " +
                                countryProps + " " +
                                postalCodeProps
                            }
                        </DialogTitle>
                    </Typography>
                    <Button variant="text" onClick={handleClose}>
                        <ClearRoundedIcon />
                    </Button>
                </div>
                <DialogContent>
                    <Box
                        sx={{
                            display: 'flex',
                            flexWrap: 'wrap',
                            flexDirection: 'column',
                            m: 'auto',
                            width: 'fit-content',
                            borderRadius: 1,
                        }}
                    >

                        <MapPage setFieldValues={setFieldValue} />

                        <LocationInputUpdate
                            handleChange={handleChange}
                            handleSubmit={handleSubmit}
                            values={values}
                        />
                    </Box>
                </DialogContent>
            </Dialog>
            <MuiSnackbar message={checkErrorAlert.message} type={checkErrorAlert.type} open={alert} setOpen={handleResetAlert} />
        </div>
    )
}