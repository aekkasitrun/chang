export interface UpdateLocationDTO {
  id: string;
  operatorName: string;
  address: AddressLocation;
  coordinates: [number, number];
  meta_data: Radius;
  // timezone: string;
}

export interface UpdateSwitchDTO {
  id: string;
  active: boolean;
}

interface AddressLocation {
  streetAndNumber: string;
  postalCode: string;
  city: string;
  state: string;
  country: string;
}
interface Radius {
  radius?: number;
}
