// import { Radius } from "./object/radius";

export interface LocationDTO {
  active: boolean;
  operatorName: string;
  address: AddressLocation;
  coordinates: [number, number];
  meta_data: Radius;
  timezone: string;
}
interface AddressLocation {
  streetAndNumber: string;
  postalCode: string;
  city: string;
  state: string;
  country: string;
}
interface Radius {
  radius: number;
}
