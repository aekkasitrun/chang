import * as React from 'react';
import Button from "@/components/Buttons/Button";
import { FormikHandlers, FormikValues } from "formik";
// import React from "react";
import { UpdateLocationDTO } from './DTO/UpdateLocationDTO';
import { Input } from "@/components/Form";
import { Divider } from '@mui/material';


interface LocationInputProps {
    handleSubmit: FormikHandlers["handleSubmit"];
    handleChange: FormikHandlers["handleChange"];
    values: UpdateLocationDTO;
}

const LocationInputUpdate: React.FC<LocationInputProps> = ({
    handleChange,
    handleSubmit,
    values,
}) => {
    return (
        <div>
            <form onSubmit={handleSubmit}>

                {/* <h3>ที่อยู่</h3> */}
                <br />
                <div className="flex justify-center">
                    <Input
                        sx={{ m: 1, width: '28.5ch' }}
                        name="coordinates[0]"
                        placeholder="กรุณาใส่ละติจูดสถานที่"
                        label="ละติจูด"
                        onChange={handleChange}
                        value={values.coordinates[0]}
                    />
                    <Input
                        sx={{ m: 1, width: '28.5ch' }}
                        name="coordinates[1]"
                        placeholder="กรุณาใส่ลองติจูดสถานที่"
                        label="ลองติจูด"
                        onChange={handleChange}
                        value={values.coordinates[1]}
                    />

                </div>

                <Input
                    sx={{ m: 1, pl: 0.5, width: '29ch' }}
                    name="meta_data.radius"
                    placeholder="Type your radius here."
                    label="radius"
                    onChange={handleChange}
                    value={values.meta_data.radius}
                />

                <Divider variant="middle" sx={{ m: 3 }} />

                <div className='flex justify-center'>
                    <Input
                        sx={{ m: 1, width: '15ch' }}
                        name="address.streetAndNumber"
                        placeholder="กรุณาใส่เลขที่อยู่ของคุณ"
                        label="เลขที่"
                        onChange={handleChange}
                        value={values.address.streetAndNumber}
                    />
                    <Input
                        sx={{ m: 1, width: '15ch' }}
                        name="address.city"
                        placeholder="กรุณาใส่ตำบลของคุณ"
                        label="ตำบล"
                        onChange={handleChange}
                        value={values.address.city}
                    />
                    <Input
                        sx={{ m: 1, width: '25ch' }}
                        name="address.state"
                        placeholder="กรุณาใส่อำเภอของคุณ"
                        label="อำเภอ"
                        onChange={handleChange}
                        value={values.address.state}
                    />
                </div>
                <div className="flex justify-center">
                    <Input
                        sx={{ m: 1, width: '28.5ch' }}
                        name="address.country"
                        placeholder="กรุณาใส่จังหวัดของคุณ"
                        label="จังหวัด"
                        onChange={handleChange}
                        value={values.address.country}
                    />
                    <Input
                        sx={{ m: 1, width: '28.5ch' }}
                        name="address.postalCode"
                        placeholder="กรุณาใส่รหะสไปรษณีย์"
                        label="รหัสไปรษณีย์"
                        onChange={handleChange}
                        value={values.address.postalCode}
                    />
                </div>
                <Divider variant="middle" sx={{ m: 3 }} />
                <div>
                    <Input
                        sx={{ m: 1, width: '60ch' }}
                        name="operatorName"
                        placeholder="Type your operatorName here."
                        label="operatorName"
                        onChange={handleChange}
                        value={values.operatorName}
                    />
                </div>
                {/* <TextField
                    sx={{ m: 1, width: '25ch' }}
                    name="timezone"
                    placeholder="Type your timezone here."
                    label="timezone"
                    onChange={handleChange}
                    value={values.timezone}
                /> */}
                <div className="flex justify-center">
                    <Button
                        className="h-full"
                        size="small"
                        variant="contained"
                        color="primary"
                        type="submit"
                    >
                        Submit
                    </Button>
                </div>


            </form>
        </div>
    )
}

export default LocationInputUpdate;