import React from "react";
import { UpdateSwitchDTO } from "../DTO/UpdateLocationDTO";
import Switch from '@mui/material/Switch';
import useLocation from "../../hooks/location";
import MuiSnackbar from "@/components/SnackBar";
import { styled } from '@mui/material/styles';
import { SwitchProps } from '@mui/material/Switch';


interface UpdateLocationSwitchProps {
    locationId: string;
    locationActive: boolean;
}

const IOSSwitch = styled((props: SwitchProps) => (
    <Switch focusVisibleClassName=".Mui-focusVisible" disableRipple {...props} />
))(({ theme }) => ({
    width: 42,
    height: 26,
    padding: 0,
    '& .MuiSwitch-switchBase': {
        padding: 0,
        margin: 2,
        transitionDuration: '300ms',
        '&.Mui-checked': {
            transform: 'translateX(16px)',
            color: '#fff',
            '& + .MuiSwitch-track': {
                backgroundColor: theme.palette.mode === 'dark' ? '#2ECA45' : '#65C466',
                opacity: 1,
                border: 0,
            },
            '&.Mui-disabled + .MuiSwitch-track': {
                opacity: 0.5,
            },
        },
        '&.Mui-focusVisible .MuiSwitch-thumb': {
            color: '#33cf4d',
            border: '6px solid #fff',
        },
        '&.Mui-disabled .MuiSwitch-thumb': {
            color:
                theme.palette.mode === 'light'
                    ? theme.palette.grey[100]
                    : theme.palette.grey[600],
        },
        '&.Mui-disabled + .MuiSwitch-track': {
            opacity: theme.palette.mode === 'light' ? 0.7 : 0.3,
        },
    },
    '& .MuiSwitch-thumb': {
        boxSizing: 'border-box',
        width: 22,
        height: 22,
    },
    '& .MuiSwitch-track': {
        borderRadius: 26 / 2,
        backgroundColor: theme.palette.mode === 'light' ? '#E9E9EA' : '#39393D',
        opacity: 1,
        transition: theme.transitions.create(['background-color'], {
            duration: 500,
        }),
    },
}));

export default function LocationUpdateSwitch(props: UpdateLocationSwitchProps) {

    const { locationId, locationActive } = props;

    const [checked, setChecked] = React.useState(locationActive);
    const { handleUpdateLocationSwitch, checkErrorAlert } = useLocation();

    const [open, setOpen] = React.useState(false);
    const handleResetAlert = () => {
        setOpen(false)
    }

    const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setChecked(event.target.checked);
        console.log(initLocation);
        // handleUpdateLocationSwitch(initLocation);
        updateSwitch()
    };

    async function updateSwitch() {
        await handleUpdateLocationSwitch(initLocation);
        // console.log("Chenfggggg");
        setOpen(true)
    }

    const initLocation: UpdateSwitchDTO = {
        id: locationId,
        active: !checked
    }
    return (
        <div>
            <IOSSwitch
                checked={checked}
                onChange={handleChange}
                inputProps={{ 'aria-label': 'controlled' }}
            />
            <MuiSnackbar message={checkErrorAlert.message} type={checkErrorAlert.type} open={open} setOpen={handleResetAlert} />
        </div>
    )
}