import * as React from 'react';
import Button from "@/components/Buttons/Button";
import { FormikErrors, FormikHandlers, FormikValues } from "formik";
import { LocationDTO } from "./DTO/LocationDTO";
import TextField from '@mui/material/TextField';
import { Divider } from '@mui/material';
import { Input } from "@/components/Form";
import MenuItem from '@mui/material/MenuItem';
import * as moment from 'moment-timezone';

interface LocationInputProps {
    handleSubmit: FormikHandlers["handleSubmit"];
    handleChange: FormikHandlers["handleChange"];
    values: LocationDTO;
    errors: FormikErrors<LocationDTO>
}

const LocationInput: React.FC<LocationInputProps> = ({
    handleChange,
    handleSubmit,
    values,
    errors,
}) => {

    // if (1) {
    //     console.log("true");

    // } else {
    //     console.log("false");

    // }

    const timezones = moment.tz.names(); // Get a list of all timezones
    // console.log('ErrorLocation: ' + JSON.stringify(errors));

    return (

        <div>
            <form onSubmit={handleSubmit}>
                <br />
                <div className="flex justify-center">
                    <Input
                        sx={{ m: 1, width: '28.5ch' }}
                        name="coordinates[0]"
                        placeholder="กรุณาใส่ละติจูดสถานที่"
                        label="ละติจูด"
                        onChange={handleChange}
                        value={values.coordinates[0]}
                        error={!!errors.coordinates}
                    />
                    <Input
                        sx={{ m: 1, width: '28.5ch' }}
                        name="coordinates[1]"
                        placeholder="กรุณาใส่ลองติจูดสถานที่"
                        label="ลองติจูด"
                        onChange={handleChange}
                        value={values.coordinates[1]}
                        error={!!errors.coordinates}
                    />

                </div>

                <Input
                    sx={{ m: 1, pl: 0.5, width: '29ch' }}
                    name="meta_data.radius"
                    placeholder="Type your radius here."
                    label="radius"
                    onChange={handleChange}
                    value={values.meta_data.radius}
                    error={!!errors.meta_data?.radius}
                />

                <Input
                    sx={{ m: 1, width: '28.5ch' }}
                    select
                    name="timezone"
                    // placeholder="Select timezone of the location."
                    label="Timezone"
                    onChange={handleChange}
                    value={values.timezone}
                    error={!!errors.timezone}
                >
                    {timezones.map((timezone: string) => (
                        <MenuItem key={timezone} value={timezone}>
                            {timezone}
                        </MenuItem>
                    ))}
                </Input>

                <Divider variant="middle" sx={{ m: 3 }} />

                <div className='flex justify-center'>
                    <Input
                        sx={{ m: 1, width: '15ch' }}
                        name="address.streetAndNumber"
                        placeholder="กรุณาใส่เลขที่อยู่ของคุณ"
                        label="เลขที่"
                        onChange={handleChange}
                        value={values.address.streetAndNumber}
                        error={!!errors.address?.streetAndNumber}
                    />
                    <Input
                        sx={{ m: 1, width: '15ch' }}
                        name="address.city"
                        placeholder="กรุณาใส่ตำบลของคุณ"
                        label="ตำบล"
                        onChange={handleChange}
                        value={values.address.city}
                        error={!!errors.address?.city}
                    />
                    <Input
                        sx={{ m: 1, width: '25ch' }}
                        name="address.state"
                        placeholder="กรุณาใส่อำเภอของคุณ"
                        label="อำเภอ"
                        onChange={handleChange}
                        value={values.address.state}
                        error={!!errors.address?.state}
                    />
                </div>
                <div className="flex justify-center">
                    <Input
                        sx={{ m: 1, width: '28.5ch' }}
                        name="address.country"
                        placeholder="กรุณาใส่จังหวัดของคุณ"
                        label="จังหวัด"
                        onChange={handleChange}
                        value={values.address.country}
                        error={!!errors.address?.country}
                    />
                    <Input
                        sx={{ m: 1, width: '28.5ch' }}
                        name="address.postalCode"
                        placeholder="กรุณาใส่รหะสไปรษณีย์"
                        label="รหัสไปรษณีย์"
                        onChange={handleChange}
                        value={values.address.postalCode}
                        error={!!errors.address?.postalCode}
                    />
                </div>

                <Divider variant="middle" sx={{ m: 3 }} />

                <div className='grid grid-cols-1'>
                    <Input
                        sx={{ m: 1, width: '60ch' }}
                        name="operatorName"
                        placeholder="Type your operatorName here."
                        label="operatorName"
                        onChange={handleChange}
                        value={values.operatorName}
                        error={!!errors?.operatorName}
                    // error={typeof errors?.operatorName !== undefined}
                    />
                    {/* {errors ?
                        <span className='text-rose-500'>{errors.operatorName}</span>
                        :
                        <></>
                    } */}
                </div>

                <div className="flex justify-center">
                    <Button
                        className="h-full"
                        size="small"
                        variant="contained"
                        color="primary"
                        type="submit"
                    >
                        Submit
                    </Button>
                </div>
            </form>
        </div>
    )
}

export default LocationInput;
