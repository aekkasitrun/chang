import { FormikHelpers } from "formik";
import { useEffect } from "react";
import LocationOnIcon from '@mui/icons-material/LocationOn';

declare global {
    interface Window {
        initMap?: () => void;
    }
}

// interface getlocationPosition {
//     latitude: number
//     logitude: number
//}

interface MapPageProps {
    setFieldValues: FormikHelpers<any>['setFieldValue']
}

const MapPage: React.FC<MapPageProps> = ({ setFieldValues }) => {

    function initMap() {
        const myLatlng = { lat: 7.0093498808425005, lng: 100.50431644144324 };

        const map = new google.maps.Map(document.getElementById("map")!, {
            zoom: 4,
            center: myLatlng,
        });

        const marker = new google.maps.Marker({
            position: myLatlng,
            map: map,

        });

        let infoWindow = new google.maps.InfoWindow({
            content: "Click the map to get Lat/Lng!",
            position: myLatlng,
        });



        // infoWindow.open(map);
        map.panTo(myLatlng);



        map.addListener("click", (mapsMouseEvent: any) => {
            // infoWindow.close();

            marker.setPosition(mapsMouseEvent.latLng);

            infoWindow = new google.maps.InfoWindow({
                position: mapsMouseEvent.latLng,
            });
            infoWindow.setContent(
                JSON.stringify(mapsMouseEvent.latLng.toJSON(), null, 2)
            );



            const { lat } = mapsMouseEvent.latLng.toJSON();
            const { lng } = mapsMouseEvent.latLng.toJSON();

            setFieldValues("coordinates", [lat, lng]);

            console.log("latitude = " + lat);
            console.log("longitude = " + lng);

            // infoWindow.open(map);
            // map.panTo(myLatlng);

        });

        marker.addListener("click", () => {
            map.setZoom(16);
            map.setCenter(marker.getPosition() as google.maps.LatLng);
        });


    }


    useEffect(() => {
        window.initMap = initMap;
        const googleMapScript = document.createElement("script");
        // console.log(process.env.NEXT_PUBLIC_GOOGLE_MAPS_API_KEY);
        googleMapScript.src = `https://maps.googleapis.com/maps/api/js?key=${process.env.NEXT_PUBLIC_GOOGLE_MAPS_API_KEY}&callback=initMap&v=weekly`;
        //จะใช้ env กับหน้าบ้านให้เขียนชื่อใน env เริ่มด้วย NEXT_PUBLIC_...
        googleMapScript.async = true;
        document.body.appendChild(googleMapScript);

        return () => {
            // Remove the script and the global function
            googleMapScript.remove();
            delete window.initMap;
        };
    }, []);

    return <div id="map" style={{ height: "50vh" }} />;
};

export default MapPage;
