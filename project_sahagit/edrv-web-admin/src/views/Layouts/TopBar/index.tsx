import { IconButton, Toolbar } from "@mui/material";
import React from "react";
import { styled } from "@mui/material/styles";
import MuiAppBar, { AppBarProps as MuiAppBarProps } from "@mui/material/AppBar";
import Typography from "@mui/material/Typography";
import MenuIcon from "@mui/icons-material/Menu";
import { DRAWER_WIDTH } from "../../../components/Drawer/constant";
import Link from 'next/link';
import Button from '@mui/material/Button';
import { useSession, signIn, signOut, getSession } from "next-auth/react"
import Avatar from '@mui/material/Avatar';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';

interface AppBarProps extends MuiAppBarProps {
  open?: boolean;
}

const AppBar = styled(MuiAppBar, {
  shouldForwardProp: (prop) => prop !== "open",
})<AppBarProps>(({ theme, open }) => ({
  zIndex: theme.zIndex.drawer + 1,
  transition: theme.transitions.create(["width", "margin"], {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  ...(open && {
    marginLeft: DRAWER_WIDTH,
    width: `calc(100% - ${DRAWER_WIDTH}px)`,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  }),
}));

const TopBar = ({ handleDrawerOpen, open }: any) => {

  const [anchorElUser, setAnchorElUser] = React.useState<null | HTMLElement>(null);
  const handleOpenUserMenu = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorElUser(event.currentTarget);
  };
  const handleCloseUserMenu = () => {
    setAnchorElUser(null);
  };

  const { data: session } = useSession()
  // console.log("Session Data = " + JSON.stringify(session?.user));

  return (
    <AppBar position="fixed" open={open} className='bg-[#0DA9B8]'>
      <Toolbar>
        <IconButton
          color="inherit"
          aria-label="open drawer"
          onClick={handleDrawerOpen}
          edge="start"
          sx={{
            marginRight: 5,
            ...(open && { display: "none" }),
          }}
        >
          <MenuIcon />
        </IconButton>
        <Typography variant="h6" noWrap component="div" sx={{ flexGrow: 1 }}>
          <Link href="/">
            EDRV Web Provider
          </Link>
        </Typography>
        {session ?
          <div>
            <div className="flex gap-2">
              <div className="pt-1">
                <Typography variant="h6" noWrap component="div">{session?.user?.name}</Typography>
              </div>
              <IconButton onClick={handleOpenUserMenu} sx={{ p: 0 }}>
                <Avatar src={session?.user?.image ?? undefined} />
              </IconButton>
            </div>
            <Menu
              sx={{ mt: '45px' }}
              id="menu-appbar"
              anchorEl={anchorElUser}
              anchorOrigin={{
                vertical: 'top',
                horizontal: 'right',
              }}
              keepMounted
              transformOrigin={{
                vertical: 'top',
                horizontal: 'right',
              }}
              open={Boolean(anchorElUser)}
              onClose={handleCloseUserMenu}
            >

              <MenuItem onClick={handleCloseUserMenu}>
                <Typography textAlign="center" onClick={() => signOut()}>Logout</Typography>
              </MenuItem>

            </Menu>
          </div>
          :
          <Button color="inherit">
            <Link href="/signin">
              Login
            </Link>
          </Button>
        }


      </Toolbar>
    </AppBar>
  );
};

export default TopBar;

// import { GetServerSideProps } from 'next'

// export const getServerSideProps: GetServerSideProps = async ({ req }) => {
//   const session = await getSession({ req })

//   if (!session) {
//     return {
//       redirect: {
//         destination: '/signin',
//         permanent: false
//       }
//     }
//   }

//   return {
//     props: { session }
//   }
// }
