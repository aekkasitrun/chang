import React, { JSXElementConstructor, ReactElement } from "react";
import { useTheme } from "@mui/material/styles";
import Box from "@mui/material/Box";
import TopBar from "./TopBar";
import Sidebar from "./SideBar";
import DrawerHeader from "../../components/Drawer/DrawerHeader/DrawerHeader";
import Head from "next/head";

interface IPageLayout {
  children: React.ReactNode;
}

const MainLayout = ({ children }: IPageLayout) => {
  const theme = useTheme();
  const [open, setOpen] = React.useState(false);

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  return (
    <>
      <Head>
        <title>Page title</title>
      </Head>

      <Box className="flex" >
        <TopBar handleDrawerOpen={handleDrawerOpen} open={open} />
        <Sidebar
          theme={theme}
          handleDrawerClose={handleDrawerClose}
          open={open}
        />
        <Box component="main" className='grow p-6 '>
          <DrawerHeader />

          {children}
        </Box>
      </Box>
    </>
  );
};

type DefaultLayoutProps = ReactElement<
  any,
  string | JSXElementConstructor<any>
>;

export function DefaultLayout(page: DefaultLayoutProps) {
  return <MainLayout>{page}</MainLayout>;
}

export default MainLayout;
