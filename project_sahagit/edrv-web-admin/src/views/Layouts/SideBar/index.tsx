import React from "react";
import IconButton from "@/components/Buttons/IconButton";
import ChevronLeftIcon from "@mui/icons-material/ChevronLeft";
import ChevronRightIcon from "@mui/icons-material/ChevronRight";
import Drawer from "@/components/Drawer";
import DrawerHeader from "../../../components/Drawer/DrawerHeader/DrawerHeader";
import MuiList from "@/components/List";
import { Divider, Theme } from "@mui/material";
import { menu } from "./menu";
import { DRAWER_WIDTH } from "@/components/Drawer/constant";
import Box from '@mui/material/Box';

interface SidebarProps {
  theme: Theme;
  handleDrawerClose: () => void;
  open: boolean;
}

const Sidebar: React.FC<SidebarProps> = ({
  theme,
  handleDrawerClose,
  open,
}: any) => {
  return (
    <Drawer variant="permanent" open={open} >
      {/* <Box className='bg-red-200' component="div"> */}
      <DrawerHeader>
        <IconButton onClick={handleDrawerClose}>
          {theme.direction === "rtl" ? (
            <ChevronRightIcon />
          ) : (
            <ChevronLeftIcon />
          )}
        </IconButton>
      </DrawerHeader>
      <Divider />
      <MuiList menuList={menu} open={open} />
      <Divider />
      {/* </Box> */}
    </Drawer>
  );
};

export default Sidebar;
