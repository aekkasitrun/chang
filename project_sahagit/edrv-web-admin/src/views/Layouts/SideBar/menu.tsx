import { Menu } from "@/components/List";
import { InfoOutlined, Home, Note, Pets } from "@mui/icons-material";

import DashboardCustomizeRoundedIcon from '@mui/icons-material/DashboardCustomizeRounded';
import LocationOnRoundedIcon from '@mui/icons-material/LocationOnRounded';
import EvStationRoundedIcon from '@mui/icons-material/EvStationRounded';
import DescriptionIcon from '@mui/icons-material/Description';
import NearMeIcon from '@mui/icons-material/NearMe';
import RequestQuoteIcon from '@mui/icons-material/RequestQuote';
import PeopleIcon from '@mui/icons-material/People';
import AutoGraphIcon from '@mui/icons-material/AutoGraph';
import PaymentsIcon from '@mui/icons-material/Payments';

export const menu: Menu[] = [
  {
    titleIndex: "dashboard",
    url: "/",
    title: "หน้าหลัก",
    icon: <DashboardCustomizeRoundedIcon />,
  },

  {
    titleIndex: "location",
    url: "/location",
    title: "สถานที่",
    icon: <LocationOnRoundedIcon />,
  },

  {
    titleIndex: "chargeStation",
    url: "/chargestation",
    title: "สถานีชาร์จ",
    icon: <EvStationRoundedIcon />,
  },

  {
    titleIndex: "rates",
    url: "/rates",
    title: "Rates",
    icon: <RequestQuoteIcon />,
  },

  {
    titleIndex: "transaction",
    url: "/transaction",
    title: "ประวัติ",
    icon: <DescriptionIcon />,
  },

  {
    titleIndex: "usage",
    url: "/usage",
    title: "สรุปรายวัน",
    icon: <AutoGraphIcon />,
  },

  // {
  //   titleIndex: "payment",
  //   url: "/payment",
  //   title: "Payment",
  //   icon: <PaymentsIcon />,
  // },

  // {
  //   titleIndex: "users",
  //   url: "/users",
  //   title: "Users",
  //   icon: <PeopleIcon />,
  // },

  // {
  //   titleIndex: "rfids",
  //   url: "/rfids",
  //   title: "RFIDs",
  //   icon: <NearMeIcon />,
  // },



  // {
  //   titleIndex: "home",
  //   url: "/",
  //   title: "Home",
  //   icon: <Home />,
  //   children: [
  //     { titleIndex: "home2", url: "/", title: "HOME2", icon: <Note /> },
  //   ],
  // },

  // {
  //   titleIndex: "example",
  //   url: "/example",
  //   title: "Example",
  //   icon: <InfoOutlined />,
  //   children: [
  //     {
  //       titleIndex: "todo",
  //       url: "/example/todo",
  //       title: "Todo",
  //       icon: <Note />,
  //     },
  //     {
  //       titleIndex: "pokemon",
  //       url: "/example/pokemon",
  //       title: "Pokemon",
  //       icon: <Pets />,
  //     },
  //   ],
  // },
];
