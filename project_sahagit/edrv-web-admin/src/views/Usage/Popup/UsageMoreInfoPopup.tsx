import IconButton from '@mui/material/IconButton';
import React from "react";
import Dialog from '@mui/material/Dialog';
import Typography from '@mui/material/Typography';
import { TransitionProps } from '@mui/material/transitions';
import Slide from '@mui/material/Slide';
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import CloseIcon from '@mui/icons-material/Close';
import { Button } from '@mui/material';
import Paper from '@mui/material/Paper';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import { useGetByDateUsageServiceQuery } from '@/services/usage.service';
import Box from '@mui/material/Box';
import Alert from '@mui/material/Alert';
import AlertTitle from '@mui/material/AlertTitle';
import Stack from '@mui/material/Stack';
import LinearProgress from '@mui/material/LinearProgress';

import { useState } from 'react';
import TablePagination, { TablePaginationProps } from '@mui/material/TablePagination';
import UsageChartComponent from '../Graph';
import formatCost from '@/utils/formatCost';

interface UsageProps {
    usageProps: string;
}

const Transition = React.forwardRef(function Transition(
    props: TransitionProps & {
        children: React.ReactElement;
    },
    ref: React.Ref<unknown>,
) {
    return <Slide direction="up" ref={ref} {...props} />;
});

export default function UsageInfo(props: UsageProps) {
    const { usageProps } = props;

    const parts = usageProps.split("/");
    const finalDate = `${parts[0]}-${parts[1]}-${parts[2]}`;
    console.log('FinalDate = ' + finalDate);

    const [open, setOpen] = React.useState(false);
    const handleClickOpen = () => {
        setOpen(true);
    };
    const handleClose = () => {
        setOpen(false);
    };

    const usageQueryByDate: any = useGetByDateUsageServiceQuery(finalDate)

    const rowsPerPageOptions = [5, 10, 25];
    const [rowsPerPage, setRowsPerPage] = useState(rowsPerPageOptions[0]);

    const [page, setPage] = useState(0);

    const handleChangePage = (_: React.MouseEvent<HTMLButtonElement> | null, newPage: number) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement>) => {
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(0);
    };

    // console.log(('Usage = ' + JSON.stringify(usageQueryByDate)));

    if (usageQueryByDate.isLoading) return <div>
        <Box sx={{ width: '100%' }}>
            <LinearProgress />
        </Box>
    </div>
    if (usageQueryByDate.isError) return <div>
        <Stack sx={{ width: '100%' }} spacing={2}>
            <Alert severity="error">
                <AlertTitle>Error</AlertTitle>
                Data Fetching failed — <strong>check it out!</strong>
            </Alert>
        </Stack>
    </div>

    // console.log(JSON.stringify(usageQueryByDate));


    return (
        <div>
            <Button aria-label="edit" onClick={handleClickOpen} variant="outlined" style={{ width: '120px', height: '40px' }}>
                {/* <GroupWorkOutlinedIcon /> */}
                <Typography className='font-bold'>รายละเอียด</Typography>
            </Button>
            <Dialog
                fullScreen
                open={open}
                onClose={handleClose}
                TransitionComponent={Transition}
            >
                <AppBar sx={{ position: 'relative' }} className='bg-[#56C3CD]'>
                    <Toolbar>
                        <Typography sx={{ ml: 2, flex: 1 }} variant="h6" component="div">
                            Summary Info
                        </Typography>
                        <IconButton
                            edge="start"
                            color="inherit"
                            onClick={handleClose}
                            aria-label="close"
                        >
                            <CloseIcon />
                        </IconButton>
                    </Toolbar>
                </AppBar>

                <Box sx={{ width: '100%', p: 10 }}>
                    <Paper sx={{ width: '100%', mb: 2 }}>
                        <Toolbar className='bg-[#1890FF1A] rounded-t-lg'>
                            <Typography sx={{ ml: 2, flex: 1 }} variant="h6" component="div" className='font-bold '>
                                Summary by Day : {usageProps}
                            </Typography>
                        </Toolbar>

                        <TableContainer component={Paper}>
                            <Table sx={{ minWidth: 1000 }} size="small" aria-label="a dense table">
                                <TableHead className='bg-[#D9D9D940]'>
                                    <TableRow>
                                        {/* <TableCell align="center">Status</TableCell> */}
                                        <TableCell align="center" className='text-sm font-semibold  text-black'>เลขที่</TableCell>
                                        <TableCell align="center" className='text-sm font-semibold  text-black'>ชื่อสถานี</TableCell>
                                        <TableCell align="center" className='text-sm font-semibold  text-black'>สถานที่</TableCell>
                                        <TableCell align="center" className='text-sm font-semibold  text-black'>จำนวนราคา</TableCell>

                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {/* {usageQueryByDate?.data */}
                                    {(rowsPerPage > 0
                                        ? usageQueryByDate?.data.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                                        : usageQueryByDate?.data
                                    ).map((usage: any) => {

                                        return (
                                            <TableRow key={usage?.transactionEdrvId}>
                                                <TableCell align="center" >{usage?.transactionEdrvId}</TableCell>
                                                <TableCell align="center" >{usage?.Connector?.ChargeStation?.name}</TableCell>
                                                <TableCell align="center" >{usage?.Connector?.ChargeStation?.Locations?.locationAddresses[0]?.streetAndNumber}</TableCell>
                                                <TableCell align="center" className='text-[#4FC63B] font-bold'>{formatCost(usage?.cost)}</TableCell>
                                            </TableRow>
                                        )
                                    })}
                                </TableBody>
                            </Table>
                            <TablePagination
                                rowsPerPageOptions={rowsPerPageOptions}
                                component="div"
                                count={usageQueryByDate?.data.length || 0}
                                rowsPerPage={rowsPerPage}
                                page={page}
                                onPageChange={handleChangePage}
                                onRowsPerPageChange={handleChangeRowsPerPage}
                            />
                        </TableContainer>
                        {/* <UsageChartComponent usageChartComponentProps={usageQueryByDate} /> */}
                    </Paper>
                </Box>

            </Dialog>
        </div>
    )
}

