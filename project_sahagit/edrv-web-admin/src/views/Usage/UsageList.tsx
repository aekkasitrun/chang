import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import React from "react";
import { useGetAllUsageServiceQuery } from '@/services/usage.service';
import Box from '@mui/material/Box';
import LinearProgress from '@mui/material/LinearProgress';
import Alert from '@mui/material/Alert';
import AlertTitle from '@mui/material/AlertTitle';
import Stack from '@mui/material/Stack';
import GroupWorkOutlinedIcon from '@mui/icons-material/GroupWorkOutlined';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import UsageInfo from './Popup/UsageMoreInfoPopup';

import { useState } from 'react';
import TablePagination, { TablePaginationProps } from '@mui/material/TablePagination';
// import TransactionInfo from './Popup/TransactionMoreInfoPopup';
import Divider from '@mui/material/Divider';
import UsageCardList from './CardSummaryList';
import formatCost from '@/utils/formatCost';

const UsageList = () => {
    const usageQuery = useGetAllUsageServiceQuery()

    const rowsPerPageOptions = [5, 10, 25];
    const [rowsPerPage, setRowsPerPage] = useState(rowsPerPageOptions[0]);

    const [page, setPage] = useState(0);

    const handleChangePage = (_: React.MouseEvent<HTMLButtonElement> | null, newPage: number) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement>) => {
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(0);
    };

    if (usageQuery.isLoading) return <div>
        <Box sx={{ width: '100%' }}>
            <LinearProgress />
        </Box>
    </div>
    if (usageQuery.isError) return <div>
        <Stack sx={{ width: '100%' }} spacing={2}>
            <Alert severity="error">
                <AlertTitle>Error</AlertTitle>
                Data Fetching failed — <strong>check it out!</strong>
            </Alert>
        </Stack>
    </div>
    return (
        <div>
            <Toolbar>
                <Typography sx={{ flex: '1 1 100%' }} variant="h6" className='text-2xl font-bold text-black'>
                    Summary
                </Typography>
            </Toolbar>
            <div className='pb-8'><Divider /></div>

            <UsageCardList />

            <TableContainer component={Paper}>
                <Table sx={{ minWidth: 1000 }} size="small" aria-label="a dense table">
                    <TableHead>
                        <TableRow className='bg-[#D9D9D940]'>
                            <TableCell align="center" className='text-base font-semibold text-black'>วันที่</TableCell>
                            <TableCell align="center" className='text-base font-semibold text-black'>จำนวนการใช้งาน</TableCell>
                            <TableCell align="center" className='text-base font-semibold text-black'>รายได้รวม</TableCell>
                            <TableCell align="center" className='text-base font-semibold text-black'>รายละเอียด</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody >
                        {(rowsPerPage > 0
                            ? usageQuery?.data.finalDate.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                            : usageQuery?.data.finalDate
                        ).map((finalDate: string, index: number) => (
                            <TableRow key={index}>
                                <TableCell align="center">
                                    {finalDate}
                                </TableCell>
                                <TableCell align="center">
                                    {usageQuery?.data?.count[index]}
                                </TableCell>
                                <TableCell align="center" className='text-[#4FC63B] font-bold'>
                                    {/* <Box style={{ maxWidth: '100%', width: 'auto', height: '50px', backgroundColor: '#4FC63B' }} className='flex justify-center items-center p-2 rounded-lg text-white font-bold '>
                                        {usageQuery?.data?.cost[index]?.toFixed(2)} บาท
                                    </Box> */}
                                    {formatCost(usageQuery?.data?.cost[index])} บาท
                                </TableCell>
                                <TableCell align="center">
                                    <UsageInfo usageProps={finalDate} />
                                </TableCell>
                            </TableRow>
                        ))}

                    </TableBody>
                </Table>
                <TablePagination
                    rowsPerPageOptions={rowsPerPageOptions}
                    component="div"
                    count={usageQuery?.data?.finalDate.length || 0}
                    rowsPerPage={rowsPerPage}
                    page={page}
                    onPageChange={handleChangePage}
                    onRowsPerPageChange={handleChangeRowsPerPage}
                />
            </TableContainer>
        </div>
    )
}

export default UsageList;