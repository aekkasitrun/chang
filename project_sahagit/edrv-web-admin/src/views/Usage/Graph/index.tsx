import { Bar, BarChart, LineChart, Line, AreaChart, Area, XAxis, YAxis, CartesianGrid, Tooltip, Legend } from 'recharts';
import { useGetAllUsageServiceQuery } from '@/services/usage.service';
import Box from '@mui/material/Box';
import LinearProgress from '@mui/material/LinearProgress';
import Alert from '@mui/material/Alert';
import AlertTitle from '@mui/material/AlertTitle';
import Stack from '@mui/material/Stack';
import Paper from '@mui/material/Paper';
import React, { FunctionComponent } from "react";

interface UsageChartComponentProps {
    usageChartComponentProps: any;
}


const UsageChartComponent: React.FC<UsageChartComponentProps> = ({ usageChartComponentProps }) => {

    const chartData = usageChartComponentProps?.data.map((chargeStation: any, index: number) => ({
        xAxis: chargeStation.Connector.ChargeStation.name,
        yAxis: chargeStation.cost.toFixed(2),
        yscale: 20
    }));

    // const costs: number[] = usageQuery?.data?.cost ?? [];
    // const maxCost: number = costs.reduce((prev, curr) => Math.max(prev, curr), 0);
    // const chartData = usageQuery?.data?.finalDate.map((finalDate: string, index: number) => ({
    //     date: `${finalDate.split("/")[0]}/${finalDate.split("/")[1]}`,
    //     cost: usageQuery?.data?.cost[index]?.toFixed(2),
    //     max: maxCost > 0 ? (usageQuery?.data?.cost[index] ?? 0) / maxCost * 20 : 0,
    //     yscale: maxCost + 2
    // }));

    return (

        <div >
            <p className='py-4 pl-2' > กราฟสรุปยอดขาย 7 วันล่าสุด</p >
            <BarChart width={600} height={300} data={chartData} >
                <CartesianGrid stroke="#eee" />
                <XAxis dataKey="xAxis" />
                <YAxis dataKey="yscale" />
                <Tooltip />
                <Bar dataKey="yAxis" fill="#8884d8" strokeWidth={2} />
                <Legend />
            </BarChart>
        </div>

    );
};
export default UsageChartComponent;
