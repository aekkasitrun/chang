import * as React from 'react';

import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import Grid from '@mui/material/Grid';
import { useGetUsageSummaryDashboardServiceQuery } from '@/services/usage.service';
import Box from '@mui/material/Box';
import Alert from '@mui/material/Alert';
import AlertTitle from '@mui/material/AlertTitle';
import Stack from '@mui/material/Stack';
import LinearProgress from '@mui/material/LinearProgress';
import AccountBalanceWalletIcon from '@mui/icons-material/AccountBalanceWallet';
import EvStationIcon from '@mui/icons-material/EvStation';
import ElectricCarIcon from '@mui/icons-material/ElectricCar';
import formatConsumption from '@/utils/formatConsumption';
import formatCost from '@/utils/formatCost';

const UsageCardList = () => {

    const usageDashboardQuery = useGetUsageSummaryDashboardServiceQuery()

    if (usageDashboardQuery.isLoading) return <div>
        <Box sx={{ width: '100%' }}>
            <LinearProgress />
        </Box>
    </div>
    if (usageDashboardQuery.isError) return <div>
        <Stack sx={{ width: '100%' }} spacing={2}>
            <Alert severity="error">
                <AlertTitle>Error</AlertTitle>
                Data Fetching failed — <strong>check it out!</strong>
            </Alert>
        </Stack>
    </div>

    return (
        <div className='grid grid-cols-1 tablet:grid-cols-1 laptop:grid-cols-2 desktop:grid-cols-3 gap-4 pb-8'>

            <Card sx={{ height: 150, }}  >
                <CardContent>
                    <div className='grid grid-rows-3 grid-flow-col pl-6'>

                        <Box style={{ width: '120px', height: '120px', backgroundColor: '#FCCB4D' }} className='rounded-full p-6 row-span-3'>
                            <AccountBalanceWalletIcon style={{ width: '70px', height: '70px' }} sx={{ color: 'white' }} />
                        </Box>

                        <div className='col-span-2 text-2xl font-bold text-black pt-6'>รายได้รวมทั้งหมด</div>
                        <div className='row-span-2 col-span-2 text-2xl font-bold text-black pl-12'>{formatCost(usageDashboardQuery.data.sum)} บาท</div>
                    </div>
                </CardContent>
            </Card>

            <Card sx={{ height: 150, }}  >
                <CardContent>
                    <div className='grid grid-rows-3 grid-flow-col pl-6'>

                        <Box style={{ width: '120px', height: '120px', backgroundColor: '#0DA9B8' }} className='rounded-full p-6 row-span-3'>
                            <EvStationIcon style={{ width: '70px', height: '70px' }} sx={{ color: 'white' }} />
                        </Box>

                        <div className='col-span-2 text-2xl font-bold text-black pt-6'>พลังงานที่ใช้ไปทั้งหมด</div>
                        <div className='row-span-2 col-span-2 text-2xl font-bold text-black pl-16'>{formatConsumption(usageDashboardQuery.data.energy)} kWh</div>
                    </div>
                </CardContent>
            </Card>

            <Card sx={{ height: 150, }}  >
                <CardContent>
                    <div className='grid grid-rows-3 grid-flow-col pl-6'>

                        <Box style={{ width: '120px', height: '120px', backgroundColor: '#032DA7' }} className='rounded-full p-6 row-span-3'>
                            <ElectricCarIcon style={{ width: '70px', height: '70px' }} sx={{ color: 'white' }} />
                        </Box>

                        <div className='col-span-2 text-2xl font-bold text-black pt-6'>จำนวนที่เข้ามาใช้บริการ</div>
                        <div className='row-span-2 col-span-2 text-2xl font-bold text-black pl-24'> {usageDashboardQuery.data.count} ครั้ง</div>
                    </div>
                </CardContent>
            </Card>

        </div>
    )
}

export default UsageCardList;