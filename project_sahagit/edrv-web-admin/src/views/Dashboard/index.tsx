import * as React from 'react';
import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';
import MapPage from '../Location/components/Map';
import CardList from './CardList';
import DashBoardChartComponent from './Graph';
import RecentCost from './RecentCost';
import Grid from '@mui/material/Grid';

const Dashboard = () => {
    return (
        <div >
            <CardList />
            {/* <Grid container spacing={3}>
                <Grid item xs={12} md={8} lg={9}>
                    <Paper
                        sx={{
                            p: 2,
                            display: 'flex',
                            flexDirection: 'column',
                            height: 400,
                        }}
                    > */}
            <DashBoardChartComponent />
            {/* </Paper>
                </Grid>
                <RecentCost />
            </Grid> */}
        </div>
    )
}

export default Dashboard;
