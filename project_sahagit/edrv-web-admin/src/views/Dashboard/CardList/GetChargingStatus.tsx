import Typography from '@mui/material/Typography';
import { useGetChargingStatusServiceQuery } from "@/services/connector.service";
import Skeleton from '@mui/material/Skeleton';

export default function ChargingStatus() {
    const connectorQuery = useGetChargingStatusServiceQuery()
    if (connectorQuery.isLoading) return (
        <div>
            <Skeleton />
        </div>)
    if (connectorQuery.isError) return <div>Error...</div>
    return (
        <Typography sx={{ fontSize: 20, p: 2, }} color='primary.contrastText' gutterBottom className='font-bold text-2xl'>
            {connectorQuery.data.charging}
        </Typography>
    )
}