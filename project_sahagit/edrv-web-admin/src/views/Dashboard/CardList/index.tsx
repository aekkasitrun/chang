import * as React from 'react';

import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import Grid from '@mui/material/Grid';
import { useGetCountChargeStationServiceQuery } from '@/services/chargestation.service';
import Box from '@mui/material/Box';
import Alert from '@mui/material/Alert';
import AlertTitle from '@mui/material/AlertTitle';
import Stack from '@mui/material/Stack';
import LinearProgress from '@mui/material/LinearProgress';
import ChargingStatus from './GetChargingStatus';

const CardList = () => {

    const chargeStationDashboardQuery = useGetCountChargeStationServiceQuery()

    if (chargeStationDashboardQuery.isLoading) return <div>
        <Box sx={{ width: '100%' }}>
            <LinearProgress />
        </Box>
    </div>
    if (chargeStationDashboardQuery.isError) return <div>
        <Stack sx={{ width: '100%' }} spacing={2}>
            <Alert severity="error">
                <AlertTitle>Error</AlertTitle>
                Data Fetching failed — <strong>check it out!</strong>
            </Alert>
        </Stack>
    </div>

    return (
        <div className='grid grid-cols-1 tablet:grid-cols-2 laptop:grid-cols-3 desktop:grid-cols-4 gap-4'>
            {/* <Grid container spacing={{ xs: 2, md: 3 }} columns={{ xs: 4, sm: 8, md: 16 }} sx={{ p: 1 }}> */}
            {/* <Grid item xs={12} sm={6} md={4}> */}
            <Card sx={{ height: 150, p: 2 }} className='bg-[#8EE0E8]' >
                <CardContent>
                    <div className='flex justify-end'>
                        <Typography sx={{ fontSize: 20, p: 2, flexGrow: 1 }} color='primary.contrastText' gutterBottom className='font-bold text-2xl'>
                            Connect
                        </Typography>
                        <Typography sx={{ fontSize: 20, p: 2, }} color='primary.contrastText' gutterBottom className='font-bold text-2xl'>
                            {chargeStationDashboardQuery.data.count}
                        </Typography>
                    </div>
                </CardContent>
            </Card>
            {/* </Grid> */}

            {/* <Grid item xs={12} sm={6} md={4}> */}
            <Card sx={{ height: 150, p: 2 }} className='bg-[#4FC63B]'>
                <CardContent>
                    <div className='flex justify-end'>
                        <Typography sx={{ fontSize: 20, p: 2, flexGrow: 1 }} color='primary.contrastText' gutterBottom className='font-bold text-2xl'>
                            Online
                        </Typography>
                        <Typography sx={{ fontSize: 20, p: 2, }} color='primary.contrastText' gutterBottom className='font-bold text-2xl'>
                            {chargeStationDashboardQuery.data.onlineCount}
                        </Typography>
                    </div>
                </CardContent>
            </Card>
            {/* </Grid> */}

            {/* <Grid item xs={12} sm={6} md={4}> */}
            <Card sx={{ height: 150, p: 2 }} className='bg-[#FCCB4D]'>
                <CardContent>
                    <div className='flex justify-end'>
                        <Typography sx={{ fontSize: 20, p: 2, flexGrow: 1 }} color='primary.contrastText' gutterBottom className='font-bold text-2xl'>
                            Charging
                        </Typography>
                        <ChargingStatus />
                    </div>
                </CardContent>
            </Card>
            {/* </Grid> */}

            {/* <Grid item xs={12} sm={6} md={4}> */}
            <Card sx={{ height: 150, p: 2 }} className='bg-[#BF202F]'>
                <CardContent>
                    <div className='flex justify-end'>
                        <Typography sx={{ fontSize: 20, p: 2, flexGrow: 1 }} color='primary.contrastText' gutterBottom className='font-bold text-2xl'>
                            Faulted
                        </Typography>
                        <Typography sx={{ fontSize: 20, p: 2, }} color='primary.contrastText' gutterBottom className='font-bold text-2xl'>
                            0
                        </Typography>
                    </div>
                </CardContent>
            </Card>
            {/* </Grid> */}
            {/* </Grid> */}
        </div>
    )
}

export default CardList;