import { LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer } from 'recharts';
import { useGetAllUsageServiceQuery } from '@/services/usage.service';
import Box from '@mui/material/Box';
import LinearProgress from '@mui/material/LinearProgress';
import Alert from '@mui/material/Alert';
import AlertTitle from '@mui/material/AlertTitle';
import Stack from '@mui/material/Stack';
import Paper from '@mui/material/Paper';
import React, { FunctionComponent } from "react";
import formatCost from '@/utils/formatCost';

const DashboardChartComponent = () => {

    const usageQuery = useGetAllUsageServiceQuery()

    if (usageQuery.isLoading) return <div>
        <Box sx={{ width: '100%' }}>
            <LinearProgress />
        </Box>
    </div>
    if (usageQuery.isError) return <div>
        <Stack sx={{ width: '100%' }} spacing={2}>
            <Alert severity="error">
                <AlertTitle>Error</AlertTitle>
                Data Fetching failed — <strong>check it out!</strong>
            </Alert>
        </Stack>
    </div>

    // const chartData = usageQuery?.data?.finalDate.map((finalDate: string, index: number) => ({
    //     date: finalDate,
    //     cost: usageQuery?.data?.cost[index]?.toFixed(2),
    //     yscale: 20
    // }));

    // console.log(JSON.stringify(usageQuery));

    // const costs: number[] = usageQuery?.data?.cost ?? [];
    // const maxCost: number = costs.reduce((prev, curr) => Math.max(prev, curr), 0);
    // const chartData = usageQuery?.data?.finalDate.map((finalDate: string, index: number) => ({
    //     date: `${finalDate.split("/")[0]}/${finalDate.split("/")[1]}`,
    //     cost: ((usageQuery?.data?.cost[index]) * 30).toFixed(2),
    //     max: maxCost > 0 ? (usageQuery?.data?.cost[index] ?? 0) / maxCost * 20 : 0,
    //     yscale: (maxCost * 30) + 2
    // }));

    const costs: number[] = usageQuery?.data?.cost ?? [];
    const maxCost: number = costs.reduce((prev, curr) => Math.max(prev, curr), 0);
    const chartData = usageQuery?.data?.finalDate.map((finalDate: string, index: number) => ({
        date: `${finalDate.split("/")[0]}/${getMonthAbbreviation(finalDate.split("/")[1])}`,
        cost: formatCost(usageQuery?.data?.cost[index]),
        max: maxCost > 0 ? (usageQuery?.data?.cost[index] ?? 0) / maxCost * 20 : 0,
        yscale: (maxCost * 30) + 2
    }));

    function getMonthAbbreviation(month: string): string {
        const monthAbbreviations: { [key: string]: string } = {
            "01": "ม.ค.",
            "02": "ก.พ.",
            "03": "มี.ค.",
            "04": "เม.ย.",
            "05": "พ.ค.",
            "06": "มิ.ย.",
            "07": "ก.ค.",
            "08": "ส.ค.",
            "09": "ก.ย.",
            "10": "ต.ค.",
            "11": "พ.ย.",
            "12": "ธ.ค."
        };
        return monthAbbreviations[month];
    }

    return (

        <div >
            <p className='py-4 pl-2' > กราฟสรุปยอดขาย 7 วันล่าสุด</p >
            <ResponsiveContainer width="99%" height={300}>
                <LineChart data={chartData}>
                    <CartesianGrid stroke="#eee" />
                    <XAxis dataKey="date" />
                    <YAxis dataKey="yscale" />
                    <Tooltip />
                    <Line dataKey="cost" fill="#8884d8" strokeWidth={2} stroke="#18A0FB" />
                    <Legend />
                </LineChart>
            </ResponsiveContainer>
        </div>

    );
};
export default DashboardChartComponent;
