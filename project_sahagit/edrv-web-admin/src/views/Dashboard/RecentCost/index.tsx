import { useGetAllUsageServiceQuery } from '@/services/usage.service';
import Box from '@mui/material/Box';
import LinearProgress from '@mui/material/LinearProgress';
import Alert from '@mui/material/Alert';
import AlertTitle from '@mui/material/AlertTitle';
import Stack from '@mui/material/Stack';
import Grid from '@mui/material/Grid';
import Paper from '@mui/material/Paper';
import Link from '@mui/material/Link';
import Typography from '@mui/material/Typography';

const RecentCost = () => {

    const usageQuery = useGetAllUsageServiceQuery()
    if (usageQuery.isLoading) return <div>
        <Box sx={{ width: '100%' }}>
            <LinearProgress />
        </Box>
    </div>
    if (usageQuery.isError) return <div>
        <Stack sx={{ width: '100%' }} spacing={2}>
            <Alert severity="error">
                <AlertTitle>Error</AlertTitle>
                Data Fetching failed — <strong>check it out!</strong>
            </Alert>
        </Stack>
    </div>
    return (
        <Grid item xs={12} md={4} lg={3}>
            <Paper
                sx={{
                    p: 2,
                    display: 'flex',
                    flexDirection: 'column',
                    height: 240,
                }}
            >
                <Typography component="h2" variant="h6" color="primary" gutterBottom>
                    Recent Cost
                </Typography>
                <Typography component="p" variant="h4">
                    {usageQuery?.data?.cost[0].toFixed(2)}
                </Typography>
                <Typography color="text.secondary" sx={{ flex: 1 }}>
                    on 15 March, 2019
                </Typography>
                <div>
                    <Link color="primary" href="#" >
                        View balance
                    </Link>
                </div>
            </Paper>
        </Grid>
    )
}

export default RecentCost;
