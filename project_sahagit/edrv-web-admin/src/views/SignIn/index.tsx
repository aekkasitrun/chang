import * as React from 'react';
import SignIn from './Signin';

const SigninView = () => {
    return (
        <SignIn />
    );
}

export default SigninView;
