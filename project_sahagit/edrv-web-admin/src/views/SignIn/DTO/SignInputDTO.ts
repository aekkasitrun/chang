export interface SigninDTO {
  providerId: string;
  password: string;
}
