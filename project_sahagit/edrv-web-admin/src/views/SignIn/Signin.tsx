import * as React from 'react';
import Avatar from '@mui/material/Avatar';
import CssBaseline from '@mui/material/CssBaseline';
import Link from '@mui/material/Link';
import Paper from '@mui/material/Paper';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import Typography from '@mui/material/Typography';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import * as Yup from "yup";
import { FormikHelpers, useFormik } from "formik";
import { SigninDTO } from './DTO/SignInputDTO';
import SigninInput from './InputSignIn';
import { signIn } from "next-auth/react"
import { useRouter } from 'next/router';

const initSignin: SigninDTO = {
    providerId: '',
    password: '',
};

const validateSchema = Yup.object().shape({
    providerId: Yup.string().required('*จำเป็น'),
    password: Yup.string()
        .min(8, 'รหัสผ่านต้องมีความยาวมากกว่า 8 และน้อยกว่า 20 อักขระ')
        .max(20, 'รหัสผ่านต้องมีความยาวมากกว่า 8 และน้อยกว่า 20 อักขระ')
        .required('*จำเป็น'),
});

const theme = createTheme();

const SignIn = () => {

    const router = useRouter();

    const { values, handleChange, handleSubmit, errors } = useFormik({
        initialValues: initSignin,
        validationSchema: validateSchema,
        validateOnChange: true, // if true the form will validate the value every time the onChange is triggered
        onSubmit: handleSubmitSignin,
    });

    // console.log('Error: ' + JSON.stringify({ values, handleChange, setFieldValue, handleSubmit, errors }.errors.operatorName));

    async function handleSubmitSignin(value: SigninDTO, formikHelper: FormikHelpers<SigninDTO>) {
        console.log(value);
        // await handleAddLocation(value)
        const status = await signIn('credentials', {
            redirect: false,
            providerId: value.providerId,
            password: value.password,
            callbackUrl: '/'
        })
        console.log('StatusLogIn = ' + JSON.stringify(status));
        if (status?.ok) router.push(status.url!)

        formikHelper.resetForm({ values: initSignin });
    }


    return (
        <ThemeProvider theme={theme}>
            <Grid container component="main" sx={{ height: '100vh' }} >
                <CssBaseline />
                <Grid
                    item
                    xs={false}
                    sm={4}
                    md={7}
                    sx={{
                        backgroundImage: 'url(https://source.unsplash.com/random)',
                        backgroundRepeat: 'no-repeat',
                        backgroundColor: (t) =>
                            t.palette.mode === 'light' ? t.palette.grey[50] : t.palette.grey[900],
                        backgroundSize: 'cover',
                        backgroundPosition: 'center',
                    }}
                />
                <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square className='bg-[#f8fafc]'>
                    <Box
                        sx={{
                            my: 8,
                            mx: 4,
                            display: 'flex',
                            flexDirection: 'column',
                            alignItems: 'center',
                        }}
                    >
                        <Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
                            <LockOutlinedIcon />
                        </Avatar>
                        <Typography component="h1" variant="h5">
                            เข้าสู่ระบบ
                        </Typography>

                        <SigninInput
                            handleChange={handleChange}
                            handleSubmit={handleSubmit}
                            values={values}
                            errors={errors}
                        />

                        <Grid container className='pt-3'>
                            {/* <Grid item xs>
                                <Link href="#" variant="body2" className='text-black'>
                                    Forgot password?
                                </Link>
                            </Grid> */}
                            <Grid item>
                                <Link href="/signup" variant="body2" className='text-black'>
                                    {"สมัครสมาชิก"}
                                </Link>
                            </Grid>
                        </Grid>
                    </Box>
                </Grid>
            </Grid>
        </ThemeProvider>
    );
}

export default SignIn;
