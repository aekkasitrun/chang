import * as React from 'react';
import Button from "@/components/Buttons/Button";
import { FormikErrors, FormikHandlers, FormikValues } from "formik";
import { Input } from "@/components/Form";
import { SigninDTO } from './DTO/SignInputDTO';
import { signIn } from "next-auth/react"
import GoogleIcon from '@mui/icons-material/Google';
import Box from '@mui/material/Box';

interface SiginInputProps {
    handleSubmit: FormikHandlers["handleSubmit"];
    handleChange: FormikHandlers["handleChange"];
    values: SigninDTO;
    errors: FormikErrors<SigninDTO>
}

const SigninInput: React.FC<SiginInputProps> = ({
    handleChange,
    handleSubmit,
    values,
    errors,
}) => {



    async function handleGoogleSignin() {
        signIn('google', { callbackUrl: 'http://localhost:3000' })
    }

    // console.log('ErrorSignin: ' + JSON.stringify(errors));

    return (
        // <div>
        <Box component="form" onSubmit={handleSubmit} sx={{ mt: 1 }} >
            {/* <form onSubmit={handleSubmit}> */}
            <Input
                margin="normal"
                // required
                fullWidth
                id="providerId"
                label="ไอดีผู้ใช้"
                name="providerId"
                autoComplete="providerId"
                autoFocus
                onChange={handleChange}
                value={values.providerId}
                error={!!errors.providerId}
            />
            {errors.providerId ?
                <span className='text-rose-500'>{errors.providerId}</span>
                :
                <></>
            }
            <Input
                margin="normal"
                // required
                fullWidth
                name="password"
                label="รหัสผ่าน"
                type="password"
                id="password"
                autoComplete="current-password"
                onChange={handleChange}
                value={values.password}
                error={!!errors.password}
            />
            {errors.password ?
                <span className='text-rose-500'>{errors.password}</span>
                :
                <></>
            }

            <Button
                type="submit"
                fullWidth
                variant="contained"
                sx={{ mt: 3, mb: 2 }}
                className='bg-red-200 text-black'

            >
                เข้าสู่ระบบ
            </Button>

            <Button
                variant="contained"
                startIcon={<GoogleIcon />}
                onClick={handleGoogleSignin}
                fullWidth
                className='bg-red-200 text-black'
            >
                เข้าสู่ระบบด้วย Google
            </Button>
            {/* </form> */}
        </Box>
        // </div>
    )
}

export default SigninInput;