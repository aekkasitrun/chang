import * as React from 'react';
import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';
import PaymentInput from './PaymentList';
import PaymentList from './PaymentList';

const Payment = () => {
    return (

        <div>
            <Box sx={{ width: '100%', p: 10 }}>
                <Paper sx={{ width: '100%', mb: 2 }}>
                    <PaymentList />
                </Paper>
            </Box>
        </div>
    )
}

export default Payment;
