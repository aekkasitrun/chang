import Button from "@/components/Buttons/Button";
import { FormikHandlers } from "formik";
import React from "react";
import { AddChargeStationDTO } from "../ChargeStation/components/DTO/AddChargeStationDTO";
import MenuItem from '@mui/material/MenuItem';
import { protocolChargeStation } from "../ChargeStation/components/MenuListDropdownItem";
import { Input } from "@/components/Form";
import { useGetAllLocationServiceQuery } from "@/services/location.service";
import Box from '@mui/material/Box';
import CircularProgress from '@mui/material/CircularProgress';

import Alert from '@mui/material/Alert';
import Stack from '@mui/material/Stack';
import FormHelperText from '@mui/material/FormHelperText';
import FormControl from '@mui/material/FormControl';
import { AddPaymentDTO } from "./DTO/AddPaymentDTO";
import { PatternFormat } from "react-number-format";
import NumberFormat from 'react-number-format';


interface PaymentInputProps {
    handleSubmit: FormikHandlers["handleSubmit"];
    handleChange: FormikHandlers["handleChange"];
    values: AddPaymentDTO;
}


const PaymentInput: React.FC<PaymentInputProps> = ({
    handleChange,
    handleSubmit,
    values,
}) => {

    return (
        <div >
            <form onSubmit={handleSubmit}>
                <div className="flex justify-center">




                    <FormControl sx={{ m: 1 }}>
                        <FormHelperText id="outlined-weight-helper-text">Credit Number</FormHelperText>
                        <PatternFormat
                            customInput={Input}
                            sx={{ m: 1, width: '25ch' }}
                            name="creditNumber"
                            placeholder="1234 1234 1234 1234"
                            // label="creditNumber"
                            onChange={handleChange}
                            value={values.creditNumber}
                            format="#### #### #### ####"
                            mask="_"
                        />
                    </FormControl>


                    <FormControl sx={{ m: 1 }}>
                        <FormHelperText id="outlined-weight-helper-text">Card Holder Name</FormHelperText>
                        <Input
                            sx={{ m: 1, width: '25ch' }}
                            name="cardHolderName"
                            placeholder="Card Holder Name"
                            // label="creditNumber"
                            onChange={handleChange}
                            value={values.cardHolderName}


                        />
                    </FormControl>

                    <FormControl sx={{ m: 1 }}>
                        <FormHelperText id="outlined-weight-helper-text">Expire Date</FormHelperText>
                        <PatternFormat
                            customInput={Input}
                            sx={{ m: 1, width: '25ch' }}
                            name="expireDate"
                            placeholder="MM/YY"
                            // label="creditNumber"
                            onChange={handleChange}
                            value={values.expireDate}
                            format="##/##"
                            mask="_"
                        />
                    </FormControl>

                    <FormControl sx={{ m: 1 }}>
                        <FormHelperText id="outlined-weight-helper-text">CVV</FormHelperText>
                        <PatternFormat
                            customInput={Input}
                            sx={{ m: 1, width: '25ch' }}
                            name="cvv"
                            placeholder="CVV"
                            // label="creditNumber"
                            onChange={handleChange}
                            value={values.cvv}
                            format="###"
                            mask="_"
                        />
                    </FormControl>

                    <FormControl sx={{ m: 1 }}>
                        <FormHelperText id="outlined-weight-helper-text">Book Bank Number</FormHelperText>
                        <Input
                            sx={{ m: 1, width: '25ch' }}
                            name="bookBankNumber"
                            placeholder="Book Bank Number"
                            // label="bookBangNumber"
                            onChange={handleChange}
                            value={values.bookBankNumber}
                        />
                    </FormControl>



                </div>

                <div className="flex justify-center py-4">
                    <Button
                        variant="contained"
                        color="primary"
                        type="submit"
                    >
                        Add Payment
                    </Button>
                </div>

            </form >
        </div >
    )
}

export default PaymentInput;
