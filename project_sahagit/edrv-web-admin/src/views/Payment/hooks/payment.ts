import { useAddPaymentServiceMutation } from "@/services/payment.service";
import { AddPaymentDTO } from "../DTO/AddPaymentDTO";
import { SnackBarType } from "@/components/SnackBar";
import React from "react";

interface CheckErrorAlert {
  message: string;
  type: SnackBarType;
}

const usePayment = () => {
  const [addPaymentService] = useAddPaymentServiceMutation();
  const [checkErrorAlert, setCheckErrorAlert] = React.useState<CheckErrorAlert>(
    {
      message: "",
      type: "success",
    }
  );

  async function handleAddPayment(data: AddPaymentDTO) {
    try {
      console.log("PaymentData = " + data);
      await addPaymentService(data).unwrap();
      setCheckErrorAlert({
        message: "Add Payment",
        type: "success",
      });
      console.log("Added ");
    } catch (error) {
      console.log("Add Payment fail");
      setCheckErrorAlert({
        message: "Add Payment fail",
        type: "error",
      });
    }
  }

  return { handleAddPayment, checkErrorAlert };
};

export default usePayment;
