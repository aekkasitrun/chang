import Typography from '@mui/material/Typography';
import Paper from '@mui/material/Paper';
import Divider from '@mui/material/Divider';
import FormHelperText from '@mui/material/FormHelperText';
import FormControl from '@mui/material/FormControl';
import * as React from 'react';

import * as Yup from "yup";
import { FormikHelpers, useFormik } from "formik";
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import CloseIcon from '@mui/icons-material/Close';
import AddIcon from '@mui/icons-material/Add';
import { SnackbarProvider, VariantType, useSnackbar } from 'notistack';
import Snackbar from '@mui/material/Snackbar';
import MuiSnackbar from '@/components/SnackBar';
import { AddPaymentDTO } from './DTO/AddPaymentDTO';
import PaymentInput from './PaymentInput';
import usePayment from './hooks/payment';


const initChargeStation: AddPaymentDTO = {
    creditNumber: "",
    cardHolderName: "",
    cvv: "",
    expireDate: "",
    bookBankNumber: "",
};

const validateSchema = Yup.object().shape({
    creditNumber: Yup.string().required("creditNumber cannot be empty."),
    cardHolderName: Yup.string().required("cardHolderName cannot be empty."),
    cvv: Yup.string().required("cvv cannot be empty."),
    expireDate: Yup.string().required("expireDate cannot be empty."),
    bookBankNumber: Yup.string().required("bookBankNumber cannot be empty."),

});

const PaymentList = () => {

    const [alert, setAlert] = React.useState(false);

    const handleResetAlert = () => {
        setAlert(false)
    }

    const { handleAddPayment, checkErrorAlert } = usePayment();

    const { values, handleChange, handleSubmit } = useFormik({
        initialValues: initChargeStation,
        validationSchema: validateSchema,
        validateOnChange: false, // if true the form will validate the value every time the onChange is triggered
        onSubmit: handleSubmitChargeStation,

    });
    async function handleSubmitChargeStation(value: AddPaymentDTO, formikHelper: FormikHelpers<AddPaymentDTO>) {
        console.log(value);
        await handleAddPayment(value)
        formikHelper.resetForm({ values: initChargeStation });
        setAlert(true)
    }

    return (
        <Paper sx={{ width: '100%', mb: 2 }} variant="outlined">
            <div className='py-5'>
                <Typography sx={{ ml: 2, flex: 1 }} variant="h6" component="div">
                    Payment Options
                </Typography>
            </div>

            <Divider />
            <PaymentInput
                handleChange={handleChange}
                handleSubmit={handleSubmit}
                values={values}
            />
            <MuiSnackbar message={checkErrorAlert.message} type={checkErrorAlert.type} open={alert} setOpen={handleResetAlert} />
        </Paper>
    )
}

export default PaymentList;