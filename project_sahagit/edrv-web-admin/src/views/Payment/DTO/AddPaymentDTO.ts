export interface AddPaymentDTO {
  creditNumber: string;
  cardHolderName: string;
  cvv: string;
  expireDate: string;
  bookBankNumber: string;
}
