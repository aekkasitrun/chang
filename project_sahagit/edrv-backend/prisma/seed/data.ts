import {
  Cars,
  LocationAddress,
  Locations,
  PaymentType,
  Post,
  ChargeStation,
  User,
  UserAddress,
  UserEdrv,
  Connector,
  Transaction,
  EnergyReport,
  Rates,
  PriceComponent,
  Provider,
  ProviderAddress,
  PaymentTypeProvider,
  // Connector,
} from '@prisma/client';

// [DEBUG] For sample prisma seeding.
export const sampleUser: Partial<User> = {
  id: '8d08dcdc-ea90-4498-aecf-c70c60536eb0',
  userName: 'John doe',
};

export const samplePost: Partial<Post> = {
  id: '42038029-c8a7-477c-b284-a96a521fb657',
  content: 'This is sample data.',
};

export const sampleUserEdrv: Partial<UserEdrv> = {
  // id: 'd417139a-83c1-467d-afc8-63f15251c6ea',
  userEdrvId: 'user1234',
  username: 'test01',
  password: '123456789',
  firstName: 'Jackson ',
  lastName: 'Fuentes',
  email: 'Jackson@email.com',
  phone: '085-547-9523',
};

export const sampleUserAddress: Partial<UserAddress> = {
  id: '0ce138da-c0f1-4ae0-8da2-44765229f79a',
  streetAndNumber: '4962 Big Elm',
  country: 'United State',
  city: 'Bellefontaine',
  postCode: '43311',
  state: 'OH',
  userEdrvId: 'user1234',
};

export const sampleUserCars: Partial<Cars> = {
  id: '36fc4235-ad0e-404e-aa8a-c398b22ca5fd',
  brand: 'Toyota',
  model: 'Hybrid',
  chargerType: 'Type 2',
  licensePlate: 'RAM 1452',
  userEdrvId: 'user1234',
};

export const sampleUserPayment: Partial<PaymentType> = {
  id: 'ccfda391-5ac9-4dc1-aa14-813e9e7eae58',
  creditNumber: '5444883371648058',
  cardholderName: 'Jackson Fuentes',
  CVV: '785',
  expireDate: '06/23',
  bookNumber: '503514925',
  userEdrvId: 'user1234',
};

// ------------------------------------------------------------------------------------------------------------------------------------------

export const sampleLocation: Partial<Locations> = {
  locationEdrvId: 'location1234',
  active: true,
  operatorName: 'Hummingbird Energy',
  timeZone: 'America/Los_Angeles',
  lat: 4.8678618860291465,
  lng: -52.33087655701755,
  radius: 16093.4,
  providerId: 'provider123456789',
};

export const sampleLocationAddress: Partial<LocationAddress> = {
  id: '707c4d3e-dd1f-4f49-a7fc-f85d8042ccca',
  streetAndNumber: '4025 Godfrey Road',
  country: 'USA',
  city: 'Davenport',
  postCode: '74026',
  state: 'Oklahoma',
  locationEdrvId: 'location1234',
};

export const sampleChargeStation: Partial<ChargeStation> = {
  chargeStationEdrvId: 'chargeStation1234',
  name: 'CS#1',
  description: 'Good and expensive station',
  protocol: 'OCPP1.6',
  public: true,
  locationEdrvId: 'location1234',
};

export const sampleConnector: Partial<Connector> = {
  connectorEdrvId: 'connector1234',
  name: 'Connector #1',
  description: 'Left Side Connector',
  active: true,
  rateEdrvId: 'rate1234',
  format: 'Cable',
  type: 'CCS1',
  power_type: 'AC Three Phase',
  power: 20.2,
  chargeStationEdrvId: 'chargeStation1234',
};

export const sampleTransaction: Partial<Transaction> = {
  transactionEdrvId: 'transaction1234',
  startTime: '2023-02-06T04:13:31.397Z',
  endTime: '2023-02-06T04:14:08.196Z',
  userEdrvId: 'user1234',
  chargeStationEdrvId: 'chargeStation1234',
  duration: 30000.254,
  cost: 44.75,
  consumption: 2744,
  connectorEdrvId: 'connector1234',
  providerId: 'provider123456789',
};

export const sampleEnergyReport: Partial<EnergyReport> = {
  id: 'c0264e97-ceba-4d07-935a-94b8288eefdd',
  timestamp: '2023-02-06T10:14:08.550Z',
  currentValue: 9.09,
  powerValue: 2.0,
  energyMeterValue: 33.0,
  stateOfChargeValue: 0.0,
  transactionEdrvId: 'transaction1234',
};

export const sampleRate: Partial<Rates> = {
  rateEdrvId: 'rate1234',
  description: '5 บาท ต่อ 1 ชั่วโมง +0.2 ต่อ กิโลวัตชั่วโมง',
  active: true,
  currency: 'THB',
};

export const samplePriceComponent: Partial<PriceComponent> = {
  id: '7b9b4b93-1007-4663-9ce4-f9da82f465f1',
  tax: 13,
  // stepSize: 1,
  type: 'FLAT',
  price: 0.77,
  gracePeriod: 5,
  ratesEdrvId: 'rate1234',
};

export const sampleProvider: Partial<Provider> = {
  id: 'provider123456789',
  providerId: 'testProvider',
  password: 'password1234',
  firstName: 'providerFirstName',
  lastName: 'providerLastName',
  email: 'provider@email.com',
  phoneNumber: '0234509634',
};

export const sampleProviderAddress: Partial<ProviderAddress> = {
  id: 'provideraddress1234',
  streetAndNumber: '56/8',
  city: 'โคกม่วง',
  state: 'คลองหลา',
  country: 'สงขลา',
  postCode: '90110',
  providerId: 'provider123456789',
};

export const samplePaymentTypeProvider: Partial<PaymentTypeProvider> = {
  id: 'a162fbd8-2d7a-433e-a0ed-a0cabed32c0b',
  creditNumber: '5444645298111621',
  cardholderName: 'providerFirstName providerLastName',
  CVV: '812',
  expireDate: '03/23',
  bookNumber: '827598724',
  providerId: 'provider123456789',
};
