export interface ILogSeeding {
  name: string;
  message?: string;
  error?: boolean;
}
