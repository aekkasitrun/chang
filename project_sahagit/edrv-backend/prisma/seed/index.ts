import {
  ChargeStation,
  Connector,
  Locations,
  PrismaClient,
  User,
  UserEdrv,
  Transaction,
  Rates,
  Provider,
} from '@prisma/client';

import * as seedData from './data';
import { ILogSeeding } from './types';

const {
  samplePost,
  sampleUser,
  sampleUserEdrv,
  sampleUserAddress,
  sampleUserCars,
  sampleUserPayment,
  sampleLocation,
  sampleLocationAddress,
  sampleChargeStation,
  sampleConnector,
  sampleTransaction,
  sampleEnergyReport,
  sampleRate,
  samplePriceComponent,
  sampleProvider,
  sampleProviderAddress,
  samplePaymentTypeProvider,
} = seedData;
// const TOTAL_SEED_NUMBER = Object.values(seedData).length;
const prisma = new PrismaClient();

function logSeeding(args: ILogSeeding) {
  if (!args.error) return console.debug(`Seeding ${args.name} `);

  throw new Error(
    `[Error] while seeding ${args.name} \n [Reason]: ${args.message}`,
  );
}

const Seeding = async () => {
  const User = new Promise((resolve, reject) => {
    try {
      // [Upsert] mean if there are data which same id in database update it, or create the new one with provided id
      const user = prisma.user.upsert({
        where: { id: sampleUser.id },
        update: sampleUser,
        create: sampleUser as User,

        // or
        // create: {
        //   id: sampleUser.id,
        //   userName: sampleUser.userName,
        // },
      });
      logSeeding({ name: 'User' });
      resolve(user);
    } catch (error) {
      logSeeding({
        name: 'User',
        error: true,
        message: JSON.stringify(error),
      });
      reject(false);
    }
  });

  const Post = new Promise((resolve, reject) => {
    try {
      // [Upsert] mean if there are data which same id in database update it, or create the new one with provided id
      const post = prisma.post.upsert({
        where: { id: samplePost.id },
        update: samplePost,
        create: {
          // If connection is needed. the upper solution will not work. Thus, Need to add field by field like below
          id: samplePost.id,
          content: samplePost.content,
          author: {
            // connect mean reference to User relation by userId
            connect: {
              id: sampleUser.id,
            },
          },
        },
      });
      logSeeding({ name: 'Post' });
      resolve(post);
    } catch (error) {
      logSeeding({
        name: 'Post',
        error: true,
        message: JSON.stringify(error),
      });
      reject(false);
    }
  });

  const UserEdrv = new Promise((resolve, reject) => {
    try {
      // [Upsert] mean if there are data which same id in database update it, or create the new one with provided id
      const useredrv = prisma.userEdrv.upsert({
        where: { userEdrvId: sampleUserEdrv.userEdrvId },
        update: sampleUserEdrv,
        create: sampleUserEdrv as UserEdrv,

        // or
        // create: {
        //   id: sampleUser.id,
        //   userName: sampleUser.userName,
        // },
      });
      logSeeding({ name: 'UserEdrv' });
      resolve(useredrv);
    } catch (error) {
      logSeeding({
        name: 'UserEdrv',
        error: true,
        message: JSON.stringify(error),
      });
      reject(false);
    }
  });

  const UserAddress = new Promise((resolve, reject) => {
    try {
      // [Upsert] mean if there are data which same id in database update it, or create the new one with provided id
      const useraddress = prisma.userAddress.upsert({
        where: { id: sampleUserAddress.id },
        update: sampleUserAddress,
        create: {
          // If connection is needed. the upper solution will not work. Thus, Need to add field by field like below
          id: sampleUserAddress.id,
          streetAndNumber: sampleUserAddress.streetAndNumber,
          city: sampleUserAddress.city,
          country: sampleUserAddress.country,
          postCode: sampleUserAddress.postCode,
          state: sampleUserAddress.state,
          // userEdrvId: sampleUserAddress.userEdrvId,
          userEdrv: {
            // connect mean reference to User relation by userId
            connect: {
              userEdrvId: sampleUserEdrv.userEdrvId,
            },
          },
        },
      });
      logSeeding({ name: 'UserAddress' });
      resolve(useraddress);
    } catch (error) {
      logSeeding({
        name: 'UserAddress',
        error: true,
        message: JSON.stringify(error),
      });
      reject(false);
    }
  });

  const UserCar = new Promise((resolve, reject) => {
    try {
      // [Upsert] mean if there are data which same id in database update it, or create the new one with provided id
      const usercar = prisma.cars.upsert({
        where: { id: sampleUserCars.id },
        update: sampleUserCars,
        create: {
          // If connection is needed. the upper solution will not work. Thus, Need to add field by field like below
          id: sampleUserCars.id,
          brand: sampleUserCars.brand,
          model: sampleUserCars.model,
          chargerType: sampleUserCars.chargerType,
          licensePlate: sampleUserCars.licensePlate,
          // userEdrvId: sampleUserAddress.userEdrvId,
          userEdrv: {
            // connect mean reference to User relation by userId
            connect: {
              userEdrvId: sampleUserEdrv.userEdrvId,
            },
          },
        },
      });
      logSeeding({ name: 'UserCars' });
      resolve(usercar);
    } catch (error) {
      logSeeding({
        name: 'UserCars',
        error: true,
        message: JSON.stringify(error),
      });
      reject(false);
    }
  });

  const UserPaymentType = new Promise((resolve, reject) => {
    try {
      // [Upsert] mean if there are data which same id in database update it, or create the new one with provided id
      const userpayment = prisma.paymentType.upsert({
        where: { id: sampleUserPayment.id },
        update: sampleUserPayment,
        create: {
          // If connection is needed. the upper solution will not work. Thus, Need to add field by field like below
          id: sampleUserPayment.id,
          creditNumber: sampleUserPayment.creditNumber,
          cardholderName: sampleUserPayment.cardholderName,
          CVV: sampleUserPayment.CVV,
          expireDate: sampleUserPayment.expireDate,
          bookNumber: sampleUserPayment.bookNumber,
          // userEdrvId: sampleUserAddress.userEdrvId,
          userEdrv: {
            // connect mean reference to User relation by userId
            connect: {
              userEdrvId: sampleUserEdrv.userEdrvId,
            },
          },
        },
      });
      logSeeding({ name: 'UserCars' });
      resolve(userpayment);
    } catch (error) {
      logSeeding({
        name: 'UserCars',
        error: true,
        message: JSON.stringify(error),
      });
      reject(false);
    }
  });

  // --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  const Locations = new Promise((resolve, reject) => {
    try {
      const location = prisma.locations.upsert({
        where: { locationEdrvId: sampleLocation.locationEdrvId },
        update: sampleLocation,
        create: sampleLocation as Locations,
      });
      logSeeding({ name: 'Locations' });
      resolve(location);
    } catch (error) {
      logSeeding({
        name: 'Locations',
        error: true,
        message: JSON.stringify(error),
      });
      reject(false);
    }
  });

  const LocationsAddress = new Promise((resolve, reject) => {
    try {
      // [Upsert] mean if there are data which same id in database update it, or create the new one with provided id
      const locationaddress = prisma.locationAddress.upsert({
        where: { id: sampleLocationAddress.id },
        update: sampleLocationAddress,
        create: {
          // If connection is needed. the upper solution will not work. Thus, Need to add field by field like below
          id: sampleLocationAddress.id,
          streetAndNumber: sampleLocationAddress.streetAndNumber,
          city: sampleLocationAddress.city,
          country: sampleLocationAddress.country,
          postCode: sampleLocationAddress.postCode,
          state: sampleLocationAddress.state,
          // userEdrvId: sampleUserAddress.userEdrvId,
          Locations: {
            // connect mean reference to User relation by userId
            connect: {
              locationEdrvId: sampleLocation.locationEdrvId,
            },
          },
        },
      });
      logSeeding({ name: 'LocationAddress' });
      resolve(locationaddress);
    } catch (error) {
      logSeeding({
        name: 'LocationAddress',
        error: true,
        message: JSON.stringify(error),
      });
      reject(false);
    }
  });

  const ChargeStation = new Promise((resolve, reject) => {
    try {
      const chargeStation = prisma.chargeStation.upsert({
        where: { chargeStationEdrvId: sampleChargeStation.chargeStationEdrvId },
        update: sampleChargeStation,
        create: sampleChargeStation as ChargeStation,
      });
      logSeeding({ name: 'ChargeStation' });
      resolve(chargeStation);
    } catch (error) {
      logSeeding({
        name: 'ChargeStation',
        error: true,
        message: JSON.stringify(error),
      });
      reject(false);
    }
  });

  const Connector = new Promise((resolve, reject) => {
    try {
      const connector = prisma.connector.upsert({
        where: { connectorEdrvId: sampleConnector.connectorEdrvId },
        update: sampleConnector,
        create: sampleConnector as Connector,
      });
      logSeeding({ name: 'Connector' });
      resolve(connector);
    } catch (error) {
      logSeeding({
        name: 'Connector',
        error: true,
        message: JSON.stringify(error),
      });
      reject(false);
    }
  });

  const Transaction = new Promise((resolve, reject) => {
    try {
      const transaction = prisma.transaction.upsert({
        where: {
          transactionEdrvId: sampleTransaction.transactionEdrvId,
        },
        update: sampleTransaction,
        create: sampleTransaction as Transaction,
      });
      logSeeding({ name: 'Transaction' });
      resolve(transaction);
    } catch (error) {
      logSeeding({
        name: 'Transaction',
        error: true,
        message: JSON.stringify(error),
      });
      reject(false);
    }
  });

  const EnergyReport = new Promise((resolve, reject) => {
    try {
      // [Upsert] mean if there are data which same id in database update it, or create the new one with provided id
      const energyReport = prisma.energyReport.upsert({
        where: { id: seedData.sampleEnergyReport.id },
        update: sampleEnergyReport,
        create: {
          // If connection is needed. the upper solution will not work. Thus, Need to add field by field like below
          id: sampleEnergyReport.id,
          timestamp: sampleEnergyReport.timestamp,
          currentValue: sampleEnergyReport.currentValue,
          powerValue: sampleEnergyReport.powerValue,
          energyMeterValue: sampleEnergyReport.energyMeterValue,
          stateOfChargeValue: sampleEnergyReport.stateOfChargeValue,

          // userEdrvId: sampleUserAddress.userEdrvId,
          Transaction: {
            // connect mean reference to User relation by userId
            connect: {
              transactionEdrvId: sampleEnergyReport.transactionEdrvId,
            },
          },
        },
      });
      logSeeding({ name: 'EnergyReport' });
      resolve(energyReport);
    } catch (error) {
      logSeeding({
        name: 'EnergyReport',
        error: true,
        message: JSON.stringify(error),
      });
      reject(false);
    }
  });

  const Rates = new Promise((resolve, reject) => {
    try {
      const rate = prisma.rates.upsert({
        where: { rateEdrvId: sampleRate.rateEdrvId },
        update: sampleRate,
        create: sampleRate as Rates,
      });
      logSeeding({ name: 'Rate' });
      resolve(rate);
    } catch (error) {
      logSeeding({
        name: 'Rate',
        error: true,
        message: JSON.stringify(error),
      });
      reject(false);
    }
  });

  const PriceComponent = new Promise((resolve, reject) => {
    try {
      const priceComponent = prisma.priceComponent.upsert({
        where: { id: samplePriceComponent.id },
        update: samplePriceComponent,
        create: {
          id: samplePriceComponent.id,
          tax: samplePriceComponent.tax,
          // stepSize: samplePriceComponent.stepSize,
          type: samplePriceComponent.type,
          price: samplePriceComponent.price,
          gracePeriod: samplePriceComponent.gracePeriod,
          Rates: {
            connect: {
              rateEdrvId: samplePriceComponent.ratesEdrvId,
            },
          },
        },
      });
      logSeeding({ name: 'PriceComponent' });
      resolve(priceComponent);
    } catch (error) {
      logSeeding({
        name: 'PriceComponent',
        error: true,
        message: JSON.stringify(error),
      });
      reject(false);
    }
  });

  const Providers = new Promise((resolve, reject) => {
    try {
      // [Upsert] mean if there are data which same id in database update it, or create the new one with provided id
      const provider = prisma.provider.upsert({
        where: { id: sampleProvider.id },
        update: sampleProvider,
        create: sampleProvider as Provider,

        // or
        // create: {
        //   id: sampleUser.id,
        //   userName: sampleUser.userName,
        // },
      });
      logSeeding({ name: 'Provider' });
      resolve(provider);
    } catch (error) {
      logSeeding({
        name: 'Provider',
        error: true,
        message: JSON.stringify(error),
      });
      reject(false);
    }
  });

  const ProviderAddress = new Promise((resolve, reject) => {
    try {
      const provideraddress = prisma.providerAddress.upsert({
        where: { id: sampleProviderAddress.id },
        update: sampleProviderAddress,
        create: {
          id: sampleProviderAddress.id,
          streetAndNumber: sampleProviderAddress.streetAndNumber,
          city: sampleProviderAddress.city,
          country: sampleProviderAddress.country,
          postCode: sampleProviderAddress.postCode,
          state: sampleProviderAddress.state,
          provider: {
            connect: {
              id: sampleProviderAddress.providerId,
            },
          },
        },
      });
      logSeeding({ name: 'ProviderAddress' });
      resolve(provideraddress);
    } catch (error) {
      logSeeding({
        name: 'ProviderAddress',
        error: true,
        message: JSON.stringify(error),
      });
      reject(false);
    }
  });

  const PaymentTypeProvider = new Promise((resolve, reject) => {
    try {
      // [Upsert] mean if there are data which same id in database update it, or create the new one with provided id
      const paymenttypeprovider = prisma.paymentTypeProvider.upsert({
        where: { id: samplePaymentTypeProvider.id },
        update: samplePaymentTypeProvider,
        create: {
          id: samplePaymentTypeProvider.id,
          creditNumber: samplePaymentTypeProvider.creditNumber,
          cardholderName: samplePaymentTypeProvider.cardholderName,
          CVV: samplePaymentTypeProvider.CVV,
          expireDate: samplePaymentTypeProvider.expireDate,
          bookNumber: samplePaymentTypeProvider.bookNumber,
          provider: {
            connect: {
              id: samplePaymentTypeProvider.providerId,
            },
          },
        },
      });
      logSeeding({ name: 'PaymentTypeProvider' });
      resolve(paymenttypeprovider);
    } catch (error) {
      logSeeding({
        name: 'PaymentTypeProvider',
        error: true,
        message: JSON.stringify(error),
      });
      reject(false);
    }
  });

  Promise.all([
    User,
    Post,
    UserEdrv,
    UserAddress,
    UserCar,
    UserPaymentType,
    Locations,
    LocationsAddress,
    ChargeStation,
    Connector,
    Transaction,
    EnergyReport,
    Rates,
    PriceComponent,
    Providers,
    ProviderAddress,
    PaymentTypeProvider,
  ]);
};

Seeding()
  .then(async () => {
    console.debug('Seeding complete');
    await prisma.$disconnect();
  })
  .catch(async (error) => {
    await prisma.$disconnect();
    throw new Error(error);
  });
