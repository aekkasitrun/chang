-- DropForeignKey
ALTER TABLE "transaction" DROP CONSTRAINT "transaction_connectorEdrvId_fkey";

-- AddForeignKey
ALTER TABLE "transaction" ADD CONSTRAINT "transaction_connectorEdrvId_fkey" FOREIGN KEY ("connectorEdrvId") REFERENCES "connector"("connectorEdrvId") ON DELETE SET NULL ON UPDATE CASCADE;
