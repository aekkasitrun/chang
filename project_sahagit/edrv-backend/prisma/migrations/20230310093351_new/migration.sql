-- CreateTable
CREATE TABLE "user" (
    "id" TEXT NOT NULL,
    "name" TEXT NOT NULL,
    "createAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updateAt" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "user_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "post" (
    "id" TEXT NOT NULL,
    "content" TEXT,
    "authorId" TEXT NOT NULL,
    "createAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updateAt" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "post_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "cars" (
    "id" TEXT NOT NULL,
    "brand" TEXT NOT NULL,
    "model" TEXT NOT NULL,
    "chargertype" TEXT NOT NULL,
    "licenseplate" TEXT NOT NULL,
    "userEdrvId" TEXT,

    CONSTRAINT "cars_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "paymentType" (
    "id" TEXT NOT NULL,
    "creditnumber" TEXT NOT NULL,
    "cardholdername" TEXT NOT NULL,
    "cvv" TEXT NOT NULL,
    "expiredate" TEXT NOT NULL,
    "booknumber" TEXT NOT NULL,
    "userEdrvId" TEXT,

    CONSTRAINT "paymentType_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "userAddress" (
    "id" TEXT NOT NULL,
    "streetAndNumber" TEXT NOT NULL,
    "country" TEXT NOT NULL,
    "city" TEXT NOT NULL,
    "postcode" TEXT NOT NULL,
    "state" TEXT NOT NULL,
    "userEdrvId" TEXT,

    CONSTRAINT "userAddress_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "userEdrv" (
    "userEdrvId" TEXT NOT NULL,
    "username" TEXT NOT NULL,
    "password" TEXT NOT NULL,
    "firstname" TEXT NOT NULL,
    "lastname" TEXT NOT NULL,
    "email" TEXT NOT NULL,
    "phone" TEXT NOT NULL,
    "createAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updateAt" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "userEdrv_pkey" PRIMARY KEY ("userEdrvId")
);

-- CreateTable
CREATE TABLE "location" (
    "locationEdrvId" TEXT NOT NULL,
    "active" BOOLEAN NOT NULL,
    "operatorname" TEXT NOT NULL,
    "timezone" TEXT NOT NULL,
    "latitude" DOUBLE PRECISION NOT NULL,
    "longitude" DOUBLE PRECISION NOT NULL,
    "radius" DOUBLE PRECISION NOT NULL,
    "createAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updateAt" TIMESTAMP(3) NOT NULL,
    "providerId" TEXT,

    CONSTRAINT "location_pkey" PRIMARY KEY ("locationEdrvId")
);

-- CreateTable
CREATE TABLE "locationAddress" (
    "id" TEXT NOT NULL,
    "streetAndNumber" TEXT NOT NULL,
    "country" TEXT NOT NULL,
    "city" TEXT NOT NULL,
    "postcode" TEXT NOT NULL,
    "state" TEXT NOT NULL,
    "locationEdrvId" TEXT,

    CONSTRAINT "locationAddress_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "chargestation" (
    "chargeStationEdrvId" TEXT NOT NULL,
    "name" TEXT NOT NULL,
    "description" TEXT NOT NULL,
    "protocol" TEXT NOT NULL,
    "public" BOOLEAN NOT NULL,
    "locationEdrvId" TEXT,

    CONSTRAINT "chargestation_pkey" PRIMARY KEY ("chargeStationEdrvId")
);

-- CreateTable
CREATE TABLE "connector" (
    "connectorEdrvId" TEXT NOT NULL,
    "name" TEXT NOT NULL,
    "description" TEXT NOT NULL,
    "active" BOOLEAN NOT NULL,
    "rateEdrvId" TEXT NOT NULL,
    "format" TEXT NOT NULL,
    "type" TEXT NOT NULL,
    "power_type" TEXT NOT NULL,
    "power" DOUBLE PRECISION NOT NULL,
    "chargeStationEdrvId" TEXT,

    CONSTRAINT "connector_pkey" PRIMARY KEY ("connectorEdrvId")
);

-- CreateTable
CREATE TABLE "transaction" (
    "transactionEdrvId" TEXT NOT NULL,
    "starttime" TEXT,
    "endtime" TEXT,
    "user" TEXT,
    "chargestation" TEXT,
    "duration" DOUBLE PRECISION,
    "consumption" DOUBLE PRECISION,
    "cost" DOUBLE PRECISION,
    "connectorEdrvId" TEXT,
    "providerId" TEXT,

    CONSTRAINT "transaction_pkey" PRIMARY KEY ("transactionEdrvId")
);

-- CreateTable
CREATE TABLE "eneygerReport" (
    "id" TEXT NOT NULL,
    "timestamp" TEXT,
    "currentValue" DOUBLE PRECISION,
    "powerValue" DOUBLE PRECISION,
    "energyMeterValue" DOUBLE PRECISION,
    "stateOfChargeValue" DOUBLE PRECISION,
    "transactionEdrvId" TEXT,

    CONSTRAINT "eneygerReport_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "rate" (
    "rateEdrvId" TEXT NOT NULL,
    "description" TEXT NOT NULL,
    "active" BOOLEAN NOT NULL,
    "currency" TEXT NOT NULL,

    CONSTRAINT "rate_pkey" PRIMARY KEY ("rateEdrvId")
);

-- CreateTable
CREATE TABLE "priceComponent" (
    "id" TEXT NOT NULL,
    "tax" DOUBLE PRECISION NOT NULL,
    "type" TEXT NOT NULL,
    "price" DOUBLE PRECISION NOT NULL,
    "gracePeriod" DOUBLE PRECISION NOT NULL,
    "ratesEdrvId" TEXT,

    CONSTRAINT "priceComponent_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "provider" (
    "id" TEXT NOT NULL,
    "providerId" TEXT NOT NULL,
    "password" TEXT NOT NULL,
    "firstName" TEXT NOT NULL,
    "lastName" TEXT NOT NULL,
    "email" TEXT NOT NULL,
    "phoneNumber" TEXT NOT NULL,

    CONSTRAINT "provider_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "providerAddress" (
    "id" TEXT NOT NULL,
    "streetAndNumber" TEXT NOT NULL,
    "country" TEXT NOT NULL,
    "city" TEXT NOT NULL,
    "postcode" TEXT NOT NULL,
    "state" TEXT NOT NULL,
    "providerId" TEXT,

    CONSTRAINT "providerAddress_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "paymentTypeProvider" (
    "id" TEXT NOT NULL,
    "creditnumber" TEXT NOT NULL,
    "cardholdername" TEXT NOT NULL,
    "cvv" TEXT NOT NULL,
    "expiredate" TEXT NOT NULL,
    "booknumber" TEXT NOT NULL,
    "providerId" TEXT,

    CONSTRAINT "paymentTypeProvider_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "userEdrv_username_key" ON "userEdrv"("username");

-- CreateIndex
CREATE UNIQUE INDEX "locationAddress_locationEdrvId_key" ON "locationAddress"("locationEdrvId");

-- CreateIndex
CREATE UNIQUE INDEX "eneygerReport_transactionEdrvId_key" ON "eneygerReport"("transactionEdrvId");

-- CreateIndex
CREATE UNIQUE INDEX "priceComponent_ratesEdrvId_key" ON "priceComponent"("ratesEdrvId");

-- CreateIndex
CREATE UNIQUE INDEX "provider_providerId_key" ON "provider"("providerId");

-- CreateIndex
CREATE UNIQUE INDEX "provider_email_key" ON "provider"("email");

-- CreateIndex
CREATE UNIQUE INDEX "provider_phoneNumber_key" ON "provider"("phoneNumber");

-- AddForeignKey
ALTER TABLE "post" ADD CONSTRAINT "post_authorId_fkey" FOREIGN KEY ("authorId") REFERENCES "user"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "cars" ADD CONSTRAINT "cars_userEdrvId_fkey" FOREIGN KEY ("userEdrvId") REFERENCES "userEdrv"("userEdrvId") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "paymentType" ADD CONSTRAINT "paymentType_userEdrvId_fkey" FOREIGN KEY ("userEdrvId") REFERENCES "userEdrv"("userEdrvId") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "userAddress" ADD CONSTRAINT "userAddress_userEdrvId_fkey" FOREIGN KEY ("userEdrvId") REFERENCES "userEdrv"("userEdrvId") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "location" ADD CONSTRAINT "location_providerId_fkey" FOREIGN KEY ("providerId") REFERENCES "provider"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "locationAddress" ADD CONSTRAINT "locationAddress_locationEdrvId_fkey" FOREIGN KEY ("locationEdrvId") REFERENCES "location"("locationEdrvId") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "chargestation" ADD CONSTRAINT "chargestation_locationEdrvId_fkey" FOREIGN KEY ("locationEdrvId") REFERENCES "location"("locationEdrvId") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "connector" ADD CONSTRAINT "connector_chargeStationEdrvId_fkey" FOREIGN KEY ("chargeStationEdrvId") REFERENCES "chargestation"("chargeStationEdrvId") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "connector" ADD CONSTRAINT "connector_rateEdrvId_fkey" FOREIGN KEY ("rateEdrvId") REFERENCES "rate"("rateEdrvId") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "transaction" ADD CONSTRAINT "transaction_connectorEdrvId_fkey" FOREIGN KEY ("connectorEdrvId") REFERENCES "connector"("connectorEdrvId") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "transaction" ADD CONSTRAINT "transaction_user_fkey" FOREIGN KEY ("user") REFERENCES "userEdrv"("userEdrvId") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "transaction" ADD CONSTRAINT "transaction_providerId_fkey" FOREIGN KEY ("providerId") REFERENCES "provider"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "eneygerReport" ADD CONSTRAINT "eneygerReport_transactionEdrvId_fkey" FOREIGN KEY ("transactionEdrvId") REFERENCES "transaction"("transactionEdrvId") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "priceComponent" ADD CONSTRAINT "priceComponent_ratesEdrvId_fkey" FOREIGN KEY ("ratesEdrvId") REFERENCES "rate"("rateEdrvId") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "providerAddress" ADD CONSTRAINT "providerAddress_providerId_fkey" FOREIGN KEY ("providerId") REFERENCES "provider"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "paymentTypeProvider" ADD CONSTRAINT "paymentTypeProvider_providerId_fkey" FOREIGN KEY ("providerId") REFERENCES "provider"("id") ON DELETE CASCADE ON UPDATE CASCADE;
