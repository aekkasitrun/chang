import {
  Controller,
  Get,
  Param,
  Res,
  Post,
  Body,
  Patch,
  Delete,
} from '@nestjs/common';
import { RfidService } from './rfid.service';
import { HttpService } from '@nestjs/axios';
import { ApiTags } from '@nestjs/swagger';
import { Response } from 'express';
import { BaseController } from 'src/common/abstracts';
import CreateNewRfid from './dto/createNewRfid.dto';
import UpdateRfid from './dto/updateRfid.dto';

@ApiTags('rfids')
@Controller('rfids')
export class RfidController extends BaseController {
  constructor(
    private readonly rfidApi: RfidService,
    private readonly httpService: HttpService,
  ) {
    super();
  }

  @Get(':rfidId')
  protected async getRfid(
    @Param('rfidId') rfidId: string,
    @Res() res: Response,
  ) {
    //const onerfid = await this.get(rfidId);
    const rfid = await this.rfidApi.getRfid(rfidId);
    return res.send(rfid.data);
  }
  @Get()
  protected async getRfids(@Res() res: Response) {
    const allrfids = await this.rfidApi.getRfids();
    return res.send(allrfids);
  }
  @Post()
  protected async createNewRfid(
    @Body() newRfid: CreateNewRfid,
    @Res() res: Response,
  ) {
    const createRfid = await this.rfidApi.createNewRfid(newRfid);
    console.log(newRfid);

    return res.send(createRfid);
  }
  @Patch(':rfidId')
  protected async updateRfid(
    @Param('rfidId') rfidId: string,
    @Body() newInfo: UpdateRfid,
    @Res() res: Response,
  ) {
    const updateRfid = await this.rfidApi.updateRfid(rfidId, newInfo);
    // const updaterfid = 'In function patch';

    return res.send(updateRfid);
  }
  @Delete(':rfidId')
  protected async deleteRfid(
    @Param('rfidId') rfidId: string,
    @Res() res: Response,
  ) {
    const deleteRfidByID = await this.rfidApi.deleteRfid(rfidId);
    return res.send(deleteRfidByID);
  }
}
