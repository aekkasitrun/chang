import { Logger, Injectable } from '@nestjs/common';
import { AxiosError } from 'axios';
import { catchError, firstValueFrom } from 'rxjs';
import { HttpService } from '@nestjs/axios/dist';
import { EdrvGateway } from 'src/gateways/edrv/edrv.gateway';
import CreateNewRfid from './dto/createNewRfid.dto';
import UpdateRfid from './dto/updateRfid.dto';

@Injectable()
export class RfidService {
  private data = [];
  private readonly logger = new Logger();
  constructor(private readonly httpService: HttpService) {}

  async getRfids(): Promise<any> {
    const payload = {
      accept: 'application/json',
      authorization: 'Bearer ' + process.env.EDRV_TOKEN,
    };
    const { data } = await firstValueFrom(
      this.httpService
        .get<any>('https://api.edrv.io/v1.1/rfids', { headers: payload })
        .pipe(
          catchError((error: AxiosError) => {
            this.logger.error(error.response.data);
            throw 'An error happened!';
          }),
        ),
    );
    console.log(data);
    return data;
  }

  async getRfid(rfidId) {
    const edrv = new EdrvGateway();
    const a = await edrv.rfids({
      method: 'get',
      rfidId,
      restURL: '',
    });
    return a;
  }

  async createNewRfid(item: CreateNewRfid): Promise<any> {
    const edrv = new EdrvGateway();
    const { data, status } = await edrv.createNewRfid(item);

    console.log(item);
    return { data, status };
  }

  async updateRfid(rfidId: string, item: UpdateRfid): Promise<any> {
    const edrv = new EdrvGateway();
    const { data, status } = await edrv.updateRfid(rfidId, item);

    return { data, status };
  }

  async deleteRfid(rfidId: string): Promise<any> {
    const edrv = new EdrvGateway();
    const { data, status } = await edrv.deleteRfid(rfidId);

    return { data, status };
  }
}
