import { Module } from '@nestjs/common';
import { RfidService } from './rfid.service';
import { RfidController } from './rfid.controller';
import { HttpModule } from '@nestjs/axios';

@Module({
  controllers: [RfidController],
  providers: [RfidService],
  imports: [HttpModule],
})
export class RfidModule {}
