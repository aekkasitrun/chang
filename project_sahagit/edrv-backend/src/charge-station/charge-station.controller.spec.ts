import { Test, TestingModule } from '@nestjs/testing';
import { ChargeStationController } from './charge-station.controller';

describe('ChargeStationController', () => {
  let controller: ChargeStationController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ChargeStationController],
    }).compile();

    controller = module.get<ChargeStationController>(ChargeStationController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
