import { Logger, Injectable } from '@nestjs/common';
import { AxiosError } from 'axios';
import { catchError, firstValueFrom } from 'rxjs';
import { HttpService } from '@nestjs/axios/dist';
import { EdrvGateway } from 'src/gateways/edrv/edrv.gateway';
import { CreateChargeStationDTO } from './dto/create-charge-station.dto';
import UpdateChargeStation from './dto/update-charge-station.dto';
import { ChargeStation } from '@prisma/client';
import { PrismaService } from 'src/shared/services/prisma/prisma.service';
import updateChargeStationWithQueryDTO from './dto/updateWithQuery.dto';
import UpdateChargeStationStatusDTO from './dto/updateChargeStationStatus.dto';
import NewChargeStationDTO from './dto/createChargeStationwithConnector.dto';
import { ConnectorService } from 'src/connector/connector.service';

@Injectable()
export class ChargeStationService {
  private data = [];
  private readonly logger = new Logger();
  constructor(
    private readonly httpService: HttpService,
    private readonly prisma: PrismaService,
    private readonly connectorApi: ConnectorService,
  ) {}

  async getChargeStations(): Promise<any> {
    const payload = {
      accept: 'application/json',
      authorization: 'Bearer ' + process.env.EDRV_TOKEN,
    };
    const { data } = await firstValueFrom(
      this.httpService
        .get<any>('https://api.edrv.io/v1.1/chargestations', {
          headers: payload,
        })
        .pipe(
          catchError((error: AxiosError) => {
            this.logger.error(error.response.data);
            throw 'An error happened!';
          }),
        ),
    );
    console.log(data);
    return data;
  }

  async getChargeStation(chargestationId) {
    const edrv = new EdrvGateway();
    const a = await edrv.chargeStations({
      method: 'get',
      chargestationId,
      restURL: '',
    });
    return a;
  }
  async getChargeStationWithQuery(locationId: string) {
    const edrv = new EdrvGateway();
    const a = await edrv.chargeStationWithQuery(locationId);
    return a;
  }
  async getPrismaChargeStation() {
    const chargeStations = await this.prisma.chargeStation.findMany({
      include: {
        Locations: {
          select: {
            lat: true,
            lng: true,
          },
        },
        Connector: true,
      },
    });
    return chargeStations;
  }

  async createChargeStationService(stationInfo: CreateChargeStationDTO) {
    const edrv = new EdrvGateway();
    const { data, status } = await edrv.createChargeStationGateway(stationInfo);

    return { data, status };
  }

  async createNewChargeStationwithConnector(item: NewChargeStationDTO) {
    const edrv = new EdrvGateway();
    const chargeStation = {
      name: item.chargeStation.name,
      description: item.chargeStation.description,
      location: item.chargeStation.location,
      protocol: item.chargeStation.protocol,
      public: item.chargeStation.public,
      is_simulator: item.chargeStation.is_simulator,
    };
    const chargeStationData = await edrv.createChargeStationGateway(
      chargeStation,
    );
    await this.createNewChargeStationToPrisma(
      chargeStationData.data.result._id,
      chargeStation,
    );

    const connector = {
      name: item.connector.name,
      description: item.connector.description,
      active: item.connector.active,
      rate: item.connector.rate,
      format: item.connector.format,
      type: item.connector.type,
      power_type: item.connector.power_type,
      power: item.connector.power,
    };

    const connectorData = await edrv.createNewConnectorGateway(
      chargeStationData.data.result._id,
      connector,
    );

    await this.connectorApi.createNewConnectorToPrisma(
      chargeStationData.data.result._id,
      connectorData.data.result._id,
      connector,
    );
    return { chargeStationData, connectorData };
  }

  async createNewChargeStationToPrisma(
    edrvId: string,
    item: CreateChargeStationDTO,
  ): Promise<ChargeStation> {
    return await this.prisma.chargeStation.create({
      data: {
        chargeStationEdrvId: edrvId,
        name: item.name,
        description: item.description,
        protocol: item.protocol,
        public: item.public,
        locationEdrvId: item.location,
      },
    });
  }

  async updateChargeStation(
    chargestationId: string,
    item: UpdateChargeStation,
  ): Promise<any> {
    const edrv = new EdrvGateway();
    // const temp = await edrv.chargeStations({
    //   method: 'get',
    //   chargestationId,
    //   restURL: '',
    // });
    const locationId = item.location;
    const temp = await edrv.locations({
      method: 'get',
      locationId,
      restURL: '',
    });
    console.log('location coordinates is :' + temp.data.result.coordinates);
    const newItem = {
      location: locationId,
      coordinates: temp.data.result.coordinates,
      protocol: item.protocol,
    };

    const { data, status } = await edrv.updateChargeStation(
      chargestationId,
      newItem,
    );

    return { data, status };
  }
  async updateChargeStationWithQuery(
    chargestationId: string,
    item: updateChargeStationWithQueryDTO,
  ) {
    const edrv = new EdrvGateway();
    const { data, status } = await edrv.updateChargeStationWithQuery(
      chargestationId,
      item,
    );
    return { data, status };
  }

  async updateChargeStationStatus(
    chargeStationId: string,
    item: UpdateChargeStationStatusDTO,
  ) {
    const edrv = new EdrvGateway();
    console.log('id= ' + chargeStationId);
    console.log('item= ' + JSON.stringify(item));
    const { data, status } = await edrv.updateChargeStationStatus(
      chargeStationId,
      item,
    );
    await this.prisma.chargeStation.update({
      where: {
        chargeStationEdrvId: chargeStationId,
      },
      data: {
        public: item.public,
      },
    });
    return { data, status };
  }

  async updateChargeStationToPrisma(
    chargestationId: string,
    item: UpdateChargeStation,
  ) {
    return await this.prisma.chargeStation.update({
      where: {
        chargeStationEdrvId: chargestationId,
      },
      data: {
        locationEdrvId: item.location,
        protocol: item.protocol,
      },
    });
  }

  async deleteChargeStation(chargestationId: string): Promise<any> {
    const edrv = new EdrvGateway();
    const { data, status } = await edrv.deleteChargeStation(chargestationId);

    return { data, status };
  }

  async deleteChargeStationPrisma(edrvId: string) {
    const result = await this.prisma.chargeStation.findMany({
      where: {
        chargeStationEdrvId: edrvId,
      },
      select: {
        chargeStationEdrvId: true,
      },
    });
    console.log('result' + JSON.stringify(result));
    console.log('result[0].id' + result[0].chargeStationEdrvId);
    return await this.prisma.chargeStation.delete({
      where: {
        chargeStationEdrvId: result[0].chargeStationEdrvId,
      },
    });
  }

  async chargeStationCount() {
    const count = await this.prisma.chargeStation.count();
    const onlineCount = await this.prisma.chargeStation.count({
      where: {
        public: true,
      },
    });

    console.log(onlineCount);

    return { count, onlineCount };
  }
}
