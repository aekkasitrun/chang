import { Module } from '@nestjs/common';
import { ChargeStationService } from './charge-station.service';
import { ChargeStationController } from './charge-station.controller';
import { HttpModule } from '@nestjs/axios';
import { PrismaService } from 'src/shared/services/prisma/prisma.service';
import { ConnectorService } from 'src/connector/connector.service';

@Module({
  controllers: [ChargeStationController],
  providers: [ChargeStationService, ConnectorService, PrismaService],
  imports: [HttpModule],
})
export class ChargeStationModule {}
