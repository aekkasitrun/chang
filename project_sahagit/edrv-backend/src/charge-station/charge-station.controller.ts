import {
  Controller,
  Get,
  Param,
  Res,
  Post,
  Patch,
  Delete,
} from '@nestjs/common';
import { ChargeStationService } from './charge-station.service';
import { HttpService } from '@nestjs/axios';
import { ApiTags } from '@nestjs/swagger';
import { Response } from 'express';
import { Body } from '@nestjs/common/decorators';
import { CreateChargeStationDTO } from './dto/create-charge-station.dto';
import { BaseController } from 'src/common/abstracts';
import UpdateChargeStation from './dto/update-charge-station.dto';
import UpdateChargeStationStatusDTO from './dto/updateChargeStationStatus.dto';
import NewChargeStationDTO from './dto/createChargeStationwithConnector.dto';

@ApiTags('charge-stations')
@Controller('charge-stations')
export class ChargeStationController extends BaseController {
  constructor(
    private readonly chargeStationApi: ChargeStationService,
    private readonly httpService: HttpService,
  ) {
    super();
  }

  @Get()
  protected async getChargestations(@Res() res: Response) {
    const allChargeStations = await this.chargeStationApi.getChargeStations();
    return res.send(allChargeStations);
  }
  @Get('/prisma')
  protected async getAllChargeStationFromPrisma(@Res() res: Response) {
    const allPrismaChargeStation =
      await this.chargeStationApi.getPrismaChargeStation();
    return res.send(allPrismaChargeStation);
  }
  @Get('/query/:locationId')
  protected async getChargeStationWithQuery(
    @Res() res: Response,
    @Param('locationId') locationId: string,
  ) {
    const queryStation = await this.chargeStationApi.getChargeStationWithQuery(
      locationId,
    );
    return res.send(queryStation.data);
  }
  @Get('/count')
  protected async chargeStationsCount(@Res() res: Response) {
    const value = await this.chargeStationApi.chargeStationCount();
    return res.send(value);
  }
  @Get(':chargestationId')
  protected async getChargestation(
    @Param('chargestationId') chargestationId: string,
    @Res() res: Response,
  ) {
    //const onechargestation = await this.get(chargestationId);
    const chargestation = await this.chargeStationApi.getChargeStation(
      chargestationId,
    );
    return res.send(chargestation.data);
  }

  @Post()
  protected async createChargeStation(
    @Body() stationInfo: CreateChargeStationDTO,
    @Res() res: Response,
  ) {
    const createChargeStation =
      await this.chargeStationApi.createChargeStationService(stationInfo);
    console.log(createChargeStation);
    await this.chargeStationApi.createNewChargeStationToPrisma(
      createChargeStation.data.result._id,
      stationInfo,
    );
    return res.send(createChargeStation);
  }
  @Post('/all')
  protected async createChargeStationWithConnecter(
    @Body() chargeStationInfo: NewChargeStationDTO,
    @Res() res: Response,
  ) {
    const createChargeStationConnector =
      await this.chargeStationApi.createNewChargeStationwithConnector(
        chargeStationInfo,
      );
    return res.send(createChargeStationConnector);
  }

  @Patch(':chargestationId')
  protected async updateChargeStation(
    @Param('chargestationId') chargestationId: string,
    @Body() newInfo: UpdateChargeStation,
    @Res() res: Response,
  ) {
    const updatechargestation = await this.chargeStationApi.updateChargeStation(
      chargestationId,
      newInfo,
    );
    await this.chargeStationApi.updateChargeStationToPrisma(
      chargestationId,
      newInfo,
    );

    return res.send(updatechargestation);
  }

  @Patch(':chargestationId/public')
  protected async updateLocationStatus(
    @Param('chargestationId') chargestationId: string,
    @Body() publicStatus: UpdateChargeStationStatusDTO,
    @Res() res: Response,
  ) {
    const locationUpdateStatus =
      await this.chargeStationApi.updateChargeStationStatus(
        chargestationId,
        publicStatus,
      );
    return res.send(locationUpdateStatus);
  }

  @Delete(':chargestationId')
  protected async deleteChargeStation(
    @Param('chargestationId') chargestationId: string,
    @Res() res: Response,
  ) {
    const deleteChargeStationByID =
      await this.chargeStationApi.deleteChargeStation(chargestationId);
    await this.chargeStationApi.deleteChargeStationPrisma(chargestationId);
    return res.send(deleteChargeStationByID);
  }
}
