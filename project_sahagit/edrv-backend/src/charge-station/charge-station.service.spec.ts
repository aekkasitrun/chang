import { Test, TestingModule } from '@nestjs/testing';
import { ChargeStationService } from './charge-station.service';

describe('ChargeStationService', () => {
  let service: ChargeStationService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ChargeStationService],
    }).compile();

    service = module.get<ChargeStationService>(ChargeStationService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
