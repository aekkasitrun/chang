import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export default class UpdateChargeStationStatusDTO {
  @ApiProperty()
  @IsNotEmpty()
  public: boolean;
}
