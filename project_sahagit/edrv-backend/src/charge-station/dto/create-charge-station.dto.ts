import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class CreateChargeStationDTO {
  @ApiProperty()
  @IsString()
  name: string;

  @ApiProperty()
  @IsString()
  description: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  location: string;
  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  protocol: string;

  @ApiProperty()
  public: boolean;

  // @ApiProperty()
  // coordinates: number[];

  @ApiProperty()
  is_simulator: boolean;
}
