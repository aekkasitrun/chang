import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';
import ChargeStationConnectorDTO from 'src/location/dto/object/newChargeStationConnector.dto';
import ConnectorDTO from 'src/location/dto/object/newConnector.dto';

export default class NewChargeStationDTO {
  @ApiProperty()
  @IsNotEmpty()
  chargeStation: ChargeStationConnectorDTO;
  @ApiProperty()
  @IsNotEmpty()
  connector: ConnectorDTO;
}
