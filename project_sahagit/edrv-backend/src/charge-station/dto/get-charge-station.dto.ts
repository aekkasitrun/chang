import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export default class GetChargeStationDTO {
  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  locationId?: string;
}
