// import GetorganizationDTO from './dto/getorganization.dto';
import { Logger, Injectable } from '@nestjs/common';
import { AxiosError } from 'axios';
import { catchError, firstValueFrom } from 'rxjs';
import { HttpService } from '@nestjs/axios/dist';
import { EdrvGateway } from 'src/gateways/edrv/edrv.gateway';

@Injectable()
export class OrganizationService {
  private data = [];
  private readonly logger = new Logger();
  constructor(private readonly httpService: HttpService) {
    // super();
  }
  async getOrganizations(): Promise<any> {
    const payload = {
      accept: 'application/json',
      authorization: 'Bearer ' + process.env.EDRV_TOKEN,
    };
    const { data } = await firstValueFrom(
      this.httpService
        .get<any>('https://api.edrv.io/v1.1/organizations', {
          headers: payload,
        })
        .pipe(
          catchError((error: AxiosError) => {
            this.logger.error(error.response.data);
            throw 'An error happened!';
          }),
        ),
    );
    console.log(data);
    return data;
  }

  async logsOrganizations(): Promise<any> {
    const edrv = new EdrvGateway();
    const { data, status } = await edrv.organizations();

    return { data, status };
  }
}
