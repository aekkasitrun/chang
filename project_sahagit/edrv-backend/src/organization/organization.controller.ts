import { Controller, Get, Res } from '@nestjs/common';
import { OrganizationService } from './organization.service';
import { HttpService } from '@nestjs/axios';
import { ApiTags } from '@nestjs/swagger';
import { Response } from 'express';
import { BaseController } from 'src/common/abstracts';

@ApiTags('organizations')
@Controller('organizations')
export class OrganizationController extends BaseController {
  constructor(
    private readonly organizationApi: OrganizationService,
    private readonly httpService: HttpService,
  ) {
    super();
  }

  @Get('/logs')
  protected async getOrganizations(@Res() res: Response) {
    const allorganizations = await this.organizationApi.logsOrganizations();
    return res.send(allorganizations);
  }
}
