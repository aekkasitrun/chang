import { Module } from '@nestjs/common';
import { OrganizationService } from './organization.service';
import { OrganizationController } from './organization.controller';
import { HttpModule } from '@nestjs/axios';

@Module({
  controllers: [OrganizationController],
  providers: [OrganizationService],
  imports: [HttpModule],
})
export class OrganizationModule {}
