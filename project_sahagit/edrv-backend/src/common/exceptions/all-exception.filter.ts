import {
  ExceptionFilter,
  Catch,
  ArgumentsHost,
  HttpException,
  HttpStatus,
  Logger,
} from '@nestjs/common';
import { Request } from 'express';
import { HttpAdapterHost } from '@nestjs/core';
import { ConfigService } from '@nestjs/config';
import { NestConfig } from '../../config/configuration.interface';

@Catch()
export class AllExceptionsFilter implements ExceptionFilter {
  private readonly logger: Logger;

  constructor(
    private readonly httpAdapterHost: HttpAdapterHost,
    private readonly configService: ConfigService,
  ) {
    this.logger = new Logger();
  }

  catch(exception: Error, host: ArgumentsHost): void {
    // In certain situations `httpAdapter` might not be available in the
    // constructor method, thus we should resolve it here.
    const { httpAdapter } = this.httpAdapterHost;

    const ctx = host.switchToHttp();
    // const response = ctx.getResponse<Response>();
    const request = ctx.getRequest<Request>();
    // const methodKey = ctx.getHandler().name; // "create"
    // const className = ctx.getClass().name; // "CatsController"
    const nestConfig = this.configService.get<NestConfig>('nest');

    const statusCode =
      exception instanceof HttpException
        ? exception.getStatus()
        : HttpStatus.INTERNAL_SERVER_ERROR;
    const message = exception.message || 'Internal server error';

    const devResponseBody = {
      statusCode,
      timestamp: new Date().toISOString(),
      path: request.url,
      method: request.method,
      errorName: exception?.name,
      message,
    };

    const prodResponseBody = {
      statusCode,
      message,
    };

    const responseBody =
      nestConfig?.env === 'development' ? devResponseBody : prodResponseBody;

    this.logger.error(
      `request method: ${request.method} request url${request.url}`,
      JSON.stringify(responseBody),
    );

    // response
    //   .status(statusCode)
    //   .json(process.env.NODE_ENV === 'development' ? devResponseBody : prodResponseBody);
    httpAdapter.reply(ctx.getResponse(), responseBody, statusCode);
  }
}
