import * as express from 'express';
import { HttpStatus } from '@nestjs/common';

export abstract class BaseController {
  /**
   * This is the implementation that we will leave to the
   * subclasses to figure out.
   */

  // protected abstract executeImpl(
  //   req: express.Request,
  //   res: express.Response,
  // ): Promise<void | any>;

  /**
   * This is what we will call on the route handler.
   * We also make sure to catch any uncaught errors in the
   * implementation.
   */

  // public async execute(req: express.Request, res: express.Response): Promise<void> {
  //   try {
  //     await this.executeImpl(req, res);
  //   } catch (err) {
  //     console.log(`[BaseController]: Uncaught controller error`);
  //     console.log(err);
  //     this.fail(res, 'An unexpected error occurred');
  //   }
  // }

  public static jsonResponse(
    res: express.Response,
    code: number,
    message: string,
    ok: boolean,
  ) {
    return res.status(code).json({ ok: ok, message });
  }

  public ok<T>(res: express.Response, dto?: T) {
    if (!!dto) {
      res.type('application/json');
      return res.status(HttpStatus.OK).json({ ok: true, data: dto });
    } else {
      return res.sendStatus(HttpStatus.OK);
    }
  }

  public created(res: express.Response) {
    return res.sendStatus(201);
  }

  public clientError(res: express.Response, message?: string) {
    return BaseController.jsonResponse(
      res,
      400,
      message ? message : 'Unauthorized',
      false,
    );
  }

  public unauthorized(res: express.Response, message?: string) {
    return BaseController.jsonResponse(
      res,
      401,
      message ? message : 'Unauthorized',
      false,
    );
  }

  public paymentRequired(res: express.Response, message?: string) {
    return BaseController.jsonResponse(
      res,
      402,
      message ? message : 'Payment required',
      false,
    );
  }

  public forbidden(res: express.Response, message?: string) {
    return BaseController.jsonResponse(
      res,
      403,
      message ? message : 'Forbidden',
      false,
    );
  }

  public notFound(res: express.Response, message?: string) {
    return BaseController.jsonResponse(
      res,
      404,
      message ? message : 'Not found',
      false,
    );
  }

  public conflict(res: express.Response, message?: string) {
    return BaseController.jsonResponse(
      res,
      409,
      message ? message : 'Conflict',
      false,
    );
  }

  public tooMany(res: express.Response, message?: string) {
    return BaseController.jsonResponse(
      res,
      429,
      message ? message : 'Too many requests',
      false,
    );
  }

  public fail(res: express.Response, error: Error | string, meta?: any) {
    console.log(error);
    return res.status(500).json({
      ok: false,
      message: error.toString(),
      meta: meta,
    });
  }
}
