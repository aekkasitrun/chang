/* eslint-disable @typescript-eslint/no-explicit-any */
export abstract class IGenericService<T> {
  protected abstract list(): Promise<T[]>;

  protected abstract get(id: number | string): Promise<T>;

  protected create?(item: T, tx?: any): Promise<T>;

  protected update?(id: number | string, item: T): Promise<T>;
}
