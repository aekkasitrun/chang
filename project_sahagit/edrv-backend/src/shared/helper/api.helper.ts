import axios, { AxiosRequestConfig } from 'axios';

export type IAxiosRequestConfig = AxiosRequestConfig & { endpoint: string };

const defaultOptions = {
  method: 'GET',
  baseURL: '',
  endpoint: '',
  data: {},
  params: {},
  timeout: 60000,
  responseType: 'json',
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + process.env.EDRV_TOKEN,
  },
};

function addDefaultApiConfig(apiOptions: any): AxiosRequestConfig {
  const endpoint = apiOptions.endpoint || defaultOptions.endpoint;

  const headers = {
    ...defaultOptions.headers,
    ...apiOptions.headers,
  };

  console.log(headers);

  return {
    ...defaultOptions,
    ...apiOptions,
    headers,
    url: endpoint,
  };
}

axios.interceptors.response.use(
  function (response) {
    // Any status code that lie within the range of 2xx cause this function to trigger
    return response;
  },
  function (error) {
    if (error.response) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx
      console.error(error.response.data);
      console.error(error.response.status);
      console.error(error.response.headers);
    } else if (error.request) {
      // The request was made but no response was received
      // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
      // http.ClientRequest in node.js
      console.error(error.request);
    } else if (axios.isCancel(error)) {
      console.debug('Cancelling Request!');
    } else {
      // Something happened in setting up the request that triggered an Error
      console.error('Error', error.message);
    }

    return Promise.reject(error);
  },
);

const api = (apiOptions: any) => {
  const apiConfig = addDefaultApiConfig(apiOptions);
  return axios(apiConfig);
};

export default api;
