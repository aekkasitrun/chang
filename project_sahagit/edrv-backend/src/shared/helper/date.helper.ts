import dayjs from 'dayjs';
import 'dayjs/locale/th';
import customParseFormatPlugin from 'dayjs/plugin/customParseFormat.js';
import isBetween from 'dayjs/plugin/isBetween.js';
dayjs.extend(customParseFormatPlugin);
dayjs.extend(isBetween);
dayjs.locale('th');

export default dayjs;
