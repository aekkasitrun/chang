import { Global, Module } from '@nestjs/common';
import { PrismaService } from 'src/shared/services/prisma/prisma.service';
const services = [PrismaService];

@Global()
@Module({
  imports: [],
  providers: [...services],
  exports: [...services],
})
export class SharedModule {}
