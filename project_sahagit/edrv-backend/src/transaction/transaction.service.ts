import { HttpService } from '@nestjs/axios';
import { Injectable } from '@nestjs/common';
import { Logger } from '@nestjs/common/services';
// import { query } from 'express';
import { PrismaService } from 'src/shared/services/prisma/prisma.service';
// import GetDatesDTO from './dto/getDates.dto';
// import { CreateTransactionDto } from './dto/create-transaction.dto';
// import { UpdateTransactionDto } from './dto/update-transaction.dto';

interface Target {
  finalDate: string[];
  cost: number[];
}

// interface Count {
//   value: number;
//   key: string;
// }

@Injectable()
export class TransactionService {
  private readonly logger = new Logger();
  constructor(
    private readonly httpService: HttpService,
    private readonly prisma: PrismaService, // private readonly ddrvGateway: EdrvGateway,
  ) {}
  // async getTransaction() {
  //   const transactionQuery = await this.prisma.transaction.findMany();
  //   const transactionCount = await this.prisma.transaction.count();
  //   const chargeStation = await this.prisma.chargeStation.findUnique({
  //     where: {
  //       chargeStationEdrvId: transactionQuery[0].chargeStationEdrvId,
  //     },
  //     select: {
  //       locationEdrvId: true,
  //       name: true,
  //     },
  //   });
  //   const streetAndName = await this.prisma.locationAddress.findUnique({
  //     where: {
  //       locationEdrvId: chargeStation.locationEdrvId,
  //     },
  //     select: {
  //       streetAndNumber: true,
  //     },
  //   });
  //   const userName = await this.prisma.userEdrv.findUnique({
  //     where: {
  //       userEdrvId: transactionQuery[0].userEdrvId,
  //     },
  //     select: {
  //       firstName: true,
  //       lastName: true,
  //     },
  //   });
  //   // const csId = await this.prisma.transaction.find

  //   // const allData = [];
  //   // for (let i = 0; i < transactionCount; i++) {
  //   //   const temp = await this.tempo2(i, transactionQuery);
  //   //   allData.push({
  //   //     transactionEdrvId: transactionQuery[i].transactionEdrvId,
  //   //     startTime: transactionQuery[i].startTime,
  //   //     endTime: transactionQuery[i].endTime,
  //   //     chargeStationName: temp.chargeStation.name,
  //   //     duration: transactionQuery[i].duration,
  //   //     cost: transactionQuery[i].cost,
  //   //     userFirstName: temp.userName.firstName,
  //   //     userLastName: temp.userName.lastName,
  //   //     locationName: temp.streetAndName.streetAndNumber,
  //   //   });
  //   // }

  //   const data = {
  //     transactionEdrvId: transactionQuery[0].transactionEdrvId,
  //     startTime: transactionQuery[0].startTime,
  //     endTime: transactionQuery[0].endTime,
  //     // chargeStationName: chargeStation.name,
  //     duration: transactionQuery[0].duration,
  //     cost: transactionQuery[0].cost,
  //     // userFirstName: userName.firstName,
  //     // userLastName: userName.lastName,
  //     // locationName: streetAndName.streetAndNumber,
  //   };

  //   // console.log(transactionQuery[0].chargeStationEdrvId);
  //   console.log(transactionQuery[0]);
  //   return data;
  // }

  // async tempo2(i, transactionQuery) {
  //   return { chargeStation, streetAndName, userName };
  // }

  async getPrismaTransaction() {
    const location = await this.prisma.transaction.findMany({
      orderBy: {
        startTime: 'asc',
      },
      include: {
        EnergyReport: true,
        Connector: {
          select: {
            Rate: {
              select: {
                currency: true,
              },
            },
            ChargeStation: {
              select: {
                name: true,
                Locations: {
                  select: {
                    locationAddresses: {
                      select: {
                        streetAndNumber: true,
                        city: true,
                        country: true,
                        postCode: true,
                        state: true,
                      },
                    },
                  },
                },
              },
            },
          },
        },
        // UserEdrv: true,
        UserEdrv: true,
      },
    });

    // const temp = await this.prisma.chargeStation.findUnique({
    //   where: {
    //     chargeStationEdrvId: location[0].chargeStationEdrvId,
    //   },
    //   select: {
    //     locationEdrvId: true,
    //     name: true,
    //   },
    // });
    // const temp2 = await this.prisma.locationAddress.findUnique({
    //   where: {
    //     locationEdrvId: temp.locationEdrvId,
    //   },
    //   select: {
    //     streetAndNumber: true,
    //   },
    // });
    // const result = {
    //   location,
    //   temp2,
    //   temp,
    // };
    console.log(location);
    return location;
  }

  // async getDate() {
  //   const date = await this.prisma.transaction.findMany({
  //     select: {
  //       startTime: true,
  //     },
  //   });
  //   const finalDate = [];
  //   const CostQuery = [];
  //   const cost = [];

  //   for (let i = 0; i < date.length; i++) {
  //     const rawDate = date[i].startTime.split('T')[0];
  //     CostQuery.push(rawDate);
  //     const splitDate = rawDate.split('-');

  //     finalDate.push(splitDate[2] + '/' + splitDate[1] + '/' + splitDate[0]);
  //   }

  //   const counts: { [key: string]: number } = {};
  //   CostQuery.forEach(function (x: string) {
  //     counts[x] = (counts[x] || 0) + 1;
  //   });
  //   console.log(
  //     Object.entries(counts).forEach(([key, val]) => {
  //       console.log(key, val);
  //     }),
  //   );
  //   cost.push(await this.getCost(CostQuery[0], counts));
  //   const object = { finalDate, cost };
  //   const finalCost = this.sumDup(object);
  //   return finalCost;
  // }

  // async getCost(cost: string, count: { [key: string]: number }) {
  //   const result = [];
  //   for (let i = 0; i < Object.keys(count).length; i++) {
  //     console.log(Object.keys(count)[i]);
  //     const query = await this.prisma.transaction.findMany({
  //       where: {
  //         startTime: {
  //           search: Object.keys(count)[i],
  //         },
  //       },
  //       select: {
  //         cost: true,
  //       },
  //     });
  //     for (let j = 0; j < Object.values(count)[i]; j++) {
  //       result[j] = query.pop().cost;
  //       console.log('result ' + j + ' : ' + result[j]);
  //     }
  //   }
  // }

  async getCost(cost: string, count: { [key: string]: number }) {
    const result = [];
    for (let i = 0; i < Object.keys(count).length; i++) {
      const query = await this.prisma.transaction.findMany({
        where: {
          startTime: {
            search: Object.keys(count)[i],
          },
        },
        select: {
          cost: true,
        },
      });
      for (let j = 0; j < Object.values(count)[i]; j++) {
        result.push(query.pop().cost);
      }
    }
    return result;
  }

  async getDate() {
    const date = await this.prisma.transaction.findMany({
      select: {
        startTime: true,
        chargeStationEdrvId: true,
      },
    });
    const finalDate = [];
    const CostQuery = [];

    for (let i = 0; i < date.length; i++) {
      const rawDate = date[i].startTime.split('T')[0];
      CostQuery.push(rawDate);
      const splitDate = rawDate.split('-');
      finalDate.push(splitDate[2] + '/' + splitDate[1] + '/' + splitDate[0]);
    }

    const counts: { [key: string]: number } = {};
    CostQuery.forEach(function (x: string) {
      counts[x] = (counts[x] || 0) + 1;
    });
    // console.log(
    //   Object.entries(counts).forEach(([key, val]) => {
    //     console.log(key, val);
    //   }),
    // );

    // const cost = await this.getCost(CostQuery[0], counts);
    // const object = { finalDate, cost };
    const result = await this.getCost(CostQuery[0], counts);
    const finalCost = this.sumDup({ finalDate, cost: result }, counts);
    return finalCost;
  }

  sumDup(target: Target, count: { [key: string]: number }) {
    const result: Record<string, number> = {};
    target.finalDate.forEach((element, index) => {
      if (element in result) {
        result[element] = result[element] + target.cost[index];
      } else {
        result[element] = target.cost[index];
      }
    });
    console.log(
      Object.entries(count).forEach(([key, val]) => {
        console.log(key, val);
      }),
    );

    return {
      finalDate: Object.keys(result),
      cost: Object.values(result),
      count: Object.values(count),
    };
  }
  async getUsage(date: string) {
    const rawDate = date.split('-');
    const finalDate = rawDate[2] + '-' + rawDate[1] + '-' + rawDate[0];
    const query = this.prisma.transaction.findMany({
      where: {
        startTime: {
          contains: finalDate,
        },
      },
      include: {
        Connector: {
          select: {
            ChargeStation: {
              select: {
                name: true,
                Locations: {
                  select: {
                    locationAddresses: {
                      select: {
                        streetAndNumber: true,
                      },
                    },
                  },
                },
              },
            },
          },
        },
      },
    });
    return query;
  }
  async getTransactions() {
    const temp = await this.prisma.transaction.findMany();
    let energy = 0;
    let count = 0;

    for (let i = 0; i < temp.length; i++) {
      energy = energy + temp[i].consumption;
      count++;
    }
    return { energy, count };
  }
}
