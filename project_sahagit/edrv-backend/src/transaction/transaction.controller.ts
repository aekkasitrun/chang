import { Controller, Get, Param, Res } from '@nestjs/common';
import { TransactionService } from './transaction.service';
import { ApiTags } from '@nestjs/swagger';
import { BaseController } from 'src/common/abstracts';
import { Response } from 'express';

@ApiTags('transactions')
@Controller('transaction')
export class TransactionController extends BaseController {
  constructor(private readonly transactionService: TransactionService) {
    super();
  }

  // @Get()
  // protected async getAllTransaction(@Res() res: Response) {
  //   const transaction = await this.transactionService.getTransaction();
  //   return res.send(transaction);
  // }

  @Get()
  protected async getAllTransaction(@Res() res: Response) {
    const transaction = await this.transactionService.getPrismaTransaction();
    return res.send(transaction);
  }
  @Get('/summary')
  protected async getDateData(@Res() res: Response) {
    const date = await this.transactionService.getDate();
    return res.send(date);
    // return this.ok(res, date);
  }
  @Get('/usage/:date')
  protected async getUsage(@Res() res: Response, @Param('date') date: string) {
    const usage = await this.transactionService.getUsage(date);
    return res.send(usage);
  }
  @Get('summaryDashboard')
  protected async summaryDashboard(@Res() res: Response) {
    const date = await this.transactionService.getDate();
    const cost = date.cost;
    let sum = 0;
    // cost.forEach((element) => {
    //   result = result + date.cost[element];
    //   console.log(typeof date.cost[element]);
    // });
    const dashboard = await this.transactionService.getTransactions();
    const energy = dashboard.energy;
    const count = dashboard.count;
    for (let i = 0; i < cost.length; i++) {
      sum = sum + date.cost[i];
      console.log(typeof date.cost[i]);
    }
    // const sum = JSON.stringify(result);
    return res.send({ sum, energy, count });
  }
}
