import { Module } from '@nestjs/common';
import { TransactionService } from './transaction.service';
import { TransactionController } from './transaction.controller';
import { PrismaService } from 'src/shared/services/prisma/prisma.service';
import { HttpModule } from '@nestjs/axios';
import { EdrvGateway } from 'src/gateways/edrv/edrv.gateway';

@Module({
  controllers: [TransactionController],
  providers: [TransactionService, PrismaService, EdrvGateway],
  imports: [HttpModule],
})
export class TransactionModule {}
