import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export default class GetDatesDTO {
  @ApiProperty()
  @IsNotEmpty()
  id: string;
}
