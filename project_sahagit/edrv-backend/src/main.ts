import { HttpAdapterHost, NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ValidationPipe, VersioningType } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import {
  CorsConfig,
  NestConfig,
  SwaggerConfig,
} from './config/configuration.interface';
import helmet from 'helmet';
import { PrismaService } from './shared/services/prisma/prisma.service';
import { AllExceptionsFilter } from './common/exceptions/all-exception.filter';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  app.enableVersioning({
    type: VersioningType.URI,
    defaultVersion: '1',
  });

  // enable shutdown hook
  const prismaService = app.get(PrismaService);
  await prismaService.enableShutdownHooks(app);

  const configService = app.get(ConfigService);
  const nestConfig = configService.get<NestConfig>('nest');
  const corsConfig = configService.get<CorsConfig>('cors');
  const swaggerConfig = configService.get<SwaggerConfig>('swagger');

  // Exception Filter for unhandled exceptions
  const httpAdapter = app.get(HttpAdapterHost);
  app.useGlobalFilters(new AllExceptionsFilter(httpAdapter, configService));
  app.useGlobalPipes(
    new ValidationPipe({
      transform: true,
    }),
  );

  if (corsConfig.enabled) {
    app.enableCors({
      allowedHeaders: ['content-type'],
      credentials: corsConfig.credential,
      origin: [
        'http://localhost:5432',
        'http://localhost:3000',

        // Add more origin here
      ],
    });
  }

  // Swagger
  if (swaggerConfig.enabled) {
    const config = new DocumentBuilder()
      .setTitle(swaggerConfig.title)
      .setDescription(swaggerConfig.description)
      .setVersion(swaggerConfig.version)
      .build();

    const document = SwaggerModule.createDocument(app, config);
    SwaggerModule.setup(swaggerConfig.path, app, document);
  }

  app.use(helmet());

  await app.listen(nestConfig.port);
  console.debug('Starting server on port', nestConfig.port);
}
bootstrap();
