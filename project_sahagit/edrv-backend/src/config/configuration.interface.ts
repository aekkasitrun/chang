export interface Config {
  nest: NestConfig;
  cors: CorsConfig;
  redis: RedisConfig;
  database: DatabaseConfig;
  security: SecurityConfig;
  swagger: SwaggerConfig;
}

export interface NestConfig {
  env: string;
  port: number;
}

export interface RedisConfig {
  host: string;
  port: string;
  db: string;
}

export interface CorsConfig {
  enabled: boolean;
  credential: boolean;
}

export interface DatabaseConfig {
  url: string;
  database: string;
  user: string;
  password: string;
}

export interface SecurityConfig {
  secret: string;
  expiresIn: string;
  refreshIn: string;
  bcryptSaltOrRound: string | number;
}

export interface SwaggerConfig {
  enabled: boolean;
  title: string;
  description: string;
  version: string;
  path: string;
}

export interface EDRV_BaseHeader{
  accept: string
  authorize: string
}
