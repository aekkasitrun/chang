import type { Config } from './configuration.interface';

export default (): Config => ({
  nest: {
    env: process.env.NODE_ENV,
    port: parseInt(process.env.NEST_PORT, 10) || 8080,
  },
  cors: {
    enabled: Number(process.env.ENABLE_CORS) === 1 || true,
    credential: Number(process.env.ENABLE_CREDENTIAL) === 1 || true,
  },
  redis: {
    host: process.env.REDIS_HOST,
    port: process.env.REDIS_PORT,
    db: process.env.REDIS_DB,
  },
  database: {
    url: process.env.DATABASE_URL,
    user: process.env.POSTGRES_USER,
    password: process.env.POSTGRES_PASSWORD,
    database: process.env.POSTGRES_DB,
  },
  security: {
    secret: process.env.SECRET,
    expiresIn: '1d', // '60s', '15m', '1d'
    refreshIn: '7d',
    bcryptSaltOrRound: 10,
  },
  swagger: {
    enabled: Boolean(process.env.SWAGGER_ENABLED),
    title: process.env.SWAGGER_TITLE,
    description: process.env.SWAGGER_DESCRIPTION,
    version: process.env.SWAGGER_VERSION,
    path: process.env.SWAGGER_PATH,
  },
  // mail: {
  //   host: process.env.MAIL_HOST,
  //   port: parseInt(process.env.MAIL_PORT) || 587,
  //   user: process.env.MAIL_USER,
  //   pass: process.env.MAIL_PASS,
  //   sender: process.env.MAIL_SENDER,
  // },
  // minio: {
  //   endPoint: process.env.MINIO_ENDPOINT,
  //   port: parseInt(process.env.MINIO_PORT, 10),
  //   accessKey: process.env.MINIO_ACCESS_KEY,
  //   secretKey: process.env.MINIO_SECRET_KEY,
  //   bucketName: process.env.MINIO_BUCKET_NAME,
  // },
});
