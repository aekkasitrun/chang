import { Controller } from '@nestjs/common';
import { Get } from '@nestjs/common/decorators';
import { ApiTags } from '@nestjs/swagger';
import { BaseController } from 'src/common/abstracts';

@ApiTags('advanced-rate')
@Controller('advanced-rate')
export class AdvancedRateController extends BaseController {
  constructor() {
    super();
  }
  //@Get()
}
