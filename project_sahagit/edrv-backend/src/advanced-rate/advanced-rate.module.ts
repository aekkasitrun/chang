import { Module } from '@nestjs/common';
import { AdvancedRateController } from './advanced-rate.controller';
import { AdvancedRateService } from './advanced-rate.service';

@Module({
  controllers: [AdvancedRateController],
  providers: [AdvancedRateService]
})
export class AdvancedRateModule {}
