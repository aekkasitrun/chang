import CreateNewUser from 'src/users/dto/createNewUser.dto';
import CreateNewSession from 'src/session/dto/createNewSession.dto';
import CreateNewRfid from 'src/rfid/dto/createNewRfid.dto';
import api from '../../shared/helper/api.helper';
import CreateNewRate from 'src/rate/dto/createNewRate.dto';
import { CreateChargeStationDTO } from 'src/charge-station/dto/create-charge-station.dto';
import CreateNewLocation from 'src/location/dto/createNewLocation.dto';
import updateLocationDTO from 'src/location/dto/updateLocation.dto';
import UpdateRate from 'src/rate/dto/updateRate.dto';
import UpdateRfid from 'src/rfid/dto/updateRfid.dto';
import UpdateChargeStation from 'src/charge-station/dto/update-charge-station.dto';
import UpdateSessionDTO from 'src/session/dto/updateSession.dto';
import { UpdateConnectorDTO } from 'src/connector/dto/update-connector.dto';
import { CreateNewConnectorDTO } from 'src/connector/dto/create-connector.dto';
import UpdateUserDTO from 'src/users/dto/updateUser.dto';
import updateChargeStationWithQueryDTO from 'src/charge-station/dto/updateWithQuery.dto';
import UpdateLocationStatusDTO from 'src/location/dto/updateStatus.dto';
import UpdateChargeStationStatusDTO from 'src/charge-station/dto/updateChargeStationStatus.dto';
import NewLocationDTO from 'src/location/dto/newLocation.dto';
import { Injectable } from '@nestjs/common';
import UpdateConnectorStatusDTO from 'src/connector/dto/updateConnectorStatus.dto';
import addNewUserDTO from 'src/users/dto/createNewUserNoPassword.dto';

type HttpMethod = 'get' | 'post' | 'patch' | 'put' | 'delete';

interface HttpBaseConfig {
  method: HttpMethod;
  restURL?: string;
}
//------------Session-------------------------
interface SessionApi extends HttpBaseConfig {
  sessionId: string;
  data?: SessionData | object;
}
interface SessionData {
  user: string;
  connector: string;
}

// interface OrganizationData extends HttpBaseConfig {
//   data: object;
// }

interface RfidData {
  name: string;
  value: string;
}

interface LocationData {
  address: object;
  coordinates: number[];
}

interface RateData {
  currency: string;
  price_components: object[];
}

// interface ConnectorApi extends HttpBaseConfig {
//   chargestationId: string;
//   data?: {
//     active: boolean;
//     format: string;
//     type: string;
//     power_type: string;
//     power: number;
//   };
// }

interface ConnectorData extends CreateNewConnectorDTO {
  active: boolean;
  format: string;
  type: string;
  power_type: string;
  power: number;
}

interface ChargeStationApi extends HttpBaseConfig {
  chargestationId: string;
  data?: ChargeStationData | object;
}

interface ChargeStationData {
  location: string;
  protocol: string;
}

// interface ChargeStationWithoutID extends HttpBaseConfig {
//   data: ChargeStationData | object;
// }

//------------Organization---------------------
// interface OrganizationApi extends HttpBaseConfig {
//   chargestationId: string;
//   data?: OrganizationData | object;
// }
// interface OrganizationData extends HttpBaseConfig {
//   data: object;
// }
//------------RFID---------------------------
interface RfidApi extends HttpBaseConfig {
  rfidId: string;
  data?: RfidData | object;
}

interface RfidData {
  name: string;
  value: string;
}
//-----------Location---------------------------
interface LocationApi extends HttpBaseConfig {
  locationId: string;
  data?: LocationData | object;
}
interface LocationData {
  address: object;
  coordinates: number[];
}
//-----------Rate------------------------------
interface RateApi extends HttpBaseConfig {
  rateId: string;
  data?: RateData | object;
}
@Injectable()
export class EdrvGateway {
  private readonly baseUrl: string;
  private readonly session: string;
  private readonly users: string;
  private readonly location: string;
  private readonly rate: string;
  private readonly rfid: string;
  private readonly chargestation: string;
  private readonly organization: string;
  private readonly connector: string;

  constructor() {
    this.baseUrl = 'https://api.edrv.io/v1.1';
    this.session = '/sessions';
    this.users = '/users';
    this.location = '/locations';
    this.rate = '/rates';
    this.rfid = '/rfids';
    this.chargestation = '/chargestations';
    this.organization = '/organizations';
    this.connector = '/connectors';
  }

  // Start Get Method--------------------------------------------------------------------------------------------------------------------------------------------

  async sessions({ method, sessionId, restURL = '/', data = {} }: SessionApi) {
    const config = {
      method: method,
      baseURL: this.baseUrl,
      endpoint: `${this.session}/${sessionId}${restURL}`,
      data: data,
    };

    console.log('config', config);

    return api(config);
  }

  async chargeStations({
    method,
    chargestationId,
    restURL = '/',
    data = {},
  }: ChargeStationApi) {
    const config = {
      method: method,
      baseURL: this.baseUrl,
      endpoint: `${this.chargestation}/${chargestationId}${restURL}`,
      data: data,
    };

    console.log('config', config);

    return api(config);
  }
  async chargeStationWithQuery(locationId: string) {
    const config = {
      method: 'get',
      baseURL: this.baseUrl,
      endpoint: `${this.chargestation}?locations=${locationId}`,
      data: {},
    };

    //console.log('config', config);

    return api(config);
  }

  async organizations() {
    const config = {
      method: 'get',
      baseURL: this.baseUrl,
      endpoint: `${this.organization}/logs`,
      data: {},
    };

    console.log('config', config);

    return api(config);
  }

  async rfids({ method, rfidId, restURL = '/', data = {} }: RfidApi) {
    const config = {
      method: method,
      baseURL: this.baseUrl,
      endpoint: `${this.rfid}/${rfidId}${restURL}`,
      data: data,
    };

    console.log('config', config);

    return api(config);
  }

  async locations({
    method,
    locationId,
    restURL = '/',
    data = {},
  }: LocationApi) {
    const config = {
      method: method,
      baseURL: this.baseUrl,
      endpoint: `${this.location}/${locationId}${restURL}`,
      data: data,
    };

    console.log('config', config);

    return api(config);
  }

  async rates({ method, rateId, restURL = '/', data = {} }: RateApi) {
    const config = {
      method: method,
      baseURL: this.baseUrl,
      endpoint: `${this.rate}/${rateId}${restURL}`,
      data: data,
    };

    console.log('config', config);

    return api(config);
  }

  // End Get Method--------------------------------------------------------------------------------------------------------------------------------------------

  // Start Post Method--------------------------------------------------------------------------------------------------------------------------------------------

  async createNewUserGateway(item: addNewUserDTO) {
    const config = {
      method: 'post',
      baseURL: this.baseUrl,
      endpoint: `${this.users}`,
      data: item,
    };

    //console.log('config', config);

    return api(config);
  }

  async createNewLocationGateway(item: CreateNewLocation) {
    const config = {
      method: 'post',
      baseURL: this.baseUrl,
      endpoint: `${this.location}`,
      data: item,
    };

    console.log('config', config);

    return api(config);
  }
  async createNewLocationwithCSandConnectorGateway(item: NewLocationDTO) {
    const config = {
      method: 'post',
      baseURL: this.baseUrl,
      endpoint: `${this.location}`,
      data: item,
    };

    console.log('config', config);

    return api(config);
  }

  async createNewSession(item: CreateNewSession) {
    const config = {
      method: 'post',
      baseURL: this.baseUrl,
      endpoint: `${this.session}`,
      data: item,
    };

    console.log('config', config);

    return api(config);
  }

  async createNewRfid(item: CreateNewRfid) {
    const config = {
      method: 'post',
      baseURL: this.baseUrl,
      endpoint: `${this.rfid}`,
      data: item,
    };

    console.log('config', config);

    return api(config);
  }

  async createNewRate(item: CreateNewRate) {
    const config = {
      method: 'post',
      baseURL: this.baseUrl,
      endpoint: `${this.rate}`,
      data: {
        description: item.description,
        currency: item.currency,
        price_components: [
          {
            type: item.price_components[0].type,
            price: parseFloat(item.price_components[0].price),
            tax: parseFloat(item.price_components[0].tax),
            grace_period: parseFloat(item.price_components[0].grace_period),
          },
        ],
      },
    };

    console.log('config', config);

    return api(config);
  }

  // async createNewChargeStation(item: CreateNewChargeStation) {
  //   const config = {
  //     method: 'post',
  //     baseURL: this.baseUrl,
  //     endpoint: `${this.chargestation}`,
  //     data: item,
  //   };

  //   console.log('config', config);

  //   return api(config);
  // }

  async createChargeStationGateway(item: CreateChargeStationDTO) {
    const config = {
      method: 'post',
      baseURL: this.baseUrl,
      endpoint: `${this.chargestation}`,
      data: item,
    };

    console.log('config', config);

    return api(config);
  }

  async createNewConnectorGateway(chargestationId, data: ConnectorData) {
    const config = {
      method: 'post',
      baseURL: this.baseUrl,
      endpoint: `${this.chargestation}/${chargestationId}/connectors`,
      data: data,
    };

    console.log('config', config);

    return api(config);
  }

  // End Post Method--------------------------------------------------------------------------------------------------------------------------------------------

  // Start Patch Method--------------------------------------------------------------------------------------------------------------------------------------------

  async updateUserGateway(userId: string, item: UpdateUserDTO) {
    const config = {
      method: 'patch',
      baseURL: this.baseUrl,
      endpoint: `${this.users}/${userId}`,
      data: item,
    };
    console.log('config', config);
    return api(config);
  }

  async updateRfid(rfidId: string, item: UpdateRfid) {
    const config = {
      method: 'patch',
      baseURL: this.baseUrl,
      endpoint: `${this.rfid}/${rfidId}`,
      data: item,
    };
    console.log('config', config);
    return api(config);
  }

  async updateRate(rateId: string, item: UpdateRate) {
    const config = {
      method: 'patch',
      baseURL: this.baseUrl,
      endpoint: `${this.rate}/${rateId}`,
      data: item,
    };
    console.log('config', config);
    return api(config);
  }

  async updateChargeStation(
    chargestationId: string,
    item: UpdateChargeStation,
  ) {
    const config = {
      method: 'patch',
      baseURL: this.baseUrl,
      endpoint: `${this.chargestation}/${chargestationId}`,
      data: item,
    };
    console.log('config', config);
    return api(config);
  }
  async updateChargeStationStatus(
    chargeStationId: string,
    publicStatus: UpdateChargeStationStatusDTO,
  ) {
    const config = {
      method: 'patch',
      baseURL: this.baseUrl,
      endpoint: `${this.chargestation}/${chargeStationId}`,
      data: publicStatus,
    };
    console.log('config', config);
    return api(config);
  }
  async updateChargeStationWithQuery(
    chargestationId: string,
    item: updateChargeStationWithQueryDTO,
  ) {
    const config = {
      method: 'patch',
      baseURL: this.baseUrl,
      endpoint: `${this.chargestation}/${chargestationId}`,
      data: item,
    };
    console.log('config', config);
    return api(config);
  }

  async updateLocationGateway(locationId: string, item: updateLocationDTO) {
    const config = {
      method: 'patch',
      baseURL: this.baseUrl,
      endpoint: `${this.location}/${locationId}`,
      data: item,
    };
    console.log('config', config);
    return api(config);
  }
  async updateLocationStatus(
    locationId: string,
    active: UpdateLocationStatusDTO,
  ) {
    const config = {
      method: 'patch',
      baseURL: this.baseUrl,
      endpoint: `${this.location}/${locationId}`,
      data: active,
    };
    console.log('config', config);
    return api(config);
  }

  async updateConnector(connectorId: string, item: UpdateConnectorDTO) {
    const config = {
      method: 'patch',
      baseURL: this.baseUrl,
      endpoint: `${this.connector}/${connectorId}`,
      data: item,
    };
    console.log('config', config);
    return api(config);
  }

  async updateConnectorStatus(
    connectorId: string,
    active: UpdateConnectorStatusDTO,
  ) {
    const config = {
      method: 'patch',
      baseURL: this.baseUrl,
      endpoint: `${this.connector}/${connectorId}`,
      data: active,
    };
    console.log('config', config);
    return api(config);
  }

  async updateSession(sessionId: string, item: UpdateSessionDTO) {
    const config = {
      method: 'patch',
      baseURL: this.baseUrl,
      endpoint: `${this.session}/${sessionId}`,
      data: item,
    };
    console.log('config', config);
    return api(config);
  }

  // End Patch Method--------------------------------------------------------------------------------------------------------------------------------------------

  // Start delete Method--------------------------------------------------------------------------------------------------------------------------------------------

  async deleteUserToGateway(userId: string) {
    const config = {
      method: 'delete',
      baseURL: this.baseUrl,
      endpoint: `${this.users}/${userId}`,
      data: {},
    };
    console.log('config', config);
    return api(config);
  }

  async deleteLocation(locationId: string) {
    const config = {
      method: 'delete',
      baseURL: this.baseUrl,
      endpoint: `${this.location}/${locationId}`,
    };
    console.log('config', config);
    return api(config);
  }

  async deleteRfid(rfidId: string) {
    const config = {
      method: 'delete',
      baseURL: this.baseUrl,
      endpoint: `${this.rfid}/${rfidId}`,
      data: {},
    };
    console.log('config', config);
    return api(config);
  }

  async deleteChargeStation(chargestationId: string) {
    const config = {
      method: 'delete',
      baseURL: this.baseUrl,
      endpoint: `${this.chargestation}/${chargestationId}`,
      data: {},
    };
    console.log('config', config);
    return api(config);
  }

  async deleteConnector(connectorId: string) {
    const config = {
      method: 'delete',
      baseURL: this.baseUrl,
      endpoint: `${this.connector}/${connectorId}`,
      data: {},
    };
    console.log('config', config);
    return api(config);
  }

  // End Delete Method--------------------------------------------------------------------------------------------------------------------------------------------
}
