import { Module } from '@nestjs/common';
import { EdrvGateway } from './edrv.gateway';

@Module({
  providers: [EdrvGateway],
})
export class EDrvModule {}
