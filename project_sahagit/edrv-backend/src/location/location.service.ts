import { Logger, Injectable } from '@nestjs/common';
import { AxiosError } from 'axios';
import { catchError, firstValueFrom } from 'rxjs';
import { HttpService } from '@nestjs/axios/dist';
// import { BaseController } from 'src/common/abstracts';
import { EdrvGateway } from 'src/gateways/edrv/edrv.gateway';
// import CreateNewLocation from './dto/createNewLocation.dto';
import updateLocationDTO from './dto/updateLocation.dto';
import CreateNewLocationDTO from './dto/createNewLocation.dto';
import { Locations } from '@prisma/client';
import { PrismaService } from 'src/shared/services/prisma/prisma.service';
import { ChargeStationService } from 'src/charge-station/charge-station.service';
import UpdateLocationStatusDTO from './dto/updateStatus.dto';
import { ConnectorService } from 'src/connector/connector.service';
import NewLocationDTO from './dto/newLocation.dto';

@Injectable()
export class LocationService {
  private data = [];
  private readonly logger = new Logger();
  constructor(
    private readonly httpService: HttpService,
    private readonly prisma: PrismaService,
    private readonly chargeStation: ChargeStationService,
    private readonly connectorApi: ConnectorService,
  ) {}

  async getLocations(): Promise<any> {
    const payload = {
      accept: 'application/json',
      authorization: 'Bearer ' + process.env.EDRV_TOKEN,
    };
    const { data } = await firstValueFrom(
      this.httpService
        .get<any>('https://api.edrv.io/v1.1/locations', { headers: payload })
        .pipe(
          catchError((error: AxiosError) => {
            this.logger.error(error.response.data);
            throw 'An error happened!';
          }),
        ),
    );
    console.log(data);
    return data;
  }

  async getLocation(locationId) {
    const edrv = new EdrvGateway();
    const a = await edrv.locations({
      method: 'get',
      locationId,
      restURL: '',
    });
    return a;
  }
  async getPrismaLocation() {
    const location = await this.prisma.locations.findMany({
      include: {
        locationAddresses: true,
      },
    });
    return location;
  }

  async createNewLocationServeic(item: CreateNewLocationDTO): Promise<any> {
    const edrv = new EdrvGateway();

    // '1,1'
    const str2 = item.coordinates.toString().split(',');
    const str3 = [parseFloat(str2[0]), parseFloat(str2[1])];

    // str3[0] = parseInt(str2[0]);
    // str3[1] = parseInt(str2[1]);

    // console.log('str ' + str);

    // console.log('str3 ' + str3);

    // const latitude = item.coordinates
    // const lo

    const temp = {
      active: item.active,
      operatorName: item.operatorName,
      address: {
        streetAndNumber: item.address.streetAndNumber,
        postalCode: item.address.postalCode,
        city: item.address.city,
        state: item.address.state,
        country: item.address.country,
      },
      coordinates: [str3[1], str3[0]],
      meta_data: {
        radius: item.meta_data.radius,
      },
      timezone: item.timezone,
    };

    const { data, status } = await edrv.createNewLocationGateway(temp);

    console.log(item);
    return { data, status };
  }
  async createLocationWithCSandConnector(item: NewLocationDTO) {
    const edrv = new EdrvGateway();
    const str2 = item.location.coordinates.toString().split(',');
    const str3 = [parseFloat(str2[0]), parseFloat(str2[1])];

    //location data
    const location = {
      active: item.location.active,
      operatorName: item.location.operatorName,
      address: {
        streetAndNumber: item.location.address.streetAndNumber,
        postalCode: item.location.address.postalCode,
        city: item.location.address.city,
        state: item.location.address.state,
        country: item.location.address.country,
      },
      coordinates: [str3[1], str3[0]],
      meta_data: {
        radius: item.location.meta_data.radius,
      },
      timezone: item.location.timezone,
    };
    const locationData = await edrv.createNewLocationGateway(location);

    await this.createNewLocationToPrisma(
      locationData.data.result._id,
      location,
    );

    //chargeStation data
    const chargeStation = {
      name: item.chargeStation.name,
      description: item.chargeStation.description,
      location: locationData.data.result._id,
      protocol: item.chargeStation.protocol,
      public: item.chargeStation.public,
      is_simulator: item.chargeStation.is_simulator,
    };
    const chargeStationData = await edrv.createChargeStationGateway(
      chargeStation,
    );
    await this.chargeStation.createNewChargeStationToPrisma(
      chargeStationData.data.result._id,
      chargeStation,
    );
    //connector data
    const connector = {
      name: item.connector.name,
      description: item.connector.description,
      active: item.connector.active,
      rate: item.connector.rate,
      format: item.connector.format,
      type: item.connector.type,
      power_type: item.connector.power_type,
      power: item.connector.power,
    };
    const connectorData = await edrv.createNewConnectorGateway(
      chargeStationData.data.result._id,
      connector,
    );

    await this.connectorApi.createNewConnectorToPrisma(
      chargeStationData.data.result._id,
      connectorData.data.result._id,
      connector,
    );

    return { locationData, chargeStationData, connectorData };
  }

  async createNewLocationToPrisma(
    edrvId: string,
    item: CreateNewLocationDTO,
  ): Promise<Locations> {
    // '1,1'
    //for coordinate
    const str = item.coordinates.toString().split(',');
    const str3 = [parseFloat(str[0]), parseFloat(str[1])];
    console.log('str3 ' + str3);
    // str3[0] = parseInt(str2[0]);
    // str3[1] = parseInt(str2[1]);
    // console.log('str ' + str);

    //for radius
    const rad = item.meta_data.radius.toString();
    const radius = parseFloat(rad);
    console.log('radius ' + typeof radius);

    return await this.prisma.locations.create({
      data: {
        locationEdrvId: edrvId,

        // user_addresses: item.address,
        active: item.active,
        operatorName: item.operatorName,
        timeZone: item.timezone,
        lat: str3[0],
        lng: str3[1],
        radius: radius,
        locationAddresses: {
          create: {
            streetAndNumber: item.address.streetAndNumber,
            country: item.address.country,
            city: item.address.city,
            postCode: item.address.postalCode,
            state: item.address.state,
          },
        },
      },
    });
  }

  async updateLocation(
    locationId: string,
    item: updateLocationDTO,
  ): Promise<any> {
    const edrv = new EdrvGateway();
    const str2 = item.coordinates.toString().split(',');
    const lat = parseFloat(str2[1]);
    const lng = parseFloat(str2[0]);
    const temp = {
      operatorName: item.operatorName,
      address: item.address,
      coordinates: [lat, lng],
      meta_data: {
        radius: item.meta_data.radius,
      },
    };

    const { data, status } = await edrv.updateLocationGateway(locationId, temp);

    const query = await this.chargeStation.getChargeStationWithQuery(
      locationId,
    );
    // let promises = [];
    const promiseReq = query?.data?.result.map(({ _id }) =>
      this.chargeStation.updateChargeStationWithQuery(_id, temp),
    );

    // for (let i = 0; i < query.data.totalDoc; i++) {
    //    promises = promises.push(this.chargeStation.updateChargeStationWithQuery(
    //            query.data.result[i]._id,
    //            temp,
    //          );)
    // }

    Promise.all(promiseReq).catch((err) =>
      this.logger.error(
        'Error Occured on UpdatewithQuery Api by : ' + err.toString,
      ),
    );
    // if (query.data.totalDoc > 1) {
    //   for (let i = 0; i < query.data.totalDoc; i++) {
    //     await this.chargeStation.updateChargeStationWithQuery(
    //       query.data.result[i]._id,
    //       temp,
    //     );
    //   }
    // }
    return { data, status };
  }
  async updateLocationStatus(
    locationId: string,
    item: UpdateLocationStatusDTO,
  ) {
    const edrv = new EdrvGateway();
    console.log('id= ' + locationId);
    console.log('item= ' + JSON.stringify(item));
    const { data, status } = await edrv.updateLocationStatus(locationId, item);
    await this.prisma.locations.update({
      where: {
        locationEdrvId: locationId,
      },
      data: {
        active: item.active,
      },
    });
    return { data, status };
  }

  async updateLocationToPrisma(locationId: string, item: updateLocationDTO) {
    const str2 = item.coordinates.toString().split(',');
    const lat = parseFloat(str2[0]);
    const lng = parseFloat(str2[1]);
    const temp = {
      coordinates: [lat, lng],
    };

    await this.prisma.locations.update({
      where: {
        locationEdrvId: locationId,
      },
      data: {
        operatorName: item.operatorName,
        lat: temp.coordinates[0],
        lng: temp.coordinates[1],
      },
    });
    await this.prisma.locationAddress.update({
      where: {
        locationEdrvId: locationId,
      },
      data: {
        streetAndNumber: item.address.streetAndNumber,
        postCode: item.address.postalCode,
        city: item.address.city,
        state: item.address.state,
        country: item.address.country,
      },
    });
  }

  async deleteLocation(locationId: string): Promise<any> {
    const edrv = new EdrvGateway();
    const { data, status } = await edrv.deleteLocation(locationId);

    return { data, status };
  }

  async deleteLocationPrisma(edrvId: string) {
    const result = await this.prisma.locations.findMany({
      where: {
        locationEdrvId: edrvId,
      },
      select: {
        locationEdrvId: true,
      },
    });
    console.log('result' + JSON.stringify(result));
    console.log('result[0].id' + result[0].locationEdrvId);
    return await this.prisma.locations.delete({
      where: {
        locationEdrvId: result[0].locationEdrvId,
      },
    });
  }

  async getChargeStationByLocationPrisma() {
    return await this.prisma.locations.findMany({
      where: {
        NOT: {
          ChargeStation: {
            every: {
              locationEdrvId: '',
            },
          },
        },
      },
      include: {
        ChargeStation: {
          include: {
            Connector: {
              include: {
                Rate: {
                  include: {
                    priceComponent: true,
                  },
                },
              },
            },
          },
        },
        locationAddresses: true,
      },
    });
  }
}
