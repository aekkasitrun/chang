import { Module } from '@nestjs/common';
import { HttpModule } from '@nestjs/axios';
import { LocationService } from './location.service';
import { LocationController } from './location.controller';
import { PrismaService } from 'src/shared/services/prisma/prisma.service';
import { ChargeStationService } from 'src/charge-station/charge-station.service';
import { ConnectorService } from 'src/connector/connector.service';

@Module({
  controllers: [LocationController],
  providers: [
    ChargeStationService,
    ConnectorService,
    LocationService,
    PrismaService,
  ],
  imports: [HttpModule],
})
export class LocationModule {}
