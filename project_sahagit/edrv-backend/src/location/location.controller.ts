import {
  Controller,
  Get,
  Param,
  Res,
  Post,
  Body,
  Patch,
  Delete,
} from '@nestjs/common';
import { LocationService } from './location.service';
import { HttpService } from '@nestjs/axios';
import { ApiTags } from '@nestjs/swagger';
import { Response } from 'express';
import { BaseController } from 'src/common/abstracts';
import CreateNewLocation from './dto/createNewLocation.dto';
import updateLocationDTO from './dto/updateLocation.dto';
import UpdateLocationStatusDTO from './dto/updateStatus.dto';
import NewLocationDTO from './dto/newLocation.dto';
import { ChargeStationService } from 'src/charge-station/charge-station.service';
import { ConnectorService } from 'src/connector/connector.service';

@ApiTags('locations')
@Controller('locations')
export class LocationController extends BaseController {
  constructor(
    private readonly locationApi: LocationService,
    private readonly httpService: HttpService,
    private readonly chargeStationApi: ChargeStationService,
    private readonly connectorApi: ConnectorService,
  ) {
    super();
  }

  @Get()
  protected async getLocations(@Res() res: Response) {
    const alllocations = await this.locationApi.getLocations();

    return res.send(alllocations);
  }
  @Get('/prisma')
  protected async getAllLocation(@Res() res: Response) {
    const locations = await this.locationApi.getChargeStationByLocationPrisma();
    return res.send(locations);
  }
  @Get(':locationId')
  protected async getLocation(
    @Param('locationId') locationId: string,
    @Res() res: Response,
  ) {
    //const onelocation = await this.get(locationId);
    const location = await this.locationApi.getLocation(locationId);
    return res.send(location.data);
  }
  @Post()
  protected async createNewLocation(
    @Body() newLocation: CreateNewLocation,
    @Res() res: Response,
  ) {
    const createLocation = await this.locationApi.createNewLocationServeic(
      newLocation,
    );
    await this.locationApi.createNewLocationToPrisma(
      createLocation.data.result._id,
      newLocation,
    );
    console.log(createLocation.data.result._id);
    return res.send(createLocation);
  }
  @Post('/all')
  protected async createNewLocationwithCSandConnector(
    @Body() newLocation: NewLocationDTO,
    @Res() res: Response,
  ) {
    // //create Location first
    // const createLocation = await this.locationApi.createNewLocationServeic(
    //   newLocation.location,
    // );
    // //create chargeStation second
    // const createNewLocation =
    //   await this.chargeStationApi.createChargeStationService(
    //     newLocation.chargeStation,
    //   );
    // //create connector last
    // const createConnector = await this.connectorApi.createNewConnectorService(

    // );
    const createNewLocation =
      await this.locationApi.createLocationWithCSandConnector(newLocation);
    return res.send(createNewLocation);
  }

  @Patch(':locationId')
  protected async updateLocation(
    @Param('locationId') locationId: string,
    @Body() newLocationInfo: updateLocationDTO,
    @Res() res: Response,
  ) {
    const updateLocationInfo = await this.locationApi.updateLocation(
      locationId,
      newLocationInfo,
    );
    await this.locationApi.updateLocationToPrisma(locationId, newLocationInfo);
    return res.send(updateLocationInfo);
  }
  @Patch(':locationId/active')
  protected async updateLocationStatus(
    @Param('locationId') locationId: string,
    @Body() activeStatus: UpdateLocationStatusDTO,
    @Res() res: Response,
  ) {
    const locationUpdateStatus = await this.locationApi.updateLocationStatus(
      locationId,
      activeStatus,
    );
    return res.send(locationUpdateStatus);
  }

  @Delete(':locationId')
  protected async deleteLocation(
    @Param('locationId') locationId: string,
    @Res() res: Response,
  ) {
    const deleteLocation = await this.locationApi.deleteLocation(locationId);
    await this.locationApi.deleteLocationPrisma(locationId);

    return res.send(deleteLocation);
  }
}
