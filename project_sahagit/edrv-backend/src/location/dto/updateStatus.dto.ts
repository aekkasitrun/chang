import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export default class UpdateLocationStatusDTO {
  @ApiProperty()
  @IsNotEmpty()
  active: boolean;
}
