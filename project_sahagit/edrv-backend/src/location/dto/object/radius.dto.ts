import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export default class Radius {
  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  radius: number;
}
