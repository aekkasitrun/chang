import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';
import addressLocation from './addressLocation.dto';
import Radius from './radius.dto';

export default class LocationDTO {
  @ApiProperty()
  @IsNotEmpty()
  active: boolean;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  operatorName: string;
  @ApiProperty()
  @IsNotEmpty()
  address: addressLocation;

  @ApiProperty()
  @IsNotEmpty()
  coordinates: number[];

  @ApiProperty()
  meta_data: Radius | null;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  timezone: string;
}
