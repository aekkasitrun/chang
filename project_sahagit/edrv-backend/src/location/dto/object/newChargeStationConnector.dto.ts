import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export default class ChargeStationConnectorDTO {
  @ApiProperty()
  @IsString()
  name: string;

  @ApiProperty()
  @IsString()
  description: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  protocol: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  location: string;
  @ApiProperty()
  public: boolean;

  @ApiProperty()
  is_simulator: boolean;
}
