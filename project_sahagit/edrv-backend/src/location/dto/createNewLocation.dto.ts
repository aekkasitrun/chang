import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';
import addressLocation from './object/addressLocation.dto';
import Radius from './object/radius.dto';

export default class CreateNewLocationDTO {
  @ApiProperty()
  @IsNotEmpty()
  active: boolean;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  operatorName: string;
  @ApiProperty()
  @IsNotEmpty()
  address: addressLocation;

  @ApiProperty()
  @IsNotEmpty()
  coordinates: number[];

  @ApiProperty()
  meta_data: Radius | null;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  timezone: string;
}
