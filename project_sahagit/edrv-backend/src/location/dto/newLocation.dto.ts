import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';
import ChargeStationDTO from './object/newChargeStation.dto';
import ConnectorDTO from './object/newConnector.dto';
import LocationDTO from './object/location.dto';

export default class NewLocationDTO {
  @ApiProperty()
  @IsNotEmpty()
  location: LocationDTO;
  @ApiProperty()
  @IsNotEmpty()
  chargeStation: ChargeStationDTO;
  @ApiProperty()
  @IsNotEmpty()
  connector: ConnectorDTO;
}
