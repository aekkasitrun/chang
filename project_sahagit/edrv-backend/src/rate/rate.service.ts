import { Logger, Injectable } from '@nestjs/common';
import { AxiosError } from 'axios';
import { catchError, firstValueFrom } from 'rxjs';
import { HttpService } from '@nestjs/axios/dist';
import { EdrvGateway } from 'src/gateways/edrv/edrv.gateway';
import CreateNewRate from './dto/createNewRate.dto';
import UpdateRate from './dto/updateRate.dto';
import { PrismaService } from 'src/shared/services/prisma/prisma.service';

@Injectable()
export class RateService {
  private data = [];
  private readonly logger = new Logger();
  constructor(
    private readonly httpService: HttpService,
    private readonly prisma: PrismaService,
  ) {}

  async getRates(): Promise<any> {
    const payload = {
      accept: 'application/json',
      authorization: 'Bearer ' + process.env.EDRV_TOKEN,
    };
    const { data } = await firstValueFrom(
      this.httpService
        .get<any>('https://api.edrv.io/v1.1/rates', { headers: payload })
        .pipe(
          catchError((error: AxiosError) => {
            this.logger.error(error.response.data);
            throw 'An error happened!';
          }),
        ),
    );
    console.log(data);
    return data;
  }

  async getRate(rateId) {
    const edrv = new EdrvGateway();
    const a = await edrv.rates({
      method: 'get',
      rateId,
      restURL: '',
    });
    return a;
  }
  //รอตาราง rate
  // async getPrismaRates() {
  //   const allRates = await this.prisma.rate;
  // }

  async createNewRate(item: CreateNewRate): Promise<any> {
    const edrv = new EdrvGateway();
    // const temp: CreateNewRate = {
    //   description: item.description,
    //   currency: item.currency,
    //   price_components: [
    //     {
    //       type: item.price_components[0].type,
    //       price: item.price_components[0].price,
    //       tax: item.price_components[0].tax,
    //       grace_period: item.price_components[0].grace_period,
    //     },
    //   ],
    // };
    const { data, status } = await edrv.createNewRate(item);

    return { data, status };
  }
  async createRatePrisma(rateId: string, item: CreateNewRate) {
    const temp = await this.prisma.rates.create({
      data: {
        rateEdrvId: rateId,
        description: item.description,
        currency: item.currency,
        active: true,
        priceComponent: {
          create: {
            type: item.price_components[0].type,
            price: parseFloat(item.price_components[0].price),
            gracePeriod: parseFloat(item.price_components[0].grace_period),
            // stepSize: parseFloat(item.price_components[0].stepSize),
            tax: parseFloat(item.price_components[0].tax),
          },
        },
      },
    });
    console.log('Prisma : ' + temp);
  }

  async updateRate(rateId: string, item: UpdateRate): Promise<any> {
    const edrv = new EdrvGateway();
    const { data, status } = await edrv.updateRate(rateId, item);

    return { data, status };
  }
}
