import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export default class GetRateDTO {
  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  rateId?: string;
}
