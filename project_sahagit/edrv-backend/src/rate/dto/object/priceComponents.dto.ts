import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export default class PriceComponents {
  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  type: string;

  @ApiProperty()
  @IsNotEmpty()
  price: string;

  @ApiProperty()
  @IsNotEmpty()
  tax: string;

  // @ApiProperty()
  // @IsNotEmpty()
  // stepSize: string | null;

  @ApiProperty()
  @IsNotEmpty()
  grace_period: string;
}
