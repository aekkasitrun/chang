import { Controller, Get, Param, Res, Post, Body, Patch } from '@nestjs/common';
import { RateService } from './rate.service';
import { HttpService } from '@nestjs/axios';
import { ApiTags } from '@nestjs/swagger';
import { Response } from 'express';
import { BaseController } from 'src/common/abstracts';
import CreateNewRate from './dto/createNewRate.dto';
import UpdateRate from './dto/updateRate.dto';

@ApiTags('rates')
@Controller('rates')
export class RateController extends BaseController {
  constructor(
    private readonly rateApi: RateService,
    private readonly httpService: HttpService,
  ) {
    super();
  }

  @Get(':rateId')
  protected async getRate(
    @Param('rateId') rateId: string,
    @Res() res: Response,
  ) {
    const rate = await this.rateApi.getRate(rateId);
    return res.send(rate.data);
  }
  @Get()
  protected async getRates(@Res() res: Response) {
    const allrates = await this.rateApi.getRates();
    return res.send(allrates);
  }
  @Post()
  protected async createNewRate(
    @Body() newRate: CreateNewRate,
    @Res() res: Response,
  ) {
    const createRate = await this.rateApi.createNewRate(newRate);
    await this.rateApi.createRatePrisma(createRate.data.result._id, newRate);
    return res.send(createRate);
  }
  @Patch(':rateId')
  protected async updateRate(
    @Param('rateId') rateId: string,
    @Body() newInfo: UpdateRate,
    @Res() res: Response,
  ) {
    const updateRate = await this.rateApi.updateRate(rateId, newInfo);
    // const updaterate = 'In function patch';

    return res.send(updateRate);
  }
}
