import { Module } from '@nestjs/common';
import { RateService } from './rate.service';
import { RateController } from './rate.controller';
import { HttpModule } from '@nestjs/axios';
import { PrismaService } from 'src/shared/services/prisma/prisma.service';

@Module({
  controllers: [RateController],
  providers: [RateService, PrismaService],
  imports: [HttpModule],
})
export class RateModule {}
