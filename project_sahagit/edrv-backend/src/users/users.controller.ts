import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Res,
  Patch,
  Put,
} from '@nestjs/common';
import { Response } from 'express';
import { ApiTags } from '@nestjs/swagger';
import { UsersService } from './users.service';
import { HttpService } from '@nestjs/axios';
import { BaseController } from 'src/common/abstracts';
import CreateNewUserDTO from './dto/createNewUser.dto';
import UpdateUserDTO from './dto/updateUser.dto';
import AddPaymentDTO from './dto/object/addPayment.dto';
@ApiTags('users')
@Controller('users')
export class UsersController extends BaseController {
  constructor(
    private readonly userApi: UsersService,
    private readonly httpService: HttpService,
  ) {
    super();
  }

  @Get()
  protected async getUsers(@Res() res: Response) {
    const allUsers = await this.userApi.getUsers();
    return res.send(allUsers);
  }
  @Get('/prisma')
  protected async getUsersFromPrisma(@Res() res: Response) {
    const users = await this.userApi.getAllUsersFromPrisma();

    return res.send(users);
  }

  @Get(':userId')
  protected async getUser(
    @Param('userId') userId: string,
    @Res() res: Response,
  ) {
    //const oneUser = await this.get(userId);
    const user = await this.userApi.getUser(userId);

    // console.log(user.result._id);

    return res.send(user);
  }
  @Post()
  protected async createNewUserToEdrv(
    @Body() newUser: CreateNewUserDTO,
    @Res() res: Response,
  ) {
    const createUserEdrv = await this.userApi.createNewUserToEdrv(newUser);
    await this.userApi.createNewUserToPrisma(
      createUserEdrv.data.result._id,
      newUser,
    );
    // await this.userApi.addAddressUserToPrisma(newUser);

    // if(createUser)
    // try {
    //   await this.sampleApi.create(newUser);
    //   return this.created(res);
    // } catch (error) {
    //   return this.fail(res, error);
    // }
    console.log(createUserEdrv.data.result._id);

    return res.send(createUserEdrv);
  }
  // @Post('/cars/:userId')
  // protected async addCars(
  //   @Param('userId') userId: string,
  //   @Body() carInfo: carInfoDTO,
  //   @Res() res: Response,
  // ) {await this.pris}
  @Put('/:userId/payment')
  protected async addPayment(
    @Param('userId') userId: string,
    @Body() paymentInfo: AddPaymentDTO,
    @Res() res: Response,
  ) {
    await this.userApi.addPaymentInfo(userId, paymentInfo); // prisma here

    return res.send({ ok: 'Data Recorded!' });
  }
  @Patch(':userId')
  protected async updateUser(
    @Param('userId') userId: string,
    @Body() newInfo: UpdateUserDTO,
    @Res() res: Response,
  ) {
    const updateUser = await this.userApi.updateUser(userId, newInfo);
    await this.userApi.updateUserToPrisma(userId, newInfo);
    return res.send(updateUser);
  }

  @Delete(':userId')
  protected async deleteUser(
    @Param('userId') userId: string,
    @Res() res: Response,
  ) {
    const deleteUserByID = await this.userApi.deleteUser(userId);
    await this.userApi.deleteUserPrisma(userId);
    return res.send(deleteUserByID);
  }
}
