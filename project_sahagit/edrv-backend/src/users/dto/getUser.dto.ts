import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export default class GetUserDTO {
  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  userId?: string;
}
