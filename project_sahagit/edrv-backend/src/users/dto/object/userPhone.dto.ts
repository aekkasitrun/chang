import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsMobilePhone } from 'class-validator';

export default class userPhone {
  @ApiProperty()
  @IsMobilePhone()
  @IsNotEmpty()
  mobile: string;
}
