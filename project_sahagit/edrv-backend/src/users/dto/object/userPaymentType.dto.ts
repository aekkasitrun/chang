import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export default class userPaymentType {
  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  creditNumber: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  cardholderName: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  CVV: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  expireDate: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  bookNumber: string;
}
