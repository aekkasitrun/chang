import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export default class AddPaymentDTO {
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  creditNumber: string;
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  cardholderName: string;
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  CVV: string;
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  expireDate: string;
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  bookNumber: string;
}
