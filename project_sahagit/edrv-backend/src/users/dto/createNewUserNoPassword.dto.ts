import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString, IsEmail } from 'class-validator';
import userAddress from './object/userAddress.dto';
import userCars from './object/userCars.dto';
import userPhone from './object/userPhone.dto';

export default class addNewUserDTO {
  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  firstname: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  lastname: string;

  @ApiProperty()
  @IsEmail()
  @IsNotEmpty()
  email: string;

  @ApiProperty()
  address: userAddress;

  @ApiProperty()
  phone: userPhone;

  @ApiProperty()
  meta_data: userCars | null;
}
