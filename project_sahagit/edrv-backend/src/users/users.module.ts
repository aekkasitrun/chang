import { HttpModule } from '@nestjs/axios';
import { Module } from '@nestjs/common';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';
// import { ConfigModule } from '@nestjs/config';
// import { ConfigService } from '@nestjs/config';
import { PrismaService } from 'src/shared/services/prisma/prisma.service';
import { EdrvGateway } from 'src/gateways/edrv/edrv.gateway';

@Module({
  controllers: [UsersController],
  providers: [UsersService, PrismaService, EdrvGateway],
  imports: [HttpModule],
  // imports: [PrismaService],
  // ... other module stuff
})
export class UsersModule {}
