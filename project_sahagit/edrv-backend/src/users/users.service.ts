import { HttpService } from '@nestjs/axios/dist';
import { Logger, Injectable } from '@nestjs/common';
import { UserEdrv } from '@prisma/client';
import { AxiosError } from 'axios';
import { catchError, firstValueFrom } from 'rxjs';
import { EdrvGateway } from 'src/gateways/edrv/edrv.gateway';
import { PrismaService } from 'src/shared/services/prisma/prisma.service';
import CreateNewUserDTO from './dto/createNewUser.dto';
import AddPaymentDTO from './dto/object/addPayment.dto';
import UpdateUserDTO from './dto/updateUser.dto';
import * as bcrypt from 'bcrypt';
// import userCarsDTO from './dto/userCars.dto';

@Injectable()
export class UsersService {
  private data = [];
  private readonly logger = new Logger();
  constructor(
    private readonly httpService: HttpService,
    private readonly prisma: PrismaService,
    private readonly eDrvGateway: EdrvGateway,
  ) {}

  async getUser(userId) {
    const payload = {
      accept: 'application/json',
      authorization: 'Bearer ' + process.env.EDRV_TOKEN,
    };
    const { data } = await firstValueFrom(
      this.httpService
        .get<any>(`https://api.edrv.io/v1.1/users/${userId}`, {
          headers: payload,
        })
        .pipe(
          catchError((error: AxiosError) => {
            this.logger.error(error.response.data);
            throw 'An error happened!';
          }),
        ),
    );
    console.log();
    return data;
  }

  async getUsers(): Promise<UserEdrv> {
    const payload = {
      accept: 'application/json',
      authorization: 'Bearer ' + process.env.EDRV_TOKEN,
    };
    const { data } = await firstValueFrom(
      this.httpService
        .get<any>('https://api.edrv.io/v1.1/users', { headers: payload })
        .pipe(
          catchError((error: AxiosError) => {
            this.logger.error(error.response.data);
            throw 'An error happened!';
          }),
        ),
    );

    console.log(data);
    return data;
  }
  async getAllUsersFromPrisma() {
    const query = this.prisma.userEdrv.findMany();
    console.log('query logs' + query);
    return query;
  }

  async createNewUserToEdrv(item: CreateNewUserDTO) {
    // const edrv = new EdrvGateway();
    // const userData = item;
    // console.log(userData);
    // delete userData.userLoginId;
    // delete userData.password;
    const { data, status } = await this.eDrvGateway.createNewUserGateway({
      firstname: item.firstname,
      lastname: item.lastname,
      email: item.email,
      address: {
        streetAndNumber: item.address.streetAndNumber,
        city: item.address.city,
        postalCode: item.address.postalCode,
        state: item.address.state,
        country: item.address.country,
      },
      phone: { mobile: item.phone.mobile },
      meta_data: {
        brand: item.meta_data.brand,
        model: item.meta_data.model,
        chargerType: item.meta_data.chargerType,
        licensePlate: item.meta_data.licensePlate,
      },
    });
    // console.log(item);
    return { data, status };
  }

  async createNewUserToPrisma(
    edrvId: string,
    item: CreateNewUserDTO,
  ): Promise<UserEdrv> {
    const hash = await this.hashPassword(item.password);
    return await this.prisma.userEdrv.create({
      data: {
        userEdrvId: edrvId,
        username: item.username,
        password: hash,
        firstName: item.firstname,
        lastName: item.lastname,
        email: item.email,
        phone: item.phone.mobile,
        // user_addresses: item.address,
        userAddresses: {
          create: {
            streetAndNumber: item.address.streetAndNumber,
            city: item.address.city,
            postCode: item.address.postalCode,
            country: item.address.country,
            state: item.address.state,
          },
        },
        cars: {
          create: {
            brand: item.meta_data.brand,
            model: item.meta_data.model,
            chargerType: item.meta_data.chargerType,
            licensePlate: item.meta_data.licensePlate,
          },
        },
      },
    });
  }
  async addPaymentInfo(userId: string, item: AddPaymentDTO): Promise<any> {
    console.log(userId);

    return this.prisma.userEdrv.update({
      where: {
        userEdrvId: userId,
      },
      data: {
        PaymentType: {
          create: {
            creditNumber: item.creditNumber,
            cardholderName: item.cardholderName,
            CVV: item.CVV,
            expireDate: item.expireDate,
            bookNumber: item.bookNumber,
          },
        },
      },
    });
  }

  async updateUser(userId: string, item: UpdateUserDTO): Promise<any> {
    // const edrv = new EdrvGateway();
    const { data, status } = await this.eDrvGateway.updateUserGateway(
      userId,
      item,
    );
    return { data, status };
  }

  // async addCars(userId: string, item: userCarsDTO) {
  //   await this.prisma.cars.create({
  //     data: {
  //       brand: item.brand,
  //       model: item.model,
  //       chargerType: item.chargerType,
  //       licensePlate: item.licensePlate,
  //     },
  //   });
  // }

  async updateUserToPrisma(
    edrvId: string,
    item: UpdateUserDTO,
  ): Promise<UserEdrv> {
    return await this.prisma.userEdrv.update({
      where: {
        userEdrvId: edrvId,
      },
      data: {
        firstName: item.firstname,
        lastName: item.lastname,
        // email: item.email,
        // user_addresses: item.address,
      },
    });
  }

  async deleteUser(userId: string) {
    const edrv = new EdrvGateway();
    const { data, status } = await edrv.deleteUserToGateway(userId);
    return { data, status };
  }

  async deleteUserPrisma(edrvId: string) {
    const result = await this.prisma.userEdrv.findMany({
      where: {
        userEdrvId: edrvId,
      },
      select: {
        userEdrvId: true,
      },
    });

    //test get userEdrvId
    console.log('result' + JSON.stringify(result));
    console.log('result[0].id' + result[0].userEdrvId);
    return await this.prisma.userEdrv.delete({
      where: {
        userEdrvId: edrvId,
      },
    });
  }
  async hashPassword(password: string) {
    const salt = parseInt(process.env.SALT_OR_ROUND);

    const hash = await bcrypt.hash(password, salt);

    return hash;
  }
}
