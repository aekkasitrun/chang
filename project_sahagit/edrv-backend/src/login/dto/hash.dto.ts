import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';
import { IsNotEmpty } from 'class-validator';

export default class HashDTO {
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  password: string;
}
