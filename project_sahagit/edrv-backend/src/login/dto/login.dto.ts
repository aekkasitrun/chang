import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export default class LoginAuthenDTO {
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  providerId: string;
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  password: string;
}
