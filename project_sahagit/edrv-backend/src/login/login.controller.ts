/*
https://docs.nestjs.com/controllers#controllers
*/

import { Controller, Get, Post, Res, Body } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { Response } from 'express';
import { BaseController } from 'src/common/abstracts';
import { LoginService } from './login.service';
import LoginAuthenDTO from './dto/login.dto';
import LoginRegisterDTO from './dto/register.dto';
import UserLoginAuthenDTO from './dto/userLogin.dto';
import HashDTO from './dto/hash.dto';

@ApiTags('login')
@Controller('login')
export class LoginController extends BaseController {
  constructor(private readonly loginService: LoginService) {
    super();
  }
  @Post('register')
  protected async register(
    @Res() res: Response,
    @Body() loginInfo: LoginRegisterDTO,
  ) {
    const registerData = await this.loginService.loginRegister(loginInfo);
    return res.send(registerData);
  }
  @Post('login')
  protected async loginAuth(
    @Res() res: Response,
    @Body() LoginData: LoginAuthenDTO,
  ) {
    const loginData = await this.loginService.loginAuth(LoginData);
    // return res.send(loginData);
    return this.ok(res, loginData);
  }
  @Post('userLogin')
  protected async userLogin(
    @Res() res: Response,
    @Body() userLogin: UserLoginAuthenDTO,
  ) {
    const userLoginData = await this.loginService.userLogin(userLogin);
    return this.ok(res, userLoginData);
  }

  @Post('hash')
  protected async hashFunction(
    @Res() res: Response,
    @Body() password: HashDTO,
  ) {
    const hash = await this.loginService.hashPassword(password.password);
    return res.send(hash);
  }
}
