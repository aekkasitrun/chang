import { LoginService } from './login.service';
import { LoginController } from './login.controller';
/*
https://docs.nestjs.com/modules
*/

import { Module } from '@nestjs/common';
import { PrismaService } from 'src/shared/services/prisma/prisma.service';
import { HttpModule } from '@nestjs/axios';

@Module({
  imports: [HttpModule],
  controllers: [LoginController],
  providers: [LoginService, PrismaService],
})
export class LoginModule {}
