/*
https://docs.nestjs.com/providers#services
*/

import { HttpService } from '@nestjs/axios';
import { Injectable, Logger } from '@nestjs/common';
import { PrismaService } from 'src/shared/services/prisma/prisma.service';
import LoginAuthenDTO from './dto/login.dto';
import LoginRegisterDTO from './dto/register.dto';
import UserLoginAuthenDTO from './dto/userLogin.dto';
import * as bcrypt from 'bcrypt';
import HashDTO from './dto/hash.dto';

@Injectable()
export class LoginService {
  private readonly logger = new Logger();
  constructor(
    private readonly httpService: HttpService,
    private readonly prisma: PrismaService,
  ) {}
  async loginRegister(item: LoginRegisterDTO) {
    const hash = await this.hashPassword(item.password);
    const register = await this.prisma.provider.create({
      data: {
        providerId: item.id,
        password: hash,
        firstName: item.firstName,
        lastName: item.lastName,
        email: item.email,
        phoneNumber: item.phoneNumber,
      },
    });
    console.log(register);
    return register;
  }
  async loginAuth(item: LoginAuthenDTO) {
    const login = await this.prisma.provider.findUnique({
      where: {
        providerId: item.providerId,
      },
    });
    console.log('LoginData = ' + JSON.stringify(login));

    const isMatch = await bcrypt.compare(item.password, login.password);
    if (login.providerId !== item.providerId) {
      return 'UserId is incorrect';
    }
    if (!isMatch) {
      return 'Password is incorrect';
    }

    return {
      firstName: login.firstName,
      lastName: login.lastName,
      email: login.email,
    };
  }
  async userLogin(item: UserLoginAuthenDTO) {
    const login = await this.prisma.userEdrv.findUnique({
      where: {
        username: item.username,
      },
    });
    console.log('LoginData = ' + JSON.stringify(login));

    if (login.username !== item.username) {
      return 'UserId is incorrect';
    }
    if (login.password !== item.password) {
      return 'Password is incorrect';
    }

    return {
      firstName: login.firstName,
      lastName: login.lastName,
      email: login.email,
    };
  }
  async hashPassword(password: string) {
    const salt = parseInt(process.env.SALT_OR_ROUND);

    const hash = await bcrypt.hash(password, salt);

    return hash;
  }
}
