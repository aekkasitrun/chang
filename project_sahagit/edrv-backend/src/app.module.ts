import { LoginModule } from './login/login.module';
import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { SampleApiModule } from './sample-api/sample-api.module';
import { getEnvPath } from './utils/env.util';
import { ConfigModule } from '@nestjs/config';
import configuration from './config/configuration';
import { APP_FILTER } from '@nestjs/core';
import { AllExceptionsFilter } from './common/exceptions/all-exception.filter';
import { UsersModule } from './users/users.module';
import { SessionsModule } from './session/session.module';
import { LocationModule } from './location/location.module';
import { RateModule } from './rate/rate.module';
import { RfidModule } from './rfid/rfid.module';
import { ChargeStationModule } from './charge-station/charge-station.module';
import { AdvanceChargeStationModule } from './advance-charge-station/advance-charge-station.module';
// import { OrganizationsModule } from './organizations/organizations.module';
import { OrganizationModule } from './organization/organization.module';
import { AdvancedRateModule } from './advanced-rate/advanced-rate.module';
import { ConnectorModule } from './connector/connector.module';
import { EDrvModule } from './gateways/edrv/edev.module';
import { TransactionModule } from './transaction/transaction.module';
import { PaymentModule } from './payment/payment.module';

const envFilePath: string = getEnvPath(process.cwd());

@Module({
  imports: [
    LoginModule,
    SampleApiModule,
    ConfigModule.forRoot({
      cache: true,
      isGlobal: true,
      envFilePath,
      expandVariables: true,
      load: [configuration],
    }),
    UsersModule,
    SessionsModule,
    LocationModule,
    RateModule,
    RfidModule,
    ChargeStationModule,
    AdvanceChargeStationModule,
    OrganizationModule,
    AdvancedRateModule,
    ConnectorModule,
    EDrvModule,
    TransactionModule,
    PaymentModule,
    // OrganizationsModule,
  ],
  controllers: [AppController],
  providers: [
    AppService,
    {
      provide: APP_FILTER,
      useClass: AllExceptionsFilter,
    },
  ],
})
export class AppModule {}
