import { Logger, Injectable } from '@nestjs/common';
import { AxiosError } from 'axios';
import { catchError, firstValueFrom } from 'rxjs';
import { HttpService } from '@nestjs/axios';
import { EdrvGateway } from 'src/gateways/edrv/edrv.gateway';
import CreateNewSession from './dto/createNewSession.dto';
import UpdateSessionDTO from './dto/updateSession.dto';
import { PrismaService } from 'src/shared/services/prisma/prisma.service';
import { DataType } from './types/Data.types';

@Injectable()
export class SessionsService {
  private readonly logger = new Logger();
  constructor(
    private readonly httpService: HttpService,
    private readonly prisma: PrismaService, // private readonly ddrvGateway: EdrvGateway,
  ) {}

  async getSessions(): Promise<any> {
    const payload = {
      accept: 'application/json',
      authorization: 'Bearer ' + process.env.EDRV_TOKEN,
    };
    const { data } = await firstValueFrom(
      this.httpService
        .get<any>('https://api.edrv.io/v1.1/sessions', { headers: payload })
        .pipe(
          catchError((error: AxiosError) => {
            this.logger.error(error.response.data);
            throw 'An error happened!';
          }),
        ),
    );
    console.log(data);
    return data;
  }

  // async getSession(sessionId) {
  //   const payload = {
  //     accept: 'application/json',
  //     authorization: 'Bearer ' + process.env.EDRV_TOKEN,
  //   };
  //   const { data } = await firstValueFrom(
  //     this.httpService
  //       .get<any>(`https://api.edrv.io/v1.1/sessions/${sessionId}`, {
  //         headers: payload,
  //       })
  //       .pipe(
  //         catchError((error: AxiosError) => {
  //           this.logger.error(error.response.data);
  //           throw 'An error happened!';
  //         }),
  //       ),
  //   );
  //   console.log();
  //   return data;
  // }
  async getSession(sessionId) {
    const edrv = new EdrvGateway();
    const a = await edrv.sessions({
      method: 'get',
      sessionId,
      restURL: '',
    });
    return a;
  }
  async stopSessionToPrisma(item: DataType) {
    console.log('ITEM' + item.result);

    await this.prisma.transaction.create({
      data: {
        transactionEdrvId: item.result._id,
        userEdrvId: item.result.user,
        chargeStationEdrvId: item.result.chargestation,
        startTime: item.result.metrics.chargingStart,
        endTime: item.result.metrics.chargingStop,
        duration: item.result.metrics.energyPeriod,
        cost: item.result.cost.amount,
        connectorEdrvId: item.result.connector,
        consumption: item.result.metrics.energyConsumed,
        EnergyReport: {
          create: {
            timestamp: item.result.energy_report.timestamp,
            currentValue: Number(item.result.energy_report.current.value),
            powerValue: Number(item.result.energy_report.power.value),
            energyMeterValue: Number(
              item.result.energy_report.energy_meter.value,
            ),
            stateOfChargeValue: Number(
              item.result.energy_report.state_of_charge.value,
            ),
          },
        },
      },
    });
  }

  async stopSession(sessionId) {
    const edrv = new EdrvGateway();
    const a = await edrv.sessions({
      method: 'get',
      sessionId,
      restURL: '/stop',
    });

    return a;
  }

  async energy_reportsSession(sessionId) {
    const edrv = new EdrvGateway();
    const a = await edrv.sessions({
      method: 'get',
      sessionId,
      restURL: '/energy_reports',
    });

    return a;
  }

  async createNewSession(item: CreateNewSession): Promise<any> {
    const edrv = new EdrvGateway();
    const { data, status } = await edrv.createNewSession(item);

    console.log(item);
    return { data, status };
  }

  async updateSession(sessionId: string, item: UpdateSessionDTO): Promise<any> {
    const edrv = new EdrvGateway();
    const { data, status } = await edrv.updateSession(sessionId, item);
    console.log(item);
    return { data, status };
  }
}
