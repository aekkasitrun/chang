import { ResultType } from './results.types';
export interface DataType {
  ok: boolean;
  message: string;
  result: ResultType;
}
