export interface EnergyReportType {
  timestamp: string;
  current: {
    value: number;
    unit: string;
  };
  power: {
    value: number;
    unit: string;
  };
  energy_meter: {
    value: number;
    unit: string;
  };
  state_of_charge: {
    unit: string;
    value: number;
  };
}
