export interface CostType {
  amount: number;
  currency: string;
  breakdown: [
    {
      type: string;
      period: {
        startTime: Date | string;
        endTime: Date | string;
      };
      rateName: string;
      quantity: number;
      unit: string;
      unitPrice: number;
      beforeTax: number;
      tax: number;
      totalTax: number;
      totalCost: number;
    },
  ];
}
