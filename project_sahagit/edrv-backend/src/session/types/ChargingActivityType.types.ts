export interface ChargingActivityType {
  startTime: string;
  endTime: string;
  status: string;
}
