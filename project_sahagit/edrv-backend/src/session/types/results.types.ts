import { ChargingActivityType } from './ChargingActivityType.types';
import { CostType } from './CostType.types';
import { EnergyReportType } from './EnergyReportType.types';
import { MetricsType } from './MatricsType.types';
import { PaymentType } from './payment.types';

export interface ResultType {
  _id: string;
  payment: PaymentType;
  metrics: MetricsType;
  chargestation: string;
  connector: string;
  status: string;
  rate: string;
  user: string;
  energy_report: EnergyReportType;
  charging_activity: ChargingActivityType;
  cost: CostType;
  createdAt: Date | string;
  updatedAt: Date | string;
  edrv_meta_data: {
    user: {
      email: string;
      name: string;
    };
  };
}
