export interface MetricsType {
  chargingStart: string;
  meterStart: number;
  timezone: string;
  chargingStop: string;
  meterStop: number;
  energyPeriod: number;
  energyConsumed: number;
}
