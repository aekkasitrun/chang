export interface PaymentType {
  owner_type: string;
  deleted: boolean;
  _id: string;
  organization: string;
  stripe_customer_id: string | null;
  stripe_payment_method_id: string | null;
  stripe_intent_id: string | null;
  stripe_custom_balance_transaction_id: string | null;
  pre_auth_amount: string | null;
  currency: string;
  transaction: string;
  user: string;
  type: string;
  createdAt: Date | string;
  updatedAt: Date | string;
  __v: number;
}
