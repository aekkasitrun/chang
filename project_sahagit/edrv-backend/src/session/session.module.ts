import { Module } from '@nestjs/common';
import { SessionsController } from './session.controller';
import { SessionsService } from './session.service';
import { HttpModule } from '@nestjs/axios';
import { PrismaService } from 'src/shared/services/prisma/prisma.service';
@Module({
  imports: [
    HttpModule.registerAsync({
      useFactory: () => ({
        timeout: 5000,
      }),
    }),
  ],
  controllers: [SessionsController],
  providers: [SessionsService, PrismaService],
})
export class SessionsModule {}
