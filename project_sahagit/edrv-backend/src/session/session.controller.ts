import { Response } from 'express';
import { ApiTags } from '@nestjs/swagger';
import { SessionsService } from './session.service';
import { Controller, Get, Param, Res, Post, Body, Patch } from '@nestjs/common';
import { BaseController } from 'src/common/abstracts';
import CreateNewSession from './dto/createNewSession.dto';
import UpdateSessionDTO from './dto/updateSession.dto';

@ApiTags('sessions')
@Controller('sessions')
export class SessionsController extends BaseController {
  constructor(private readonly sessionsApi: SessionsService) {
    super();
  }

  @Get()
  protected async getSessions(@Res() res: Response) {
    const allSessions = await this.sessionsApi.getSessions();
    return res.send(allSessions);
  }

  @Get(':sessionId')
  protected async getSession(
    @Param('sessionId') sessionId: string,
    @Res() res: Response,
  ) {
    const getSession = await this.sessionsApi.getSession(sessionId);

    return res.send(getSession.data);
  }

  @Get(':sessionId/stop')
  protected async stopSession(
    @Param('sessionId') sessionId: string,
    @Res() res: Response,
  ) {
    /*const stopSession =*/ await this.sessionsApi.stopSession(sessionId);
    //console.log(stopSession.data);
    await new Promise((f) => setTimeout(f, 5000));
    const temp = await this.sessionsApi.getSession(sessionId);
    await new Promise((f) => setTimeout(f, 5000));
    //console.log(temp.data);
    console.log(JSON.stringify(JSON.parse(JSON.stringify(temp.data))));

    await this.sessionsApi.stopSessionToPrisma(temp.data);
    return res.send(temp.data);
  }

  @Get(':sessionId/energy_reports')
  protected async energy_reportsSession(
    @Param('sessionId') sessionId: string,
    @Res() res: Response,
  ) {
    //const onesession = await this.get(sessionId);
    const energy_reportsSession = await this.sessionsApi.energy_reportsSession(
      sessionId,
    );

    return res.send(energy_reportsSession.data);
  }
  @Post()
  protected async createNewSession(
    @Body() newSession: CreateNewSession,
    @Res() res: Response,
  ) {
    const createSession = await this.sessionsApi.createNewSession(newSession);
    return res.send(createSession);
  }
  @Patch(':sessionId')
  protected async updateSession(
    @Param('sessionId') sessionId: string,
    @Body() sessionInfo: UpdateSessionDTO,
    @Res() res: Response,
  ) {
    const updateSessionInfo = await this.sessionsApi.updateSession(
      sessionId,
      sessionInfo,
    );
    return res.send(updateSessionInfo);
  }
}
