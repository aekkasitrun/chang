import { Test, TestingModule } from '@nestjs/testing';
import { SampleApiService } from './sample-api.service';

describe('SampleApiService', () => {
  let service: SampleApiService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [SampleApiService],
    }).compile();

    service = module.get<SampleApiService>(SampleApiService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
