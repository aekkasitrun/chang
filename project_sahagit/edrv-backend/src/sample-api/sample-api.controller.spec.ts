import { Test, TestingModule } from '@nestjs/testing';
import { SampleApiController } from './sample-api.controller';

describe('SampleApiController', () => {
  let controller: SampleApiController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [SampleApiController],
    }).compile();

    controller = module.get<SampleApiController>(SampleApiController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
