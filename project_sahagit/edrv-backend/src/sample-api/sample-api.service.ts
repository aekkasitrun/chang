import { Injectable } from '@nestjs/common';
import { Post, User } from '@prisma/client';
import { IGenericService } from 'src/common/abstracts';
import { PrismaService } from 'src/shared/services/prisma/prisma.service';
import CreateUserDTO from './dto/createUser.dto';
import CreatePostDTO from './dto/createPost.dto';
import EditPostDTO from './dto/editPost.dto';

@Injectable()
export class SampleApiService extends IGenericService<
  User | Post | CreateUserDTO | CreatePostDTO | EditPostDTO
> {
  constructor(private readonly prisma: PrismaService) {
    super();
  }

  async list(): Promise<(User | Post)[]> {
    // Some logic here
    const result = await this.prisma.user.findMany({});

    return result;
  }

  async get(id: string): Promise<User | Post> {
    const result = await this.prisma.user.findUnique({
      where: {
        id: id,
      },
      include: {
        posts: true,
      },
    });

    return result;
  }

  //   Create normal
  async create(item: CreateUserDTO): Promise<User> {
    return await this.prisma.user.create({
      data: {
        userName: item.userName,
      },
    });
  }

  //   Create with reference
  async addPost({ userId, content }: CreatePostDTO): Promise<Post> {
    return this.prisma.post.create({
      data: {
        content: content,
        // content,
        author: {
          // Reference this Post by to user using userId
          connect: {
            id: userId,
          },
        },
      },
    });
  }

  // async editPost({ postId, content }: EditPostDTO): Promise<Post> {
  //   return await this.prisma.post.update({
  //     where: {
  //       id: postId,
  //     },
  //     data: {
  //       content, // short term of {content:content}
  //     },
  //   });
  // }

  // async deletePost(postId: string) {
  //   return await this.prisma.post.delete({
  //     where: {
  //       id: postId,
  //     },
  //   });
  // }

  async updateUser(userId: string, item: CreateUserDTO): Promise<User> {
    return await this.prisma.user.update({
      where: {
        id: userId,
      },
      data: {
        userName: item.userName, // short term of {content:content}
      },
    });
  }

  async deletePost(userId: string) {
    return await this.prisma.user.delete({
      where: {
        id: userId,
      },
    });
  }
}
