import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';

export default class CreateUserDTO {
  @ApiProperty()
  @IsString()
  userName: string;
}
