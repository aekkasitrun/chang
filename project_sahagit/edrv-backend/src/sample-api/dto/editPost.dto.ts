import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export default class EditPostDTO {
  @ApiProperty()
  @IsString()
  postId?: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  content: string;
}
