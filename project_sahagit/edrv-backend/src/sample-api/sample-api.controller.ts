import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Res,
} from '@nestjs/common';
import { Response } from 'express';
import { BaseController } from 'src/common/abstracts';
import { SampleApiService } from './sample-api.service';
import CreateUserDTO from './dto/createUser.dto';
// import EditPostDTO from './dto/editPost.dto';
import { ApiTags } from '@nestjs/swagger';

@ApiTags('sample-api')
@Controller('sample-api')
export class SampleApiController extends BaseController {
  constructor(
    private readonly sampleApi: SampleApiService, // More service here
  ) {
    super();
  }

  @Get()
  protected async getUsers(@Res() res: Response) {
    const users = await this.sampleApi.list();
    return this.ok(res, users);
  }

  @Get(':userId')
  protected async getOneUser(
    @Param('userId') userId: string,
    @Res() res: Response,
  ) {
    const oneUser = await this.sampleApi.get(userId);

    return this.ok(res, oneUser);
  }

  @Post()
  protected async createUser(
    @Body() newUser: CreateUserDTO,
    @Res() res: Response,
  ) {
    try {
      await this.sampleApi.create(newUser);
      return this.created(res);
    } catch (error) {
      return this.fail(res, error);
    }
  }

  // @Put(':postId')
  // protected async updatePost(
  //   @Param('postId') postId: string,
  //   @Body() postData: EditPostDTO,
  //   @Res() res: Response,
  // ) {
  //   const updateResult = await this.sampleApi.editPost({
  //     postId,
  //     content: postData.content,
  //   });
  //   return this.ok(res, updateResult);
  // }

  @Put(':userId')
  protected async updateUser(
    @Param('userId') userId: string,
    @Body() userData: CreateUserDTO,
    @Res() res: Response,
  ) {
    const updateResult = await this.sampleApi.updateUser(userId, userData);
    return res.send(updateResult);
  }

  @Delete(':userId')
  protected async deleteUser(
    @Param('userId') userId: string,
    @Res() res: Response,
  ) {
    const deleteResult = await this.sampleApi.deletePost(userId);
    return res.send(deleteResult);
  }
}
