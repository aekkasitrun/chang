import { Module } from '@nestjs/common';
import { SampleApiController } from './sample-api.controller';
import { SampleApiService } from './sample-api.service';
import { PrismaService } from 'src/shared/services/prisma/prisma.service';

@Module({
  controllers: [SampleApiController],
  providers: [SampleApiService, PrismaService],
})
export class SampleApiModule {}
