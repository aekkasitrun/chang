import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class UpdateConnectorDTO {
  // @ApiProperty()
  // @IsNotEmpty()
  // active: boolean;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  rate: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  format: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  type: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  power_type: string;

  @ApiProperty()
  @IsNotEmpty()
  power: number;
}
