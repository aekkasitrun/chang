import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class CreateNewConnectorDTO {
  @ApiProperty()
  @IsString()
  name: string;

  @ApiProperty()
  @IsString()
  description: string;

  @ApiProperty()
  @IsNotEmpty()
  active: boolean;

  @ApiProperty()
  @IsString()
  rate: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  format: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  type: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  power_type: string;

  @ApiProperty()
  @IsNotEmpty()
  power: number;
}
