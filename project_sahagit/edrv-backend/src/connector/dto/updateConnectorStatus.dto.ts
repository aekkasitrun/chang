import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export default class UpdateConnectorStatusDTO {
  @ApiProperty()
  @IsNotEmpty()
  active: boolean;
}
