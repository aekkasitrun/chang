import { Module } from '@nestjs/common';
import { ConnectorService } from './connector.service';
import { ConnectorController } from './connector.controller';
import { HttpModule } from '@nestjs/axios';
import { PrismaService } from 'src/shared/services/prisma/prisma.service';

@Module({
  controllers: [ConnectorController],
  providers: [ConnectorService, PrismaService],
  imports: [HttpModule],
})
export class ConnectorModule {}
