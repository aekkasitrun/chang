import { Logger, Injectable } from '@nestjs/common';
import { AxiosError } from 'axios';
import { catchError, firstValueFrom } from 'rxjs';
import { HttpService } from '@nestjs/axios/dist';
import { EdrvGateway } from 'src/gateways/edrv/edrv.gateway';
import { CreateNewConnectorDTO } from './dto/create-connector.dto';
import { UpdateConnectorDTO } from './dto/update-connector.dto';
import { PrismaService } from 'src/shared/services/prisma/prisma.service';
import { Connector } from '@prisma/client';
import UpdateConnectorStatusDTO from './dto/updateConnectorStatus.dto';
enum STATUS {
  STARTED = 'started',
  ENDED = 'ended',
}

@Injectable()
export class ConnectorService {
  private data = [];
  private readonly logger = new Logger();
  constructor(
    private readonly httpService: HttpService,
    private readonly prisma: PrismaService,
  ) {}

  async createNewConnectorService(
    chargestationId: string,
    item: CreateNewConnectorDTO,
  ): Promise<any> {
    const edrv = new EdrvGateway();
    const { data, status } = await edrv.createNewConnectorGateway(
      chargestationId,
      item,
    );

    console.log('item' + item);
    return { data, status };
  }

  async createNewConnectorToPrisma(
    chargestationId: string,
    edrvId: string,
    item: CreateNewConnectorDTO,
  ): Promise<Connector> {
    return await this.prisma.connector.create({
      data: {
        connectorEdrvId: edrvId,
        name: item.name,
        description: item.description,
        active: item.active,
        rateEdrvId: item.rate,
        format: item.format,
        type: item.type,
        power_type: item.power_type,
        power: item.power,
        chargeStationEdrvId: chargestationId,
      },
    });
  }

  async updateConnector(
    connectorId: string,
    item: UpdateConnectorDTO,
  ): Promise<any> {
    const edrv = new EdrvGateway();
    const { data, status } = await edrv.updateConnector(connectorId, item);

    return { data, status };
  }

  async updateConnectorToPrisma(connectorId: string, item: UpdateConnectorDTO) {
    return await this.prisma.connector.update({
      where: {
        connectorEdrvId: connectorId,
      },
      data: {
        rateEdrvId: item.rate,
        format: item.format,
        type: item.type,
        power_type: item.power_type,
        power: item.power,
      },
    });
  }

  async updateConnectorStatus(
    connectorId: string,
    item: UpdateConnectorStatusDTO,
  ) {
    const edrv = new EdrvGateway();
    console.log('id= ' + connectorId);
    console.log('item = ' + JSON.stringify(item));
    const { data, status } = await edrv.updateConnectorStatus(
      connectorId,
      item,
    );
    await this.prisma.connector.update({
      where: {
        connectorEdrvId: connectorId,
      },
      data: {
        active: item.active,
      },
    });
    return { data, status };
  }

  async deleteConnector(connectorId: string): Promise<any> {
    const edrv = new EdrvGateway();
    const { data, status } = await edrv.deleteConnector(connectorId);

    return { data, status };
  }

  async deleteConnectorPrisma(edrvId: string) {
    const result = await this.prisma.connector.findMany({
      where: {
        connectorEdrvId: edrvId,
      },
      select: {
        connectorEdrvId: true,
      },
    });
    console.log('result' + JSON.stringify(result));
    console.log('result[0].id' + result[0].connectorEdrvId);
    return await this.prisma.connector.delete({
      where: {
        connectorEdrvId: result[0].connectorEdrvId,
      },
    });
  }

  async connectorRate() {
    const temp = await this.prisma.connector.groupBy({
      by: ['rateEdrvId'],
      _count: true,
    });
    return temp;
  }

  async onlineConnectorCount() {
    const payload = {
      accept: 'application/json',
      authorization: 'Bearer ' + process.env.EDRV_TOKEN,
    };
    const { data } = await firstValueFrom(
      this.httpService
        .get<any>('https://api.edrv.io/v1.1/sessions', { headers: payload })
        .pipe(
          catchError((error: AxiosError) => {
            this.logger.error(error.response.data);
            throw 'An error happened!';
          }),
        ),
    );
    // const chargingCount = { data };
    // let count = 0;
    // data.result.connector.status
    // data.result.forEach((element) => {
    //     data.result.connector[element].status.forEach(element => {

    //     });
    // });
    const temp = data.result;
    let charging = 0;
    // temp.forEach((element) => {
    //   if (temp.result[element].status == 'started') {
    //     count++;
    //   }
    // });
    // console.log(temp.length);

    for (let i = 0; i < temp.length; i++) {
      if (temp[i].status == STATUS.STARTED) {
        charging++;
      }
    }
    // const temp2 = temp[0].connectors[0].status;
    return { charging };
  }
}
