import {
  Controller,
  Param,
  Get,
  Res,
  Post,
  Body,
  Patch,
  Delete,
} from '@nestjs/common';
import { ConnectorService } from './connector.service';
import { HttpService } from '@nestjs/axios';
import { ApiTags } from '@nestjs/swagger';
import { Response } from 'express';
import { BaseController } from 'src/common/abstracts';
import { CreateNewConnectorDTO } from './dto/create-connector.dto';
import { UpdateConnectorDTO } from './dto/update-connector.dto';
import UpdateConnectorStatusDTO from './dto/updateConnectorStatus.dto';

@ApiTags('connectors')
@Controller('connectors')
export class ConnectorController extends BaseController {
  constructor(
    private readonly connectorApi: ConnectorService,
    private readonly httpService: HttpService,
  ) {
    super();
  }

  @Post(':chargeStationId')
  protected async createNewConnector(
    @Param('chargeStationId') chargestationId: string,
    @Body() newConnector: CreateNewConnectorDTO,
    @Res() res: Response,
  ) {
    const createConnector = await this.connectorApi.createNewConnectorService(
      chargestationId,
      newConnector,
    );
    await this.connectorApi.createNewConnectorToPrisma(
      chargestationId,
      createConnector.data.result._id,
      newConnector,
    );
    return res.send(createConnector);
  }

  @Patch(':connectorId')
  protected async updateConnector(
    @Param('connectorId') connectorId: string,
    @Body() newInfo: UpdateConnectorDTO,
    @Res() res: Response,
  ) {
    const updateConnector = await this.connectorApi.updateConnector(
      connectorId,
      newInfo,
    );
    await this.connectorApi.updateConnectorToPrisma(connectorId, newInfo);
    // const updateConnector = 'In function patch';

    return this.ok(res, updateConnector);
  }

  @Patch(':connectorId/active')
  protected async updateConnectorStatus(
    @Param('connectorId') connectorId: string,
    @Body() newInfo: UpdateConnectorStatusDTO,
    @Res() res: Response,
  ) {
    const updateConnector = await this.connectorApi.updateConnectorStatus(
      connectorId,
      newInfo,
    );
    // const updateConnector = 'In function patch';

    return res.send(updateConnector);
  }

  @Delete(':connectorId')
  protected async deleteConnector(
    @Param('connectorId') connectorId: string,
    @Res() res: Response,
  ) {
    const deleteConnectorByID = await this.connectorApi.deleteConnector(
      connectorId,
    );
    await this.connectorApi.deleteConnectorPrisma(connectorId);
    return res.send(deleteConnectorByID);
  }
  @Get()
  protected async getConnectorAmount(@Res() res: Response) {
    const connectorAmount = await this.connectorApi.connectorRate();
    return res.send(connectorAmount);
  }
  @Get('onlineCount')
  protected async countConnectorOnline(@Res() res: Response) {
    const onlineConnector = await this.connectorApi.onlineConnectorCount();
    return res.send(onlineConnector);
  }
}
