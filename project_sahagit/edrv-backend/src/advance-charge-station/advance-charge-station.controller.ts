import { Controller, Get, Param, Res } from '@nestjs/common';
import { AdvanceChargeStationService } from './advance-charge-station.service';
import { HttpService } from '@nestjs/axios';
import { ApiTags } from '@nestjs/swagger';
import { Response } from 'express';

@ApiTags('advance-charge-stations')
@Controller('advance-charge-stations')
export class AdvanceChargeStationController {
  constructor(
    private readonly chargeStationApi: AdvanceChargeStationService,
    private readonly httpService: HttpService,
  ) {}

  @Get(':chargestationId/configurations')
  protected async getChargeStation(
    @Param('chargestationId') chargestationId: string,
    @Res() res: Response,
  ) {
    //const oneUser = await this.get(userId);
    const chargeStation = await this.chargeStationApi.getChargestations(
      chargestationId,
    );

    return res.send(chargeStation.data);
  }
}
