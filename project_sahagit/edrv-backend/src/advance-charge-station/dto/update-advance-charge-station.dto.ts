import { PartialType } from '@nestjs/swagger';
import { CreateAdvanceChargeStationDto } from './create-advance-charge-station.dto';

export class UpdateAdvanceChargeStationDto extends PartialType(CreateAdvanceChargeStationDto) {}
