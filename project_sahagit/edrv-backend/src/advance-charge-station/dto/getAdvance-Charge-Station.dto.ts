import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export default class GetAdvanceChargeStationDTO {
  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  chargestationId?: string;
}
