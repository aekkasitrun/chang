import { Test, TestingModule } from '@nestjs/testing';
import { AdvanceChargeStationService } from './advance-charge-station.service';

describe('AdvanceChargeStationService', () => {
  let service: AdvanceChargeStationService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [AdvanceChargeStationService],
    }).compile();

    service = module.get<AdvanceChargeStationService>(
      AdvanceChargeStationService,
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
