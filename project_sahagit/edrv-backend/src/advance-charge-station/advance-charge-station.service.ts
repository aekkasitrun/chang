import { Logger, Injectable } from '@nestjs/common';
// import { AxiosError } from 'axios';
// import { catchError, firstValueFrom } from 'rxjs';
import { HttpService } from '@nestjs/axios/dist';
import { EdrvGateway } from 'src/gateways/edrv/edrv.gateway';
// import { BaseController } from 'src/common/abstracts';

@Injectable()
export class AdvanceChargeStationService {
  private data = [];
  private readonly logger = new Logger();
  constructor(private readonly httpService: HttpService) {}

  async getChargestations(chargestationId) {
    const edrv = new EdrvGateway();
    const a = await edrv.chargeStations({
      method: 'get',
      chargestationId,
      restURL: '/configurations',
    });
    return a;
  }
}
