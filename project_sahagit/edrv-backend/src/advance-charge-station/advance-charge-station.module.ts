import { Module } from '@nestjs/common';
import { AdvanceChargeStationService } from './advance-charge-station.service';
import { AdvanceChargeStationController } from './advance-charge-station.controller';
import { HttpModule } from '@nestjs/axios';

@Module({
  controllers: [AdvanceChargeStationController],
  providers: [AdvanceChargeStationService],
  imports: [HttpModule],
})
export class AdvanceChargeStationModule {}
