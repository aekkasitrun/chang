import { Test, TestingModule } from '@nestjs/testing';
import { AdvanceChargeStationController } from './advance-charge-station.controller';

describe('AdvanceChargeStationController', () => {
  let controller: AdvanceChargeStationController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [AdvanceChargeStationController],
    }).compile();

    controller = module.get<AdvanceChargeStationController>(
      AdvanceChargeStationController,
    );
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
