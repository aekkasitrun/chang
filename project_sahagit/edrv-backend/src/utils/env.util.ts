import { existsSync } from 'fs';
import { resolve } from 'path';

export function getEnvPath(dest: string): string {
  const env: string | undefined = process.env.NODE_ENV;
  const fallback: string = resolve(`${dest}/.env.development.local`);
  const filename: string = env ? `.env.${env}.local` : '.env.development.local';
  let filePath: string = resolve(`${dest}/${filename}`);

  if (!existsSync(filePath)) {
    console.log('notfound env file!', env);
    filePath = fallback;
  }
  console.log(filePath);

  return filePath;
}
