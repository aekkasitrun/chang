import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Res,
  HttpStatus,
  Req,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { Request, Response } from 'express';
import { BaseController } from 'src/common/abstracts';
import NewPaymentRecordDTO from './dto/newPayment.dto';
import { PaymentService } from './payment.service';
import PaymentRequestBody from './types/PaymentRequestBody';
@ApiTags('payment')
@Controller('payment')
export class PaymentController extends BaseController {
  constructor(private readonly paymentService: PaymentService) {
    super();
  }
  @Post()
  protected async createNewPaymentData(
    @Res() res: Response,
    @Body() paymentData: NewPaymentRecordDTO,
  ) {
    const paymentRecord = await this.paymentService.paymentRecord(paymentData);
    return res.send(paymentRecord);
  }
  @Post('/payment')
  async createPayments(
    @Res() res: Response,
    @Body() paymentRequestBody: PaymentRequestBody,
    // @Req() req: Request,
  ) {
    // console.log(req.rawHeaders);
    // console.log(req.body);
    // return response.send(paymentRequestBody);
    // return this.paymentService
    //   .createPayment(paymentRequestBody)
    //   .then((res) => {
    //     // console.log('success' + JSON.stringify(res));
    //     return res.status(HttpStatus.CREATED).json(res);
    //   })
    //   .catch((err) => {
    //     console.log('fail' + JSON.stringify(err));
    //     return res.status(HttpStatus.BAD_REQUEST).json(err);
    //   });
    const data = await this.paymentService.createPayment(paymentRequestBody);
    console.log('success');
    return res.status(HttpStatus.CREATED).json(data);

    // try {

    //
    // } catch (err) {
    //   console.log('fail');
    //   return res.status(HttpStatus.BAD_REQUEST).json(err);
    // }
  }
}
