import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export default class NewPaymentRecordDTO {
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  creditNumber: string;
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  cardHolderName: string;
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  expireDate: string;
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  cvv: string;
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  bookBankNumber: string;
}
