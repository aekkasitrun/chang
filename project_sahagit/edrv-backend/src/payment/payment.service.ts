import { HttpService } from '@nestjs/axios';
import { Injectable } from '@nestjs/common';
import { Logger } from '@nestjs/common/services';
import { PrismaService } from 'src/shared/services/prisma/prisma.service';
import NewPaymentRecordDTO from './dto/newPayment.dto';
import Stripe from 'stripe';
import PaymentRequestBody from './types/PaymentRequestBody';
@Injectable()
export class PaymentService {
  private stripe;
  private readonly logger = new Logger();
  constructor(
    private readonly httpService: HttpService,
    private readonly prisma: PrismaService, // private readonly ddrvGateway: EdrvGateway,
  ) {
    this.stripe = new Stripe(process.env.STRIPE_PRIVATE_APIKEY, {
      apiVersion: '2022-11-15',
    });
  }
  async paymentRecord(item: NewPaymentRecordDTO) {
    const payment = await this.prisma.paymentType.create({
      data: {
        creditNumber: item.creditNumber,
        cardholderName: item.cardHolderName,
        expireDate: item.expireDate,
        CVV: item.cvv,
        bookNumber: item.bookBankNumber,
      },
    });
    // console.log(payment);

    return payment;
  }
  async createPayment(paymentRequestBody: PaymentRequestBody) {
    let sumAmount = 0;
    sumAmount = paymentRequestBody.price;
    console.log(sumAmount);

    return await this.stripe.paymentIntents.create({
      amount: sumAmount * 100,
      currency: paymentRequestBody.currency,
      payment_method_types: ['card'],
      // automatic_payment_methods: { enabled: true },
    });
  }
}
