import { Product } from './Product';

export interface PaymentRequestTypes {
  products: Product[];
  currency: string;
}
