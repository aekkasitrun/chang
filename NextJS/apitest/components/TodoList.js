import React from 'react'
import { useState } from 'react'
import { addTodo, removeTodo } from '../redux/action'
import { useDispatch, useSelector } from 'react-redux'
import { Formik, Form, Field } from 'formik'
import * as Yup from 'yup';



const TodoListSchema = Yup.object().shape({ //shape=หน้าตาของ object เป็นดังนี้
    name: Yup.string()
        .min(2, 'Too Short!')   //ประกาศตัวแปรชื่อ TodoListSchema ให้มีความสามารถของ Yup ซึ่งเป็น Schema ของข้อมูลที่อยู่ในรูป Object ซึ่งใช้สำหรับการทำ validation โดยการตั้งชื่อ key จะสอดคล้องกับ name ใน Field
        .max(50, 'Too Long!')
        .required('Required'),
});

export const TodoList = () => {
    const [inputEvent, setInputEvent] = useState('')    //ประกาศตัวแปรในรูปแบบของ array ประกอบด้วย inputEvent และ setInputEvent เพื่อใช้ set state ที่ผู้ใช้พิมพ์เข้ามา
    const dispatch = useDispatch()                      //ประกาศตัวแปร dispatch ใช้ในการเปลี่ยนแปลง state 



    const selector = useSelector(state => state.todoReducer.list)   //ประกาศตัวแปร selector ใช้ในการดึง state ที่อยู่ใน redux store ออกมา ณ. ที่นี่คือ todoReducer
    console.log(selector)

    return (
        <div className='flex flex-col gap-4 bg-slate-200 p-5'>
            <Formik
                initialValues={{
                    name: ''

                }}
                validationSchema={TodoListSchema}
                onSubmit={(values, { resetForm }) => {
                    // same shape as initial values
                    dispatch(addTodo(values.name), setInputEvent(''))

                    resetForm({ values: '' })
                    console.log(values);
                }}
            >
                {({ values, handleChange, errors, touched }) => (
                    <Form className='space-x-4'>
                        <Field type="text"
                            name="name"
                            // onChange={(e) => { setInputEvent(e.target.value) }}
                            // เมื่อมีการพิมพ์ให้ set state ที่ตัวแปร value={inputEvent}
                            onChange={handleChange}
                            placeholder='Enter Here...'
                            className='pl-4 p-3 border-2 border-black w-96'
                            value={values.name}
                        />
                        {errors.name && touched.name ? (
                            <div>{errors.name}</div>
                        ) : null}

                        <button type="submit"
                            // onClick={() => dispatch(addTodo(inputEvent), setInputEvent(''))}
                            className='border-2 border-black pl-4 p-3 bg-blue-500 text-white rounded-lg '>
                            ADD
                        </button>
                    </Form>
                )}
            </Formik>

            {selector.map((item) => {
                // map: หนึ่งใน Method ที่ใช้ในการจัดการ array และ return ค่าออกมาเป็น array ในที่นี้เป็นการดึง state ที่อยู่ใน redux store โดยใช้ตัว selector ในการดึงและนำค่าที่ได้ไปเก็บไว้ตัวแปร item
                return (
                    <div key={item.id} className='flex justify-between items-center w-full'>
                        <p className='font-semibold'>{item.data} </p> <button onClick={() => dispatch(removeTodo(item.id))} className='border-2 border-black pl-4 p-3 bg-red-500 text-white rounded-lg'>Delete</button>
                    </div>
                )
            })}


        </div>

    )
}