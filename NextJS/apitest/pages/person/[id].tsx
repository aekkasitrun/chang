import { useRouter } from 'next/router'
import useSWR from 'swr'        //library ที่เอาไว้ใช้ fetch api /fetch = ดึงข้อมูล

const fetcher = async (url: string) => {    //
    const res = await fetch(url)// ดึงค่าที่รับมาจาก url
    const data = await res.json()//แปลง data เป็น json

    if (res.status !== 200) {//จับเวลาการดึงข้อมูลซึ่ง 200 เป็นเวลามาตรฐานของ http code =success ถ้ามากกว่า 200 ก็ส่ง error ไป
        throw new Error(data.message)
    }
    return data
}

const trBar = [
    {
        title: "Name"
    }, {

        title: "Height"
    }, {

        title: "Mass"
    }, {

        title: "Hair color"
    }, {

        title: "Skin color"
    }, {

        title: "Eye color"
    },
    {

        title: "gender"
    },]



export default function Person() {

    const router = useRouter()

    //const { router.query } = useRouter()  //เป็นการเขียนแบบ restructing ทำให้เราใช้ object query ตรงๆได้เลยไม่ต้องมาอ้างอิงตัวแปร router อยู่

    const { data, error } = useSWR(
        () => router.query.id && `/api/people/${router.query.id}`,
        fetcher
    )

    if (error) return <div>{error.message}</div>
    if (!data) return <div>Loading...</div>
    console.log(router.query.id)

    return (
        <table className="min-w-full leading-normal">

            <thead>
                <tr>
                    {trBar.map((trB, index: number) => {
                        return (
                            <th key={index} className="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-center text-xs font-semibold text-gray-600 uppercase tracking-wider">{trB.title}</th>
                        )
                    })}

                </tr>
            </thead>
            <tbody>
                <tr>
                    <td className="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                        <p className="text-gray-900 whitespace-no-wrap text-center" key={data.name}>{data.name}</p>
                    </td>

                    <td className="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                        <p className="text-gray-900 whitespace-no-wrap text-center" key={data.height}>{data.height}</p>
                    </td>

                    <td className="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                        <p className="text-gray-900 whitespace-no-wrap text-center" key={data.mass}>{data.mass}</p>
                    </td>

                    <td className="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                        <p className="text-gray-900 whitespace-no-wrap text-center" key={data.hair_color}>{data.hair_color}</p>
                    </td>

                    <td className="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                        <p className="text-gray-900 whitespace-no-wrap text-center" key={data.skin_color}>{data.skin_color}</p>
                    </td>

                    <td className="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                        <p className="text-gray-900 whitespace-no-wrap text-center" key={data.eye}>{data.eye_color}</p>
                    </td>

                    <td className="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                        <p className="text-gray-900 whitespace-no-wrap text-center" key={data.gender}>{data.gender}</p>
                    </td>


                </tr>
            </tbody>
        </table>


    )
}