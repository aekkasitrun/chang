import React, { useCallback, useEffect, useRef, useState } from 'react';
import { InputText } from 'primereact/inputtext';
import { TabMenu } from 'primereact/tabmenu';
import { useRouter } from 'next/router';
import { Menubar } from 'primereact/menubar';

const MenuDemo = ({ children }) => {
    const [activeIndex, setActiveIndex] = useState(0);
    const router = useRouter();
    const checkActiveIndex = useCallback(() => {
        const paths = router.pathname.split('/');
        const currentPath = paths[paths.length - 1];

        switch (currentPath) {
            case 'seat':
                setActiveIndex(1);
                break;
            case 'payment':
                setActiveIndex(2);
                break;
            case 'confirmation':
                setActiveIndex(3);
                break;
            default:
                break;
        }
    }, [router]);

    useEffect(() => {
        checkActiveIndex();
    }, [checkActiveIndex]);




    const wizardItems = [
        { label: 'Charge Stations', command: () => router.push('/uikit/menu') },
        { label: 'Analytics', command: () => router.push('/uikit/menu/seat') },
    ];

    const menubarEndTemplate = () => {
        return (
            <span className="p-input-icon-left">
                <i className="pi pi-search" />
                <InputText type="text" placeholder="Search by endpoint" />
            </span>
        );
    };

    const nestedMenuitems = [
        {
            label: 'Online/Offline',
            items: [
                {
                    label: 'Online',
                    icon: 'pi pi-circle-fill'
                },
                {
                    label: 'Offline',
                    icon: 'pi pi-circle'
                }
            ]
        },
        {
            label: 'Public/Private',

            items: [
                {
                    label: 'Public',
                },
                {
                    label: 'Private',
                }
            ]
        },
        {
            label: 'Physical/Simulator',
            items: [
                {
                    label: 'Physical',
                },
                {
                    label: 'Simulator',
                },
            ]
        },
        {
            label: 'Select Location',
            items: [
                {
                    label: 'nothing',
                },
                {
                    label: 'nothing',
                }
            ]
        },

    ];

    return (

        <div className="col-12">
            <div className="card">
                <TabMenu model={wizardItems} activeIndex={activeIndex} onTabChange={(e) => setActiveIndex(e.index)} />
                {router.pathname === '/uikit/menu' ? (
                    <div className="flex align-items-center py-5 px-3">
                        <Menubar model={nestedMenuitems} end={menubarEndTemplate}></Menubar>
                    </div>
                ) : (
                    <>{children}</>
                )}
            </div>
        </div>

    );
};

export default MenuDemo;
