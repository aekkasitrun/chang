import getConfig from 'next/config';
import React, { useContext } from 'react';
import AppMenuitem from './AppMenuitem';
import { LayoutContext } from './context/layoutcontext';
import { MenuProvider } from './context/menucontext';

const AppMenu = () => {
    const { layoutConfig } = useContext(LayoutContext);
    const contextPath = getConfig().publicRuntimeConfig.contextPath;
    const model = [
        {

            items: [
                { label: 'Dashboard', icon: 'pi pi-home', to: '/' },
                { label: 'Locations', icon: 'pi pi-map-marker', to: '/pages/crud/' },
                { label: 'Charge Stations', icon: 'pi pi-bolt', to: '/uikit/menu' },
                { label: 'Transactions', icon: 'pi pi-folder', to: '/uikit/floatlabel' },
                { label: 'Users', icon: 'pi pi-users', to: '/uikit/invalidstate' },
                { label: 'RFIDs', icon: 'pi pi-send', to: '/uikit/button', class: 'rotated-icon' },
                { label: 'Rates', icon: 'pi pi-chart-line', to: '/uikit/table' },
                {
                    label: 'Settings',
                    icon: 'pi pi-cog',
                    items: [
                        {
                            label: 'API',
                            icon: 'pi pi-cloud-upload',
                            to: '/auth/login'
                        },
                        {
                            label: 'Integrations',
                            icon: 'pi pi-circle',
                            to: '/auth/error'
                        },
                        {
                            label: 'Driver App',
                            icon: 'pi pi-mobile',
                            to: '/auth/access'
                        },
                        {
                            label: 'Account',
                            icon: 'pi pi-user',
                            to: '/auth/error'
                        },
                        {
                            label: 'API Playground',
                            icon: 'pi pi-pencil',
                            to: '/auth/access'
                        }
                    ]
                },
                {
                    label: 'Billing',
                    icon: 'pi pi-wallet',
                    items: [
                        {
                            label: 'Plans',
                            icon: 'pi pi-book',
                            to: '/auth/login'
                        },
                        {
                            label: 'Payment',
                            icon: 'pi pi-credit-card',
                            to: '/auth/error'
                        },
                        {
                            label: 'Usage',
                            icon: 'pi pi-chart-bar',
                            to: '/auth/access'
                        }
                    ]
                },
                { label: 'Logs', icon: 'pi pi-file', to: '/uikit/table' },
                { label: 'Support', icon: 'pi pi-phone', to: '/uikit/table' },

            ]
        }
    ];

    return (
        <MenuProvider>
            <ul className="layout-menu">
                {model.map((item, i) => {
                    return !item.seperator ? <AppMenuitem item={item} root={true} index={i} key={item.label} /> : <li className="menu-separator"></li>;
                })}
            </ul>
        </MenuProvider>
    );
};

export default AppMenu;
