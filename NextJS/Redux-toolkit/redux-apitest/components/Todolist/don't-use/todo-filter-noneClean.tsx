import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { AppDispatch, Appstate } from "../../../Redux-toolkit/store";
import { sort, deleteTodo, completeTodo } from "../../../Redux-toolkit/todos"


const TodoFilter: React.FC = () => {
    const [selectedOption, setSelectedOption] = useState<String>(" ");

    const dispatch: AppDispatch = useDispatch();
    const selector = useSelector((state: Appstate) => state.todos)


    const selectChange = (event: React.ChangeEvent<HTMLSelectElement>) => { //React.ChangeEvent<HTMLSelectElement> เป็นการประกาศ type ในภาษา typescript 
        const value = event.target.value;                                   // มีประโยชน์ในการระบุชนิดของข้อมูลที่จะถูกใช้งานในหน้าจอ (UI) ของแอพลิเคชั่น
        setSelectedOption(value);

    };

    return (
        <div>
            <div className="row m-1 p-3 px-5 justify-content-end">
                <div className="col-auto d-flex align-items-center">
                    <label className="text-secondary my-2 pr-2 view-opt-label">Filter</label>
                    <select onChange={selectChange} className="custom-select custom-select-sm btn my-2">
                        <option selected disabled>Choose one</option>
                        <option value="active" >Active</option>
                        <option value="complete" >Completed</option>
                        <option value="all" >All</option>
                    </select>
                </div>
            </div>

            {/* start select filter */}
            <div>
                {/* for none select any filter */}
                {selectedOption === " "
                    ? selector.map((todo) => {
                        return (
                            <div
                                key={todo.id}
                                className='flex justify-between items-center w-full'
                            >
                                <input
                                    className="w-4 h-4 text-blue-600 bg-gray-100 rounded border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600"
                                    type="checkbox"
                                    checked={todo.completed}
                                    onChange={() => dispatch(completeTodo(todo.id))}

                                />
                                <p className='font-semibold'>{todo.message}</p>

                                <button
                                    type="button"
                                    className='border-2 border-black pl-4 p-3 bg-red-500 text-white rounded-lg'
                                    onClick={() => dispatch(deleteTodo(todo.id))}
                                >
                                    Delete
                                </button>

                            </div>
                        );
                    })
                    : null}

                {/* for active item */}
                {selectedOption === "active"
                    ? selector.map((todo) => {
                        return (
                            todo.completed === false && (
                                <div
                                    key={todo.id}
                                    className='flex justify-between items-center w-full'
                                >
                                    <input
                                        className="w-4 h-4 text-blue-600 bg-gray-100 rounded border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600"
                                        type="checkbox"
                                        checked={todo.completed}
                                        onChange={() => dispatch(completeTodo(todo.id))}

                                    />
                                    <p className='font-semibold'>{todo.message}</p>

                                    <button
                                        type="button"
                                        className='border-2 border-black pl-4 p-3 bg-red-500 text-white rounded-lg'
                                        onClick={() => dispatch(deleteTodo(todo.id))}
                                    >
                                        Delete
                                    </button>

                                </div>
                            )
                        );
                    })
                    : null}

                {/* for completed items */}
                {selectedOption === "complete"
                    ? selector.map((todo) => {
                        return (
                            todo.completed === true && (
                                <div
                                    key={todo.id}
                                    className='flex justify-between items-center w-full'
                                >
                                    <input
                                        className="w-4 h-4 text-blue-600 bg-gray-100 rounded border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600"
                                        type="checkbox"
                                        checked={todo.completed}
                                        onChange={() => dispatch(completeTodo(todo.id))}

                                    />
                                    <p className='font-semibold'>{todo.message}</p>

                                    <button
                                        type="button"
                                        className='border-2 border-black pl-4 p-3 bg-red-500 text-white rounded-lg'
                                        onClick={() => dispatch(deleteTodo(todo.id))}
                                    >
                                        Delete
                                    </button>

                                </div>
                            )
                        );
                    })
                    : null}
                {/* for all items */}
                {selectedOption === "all"
                    ? selector.map((todo) => {
                        return (
                            <div
                                key={todo.id}
                                className='flex justify-between items-center w-full'
                            >
                                <input
                                    className="w-4 h-4 text-blue-600 bg-gray-100 rounded border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600"
                                    type="checkbox"
                                    checked={todo.completed}
                                    onChange={() => dispatch(completeTodo(todo.id))}

                                />
                                <p className='font-semibold'>{todo.message}</p>

                                <button
                                    type="button"
                                    className='border-2 border-black pl-4 p-3 bg-red-500 text-white rounded-lg'
                                    onClick={() => dispatch(deleteTodo(todo.id))}
                                >
                                    Delete
                                </button>

                            </div>
                        );
                    })
                    : null}
            </div>
            {/* end select filter */}

            <button className="border-1 inline-block px-6 py-2.5 bg-gray-200 text-gray-700 font-medium text-xs leading-tight uppercase rounded-full shadow-md hover:bg-gray-300 hover:shadow-lg focus:bg-gray-300 focus:shadow-lg focus:outline-none focus:ring-0 active:bg-gray-400 active:shadow-lg transition duration-150 ease-in-out"
                onClick={() => dispatch(sort())}>
                Sort Item!
            </button>

        </div>
    )
}

export default TodoFilter;