import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { AppDispatch, Appstate } from "../../../Redux-toolkit/store";
//import todos from "../store/todos";
import { sort, deleteTodo, completeTodo } from "../../../Redux-toolkit/todos"
import TodoFilter from "../todo-filter";

const TodoList: React.FC = () => {
    const dispatch: AppDispatch = useDispatch();
    const selector = useSelector((state: Appstate) => state.todos)

    return (
        <div>
            {selector.map((todo) => (
                <div
                    key={todo.id}
                    className='flex justify-between items-center w-full'
                >
                    <input
                        className="w-4 h-4 text-blue-600 bg-gray-100 rounded border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600"
                        type="checkbox"
                        checked={todo.completed}
                        onChange={() => dispatch(completeTodo(todo.id))}

                    />

                    <p className='font-semibold'>{todo.message}</p>

                    <button
                        type="button"
                        className='border-2 border-black pl-4 p-3 bg-red-500 text-white rounded-lg'
                        onClick={() => dispatch(deleteTodo(todo.id))}
                    >
                        Delete
                    </button>
                </div>
            ))}
            <button className="border-1 inline-block px-6 py-2.5 bg-gray-200 text-gray-700 font-medium text-xs leading-tight uppercase rounded-full shadow-md hover:bg-gray-300 hover:shadow-lg focus:bg-gray-300 focus:shadow-lg focus:outline-none focus:ring-0 active:bg-gray-400 active:shadow-lg transition duration-150 ease-in-out"
                onClick={() => dispatch(sort())}>
                Sort Item!
            </button>
        </div>
    );
};

export default TodoList;
