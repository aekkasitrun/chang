import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { AppDispatch } from "../../Redux-toolkit/store";
import { addTodo } from "../../Redux-toolkit/todos";
import { Formik, Form, Field } from 'formik'
import * as Yup from 'yup';
import { GoPlus } from "react-icons/go";
import { motion } from "framer-motion";

const TodoSchema = Yup.object().shape({     //เราจะใช้ Yup กับ object นี้ซึ่งมีหน้าตา(shape) เป็นดังนี้                                
    name: Yup.string()
        .min(2, 'Too Short!')   //ประกาศตัวแปรชื่อ TodoSchema ให้มีความสามารถของ Yup ซึ่งเป็น Schema ของข้อมูลที่อยู่ในรูป Object 
        .max(50, 'Too Long!')   //ซึ่งใช้สำหรับการทำ validation โดยการตั้งชื่อ key จะสอดคล้องกับ name ใน Field
        .required('Required'),

});
const TodoInput: React.FC = () => {
    const dispatch: AppDispatch = useDispatch();
    const [input, setInput] = useState('');

    return (

        <Formik
            initialValues={{
                name: ''
            }}
            validationSchema={TodoSchema}
            onSubmit={(values, { resetForm }) => {
                // same shape as initial values
                dispatch(addTodo(values.name));
                setInput("");
                resetForm({
                    values: {
                        name: ''

                    }
                })
                console.log(values);
            }}
        >
            {({ values, handleChange, errors, touched }) => ( //เขียนแบบนี้เพื่อเอา medthod ของ formik มาใช้ได้ตรงๆ ไม่ต้องมาทำการ . เพื่ออ้างไปให้ถึง method พวกนี้อยู่
                <Form className='space-x-4' >
                    <Field
                        // ส่วนใส่รายการที่ต้องการจะทำ
                        type="text"
                        name="name"
                        id="todo-input"
                        onChange={handleChange}
                        // placeholder='Enter Here...'
                        className='grow w-96 h-8  pl-1 p-3 border bg-[#e1ebfd] border-[#e1ebfd] focus:outline-none focus:ring-2 focus:ring-[#271c6c] rounded-md'
                        value={values.name}

                    />
                    {/* ส่วนปุ่มเพิ่มรายการ */}
                    <motion.button
                        whileHover={{ scale: 1.1 }}
                        whileTap={{ scale: 0.9 }}
                        type="submit"
                        className='border-3 border-[#e1ebfd] pl-4 p-4 bg-[#271c6c] text-white rounded-full '>
                        <GoPlus />
                    </motion.button>

                    {/* ส่วนแสดง error หากผู้ใช้กดปุ่มเพิ่มในขณะที่ยังไม่ได้ใส่ข้อความ, พิมมาแค่ตัวเดียวหรือพิมพ์เกิน 50 ตัวอักษร */}
                    {errors.name && touched.name ? (
                        <div className="flex items-center py-4 text-white">{errors.name}</div>
                    ) : null}

                </Form>
            )}
        </Formik>
    );
};

export default TodoInput;
