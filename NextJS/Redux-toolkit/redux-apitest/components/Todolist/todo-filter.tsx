import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { AppDispatch, Appstate } from "../../Redux-toolkit/store";
import { sort, deleteTodo, completeTodo, clearTodo } from "../../Redux-toolkit/todos";

import { motion, AnimatePresence, Variants } from "framer-motion";

import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import DeleteIcon from '@mui/icons-material/Delete';
import IconButton from '@mui/material/IconButton';
import Checkbox from '@mui/material/Checkbox';

const TodoFilter: React.FC = () => {
    const [selectedOption, setSelectedOption] = useState<String>(" ");

    const dispatch: AppDispatch = useDispatch();
    const selector = useSelector((state: Appstate) => state.todos);

    const selectChange = (event: React.ChangeEvent<HTMLSelectElement>) => { //React.ChangeEvent<HTMLSelectElement> เป็นการประกาศ type ในภาษา typescript 
        const value = event.target.value;                                   // มีประโยชน์ในการระบุชนิดของข้อมูลที่จะถูกใช้งานในหน้าจอ (UI) ของแอพลิเคชั่น
        setSelectedOption(value);

    };

    const renderTodo = (todo: any) => (
        <div key={todo.id} >

            <motion.div
                // ส่วนการเคลื่อนไหวของ card todolist เมื่อมีการเพิ่มสิ่งที่จะทำเข้ามา
                initial={{ x: "150vw" }}
                animate={{ x: 0 }}
                transition={{ type: "spring", duration: 2 }}
                //whileHover={{ scale: 1.1 }} เป็นส่วนที่เมื่อเอาลูกศรไปอยู่ตรง list สิ่งที่จะทำมันจะเคลื่อนไหวแบบขยายตัวให้ใหญ่ขึ้นกว่าเดิม เอาออกเพราะรู้สึกรำคาญ

                // ส่วนการเคลื่อนไหวของ card เมื่อมีการลบ card ทิ้ง
                exit={{
                    x: "-60vw",
                    scale: [1, 0],
                    transition: { duration: 0.5 },
                }}
                key={todo.id}
            >

                {/* ส่วนแสดงรายการสิ่งที่จะทำ โดย component ต่างๆได้แก่ list รายการ, checkbox, delete button ถูกรวมไว้ใน mui ของ card */}
                <Card sx={{ maxWidth: 345, m: 1, mx: 'auto', }} style={{ backgroundColor: "#c3c7e7" }}>
                    <CardContent>
                        <Typography>
                            {todo.message}
                        </Typography>
                    </CardContent>
                    <CardActions>
                        <Checkbox
                            defaultChecked color="success"
                            checked={todo.completed}
                            onChange={() => dispatch(completeTodo(todo.id))} />

                        <IconButton
                            type="button"
                            onClick={() => dispatch(deleteTodo(todo.id))}
                        >
                            <DeleteIcon />
                        </IconButton>
                    </CardActions>
                </Card>
            </motion.div>

        </div>
    );

    return (
        <div>
            <div className="row m-1 p-3 px-5 justify-content-end">
                <div className="flex justify-center gap-x-6 ">

                    {/* ส่วนของ filter ไว้ใช้ในการแยกประเภทรายการโดยทำออกมาในรูปแบบของปุ่ม */}
                    <motion.button
                        className="rounded-full w-20 h-10 border-0 font-bold"
                        onClick={() => setSelectedOption("active")}
                        whileHover={{ scale: 1.1 }}
                        whileTap={{ scale: 0.9 }}
                    >
                        Active
                    </motion.button>
                    <motion.button
                        className="rounded-full w-20 h-10 border-0 font-bold"
                        onClick={() => setSelectedOption("complete")}
                        whileHover={{ scale: 1.1 }}
                        whileTap={{ scale: 0.9 }}
                    >
                        Complete
                    </motion.button>
                    <motion.button
                        className="rounded-full w-20 h-10 border-0 font-bold"
                        onClick={() => setSelectedOption("all")}
                        whileHover={{ scale: 1.1 }}
                        whileTap={{ scale: 0.9 }}
                    >
                        All
                    </motion.button>

                </div>
            </div>

            {/* ส่วนของเงื่อนไขจากการใช้ filter  */}
            <AnimatePresence>  {/* เป็นเครื่องมือสำหรับจัดการการแสดงผลและการเคลื่อนที่ของออบเจกต์ (React component) ต่าง ๆ ที่ใช้ร่วมกันในแอพพลิเคชัน */}
                {selectedOption === " "
                    ? selector.map(renderTodo)
                    : selectedOption === "active"
                        ? selector.map((todo) =>
                            todo.completed === false ? renderTodo(todo) : null
                        )
                        : selectedOption === "complete"
                            ? selector.map((todo) =>
                                todo.completed === true ? renderTodo(todo) : null
                            )
                            : selector.map(renderTodo)}
            </AnimatePresence>

            <motion.button
                className="rounded-full w-20 h-10 border-0 font-bold"
                onClick={() => dispatch(clearTodo())}
                whileHover={{ scale: 1.1 }}
                whileTap={{ scale: 0.9 }}
            >
                clear
            </motion.button>
        </div>
    );
};

export default TodoFilter;
