import TodoInput from "../components/Todolist/todo-input";
import TodoFilter from "../components/Todolist/todo-filter";
import { motion } from "framer-motion";



export default function todoList() {
    return (
        <div className="flex flex-col items-center min-h-screen bg-[#8276c8]">

            <motion.h1
                className="flex justify-center mt-12 text-[#dfe9fc] shadow-none"
                initial={{ y: -200 }}
                animate={{ y: 0 }}
                transition={{ type: "spring", duration: 1 }}
                whileHover={{ scale: 1.1 }}
            >
                Todo App
            </motion.h1>
            <div>
                <TodoInput />
                <TodoFilter />
            </div>



        </div>
    )
}