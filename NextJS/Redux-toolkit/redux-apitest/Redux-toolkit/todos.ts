import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { v4 as uuidv4 } from "uuid";

export interface Todo {
  //interface = บอกลักษณะของแม่พิมพ์ , class = แม่พิมพ์ที่เอาไว้ใช้สร้าง object
  id: string;
  message: string;
  completed: boolean;
}

const initialState: Todo[] = [];

const todos = createSlice({
  name: "todos",
  initialState: initialState,
  reducers: {
    addTodo: (state, action: PayloadAction<string>) => {
      state.push({
        id: uuidv4(),
        message: action.payload,
        completed: false,
      });
    },

    clearTodo: (state) => {
      return (state = initialState);
    },

    deleteTodo: (state, action: PayloadAction<string>) => {
      return state.filter((todo) => todo.id !== action.payload);
    },
    completeTodo: (state, action: PayloadAction<string>) => {
      const completeTodo = state.find((todo) => todo.id === action.payload);
      completeTodo!.completed = true;
      return state;
    },
    sort: (state) => {
      state.sort((a, b) => a.message.localeCompare(b.message));
    },
  },
});

export const { sort, deleteTodo, addTodo, completeTodo, clearTodo } =
  todos.actions;
export default todos;
