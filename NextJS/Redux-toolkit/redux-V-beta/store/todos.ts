import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { v4 as uuidv4 } from "uuid";

export interface Todo {
  id: string;
  message: string;
  completed: boolean;
}

const todos = createSlice({
  name: "todos",
  initialState: [] as Todo[],
  reducers: {
    addTodo: (state, action: PayloadAction<string>) => {
      state.push({
        id: uuidv4(),
        message: action.payload,
        completed: false,
      });
    },
    deleteTodo: (state, action: PayloadAction<string>) => {
      return state.filter((todo) => todo.id !== action.payload);
    },
    completeTodo: (state, action: PayloadAction<string>) => {
      const completeTodo = state.find((todo) => todo.id === action.payload);
      completeTodo!.completed = true;
      return state;
    },
    sort: (state) => {
      state.sort((a, b) => a.message.localeCompare(b.message));
    },
  },
});

export const { sort, deleteTodo, addTodo, completeTodo } = todos.actions;
export default todos;
