import * as React from "react";


import TodoInput from "../components/todo-input";
import TodoList from "../components/todo-list";

export default function Home() {
  return (
    <h1 >
      <TodoInput />
      <TodoList />
    </h1>
  )
}

