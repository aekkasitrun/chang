import * as React from "react";
import { render } from "react-dom";
import { Provider } from "react-redux";
import type { AppProps } from 'next/app'

import store from "../store/store";

import "../styles/globals.css";



export default function App({ Component, pageProps }: AppProps) {
  return <Provider store={store}>

    <Component {...pageProps} />
  </Provider>

}

