import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { AppDispatch } from "../store/store";
import { addTodo } from "../store/todos";

const TodoInput: React.FC = () => {
    const dispatch: AppDispatch = useDispatch();
    const [input, setInput] = useState('');

    return (
        <form
            onSubmit={(ev) => {
                ev.preventDefault();
                dispatch(addTodo(input));
                setInput("");
            }}>
            <input
                id="todo-input"
                value={input}
                onChange={(event) => setInput(event.currentTarget.value)}
            />
            <button type="submit">Add Todo</button>
        </form>
    );
};

export default TodoInput;
