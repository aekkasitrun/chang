import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { AppDispatch, Appstate } from "../store/store";
import todos from "../store/todos";
import { sort, deleteTodo, completeTodo } from "../store/todos"

const TodoList: React.FC = () => {
    const dispatch: AppDispatch = useDispatch();
    const todos = useSelector((state: Appstate) => state.todos)

    return (
        <div className="todoList">
            {todos.map((todo) => (
                <div
                    key={todo.id}
                >
                    <input
                        className="todoCheck"
                        type="checkbox"
                        checked={todo.completed}
                        onChange={() => dispatch(completeTodo(todo.id))}

                    />

                    <span className="todoMessage">{todo.message}</span>
                    <button
                        type="button"
                        className="todoDelete"
                        onClick={() => dispatch(deleteTodo(todo.id))}
                    >
                        X
                    </button>
                </div>
            ))}
            <button onClick={() => dispatch(sort())}>Sort them!</button>
        </div>
    );
};

export default TodoList;
