from sklearn.datasets import load_iris
from sklearn.model_selection import train_test_split
iris_dataset = load_iris()

#แบ่งข้อมูล 75% , 25%
x_train, x_test, y_train, y_test = train_test_split(iris_dataset["data"], iris_dataset["target"], test_size=0.2,random_state=0)

#test_size 

print(x_train.shape)
print(x_test.shape)
print(y_train.shape)
print(y_test.shape)

#ผลลัพธ์ที่เกิดขึ้นจากการแบ่งข้อมูล 150
#แบ่งออกเป็น
#train 75% = 112
#test 25% = 38
