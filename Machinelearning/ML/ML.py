from scipy.io import loadmat
import matplotlib.pyplot as plt
 
mnist_raw = loadmat("mnist-original.mat")

mnist = {
    "data":mnist_raw["data"].T,#transpost
    "target":mnist_raw["label"][0]
}

x,y = mnist["data"],mnist["target"]


number = x[35000]
number_image = number.reshape(28,28) #แปลงเป็น array 2 มิติ

print(y[35000])

plt.imshow(
    number_image,
    cmap=plt.cm.binary,
    interpolation="nearest"
)

plt.show()

print("x.shape")