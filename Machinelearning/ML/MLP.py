from scipy.io import loadmat
import matplotlib.pyplot as plt
from sklearn.neural_network import MLPClassifier
from sklearn.metrics import accuracy_score
import numpy as np
mnist_raw=loadmat("mnist-original.mat")
mnist={
    "data":mnist_raw["data"].T,
    "target":mnist_raw["label"][0]
}

#หั่นข้อมูล x = ข้อมูลรูปภาพ , y = ข้อมูลตัวเลข
x,y=mnist["data"],mnist["target"]
#shuffle data
shuffle=np.random.permutation(70000)
x,y=x[shuffle],y[shuffle]

#ข้อมูลที่ใช้เรียนรู้ 1-60000 เก็บไว้ที่ x_train, x_test ข้อมูลที่ใช้ทดสอบตัวที่ 60001 - 70000 เก็บไว้ที่ y_train, y_test
x_train , x_test,y_train,y_test= x[:60000],x[60000:],y[:60000],y[60000:]


#create model 
model=MLPClassifier()
model.fit(x_train,y_train)

y_pred=model.predict(x_test)


#subplot : สร้างตาราง 10 แถว 10 คอลัม
fig,ax=plt.subplots(10,10,
#fig ขนาดรูปเป็น 8*8 pixcel
figsize=(8,8),
subplot_kw={'xticks':[],'yticks':[]},

#ระบุความกว้าง ความยาวของ grid
gridspec_kw=dict(hspace=0.1,wspace=0.1))

#display image data after training & prediction
# .flat ทำให้เป็นตาราง 2 มิติ
for i , axi in enumerate(ax.flat):
    #display test image data
    axi.imshow(x_test[i].reshape(28,28),cmap='binary',interpolation='nearest')
    #display text true number image data
    axi.text(0.05,0.05,str(int(y_test[i])),transform=axi.transAxes,color="black")
    ##display text predict number image data
    axi.text(0.75,0.05,str(int(y_pred[i])),transform=axi.transAxes,
    color="green" if y_pred[i] == y_test[i] else "red")
plt.show()

